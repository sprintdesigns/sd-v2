<section class="section-appender">
	<div class="contact-form">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="text">
						<h2>Let's hear from you</h2>
						<h1>Drop Us a line</h1>
					</div>
				</div>
			</div>
			<div class="box">
				<form class="sd-form" method="post" sd-form-title="Contact Form" data-toggle="validator">
				<div class="row">
					<div class="col-md-6 col-xs-12 col-sm-6">
						<div class="one-line">
							<div class="form-group">
								<label>First Name *</label>
								<input type="text" class="form-control one-line" placeholder="John" id="fname" name="firstname" required>
							</div>
							<div class="form-group">
								<label>Last Name *</label>
								<input type="text" class="form-control one-line " placeholder="Doe" id="lname" name="lastname" required>
			              	</div>
						</div>
		              	<div class="form-group">
							<label>Company *</label>
							<input type="text" class="form-control" placeholder="Bob's Donuts" id="company" name="company" required>
		              	</div>
		              	<div class="form-group">
							<label>Phone Number *</label>
							<input type="text" class="form-control" placeholder="555-865-2908" id="number" name="contact" required>
		              	</div>
					</div>
					<div class="col-md-6 col-xs-12 col-sm-6">
						<div class="form-group">
							<label>Email address *</label>
							<input type="email" class="form-control" placeholder="bob@email.com" id="eaddress" name="email" required>
						</div>
						<div class="form-group">
							<label>Message *</label>
							<textarea class="form-control" placeholder="Type your message here" id="message" name="message" required></textarea>
						</div>
					</div>
					<div class="col-md-12 text-center">
						<button class="btn btn-primary" type="submit">Send</button>
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>
	<?php $sd->column->bar($sd); ?>
</section>