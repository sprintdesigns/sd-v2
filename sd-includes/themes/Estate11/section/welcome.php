<?php
$folder = 'Estate11';
?>

<section class="section-appender">
	<div class="welcome">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="text text-colored">
						<h2 class="text-only">Welcome to</h2>
						<h1 class="text-only">Company Name</h1>
					</div>
					<div class="content editable">
						<p><span><img style="padding: 5px" align="left" src="<?php echo $sd->theme->get_theme_path($folder); ?>img/welcome-photo.jpg"></span>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitaae erat consequat auctor eu in elit.</p>

						<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed</p>

						<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitaae erat consequat auctor eu in elit.</p>
					</div>
					<div class="editable">
						<ul class="list-inline text-center">
							<li><a href="javascript:void(0);" class="btn btn-about btn-default">MORE ABOUT US</a></li>
							<li><a href="javascript:void(0);" class="btn btn-contact btn-default">CONTACT US</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php $sd->column->bar($sd); ?>
</section>