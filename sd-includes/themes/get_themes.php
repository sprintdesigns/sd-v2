<?php

$theme_names = ['Travel1', 'Travel2', 'Travel3', 'Travel4', 'Travel5', 'Travel6', 'Travel7', 'Travel8', 'Travel9', 'Travel10',
				'Travel11', 'Travel12', 'Travel13', 'Travel14', 'Travel15', 'Travel16', 'Travel17', 'Travel18', 'Travel19', 'Travel20','Store1', 'Store2', 'Store3', 'Store4', 'Store5', 'Store6', 'Store7', 'Store8', 'Store9', 'Store10','Store11', 'Store12', 'Estate1', 'Estate2', 'Estate3', 'Estate4', 'Estate5', 'Estate6', 'Estate7', 'Estate8', 'Estate9', 'Estate10', 'Estate11', 'Estate12','Food1', 'Food2', 'Food3', 'Food4'];

foreach ($theme_names as $name) {
	$themes[$name]['name'] = $name;
	$themes[$name]['theme_folder'] = $name;
	$themes[$name]['thumbnail'] = "http://sprintdesignsph.com/store-theme/{$name}/base-image.jpg";
	$themes[$name]['zip_file'] = "http://sprintdesignsph.com/store-theme/{$name}/{$name}.zip";
	$themes[$name]['preview'] = "http://sprintdesignsph.com/store-theme/{$name}/index.html";
}

header('Content-Type: application/json');
echo json_encode($themes);