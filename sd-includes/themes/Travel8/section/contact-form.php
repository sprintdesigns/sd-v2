	<section class="section-appender">
	<div id="book-travel-form">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 class="text-only">Contact Us</h1>
				</div>
			</div>
			<div class="row">
				<form class="sd-form" method="post" data-toggle="validator" sd-form-title="Contact Form">
					<div class="col-md-5 col-sm-5">
						<div class="form-group">
							<label for="inputFullname">First Name</label>
							<input name="firstname" type="text" class="form-control" id="firstname" required>
		              	</div>
		              	<div class="form-group">
							<label for="inputLastname">Last Name</label>
							<input name="lastname" type="text" class="form-control" id="lastname" required>
		              	</div>
		              	<div class="form-group">
							<label for="inputEmail">Email</label>
							<input name="email" type="email" class="form-control" id="email" required>
		              	</div>
		              	<div class="form-group">
							<label for="inputPhone">Phone</label>
							<input name="phone" type="text" class="form-control" id="phone" required>
		              	</div>
					</div>
					<div class="col-md-5 col-md-offset-2 col-sm-5 col-sm-offset-2">
						<div class="form-group">
							<label for="inputDepart">Message</label>
							<textarea name="message" class="form-control"></textarea>
		              	</div>
		              	<button class="btn btn-primary" type="submit">Send</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<?php $sd->column->bar($sd); ?>
	</section>