<section class="section-appender colored-gallery">
	<div class="best-meals">
		<div class="container">
			<div class="row no-gutters">
					<div class="text text-colored">
						<h2 class="text-only">Have a look on our</h2>
						<h1 class="text-only">Best Products</h1>
					</div>			
				<div class="col-sm-4">
					<div class="service-content cabinets" style="background-image: url(<?php echo $sd->theme->get_theme_path('Store2'); ?>img/photo1.jpg)">
					  <button class="galleryShow"><i class="fa fa-image"></i></button>
						<!-- <div class="services-text">
							<h1>Cabinets</h1>
						</div> -->
					</div>
				</div>
				<div class="col-sm-4">
					<div class="service-content outdoor-tables" style="background-image: url(<?php echo $sd->theme->get_theme_path('Store2'); ?>img/photo2.jpg)">
					  <button class="galleryShow"><i class="fa fa-image"></i></button>
						<!-- <div class="services-text">
							<h1>Outdoor Tables</h1>
						</div> -->
					</div>
				</div>
				<div class="col-sm-4">
					<div class="service-content table" style="background-image: url(<?php echo $sd->theme->get_theme_path('Store2'); ?>img/photo3.jpg)">
					  <button class="galleryShow"><i class="fa fa-image"></i></button>
						<!-- <div class="services-text">
							<h1>Table</h1>
						</div> -->
					</div>
				</div>
				<div class="col-sm-4">
					<div class="service-content diner" style="background-image: url(<?php echo $sd->theme->get_theme_path('Store2'); ?>img/photo4.jpg)">
					  <button class="galleryShow"><i class="fa fa-image"></i></button>
						<!-- <div class="services-text">
							<h1>Diner</h1>
						</div> -->
					</div>
				</div>
				<div class="col-sm-4">
					<div class="service-content chairs" style="background-image: url(<?php echo $sd->theme->get_theme_path('Store2'); ?>img/photo5.jpg)">
					  <button class="galleryShow"><i class="fa fa-image"></i></button>
						<!-- <div class="services-text">
							<h1>Chairs</h1>
						</div> -->
					</div>
				</div>
				<div class="col-sm-4">
					<div class="service-content decor" style="background-image: url(<?php echo $sd->theme->get_theme_path('Store2'); ?>img/photo6.jpg)">
					  <button class="galleryShow"><i class="fa fa-image"></i></button>
						<!-- <div class="services-text">
							<h1>Decor</h1>
						</div> -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php $sd->column->bar($sd); ?>
</section>