	</div>
	<div id="footer">
		<?php $sd->page->social_icons($sd);?>
		<div class="container-fluid">
			<div class="row">
				<div class=" col-md-12 copyright">
					<center><p>Copyright &copy; <?php echo date('Y'); ?> | Site Design and Creation by <img src="sd-assets/images/sprintdesigns-logo.png" />. All Rights Reserved</p></center>
				</div>
			</div>
		</div>
	</div>
    <?php $sd->page->include_footer($sd, $page_tools);?>
</body>
</html>