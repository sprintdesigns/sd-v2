<section class="section-appender">
	<div class="best-meals">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<div class="text text-colored">
						<h2 class="text-only">Have a look on our</h2>
						<h1 class="text-only">Best Tasting Meals</h1>
					</div>
					<ul class="list-inline">
						<li><img src="<?php echo $sd->theme->get_theme_path('Food1'); ?>img/food1.png"></li>
						<li><img src="<?php echo $sd->theme->get_theme_path('Food1'); ?>img/food2.png"></li>
						<li><img src="<?php echo $sd->theme->get_theme_path('Food1'); ?>img/food3.png"></li>
						<li><img src="<?php echo $sd->theme->get_theme_path('Food1'); ?>img/food4.png"></li>
						<li><img src="<?php echo $sd->theme->get_theme_path('Food1'); ?>img/food5.png"></li>
						<li><img src="<?php echo $sd->theme->get_theme_path('Food1'); ?>img/food6.png"></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<?php $sd->column->bar($sd); ?>
</section>