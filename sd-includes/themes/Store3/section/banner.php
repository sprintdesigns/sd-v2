<?php
$folder = 'Store3';
?>
<section class="section-appender section-banner">
	<div class="container">
		<div class="row">	
			<div class="col-md-12">
				<div id="banner" class="carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class="carousel-indicators">
						<li data-target="#banner" data-slide-to="0"></li>
						<li data-target="#banner" data-slide-to="1"></li>
						<li data-target="#banner" data-slide-to="2" class="active"></li>
					</ol>

					<!-- Wrapper for slides -->
					<div class="carousel-inner">
						<div class="item sd-image-container" style="background-image: url('<?php echo $sd->theme->get_theme_path($folder); ?>img/banner1.jpg');"><button class="galleryShow cropit"><i class="fa fa-image"></i></button>
							<div class="carousel-caption">
								<div class="banner-text">
									<h1 class="text-only">On-trend <span>Fashion </span> at the Best Value</h1>
								</div>
							</div>
						</div>

						<div class="item sd-image-container" style="background-image: url('<?php echo $sd->theme->get_theme_path($folder); ?>img/banner2.jpg');"><button class="galleryShow cropit"><i class="fa fa-image"></i></button>
							<div class="carousel-caption">
								<div class="banner-text">
									<h1 class="text-only">On-trend <span>Fashion </span> at the Best Value</h1>
								</div>
							</div>
						</div>

						<div class="item sd-image-container active" style="background-image: url('<?php echo $sd->theme->get_theme_path($folder); ?>img/banner3.jpg');"><button class="galleryShow cropit"><i class="fa fa-image"></i></button>
							<div class="carousel-caption">
								<div class="banner-text">
									<h1 class="text-only">On-trend <span>Fashion </span> at the Best Value</h1>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<?php $sd->column->bar($sd); ?>
</section>