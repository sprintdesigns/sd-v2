<!DOCTYPE html>
<html>
<head>
	<title><?= $sd->site->get_name() ?></title>
	<?php $sd->page->include_header($sd, $page_tools);?> 
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="<?php echo $sd->theme->get_theme_path('Travel4'); ?>css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $sd->theme->get_theme_path('Travel4'); ?>css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $sd->theme->get_theme_path('Travel4'); ?>slick/slick.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo $sd->theme->get_theme_path('Travel4'); ?>slick/slick-theme.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo $sd->theme->get_theme_path('Travel4'); ?>custom.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $sd->theme->get_theme_path('Travel4'); ?>css/colorchanger.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Qwigley" rel="stylesheet"> 
</head>
<body class="<?php echo $sd->page->get_body_class($sd, $page_tools);?>">
<?php $sd->page->include_body($sd); ?>
	<div id="masthead">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-xs-8 col-sm-5">
					<div class="site-title">
						<div class="sd-image-logo"><?php echo $sd->page->get_logo($header);?></div>
					</div>
				</div>
				<div class="col-md-6 col-xs-4 col-sm-7">
					<div class="menu">
				        <div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar2" aria-expanded="false" aria-controls="navbar2">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
				        </div>
				        <div id="navbar" class="hidden-xs">
				          <ul class="nav navbar-nav navbar-right">
							<?php echo $sd->page->nav_bar_web(); ?>
				          </ul>
				        </div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 menu-mobile visible-xs hidden-sm hidden-md hidden-lg">
			        <div id="navbar2" class="navbar-collapse collapse">
			          <ul class="nav navbar-nav navbar-right">
						<?php echo $sd->page->nav_bar_web(); ?>
			          </ul>
			        </div>
				</div>
			</div>
		</div>
	</div>