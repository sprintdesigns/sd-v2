	<section class="section-appender">
	<div id="packages">
		<div class="container">
			<div class="row">
				<h1 class="text-only">Vacation Package</h1>
			</div>
			<div class="list">
				<div class="row">
					<div class="col-md-6 col-sm-6">
						<article class="package">
							<figure class="package__img sd-image-container" style="background-image: url('<?php echo $sd->theme->get_theme_path('Travel20'); ?>img/package01.jpg');"><button class="galleryShow cropit"><i class="fa fa-image"></i></button>
								<figcaption class="text-only">Tokyo</figcaption>
							</figure>
							<div class="package__details">
								<h1 class="h4 text-only">4 days / 3 nights Package</h1>
								<h2 class="h3 text-only">Php 16,294<br> <span>Per Month</span></h2>
								<p class="text-only">3 months at 0% interest for <br>October 28-31, 2016</p>
								<a href="{base_url}package1">Select</a>
							</div>
						</article>
					</div>
					<div class="col-md-6 col-sm-6">
						<article class="package">
							<figure class="package__img sd-image-container" style="background-image: url('<?php echo $sd->theme->get_theme_path('Travel20'); ?>img/package02.jpg');"><button class="galleryShow cropit"><i class="fa fa-image"></i></button>
								<figcaption class="text-only">Tokyo</figcaption>
							</figure>
							<div class="package__details">
								<h1 class="h4 text-only">4 days / 3 nights Package</h1>
								<h2 class="h3 text-only">Php 16,294<br> <span>Per Month</span></h2>
								<p class="text-only">3 months at 0% interest for <br>October 28-31, 2016</p>
								<a href="{base_url}package2">Select</a>
							</div>
						</article>
					</div>
				</div>
			</div>
			<div class="list">
				<div class="row">
					<div class="col-md-6 col-sm-6">
						<article class="package">
							<figure class="package__img sd-image-container" style="background-image: url('<?php echo $sd->theme->get_theme_path('Travel20'); ?>img/package03.jpg');"><button class="galleryShow cropit"><i class="fa fa-image"></i></button>
								<figcaption class="text-only">Tokyo</figcaption>
							</figure>
							<div class="package__details">
								<h1 class="h4 text-only">4 days / 3 nights Package</h1>
								<h2 class="h3 text-only">Php 16,294<br> <span>Per Month</span></h2>
								<p class="text-only">3 months at 0% interest for <br>October 28-31, 2016</p>
								<a href="{base_url}package3">Select</a>
							</div>
						</article>
					</div>
					<div class="col-md-6 col-sm-6">
						<article class="package">
							<figure class="package__img sd-image-container" style="background-image: url('<?php echo $sd->theme->get_theme_path('Travel20'); ?>img/package04.jpg');"><button class="galleryShow cropit"><i class="fa fa-image"></i></button>
								<figcaption class="text-only">Tokyo</figcaption>
							</figure>
							<div class="package__details">
								<h1 class="h4 text-only">4 days / 3 nights Package</h1>
								<h2 class="h3 text-only">Php 16,294<br> <span>Per Month</span></h2>
								<p class="text-only">3 months at 0% interest for <br>October 28-31, 2016</p>
								<a href="{base_url}package4">Select</a>
							</div>
						</article>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php $sd->column->bar($sd); ?>
	</section>
