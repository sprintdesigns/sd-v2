<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title><?= $sd->site->get_name() ?></title>

    <?php
    $sd->page->include_header($sd, $page_tools);
    ?>

    <link href="<?php echo $sd->theme->get_theme_path('Spa1'); ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $sd->theme->get_theme_path('Spa1'); ?>css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo $sd->theme->get_theme_path('Spa1'); ?>css/style.min.css" rel="stylesheet">
    <link href="<?php echo $sd->theme->get_theme_path('Spa1'); ?>css/colorchanger.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head> 
  <body class="<?php echo $sd->page->get_body_class($sd, $page_tools);?>">

  <?php
  $sd->page->include_body($sd);
  ?>    

  <header class="header">
    <div class="container pt10 hidden-xs hidden-sm hidden-md">
      <div class="row">
        <div class="col-md-6">
          <p><em><?php echo $sd->page->editable_text('reservation', 'For Reservations and I nquiry, Please Call: 0917-1233-123');?></em></p>
        </div>
        <div class="col-md-6 text-right">
          <p><em><?php echo $sd->page->editable_text('top_right', 'Email Us : email@sample.com');?></em></p>
        </div>
      </div>
    </div>

    <nav class="navbar navbar-default navbar-default--flex">
      <div class="container">
        <div class="navbar-header">
          <div class="navbar-brand sd-image-logo"><?php echo $sd->page->get_logo($header);?></div>
        </div>
        <ul class="nav navbar-nav  navbar-right">
			<?php echo $sd->page->nav_bar_web(); ?>
        </ul>
      </div>
    </nav><!-- Main Navigation -->
  </header><!-- header -->