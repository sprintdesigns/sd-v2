    <section class="section section-appender">
      <div class="container">
        <div class="row">
          <div class="col-md-offset-3 col-md-6 text-center">
            <header class="section__header">
              <h2 class="section__title has-decor has-decor--green">
                <small class="text-only">Welcome to our</small>
                <em class="text-only">Massage Therapy Center</em>
              </h2>
            </header>
            <div class="editable">
            <p>If you feel tired and stressed after a working day, we are happy to give you an enjoyable and healthy solution to find your balance again.</p>

            <p><strong>OPEN HOURS:</strong><br/>Mon-Fri: 9 AM &ndash; 6 PM<br/>Saturday: 9 AM &ndash; 4 PM<br/>Sunday: Closed</p>

            <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. </p>

            <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p>
            </div>
          </div>
        </div>
      </div>
      <?php $sd->column->bar($sd); ?>
    </section><!-- Default Section -->