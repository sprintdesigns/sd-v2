  <section class="ui-state-default section-appender">
  <div class="banner" style="background-image: url('<?php echo $sd->theme->get_theme_path('Spa1'); ?>images/banner-bg.jpg');">
    <div class="banner__overlay">
      <p class="has-decor"></p>
      <p class="banner__overlay__header"><?php echo $sd->page->editable_text('first_slogan', 'wonderful field of bodywork...');?></p>
      <p class="banner__overlay__subheader"><?php echo $sd->page->editable_text('second_slogan', 'The Perfect Massage');?></p>
      <a href="#" class="btn btn-success btn-lg">Book Now!</a>
    </div>
  </div><!-- Banner -->
  <?php $sd->column->bar($sd); ?>
  </section>