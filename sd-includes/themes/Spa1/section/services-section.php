    <section class="section section--services section-appender">
      <header class="section__header">
        <h2 class="section__title has-decor has-decor--green">
          <small class="text-only">Welcome to our</small>
          <em class="text-only">Massage Therapy Center</em>
        </h2>
      </header>

      <div class="container">
        <div class="row">

          <div class="col-md-4">
            <article class="service sd-image-container" style="background-image:url('<?php echo $sd->theme->get_theme_path('Spa1'); ?>images/s1.jpg');"><button class="galleryShow cropit"><i class="fa fa-image"></i></button>
              <div class="service__content">
                <div class="service__content-wrap editable">
                  <h3 class="service__title"></h3>
                  <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis</p>
                  <div><a href="#" class="btn btn-success btn-lg">Learn More</a></div>
                </div>
              </div>
            </article>
          </div>

          <div class="col-md-4">
            <article class="service sd-image-container" style="background-image: url('<?php echo $sd->theme->get_theme_path('Spa1'); ?>images/s2.jpg');"><button class="galleryShow cropit"><i class="fa fa-image"></i></button>
              <div class="service__content">
                <div class="service__content-wrap editable">
                  <h3 class="service__title">Service 2</h3>
                  <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis</p>
                  <div><a href="#" class="btn btn-success btn-lg">Learn More</a></div>
                </div>
              </div>
            </article>
          </div>

          <div class="col-md-4">
            <article class="service sd-image-container" style="background-image: url('<?php echo $sd->theme->get_theme_path('Spa1'); ?>images/s3.jpg');"><button class="galleryShow cropit"><i class="fa fa-image"></i></button>
              <div class="service__content">
                <div class="service__content-wrap editable">
                  <h3 class="service__title">Service 3</h3>
                  <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis</p>
                  <div><a href="#" class="btn btn-success btn-lg">Learn More</a></div>
                </div>
              </div>
            </article>
          </div>

        </div>
      </div>
      <?php $sd->column->bar($sd); ?>
    </section><!-- Services -->