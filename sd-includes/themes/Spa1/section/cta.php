    <section class="section section-appender nopadding cta-section">
      <div class="section--cta" style="background-image: url('<?php echo $sd->theme->get_theme_path('Spa1'); ?>images/cta-bg.jpg');">
        <header class="section__header">
          <h2 class="section__title mb25">
            <em class="text-only">You owe yourself this moment</em>
            <small class="text-only">Visit one of our multiple sessions of relaxation.</small>
          </h2>
          <a href="#" class="btn btn-success btn-lg">Book Now!</a>
        </header>
      </div>  
      <?php $sd->column->bar($sd); ?>
    </section><!-- CTA Section -->