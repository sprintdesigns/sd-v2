<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>SprintDesign SPA</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/style.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  
  <header class="header">
    <div class="container pt10 hidden-xs hidden-sm hidden-md">
      <div class="row">
        <div class="col-md-6">
          <p><em>For Reservations and I nquiry, Please Call: 0917-1233-123</em></p>
        </div>
        <div class="col-md-6 text-right">
          <p><em>Email Us : email@sample.com</em></p>
        </div>
      </div>
    </div>

    <nav class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <h1 class="navbar-brand"><a href="#">Spa<span class="text-gray">get</span>it</a></h1>
        </div>
        <ul class="nav navbar-nav  navbar-right">
          <li class="active"><a href="#">Home</a></li>
          <li><a href="#">About Us</a></li>
          <li><a href="#">Services</a></li>
          <li><a href="#">Store Hours</a></li>
          <li><a href="#">Booking</a></li>
        </ul>
      </div>
    </nav><!-- Main Navigation -->
  </header><!-- header -->

  <div class="banner">
    <div class="banner__overlay">
      <p class="has-decor"></p>
      <p class="banner__overlay__header">wonderful field of bodywork...</p>
      <p class="banner__overlay__subheader">The Perfect Massage</p>
      <a href="#" class="btn btn-success btn-lg">Book Now!</a>
    </div>
  </div><!-- Banner -->

  <main class="main-content">

    <section class="section section--services">
      <header class="section__header">
        <h2 class="section__title has-decor has-decor--green">
          <small>Welcome to our</small>
          <em>Massage Therapy Center</em>
        </h2>
      </header>

      <div class="container">
        <div class="row">

          <div class="col-md-4">
            <article class="service service--1">
              <img src="images/s1.jpg" alt="Service 1" title="Service 1" class="service__img">
              <div class="service__content">
                <div class="service__content-wrap">
                  <h3 class="service__title">Service 1</h3>
                  <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis</p>
                  <a href="#" class="btn btn-success btn-lg">Learn More</a>
                </div>
              </div>
            </article>
          </div>

          <div class="col-md-4">
            <article class="service service--2">
              <img src="images/s2.jpg" alt="Service 2" title="Service 2" class="service__img">
              <div class="service__content">
                <div class="service__content-wrap">
                  <h3 class="service__title">Service 2</h3>
                  <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis</p>
                  <a href="#" class="btn btn-success btn-lg">Learn More</a>
                </div>
              </div>
            </article>
          </div>

          <div class="col-md-4">
            <article class="service service--3">
              <img src="images/s3.jpg" alt="Service 3" title="Service 3" class="service__img">
              <div class="service__content">
                <div class="service__content-wrap">
                  <h3 class="service__title">Service 3</h3>
                  <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis</p>
                  <a href="#" class="btn btn-success btn-lg">Learn More</a>
                </div>
              </div>
            </article>
          </div>

        </div>
      </div>

    </section><!-- Services -->
    
    <section class="section section--about">
      <div class="container">
        <div class="row">
          <div class="col-md-offset-6 col-md-6 text-center">
            <header class="section__header">
              <h2 class="section__title has-decor has-decor--green">
                <small>Welcome to our</small>
                <em>Massage Therapy Center</em>
              </h2>
            </header>
            <p>You deserve better than a rushed massage by a rookie therapist in</p>

            <p>A place that makes you feel more stressed This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, </p>

            <p class="mb25">Nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. </p>

            <a href="#" class="btn btn-success btn-lg">More About Us</a>
          </div>
        </div>
      </div>
    </section><!-- About Us -->

    <section class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-offset-3 col-md-6 text-center">
            <header class="section__header">
              <h2 class="section__title has-decor has-decor--green">
                <small>Welcome to our</small>
                <em>Massage Therapy Center</em>
              </h2>
            </header>
            <p>If you feel tired and stressed after a working day, we are happy to give you an enjoyable and healthy solution to find your balance again.</p>

            <p><strong>OPEN HOURS:</strong><br/>Mon-Fri: 9 AM &ndash; 6 PM<br/>Saturday: 9 AM &ndash; 4 PM<br/>Sunday: Closed</p>

            <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. </p>

            <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p>
          </div>
        </div>
      </div>
    </section><!-- Default Section -->

    <section class="section section--cta">
        <header class="section__header">
          <h2 class="section__title mb25">
            <em>You owe yourself this moment</em>
            <small>Visit one of our multiple sessions of relaxation.</small>
          </h2>
          <a href="#" class="btn btn-success btn-lg">Book Now!</a>
        </header>
    </section><!-- CTA Section -->

  </main><!-- Main content -->

  <footer class="footer text-center">
    <ul class="list-unstyled list-social">
      <li>
        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
      </li>
      <li>
        <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
      </li>
      <li>
        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
      </li>
      <li>
        <a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a>
      </li>
    </ul>
    <p>Copyright &copy; 2017. Company Name. All Rights Reserved. Web &amp; Development by: <img class="img-inline" src="images/sprintdesigns-logo.png"></p>
  </footer><!-- footer -->

  </body>
</html>