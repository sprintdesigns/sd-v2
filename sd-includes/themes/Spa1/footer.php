  </main><!-- Main content -->

  <footer class="footer text-center">
  <?php
  $sd->page->social_icons($sd);
  ?>
    <p>Copyright &copy; 2017. Company Name. All Rights Reserved. Web &amp; Development by: <img class="img-inline" src="<?php echo $sd->theme->get_theme_path('Spa1'); ?>images/sprintdesigns-logo.png"></p>
  </footer><!-- footer -->

  <?php
  $sd->page->include_footer($sd, $page_tools);
  ?>

  </body>
</html>