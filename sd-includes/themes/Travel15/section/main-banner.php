	<section class="section-appender">
	<div id="banner">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="banner-text">
						<h1><?php echo $sd->page->editable_text('header1', 'We Made the world');?></h1>
						<h2><span><?php echo $sd->page->editable_text('header2', 'So small');?></span></h2>
						<h3><?php echo $sd->page->editable_text('header3', 'that you can travel through it in a click');?></h3>
					</div>
					<div class="banner-btn">
						<a href="{base_url}contact" class="btn btn-primary">Start Now</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="packages">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-3">
					<article class="package">
						<figure class="package__img sd-image-container" style="background-image: url('<?php echo $sd->theme->get_theme_path('Travel15'); ?>img/package01.jpg');"><button class="galleryShow cropit"><i class="fa fa-image"></i></button>
							<figcaption><?php echo $sd->page->editable_text('package1', 'Tokyo');?></figcaption>
						</figure>
						<div class="package__details">
							<h1 class="h4"><?php echo $sd->page->editable_text('package1_days', '4 days / 3 nights Package');?></h1>
							<h2 class="h3"><?php echo $sd->page->editable_text('package1_price', 'Php 16,294');?><br> <span class="text-only">Per Month</span></h2>
							<p><?php echo $sd->page->editable_text('package1_info', '3 months at 0% interest for <br>October 28-31, 2016');?></p>
							<a href="{base_url}package1">Select</a>
						</div>
					</article>
				</div>
				<div class="col-md-3 col-sm-3">
					<article class="package">
						<figure class="package__img sd-image-container" style="background-image: url('<?php echo $sd->theme->get_theme_path('Travel15'); ?>img/package02.jpg');"><button class="galleryShow cropit"><i class="fa fa-image"></i></button>
							<figcaption><?php echo $sd->page->editable_text('package2', 'Bali');?></figcaption>
						</figure>
						<div class="package__details">
							<h1 class="h4"><?php echo $sd->page->editable_text('package2_days', '4 days / 3 nights Package');?></h1>
							<h2 class="h3"><?php echo $sd->page->editable_text('package2_price', 'Php 16,294');?><br> <span class="text-only">Per Month</span></h2>
							<p><?php echo $sd->page->editable_text('package2_info', '3 months at 0% interest for <br>October 28-31, 2016');?></p>
							<a href="{base_url}package2">Select</a>
						</div>
					</article>
				</div>
				<div class="col-md-3 col-sm-3">
					<article class="package">
						<figure class="package__img sd-image-container" style="background-image: url('<?php echo $sd->theme->get_theme_path('Travel15'); ?>img/package03.jpg');"><button class="galleryShow cropit"><i class="fa fa-image"></i></button>
							<figcaption><?php echo $sd->page->editable_text('package3', 'Bangkok');?></figcaption>
						</figure>
						<div class="package__details">
							<h1 class="h4"><?php echo $sd->page->editable_text('package3_days', '4 days / 3 nights Package');?></h1>
							<h2 class="h3"><?php echo $sd->page->editable_text('package3_price', 'Php 16,294');?><br> <span class="text-only">Per Month</span></h2>
							<p><?php echo $sd->page->editable_text('package3_info', '3 months at 0% interest for <br>October 28-31, 2016');?></p>
							<a href="{base_url}package3">Select</a>
						</div>
					</article>
				</div>
				<div class="col-md-3 col-sm-3">
					<article class="package">
						<figure class="package__img sd-image-container" style="background-image: url('<?php echo $sd->theme->get_theme_path('Travel15'); ?>img/package04.jpg');"><button class="galleryShow cropit"><i class="fa fa-image"></i></button>
							<figcaption><?php echo $sd->page->editable_text('package4', 'Disney Land');?></figcaption>
						</figure>
						<div class="package__details">
							<h1 class="h4"><?php echo $sd->page->editable_text('package4_days', '4 days / 3 nights Package');?></h1>
							<h2 class="h3"><?php echo $sd->page->editable_text('package4_price', 'Php 16,294');?><br> <span class="text-only">Per Month</span></h2>
							<p><?php echo $sd->page->editable_text('package4_info', '3 months at 0% interest for October 28-31, 2016');?></p>
							<a href="{base_url}package4">Select</a>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
	<?php $sd->column->bar($sd); ?>
	</section>
