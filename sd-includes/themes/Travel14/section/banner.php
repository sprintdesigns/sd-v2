	<section class="section-appender">
	<div id="banner">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-6">	
					<div id="book-travel-form">
						<h1 class="text-only">Book your travel</h1>
						<form class="sd-form" method="post" data-toggle="validator" sd-form-title="Booking Form">						
						<div class="form-group">
							<label for="inputFullname">Name</label>
							<input name="name" type="fname" class="form-control" id="fullname" required>
		              	</div>
		              	<div class="form-group">
							<label for="inputEmail">Email</label>
							<input name="email" type="email" class="form-control" id="email" required>
		              	</div>
		              	<div class="form-group">
							<label for="inputPhone">Phone</label>
							<input name="phone" type="phone" class="form-control" id="phone" required>
		              	</div>
						<div class="form-group">
							<label for="inputDepart">Departure</label>
							<input name="departure" type="departure" class="form-control sd-date" id="departure" required>
		              	</div>
		              	<div class="radio">
							<label><input type="radio" name="trip_type" required>One-way</label>
						</div>
						<div class="radio">
						  	<label><input type="radio" name="trip_type" required>Round Trip</label>
						</div>
		              	<div class="form-group">
							<label for="inputReturn">Return</label>
							<input name="return" type="return" class="form-control sd-date" id="return" required>
		              	</div>
		              	<button type="submit" class="btn btn-primary">Book Now!</button>
		              	</form>
					</div>
				</div>
				<div class="col-md-8 col-sm-6 hidden-xs">
					<div class="banner-text">
						<div class="border">
							<h1 class="text-only">Start your <span>journey</span> with us!</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php $sd->column->bar($sd); ?>
	</section>