	<section class="section-appender">
	<div id="book-travel-form">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 class="text-only">Book your travel</h1>
				</div>
			</div>
			<div class="row">
				<form class="sd-form" method="post" data-toggle="validator" sd-form-title="Booking Form">
					<div class="col-md-5 col-sm-5">
						<div class="form-group">
							<label for="inputFullname">First Name</label>
							<input name="firstname" type="text" class="form-control" id="firstname" required>
		              	</div>
		              	<div class="form-group">
							<label for="inputLastname">Last Name</label>
							<input name="lastname" type="text" class="form-control" id="lastname" required>
		              	</div>
		              	<div class="form-group">
							<label for="inputEmail">Email</label>
							<input name="email" type="email" class="form-control" id="email" required>
		              	</div>
		              	<div class="form-group">
							<label for="inputPhone">Phone</label>
							<input name="phone" type="text" class="form-control" id="phone" required>
		              	</div>
					</div>
					<div class="col-md-5 col-md-offset-2 col-sm-5 col-sm-offset-2">
						<div class="form-group">
							<label for="inputDepart">Departure Date</label>
							<input name="departure_date" type="text" class="form-control sd-date" id="departure" required>
		              	</div>
		              	<div class="radio">
							<label><input type="radio" name="trip_type" value="One-way" required>One-way</label>
						</div>
						<div class="radio">
						  	<label><input type="radio" name="trip_type" value="Round Trip" required>Round Trip</label>
						</div>
		              	<div class="form-group">
							<label for="inputReturn">Return Date</label>
							<input name="return_date" type="text" class="form-control sd-date" id="return" required>
		              	</div>
		              	<button class="btn btn-primary" type="submit">Book Now!</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<?php $sd->column->bar($sd); ?>
	</section>