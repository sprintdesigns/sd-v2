
</div>
<!-- END CONTAINER -->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/respond.min.js"></script>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="sd-assets/pluggables/plugins/jquery.min.js" type="text/javascript"></script>
<script src="sd-assets/pluggables/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="sd-assets/pluggables/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="sd-assets/pluggables/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="sd-assets/pluggables/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="sd-assets/pluggables/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="sd-assets/pluggables/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="sd-assets/pluggables/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="sd-assets/pluggables/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="sd-assets/pluggables/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="sd-assets/pluggables/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
<script src="sd-assets/pluggables/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
<script src="sd-assets/pluggables/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
<script src="sd-assets/pluggables/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
<script src="sd-assets/pluggables/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
<script src="sd-assets/pluggables/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
<script src="sd-assets/pluggables/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
<!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
<!--<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/morris/morris.min.js" type="text/javascript"></script>-->
<script src="sd-assets/pluggables/plugins/morris/raphael-min.js" type="text/javascript"></script>
<script src="sd-assets/pluggables/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="sd-assets/pluggables/scripts/metronic.js" type="text/javascript"></script>
<script src="sd-assets/admin/layout4/scripts/layout.js" type="text/javascript"></script>
<script src="sd-assets/admin/layout4/scripts/demo.js" type="text/javascript"></script>
<script src="sd-assets/admin/pages/scripts/index3.js" type="text/javascript"></script>
<script src="sd-assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   Demo.init(); // init demo features 
   //Index.init(); // init index page
 Tasks.initDashboardWidget(); // init tash dashboard widget  

 $("#span-error").html('');
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>