<?php
	$file = 'corefile/sd.json';

	if (file_exists($file)) {
		$str = file_get_contents($file);
		$sd = json_decode($str, true);
	}

	if(!$sd['install'])
	{
?>
<?php
	include 'sd-includes/header.php';
?>
<?php

	if(!array_key_exists('step2', $sd))
	{

		$sd_json = file_get_contents('corefile/sd.json');
		$sd_json = json_decode($sd_json);

		$sd_json->step2 = true;
		$jsonData = json_encode($sd_json);
		file_put_contents('corefile/sd.json', $jsonData);

		$con=mysqli_connect($sd_json->dbhost,$sd_json->username,$sd_json->password,$sd_json->dbname);
		
		$result = mysqli_query($con,"SHOW TABLES FROM ".$sd_json->dbname);		
		mysqli_close($con);

		if($result->num_rows > 0)
		{

	?>
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="container">
		  <div class="row">
		    <div class="white-box">
	           <h1>Database has contents in it</h1>
	           <p>It seems that the database has tables in it. To proceed, please clear your old database tables first then refresh this page.</p>
	            <p class="step"><a href="install.php" class="button button-large">Refresh</a></p>
		    </div>
		  </div>
		</div>
	</div>
	<!-- END CONTENT -->
<?php
		}else
		{
			echo "<script> location.href='install.php'; </script>";

		}
	}else{
?>		

<?php
	$str = file_get_contents('corefile/sd.json');
	$sd = json_decode($str, true);

	if(array_key_exists('step2', $sd)){
		$sd_json = file_get_contents('corefile/sd.json');
		$sd_json = json_decode($sd_json);

		$con=mysqli_connect($sd_json->dbhost,$sd_json->username,$sd_json->password,$sd_json->dbname);
		
		$result = mysqli_query($con,"SHOW TABLES FROM ".$sd_json->dbname);		
		mysqli_close($con);

		if($result->num_rows > 0)
		{
?>
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
		<div class="container">
		  <div class="row">
		    <div class="white-box">
		       <h1>Database has contents in it</h1>
		       <p>It seems that the database has tables in it. To proceed, please clear your old database tables first then refresh this page.</p>
		        <p class="step"><a href="install.php" class="button button-large">Refresh</a></p>
		    </div>
		  </div>
		</div>
		</div>
		<!-- END CONTENT -->
<?php
		}else
		{
			
?>		
		<?php
			$error_email=false;
			$error_username=false;
			$error_password=false;
			$error_base_url=false;

			$val_email='';
			$val_username='';
			$val_password='';
			$val_site='';
			$val_baseurl='';
			if(isset($_POST['submit-btn-install']))
			{
				$val_email=$_POST['admin_email'];
				$val_username=$_POST['user_name'];
				$val_password=$_POST['user_password'];
				$val_site=$_POST['weblog_title'];
				$val_baseurl=$_POST['base_url'];
				if($_POST['base_url'] != "")
				{
					if($_POST['user_name'] != "")
					{
						if($_POST['user_password'] != '' && $_POST['admin_password2'] == $_POST['user_password'])
						{
							if($_POST['admin_email'] != '')
							{

								$sd_json = file_get_contents('corefile/sd.json');
								$sd_json = json_decode($sd_json);

								$admin_data = array(
										'email' => $val_email, 
										'username' => $val_username, 
										'password' => $val_password, 
										'site' => $val_site, 
									);
								$sd_json->admin_data = $admin_data;
								$sd_json->base_url = $val_baseurl=$_POST['base_url'];
								
								

								$sd_json->install = true;

								$jsonData = json_encode($sd_json);
								file_put_contents('corefile/sd.json', $jsonData);

								include "create-tables.php";

								$sd_json = file_get_contents('corefile/sd.json');
								$sd_json = json_decode($sd_json);

								$sd_json->install = true;

								$jsonData = json_encode($sd_json);
								file_put_contents('corefile/sd.json', $jsonData);
								

								$new_request = explode('install.php', $_SERVER['REQUEST_URI']);
								echo "<script> location.href='http://".$_SERVER['SERVER_NAME'].$new_request[0]."sdlogin/success'; </script>";


							}else
							{
								$error_email=true;
							}

						}else
						{
							$error_password=true;
						}
					}else
					{
						$error_username=true;
						
					}
				}else
				{
					$error_base_url=true;
					
				}
				
				
				
			}
		?>	
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="container">
			  <div class="row">
			    <div class="white-box">
			    	<?php if($error_email){ ?>
			        <div class="note note-danger note-shadow">
						<p>
							 You need to setup email address
						</p>
					</div>
					<?php } ?>
					<?php if($error_username){ ?>
			        <div class="note note-danger note-shadow">
						<p>
							 You need to setup username
						</p>
					</div>
					<?php } ?>
					<?php if($error_password){ ?>
			        <div class="note note-danger note-shadow">
						<p>
							 Password did not match
						</p>
					</div>
					<?php } ?>
					<?php if($error_base_url){ ?>
			        <div class="note note-danger note-shadow">
						<p>
							 You need to setup you base url
						</p>
					</div>
					<?php } ?>
					<h1>SUCCESS!</h1>
					<p>We are almost there! We just need you to fill in the information bellow so you can start using our graphical user -friendly platform. You can change these information anytime anyway, so you don’t have to worry.</p>
					<form id="setup" method="post" action="install.php" novalidate="novalidate">
						<table class="form-table">
							<tbody>
							<tr>
								<th scope="row"><label for="weblog_title">Website Name</label></th>
								<td><input name="weblog_title" id="weblog_title" size="25" value="<?= $val_site ?>" type="text" required></td>
							</tr>
							<tr>
								<th scope="row"><label for="weblog_title">Base URL</label></th>
								<td><input name="base_url" id="base_url" size="25" value="<?= $val_baseurl ?>" type="text" required></td>
								
							</tr>
							<tr>
								<td colspan="2">
									<p><span class="description important">For your base url,You can use your domain <code>http://domain.com</code> or if in local <code>http://localhost/projectName</code>.</span></p>
								</td>
							</tr>
							<tr>
								<th scope="row"><label for="user_login">Username</label></th>
								<td>
								<input name="user_name" id="user_login" size="25" value="<?= $val_username ?>" type="text" required>
									<p><span class="description important">Usernames can have only alphanumeric characters, spaces, underscores, hyphens, periods, and the @ symbol.</span></p>
											</td>
							</tr>
									<tr class="form-field form-required user-pass1-wrap">
								<th scope="row">
									<label for="pass1-text">
										Password				
									</label>
								</th>
								<td>
									<div class="form-group">
								        <div class="input-group">
								          <input type="password" id="user_password" name="user_password" class="form-control" rel="gp" data-size="32" data-character-set="a-z,A-Z,0-9,#" value="<?= $val_password ?>" required>
								          <div class="notification"><i class="fa fa-check-circle"></i><i class="fa fa-times-circle"></i></div>
								          <!-- <span class="input-group-btn"><button type="button" class="btn btn-default btn-lg getNewPass"><span class="fa fa-refresh"></span></button></span> -->
								        </div>
								      </div>
									<p><span class="description important">
									<strong>Important:</strong>
													You will need this password to log&nbsp;in. Please store it in a secure location.</span></p>
								</td>
							</tr>
							<tr class="form-field form-required user-pass2-wrap hide-if-js">
								<th scope="row">
									<label for="pass2">Repeat Password
									</label>
								</th>
								<td>
									<div class="form-group">
										<div class="input-group">
											<input name="admin_password2" id="admin_password2" autocomplete="off" type="password" class="form-control" required>
											<div class="notification"><i class="fa fa-check-circle"></i><i class="fa fa-times-circle"></i></div>
										</div>
									</div>

								</td>
							</tr>
							<tr class="pw-weak" style="display: none;">
								<th scope="row">Confirm Password</th>
								<td>
									<label>
										<input name="pw_weak" class="pw-checkbox" type="checkbox">
										<div class="notification"><i class="fa fa-check-circle"></i><i class="fa fa-times-circle"></i></div>
										Confirm use of weak password</label>
 
								</td>
							</tr>
									<tr>
								<th scope="row"><label for="admin_email">Your Email</label></th>
								<td><input name="admin_email" id="admin_email" size="25" value="<?= $val_email ?>" type="email" required>
								<p><span class="description important">Double check email address before you continue.</span></p></td>
							</tr>
						</tbody></table>
						<p class="step"><input name="submit-btn-install" id="submit" class="button button-large" value="Install sdCMS" type="submit"></p>
						<input name="language" value="en_US" type="hidden">
					</form>
			    </div>
			  </div>
			</div>
		</div>
		<!-- END CONTENT -->
		<?php } ?>
	<?php } ?>
<?php } ?>
<script type="text/javascript" src="sd-assets/pluggables/plugins/jquery.min.js" ></script>
<script>
$(document).ready(function(){

	$('input').blur(function() {
		 $('#user_password').removeClass('has-error');
	       $('#admin_password2').removeClass('has-error');
	       $('#user_password').removeClass('has-success');
	       $('#admin_password2').removeClass('has-success');
	       $('#submit').removeClass('disabled');
        	$('.notification i.fa-check-circle').removeClass('show');
	       $('.notification i.fa-times-circle').removeClass('show');

	   var pass = $('input[name=user_password]').val();
	   var repass = $('input[name=admin_password2]').val();
	   if(($('input[name=user_password]').val().length == 0) || ($('input[name=admin_password2]').val().length == 0)){
	       $('#user_password').addClass('has-error');
	       $('.notification i.fa-check-circle').removeClass('show');
	       $('.notification i.fa-times-circle').addClass('show');
	       $('#submit').addClass('disabled');
	   }
	   else if (pass != repass) {
	       $('#user_password').addClass('has-error');
	       $('#admin_password2').addClass('has-error');
	       $('#submit').addClass('disabled');
	       $('.notification i.fa-check-circle').removeClass('show');
	       $('.notification i.fa-times-circle').addClass('show');
	   }
	   else {
	       $('#user_password').removeClass().addClass('has-success');
	       $('#admin_password2').removeClass().addClass('has-success');
	       $('.notification i.fa-check-circle').addClass('show');
	       $('.notification i.fa-times-circle').removeClass('show');
	   }
	});
	
});
	
</script>
<?php
	include 'sd-includes/footer.php';
?>

<?php }else { 

		$new_request = explode('/', $_SERVER['REQUEST_URI']);
		echo "<script> location.href='http://".$_SERVER['SERVER_NAME'].'/'.$new_request[1]."/sdlogin'; </script>";
	}

?>

