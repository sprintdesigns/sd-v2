
<?php if($sd->edit->mode()){ ?>
	
	<form style=" background-color: #ccc;
	  margin: 10px;
	  padding: 12px;" data-action="<?= base_url() ?>form/use_form" >
		<div class="form-group">
		    <label for="pwd">Select Form:</label>
		    <select class="form-control sd-form-select">
		    	<option value="invalid" selected>-- Select Form --</option>
		    	<?php
		    		$form = $sd->form->all_form();
			    	if($form){		
			    		foreach ($form as $index => $f) {
		    	?>
					    	<option value="<?= $sd->utilities->encrypt($f->id); ?>"><?= $f->name ?></option>
					   
					    <?php } ?>
			    <?php }else{ ?>
			    		<option value="invalid" selected>No Form</option>
			    <?php } ?>
		    </select>
	  	</div>
	  	<button type="button" class="btn btn-primary sd-use-form">Use Form</button>
	  	<button type="button" class="btn btn-default sd-add-form">Add Form</button>
	</form>
<?php } ?>
	<form class="sd-contact-form" >
</form>