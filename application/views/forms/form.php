<?php if($sd->edit->mode() && !$public){ ?>
<div class="row container"   style="margin-top:72px;">
<?php $response = $this->session->flashdata('response'); ?>
<?php if($response != null){ ?>
	<div class="alert alert-success">
		<button class="close" data-close="alert"></button>
		<span>
		<?= $response['message'] ?>. </span>
	</div>
<?php } ?>
	<div class="col-md-4">
		
		
			<div class="form-group" style="background-color: #ddd;
			  padding: 12px;" id="form-add-field-<?= $id ?>">
			<div class="form-group">
			<label for="name">Field Name:</label>
			<input type="text" class="form-control" id="sd-form-field-name" name="sd-form-field-name">
			</div>
			<label for="pwd">Fields type:</label>
			<select class="form-control" id="sd-form-field-type" name="sd-form-field-type" >
				<option value="text">Text</option>
				<option value="email">Email</option>
				<option value="textarea">Text Area</option>
			</select>
			<button type="button" class="btn btn-default" onClick="addField(this)" data-form-id="<?= $id ?>" data-form-action="<?= base_url() ?>form/add_field" >Add Field</button>
			</div>
		

	</div>
	<div class="col-md-7">
		<form id="form-to-save" onsubmit="return false;" data-form-id="<?= $id ?>">
			<?php $sd->form->field($sd->form->get_form_data($id)); ?>
		</form>
	</div>
</div>
<?php }else { ?>
	<h2 class="text-only">Let's hear from you</h2>
	<h1 class="text-only">Drop us <span style="color: #f78f56;"> A Line</span></h1>
<?php if(!$sd->edit->mode()){ ?>
	<form method="POST" action="<?= base_url() ?>email/send/<?= $id ?>">
<?php } ?>
	<div class="container sd-send-form" id="form-id-<?= $id ?>">
		<div class="row" style="margin-top:35px;">
		<input type="hidden"  name="form-id" id="form-id" value="<?= $id ?>" >
		<?php if($fields){ ?>
			<?php foreach ($fields as $index => $f) { ?>
				<?php if($f->status){ ?>
					<?php if($f->type != 'textarea'){ ?>
						<div class="form-group col-md-6 col-sm-6">
							<?php  if($f->type != 'submit'){ ?>
								<label for="<?= $f->name ?>-<?= $f->id ?>"><?= $f->label ?> : <span class="text text-danger" ><?= $f->required? '*':'' ?></span> </label>
							<?php } ?>
							<input type="<?= $f->type=='date'? 'text':$f->type ?>" class="form-control <?= $f->type=='date'? 'sd-date-picker':'' ?>" id="<?= $f->name ?>-<?= $f->id ?>" name="<?= $f->name ?>-<?= $f->id ?>" value="<?= $f->type == 'submit'? $f->label:''  ?>" <?= $f->required? 'required':'' ?>  />
						</div>
					<?php }else{ ?>
						<div class="form-group col-md-12 col-sm-12">
							<label for="<?= $f->name ?>-<?= $f->id ?>"><?= $f->label ?> : <span class="text text-danger" ><?= $f->required? '*':'' ?></span> </label>
							<textarea class="form-control" id="<?= $f->name ?>-<?= $f->id ?>" name="<?= $f->name ?>-<?= $f->id ?>" <?= $f->required? 'required':'' ?> ></textarea>
						</div>
					<?php } ?>
				<?php } ?>
			<?php } ?>
		<?php } ?>
		<?php if(!$sd->edit->mode()){ ?>
		<div class="form-group col-md-12">	
			<button type="submit" class="btn btn-default">Send</button>
		</div>	
		<?php } ?>
	   </div>
	</div>
</form>
<?php } ?>
