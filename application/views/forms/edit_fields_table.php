<?php
	$form_data = $sd->form->get_form_data($form_id);
?>
<table class="table compressed">
			<thead>
			<tr>
				<th>
					 Status
				</th>
				<th>
					 Field Type
				</th>
				<th>
					Field Name
				</th>
				<th>
					 Required
				</th>
				<th> &nbsp; </th>
			</tr>
			</thead>
			<tbody>

				<?php if($form_data) { 
					foreach ($form_data as $index => $data) { ?>
					<tr>
						<td>
							<div class="ddeye-handle eye-active">
							  <label>
							  	<input class="ios-switch green tinyswitch" <?= $data->status? 'checked':'' ?> type="checkbox">
							  	<div onclick="switch_status_field('<?= $data->id ?>')" data-status="<?= $data->status ?>" class="status-field-<?= $data->id ?>" >
							  		<span class="onswitch">On</span><span class="offswitch">Off</span>
						  			<div></div>
					  			</div>
							  </label>
							  <input class="nav-id" value="108" type="hidden">
							</div>
						</td>
						<td>
							 <span class="type-<?= $data->id ?>" ><?= $data->type ?></span>
							 <select class="select-type-<?= $data->id ?>" style="display:none;" >
							 	<option value="text" <?= $data->type=="text"? 'selected':'' ?> >Text</option>
							 	<option value="email" <?= $data->type=="email"? 'selected':'' ?> >Email</option>
							 	<option value="textarea" <?= $data->type=="textarea"? 'selected':'' ?> >Text Area</option>
							 	<option value="number" <?= $data->type=="number"? 'selected':'' ?> >Number</option>
							 	<option value="radio" <?= $data->type=="radio"? 'selected':'' ?> >Radio Button</option>
							 	<option value="date" <?= $data->type=="date"? 'selected':'' ?> >Date</option>
							 </select>
						</td>
						<td>
							 <span class="label-<?= $data->id ?>" ><?= $data->label ?></span>
							 <input type="text" class="input-label-<?= $data->id ?>" value="<?= $data->label ?>" style="display:none;" /> 
						</td>
						<td>
						  <input id="val-<?= $data->id ?>" type="checkbox" class="sdCheckbox" <?= $data->required? 'checked':'' ?> onclick="switch_required_field('<?= $data->id ?>')" />
						  <label for="val-<?= $data->id ?>">&nbsp;</label>
						</td>
						<td>
						  <div class="contactform-container-buttons">

						  	<button class="btn btn-success btn-field-func-<?= $data->id ?>" onclick="duplicate_field('<?= $data->id ?>')" >Duplicate</button>
						  	<button class="btn btn-success btn-field-func-<?= $data->id ?> edit-field" data-id="<?= $data->id ?>" >Edit</button>
						  	<button class="btn btn-danger delete btn-field-func-<?= $data->id ?>" onclick="delete_field('<?= $data->id ?>')" >Delete</button>
						  	<button class="btn btn-success btn-save-changes-field btn-edit-field-<?= $data->id ?>" data-id="<?= $data->id ?>" style="display:none;" >Save</button>
						  	<button class="btn btn-danger delete btn-cancel-changes-field btn-edit-field-<?= $data->id ?>" data-id="<?= $data->id ?>" style="display:none;" >Cancel</button>
						  </div>
						  </div>
						</td>
					</tr> 
				<?php } /* end foreach */ } ?>
				<tr class="add-new-field-tr hidden" >
					<td>
						<div class="ddeye-handle eye-active disabled">
						  <label>
						  	<input class="ios-switch green tinyswitch" type="checkbox">
						  	<div>
						  		<span class="onswitch">On</span><span class="offswitch">Off</span>
					  			<div></div>
				  			</div>
						  </label>
						  <input class="nav-id" value="108" type="hidden">
						</div>
					</td>
					<td>
						 <select class="new-select-type" >
						 	<option value="text" >Text</option>
						 	<option value="email" >Email</option>
						 	<option value="textarea" >Text Area</option>
						 	<option value="number" >Number</option>
						 	<option value="radio" >Radio Button</option>
						 	<option value="date" >Date</option>
						 </select>
					</td>
					<td>
						 <div class="form-group">
						    <input type="text" class="new-input-label form-control" value="" /> 
						  </div>
						 
					</td>
					<td>
					  <input id="val-new-field" type="checkbox" class="sdCheckbox" />
					  <label for="val-new-field">&nbsp;</label>
					</td>
					<td>
					  <div class="contactform-container-buttons">
					  	<button class="btn btn-success btn-save-new-field" >Save</button>
					  	<button class="btn btn-danger delete btn-cancel-new-field" >Cancel</button>
					  </div>
					 
					</td>
				</tr> 
			
			</tbody>
			</table>