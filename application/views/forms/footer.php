    <div id="contact-form-modal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Add Form</h4>
	      </div>
	      <div class="modal-body">
	        <form id="sd-add-form-form" data-action="<?= base_url(); ?>form/add_contact_form" onSubmit="return false;" > 

	        	<div class="form-group">
				    <label for="sd-form-name">Form Name:</label>
				    <input type="text" class="form-control" id="sd-form-name" name="sd-form-name">
				  </div>
	        </form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-primary" id="sd-add-form-submit">Add</button>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>

	  </div>
	</div>


    <script type="text/javascript">
      /* Global */
        var base_url = '<?php echo base_url(); ?>' + '';
        var site_url = '<?php echo base_url(); ?>' + '';
    </script>

   
    <script src="<?= base_url() ?>sd-assets/js/jquery.dataTables.min.js"></script>
    <script src="<?= base_url() ?>sd-assets/js/sd-form-2.js"></script>
  </body>
</html>