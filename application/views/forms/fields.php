<?php
	$count_row = 0;
?>
<form method="POST" action="<?= base_url() ?>email/send/<?= $fields[0]->form_id ?>" >
	<?php for ($f=0; $f < $args['column']; $f++) { ?>
		
		<div class="<?= $args['div'][$f]; ?>" >
			<?php for ($i=0; $i < $args['field']; $i++) { ?>
					<?php if($count_row < sizeof($fields)){ ?>
						<?php if($fields[$count_row]->type != 'textarea'){ ?>
							<?php if($fields[$count_row]->type != 'radio'){ ?>
								<div class="form-group">
									<?php  if($fields[$count_row]->type != 'submit'){ ?>
										<label for="<?= $fields[$count_row]->name ?>-<?= $fields[$count_row]->id ?>"><?= $fields[$count_row]->label ?> : <span class="text text-danger" ><?= $fields[$count_row]->required? '*':'' ?></span> </label>
									<?php } ?>
									<input type="<?= $fields[$count_row]->type=='date'? 'text':$fields[$count_row]->type ?>" class="<?= $fields[$count_row]->type=='date'? 'sd-date-picker':'' ?> form-control" id="<?= $fields[$count_row]->name ?>-<?= $fields[$count_row]->id ?>" name="<?= $fields[$count_row]->name ?>-<?= $fields[$count_row]->id ?>" value="<?= $fields[$count_row]->type == 'submit'? $fields[$count_row]->label:''  ?>" <?= $fields[$count_row]->required? 'required':'' ?>  />	
								</div>
							<?php }else{ ?>
								<div class="radio">
									<label ><input type="<?= $fields[$count_row]->type ?>" class="input-form-to-save"  value="<?= $fields[$count_row]->label  ?>" name="radio-btn-<?= $fields[$count_row]->form_id ?>" data-type="<?= $fields[$count_row]->type ?>" /> <?= $fields[$count_row]->label ?></label>	
								</div>
							<?php } ?>				
							
						<?php }else{ ?>
							<div class="form-group">
								<label for="<?= $fields[$count_row]->name ?>-<?= $fields[$count_row]->id ?>"><?= $fields[$count_row]->label ?> : <span class="text text-danger" ><?= $fields[$count_row]->required? '*':'' ?></span> </label>
								<textarea class="form-control" id="<?= $fields[$count_row]->name ?>-<?= $fields[$count_row]->id ?>" name="<?= $fields[$count_row]->name ?>-<?= $fields[$count_row]->id ?>" <?= $fields[$count_row]->required? 'required':'' ?> ></textarea>
							</div>
						<?php } ?>
					<?php } ?>

					<?php $count_row++; ?>
			<?php } ?>
			<?php if($f == $args['column']-1){ ?>
				<?php if($count_row < sizeof($fields)){ ?>
					<?php for ($i=$count_row; $i < sizeof($fields); $i++) { ?>
								<?php if($fields[$i]->type != 'textarea'){ ?>
									<?php if($fields[$i]->type != 'radio'){ ?>
										<div class="form-group">
											<?php  if($fields[$i]->type != 'submit'){ ?>
												<label for="<?= $fields[$i]->name ?>-<?= $fields[$i]->id ?>"><?= $fields[$i]->label ?> : <span class="text text-danger" ><?= $fields[$i]->required? '*':'' ?></span> </label>
											<?php } ?>
											<input type="<?= $fields[$i]->type=='date'? 'text':$fields[$i]->type ?>" class="<?= $fields[$i]->type=='date'? 'sd-date-picker':'' ?> form-control" id="<?= $fields[$i]->name ?>-<?= $fields[$i]->id ?>" name="<?= $fields[$i]->name ?>-<?= $fields[$i]->id ?>" value="<?= $fields[$i]->type == 'submit'? $fields[$i]->label:''  ?>" <?= $fields[$i]->required? 'required':'' ?>  />	
										</div>
									<?php }else{ ?>
										<div class="radio">
											<label ><input type="<?= $fields[$i]->type ?>" class="input-form-to-save"  value="<?= $fields[$i]->label  ?>" name="radio-btn-<?= $fields[$i]->form_id ?>" data-type="<?= $fields[$i]->type ?>" /> <?= $fields[$i]->label ?></label>	
										</div>
									<?php } ?>				
									
								<?php }else{ ?>
									<div class="form-group">
										<label for="<?= $fields[$i]->name ?>-<?= $fields[$i]->id ?>"><?= $fields[$i]->label ?> : <span class="text text-danger" ><?= $fields[$i]->required? '*':'' ?></span> </label>
										<textarea class="form-control" id="<?= $fields[$i]->name ?>-<?= $fields[$i]->id ?>" name="<?= $fields[$i]->name ?>-<?= $fields[$i]->id ?>" <?= $fields[$i]->required? 'required':'' ?> ></textarea>
									</div>
								<?php } ?>
					<?php } ?>
				<?php } ?>
				<button type="submit" class="<?= $args['button']['class']; ?>" ><?= $args['button']['text']; ?></button>
			<?php } ?>
		</div>
	<?php } ?>

</form>
