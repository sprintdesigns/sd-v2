<div id="page-content-wrapper">
<!-- <div class="col-md-12">
	<button class="btn btn-primary pull-left"  data-toggle="modal" data-target="#contact-form-modal" >+</button>
	<a class="btn btn-default pull-right" href="<?= base_url(); ?>" style="color: #000;font-size: 31px;argin: 0px 0px 40px 0px;" >&times;</a>
</div> -->
	<div class="row">
	 	<div class="col-md-12"> 
		  	<div class="alert alert-info">
			  <p>Set the behavior of this form in the way you want to do. you can use this fallowing fields of your form:</p>
			  <p>
			  	<?php if($form_data){ ?>
				  	<?php foreach ($form_data as $index => $data) { ?>
				  		<code>[<?= $data->name ?>]</code>
				  	<?php } ?>
			  	<?php }else{ ?>
			  		You don't have fields for your form. <a href="<?= base_url() ?>form/edit/<?= $id ?>"  class="btn btn-success"  >Edit you Form</a>
			  	<?php } ?>
			  </p>
			</div>		  
		</div>
		<div class="col-md-10"><br/>
			<?php $response = $this->session->flashdata('response'); ?>
			<?php if($response != null){ ?>
				<div class="alert alert-<?= $response['status']? 'success':'danger' ?>">
					<button class="close" data-close="alert"></button>
					<span>
					<?= $response['message'] ?>. </span>
				</div>
			<?php } ?>
			 <form method="post" action="<?= base_url() ?>form/save_settings/<?= $id ?>">
			 	<input type="hidden" id="email-form-type" name="email-form-type" value="sender" >
			 	<input type="hidden" id="email-form-id" name="email-form-id" value="<?= !empty($settings)? $settings[0]->id:'' ?>" >
			  <div class="form-group">
			    <label for="email-form-to">To:</label>
			    <input type="text" class="form-control" id="email-form-to" name="email-form-to" value="<?= !empty($settings)? $settings[0]->to_email:'' ?>" >
			  </div>

			  <div class="form-group">
			    <label for="email-form-from">From:</label>
			    <input type="text" class="form-control" id="email-form-from" name="email-form-from" value="<?= !empty($settings)? $settings[0]->from_email:'' ?>" >
			  </div>

			  <div class="form-group">
			    <label for="email-form-subject">Subject:</label>
			    <input type="text" class="form-control" id="email-form-subject" name="email-form-subject" value="<?= !empty($settings)? $settings[0]->subject:'' ?>" >
			  </div>

			  <div class="form-group">
			    <label for="email-form-message">Message Body:</label>
			    <textarea class="form-control" id="email-form-message" name="email-form-message" ><?= !empty($settings)? $settings[0]->message:'' ?></textarea>
			  </div>
			  
			  <button type="submit" class="btn btn-primary">Save</button>
			</form>
		</div>
	</div>
</div>
