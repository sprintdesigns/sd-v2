<div id="page-content-wrapper">
<!-- <div class="col-md-12">
	<button class="btn btn-primary pull-left"  data-toggle="modal" data-target="#contact-form-modal" >+</button>
	<a class="btn btn-default pull-right" href="<?= base_url(); ?>" style="color: #000;font-size: 31px;argin: 0px 0px 40px 0px;" >&times;</a>
</div> -->
	<div class="row">
	 	<div class="col-md-12"> 
		  	<div class="alert alert-info">
			  <p>Set the behavior of this form in the way you want to do. you can use this fallowing fields of your form:</p>
			</div>		  
		</div>
		<div class="col-md-10"><br/>
			<?php $response = $this->session->flashdata('response'); ?>
			<?php if($response != null){ ?>
				<div class="alert alert-<?= $response['status']? 'success':'danger' ?>">
					<button class="close" data-close="alert"></button>
					<span>
					<?= $response['message'] ?>. </span>
				</div>
			<?php } ?>
			 <form method="post" action="<?= base_url() ?>form/save_email">
			 
			  <div class="form-group">
			    <label for="form-email">Email:</label>
			    <input type="email" class="form-control" id="form-email" name="form-email" value="<?= $data_email? $data_email->email:'' ?>" >
			  </div>

			  <div class="form-group">
			    <label for="form-password">Password:</label>
			    <input type="password" class="form-control" id="form-password" name="form-password" value="<?= $data_email? $data_email->password:'' ?>" >
			  </div>

			   <div class="form-group">
			    <label for="form-crypto">Host:</label>
			    <input type="text" class="form-control" id="form-host" name="form-host" value="<?= $data_email? $data_email->host:'' ?>" >
			  </div>

			   <div class="form-group">
			    <label for="form-protocol">Protocol:</label>
			    <input type="text" class="form-control" id="form-protocol" name="form-protocol" value="<?= $data_email? $data_email->protocol:'' ?>" >
			  </div>

			  <div class="form-group">
			    <label for="form-port">Port:</label>
			    <input type="text" class="form-control" id="form-port" name="form-port" value="<?= $data_email? $data_email->port:'' ?>" >
			  </div>

			  <div class="form-group">
			    <label for="form-crypto">SMTP encryption:</label>
			    <input type="text" class="form-control" id="form-crypto" name="form-crypto" value="<?= $data_email? $data_email->encryption:'' ?>" >
			  </div>

			 
			  
			  <button type="submit" class="btn btn-primary">Save</button>
			</form>
		</div>
	</div>
</div>
