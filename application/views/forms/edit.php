 <?php 

 	$form_data = $sd->form->get_form_data($form_id);
 	$form = $sd->form->form($form_id); 
 	$settings = $sd->form->settings($form_id);

 	
 ?>
 <input type="hidden" value="<?= $form[0]->id ?>" id="form-id-hidden" />
 <h2 class="title"><button class="btn btn-default backpage"><i class="fa fa-angle-left"></i></button><?= $form[0]->name ?> <button class="btn btn-primary add-new-field"><i class="fa fa-plus-circle"></i> Add Field</button> <button class="btn btn-dark disabled"><i class="fa fa-file-text-o"></i> Generate Report</button></h2>
 <div class="portlet scrolly">
	<div class="portlet-body">
		<div class="table-responsive fields-table-container">
			<table class="table compressed">
			<thead>
			<tr>
				<th>
					 Status
				</th>
				<th>
					 Field Type
				</th>
				<th>
					Field Name
				</th>
				<th>
					 Required
				</th>
				<th> &nbsp; </th>
			</tr>
			</thead>
			<tbody>

				<?php if($form_data) { 
					foreach ($form_data as $index => $data) { ?>
					<tr>
						<td>
							<div class="ddeye-handle eye-active">
							  <label>
							  	<input class="ios-switch green tinyswitch" <?= $data->status? 'checked':'' ?> type="checkbox">
							  	<div onclick="switch_status_field('<?= $data->id ?>')" data-status="<?= $data->status ?>" class="status-field-<?= $data->id ?>" >
							  		<span class="onswitch">On</span><span class="offswitch">Off</span>
						  			<div></div>
					  			</div>
							  </label>
							  <input class="nav-id" value="108" type="hidden">
							</div>
						</td>
						<td>
							 <span class="type-<?= $data->id ?>" ><?= $data->type ?></span>
							 <select class="select-type-<?= $data->id ?>" style="display:none;" >
							 	<option value="text" <?= $data->type=="text"? 'selected':'' ?> >Text</option>
							 	<option value="email" <?= $data->type=="email"? 'selected':'' ?> >Email</option>
							 	<option value="textarea" <?= $data->type=="textarea"? 'selected':'' ?> >Text Area</option>
							 	<option value="number" <?= $data->type=="number"? 'selected':'' ?> >Number</option>
							 	<option value="radio" <?= $data->type=="radio"? 'selected':'' ?> >Radio Button</option>
							 	<option value="date" <?= $data->type=="date"? 'selected':'' ?> >Date</option>
							 </select>
						</td>
						<td>
							 <span class="label-<?= $data->id ?>" ><?= $data->label ?></span>
							 <input type="text" class="input-label-<?= $data->id ?>" value="<?= $data->label ?>" style="display:none;" /> 
						</td>
						<td>
						  <input id="val-<?= $data->id ?>" type="checkbox" class="sdCheckbox" <?= $data->required? 'checked':'' ?> onclick="switch_required_field('<?= $data->id ?>')" />
						  <label for="val-<?= $data->id ?>">&nbsp;</label>
						</td>
						<td>
						  <div class="contactform-container-buttons">

						  	<button class="btn btn-success btn-field-func-<?= $data->id ?>" onclick="duplicate_field('<?= $data->id ?>')" >Duplicate</button>
						  	<button class="btn btn-success btn-field-func-<?= $data->id ?> edit-field" data-id="<?= $data->id ?>" >Edit</button>
						  	<button class="btn btn-danger delete btn-field-func-<?= $data->id ?>" onclick="delete_field('<?= $data->id ?>')" >Delete</button>
						  	<button class="btn btn-success btn-save-changes-field btn-edit-field-<?= $data->id ?>" data-id="<?= $data->id ?>" style="display:none;" >Save</button>
						  	<button class="btn btn-danger delete btn-cancel-changes-field btn-edit-field-<?= $data->id ?>" data-id="<?= $data->id ?>" style="display:none;" >Cancel</button>
						  </div>
						  </div>
						</td>
					</tr> 
				<?php } /*end foreach*/ } ?>
				<tr class="add-new-field-tr hidden" >
					<td>
						<div class="ddeye-handle eye-active disabled">
						  <label>
						  	<input class="ios-switch green tinyswitch" type="checkbox">
						  	<div>
						  		<span class="onswitch">On</span><span class="offswitch">Off</span>
					  			<div></div>
				  			</div>
						  </label>
						  <input class="nav-id" value="108" type="hidden">
						</div>
					</td>
					<td>
						 <select class="new-select-type" >
						 	<option value="text" >Text</option>
						 	<option value="email" >Email</option>
						 	<option value="textarea" >Text Area</option>
						 	<option value="number" >Number</option>
						 	<option value="radio" >Radio Button</option>
						 	<option value="date" >Date</option>

						 </select>
					</td>
					<td>
						 <div class="form-group">
						    <input type="text" class="new-input-label form-control" value="" /> 
						  </div>
						 
					</td>
					<td>
					  <input id="val-new-field" type="checkbox" class="sdCheckbox" />
					  <label for="val-new-field">&nbsp;</label>
					</td>
					<td>
					  <div class="contactform-container-buttons">
					  	<button class="btn btn-success btn-save-new-field" >Save</button>
					  	<button class="btn btn-danger delete btn-cancel-new-field" >Cancel</button>
					  </div>
					 
					</td>
				</tr> 
			
			</tbody>
			</table>
		</div>
	</div>
	 <button class="btn btn-primary add-new-field"><i class="fa fa-plus-circle"></i> Add Field</button>
  </div>
  <div class="portlet last">
	<div class="portlet-body">
		<div id="test-email-response" ></div>
		<input type="hidden" value="<?= $settings? $settings[0]->id:'' ?>" id="settings-id" name="setting-id" />
		<input type="hidden" value="<?= $form_id ?>" id="form-id" name="form-id" />
		<label>Mailer Setup</label>
		<form class="form-inline">
			<?php
				if($settings)
				{
					if($settings[0]->from_email != '')
					{
						$mailer = json_decode($settings[0]->from_email);
					}else
					{
						$mailer = false;
					}
				}else
				{
					$mailer = false;
				}
				
				
			?>
		  <div class="form-group">
		    <label for="mailer-setting-email"><i class="fa fa-question-circle"></i> Webmail:</label>
		    <input type="email" class="form-control" id="mailer-settings-email" name="mailer-settings-email" placeholder="Jane Doe" value="<?= $mailer? $mailer->email:'' ?>" >
		  </div>
		  <div class="form-group">
		    <label for="mailer-settings-password">Password:</label>
		    <input type="password" class="form-control" id="mailer-settings-password" name="mailer-settings-password" value="<?= $mailer? $mailer->password:'' ?>" >
		  </div>
		  <div class="form-group">
		    <label for="mailer-settings-verify">Verify:</label>
		    <input type="password" class="form-control" id="mailer-settings-verify" name="mailer-settings-verify" >
		  </div>
		  <button type="button" class="btn btn-success btn-settings-mailer-save">Save</button>
		</form>
		<form class="form-inline">
		  <div class="form-group">
		    <label for="webmailsubject"><i class="fa fa-question-circle"></i> Subject:</label>
		    <input style="min-width:400px;" type="text" class="form-control" id="webmail-subject" placeholder="An interested customer has an inquiry" value="<?= $settings? $settings[0]->subject:'' ?>" >
		  </div>
		  <button type="button" class="btn btn-success  btn-settings-subject-save">Save</button>
		</form>
		<form class="form-inline last" id="email-to-form" >
		  <div class="form-group">
		    <label for="emailreceivers"><i class="fa fa-question-circle"></i> Email Receivers</label>
		    <?php if($settings){ ?>
		    	<?php if($settings[0]->to_email != null && $settings[0]->to_email != "" ){ ?>
		    		<?php $email = json_decode($settings[0]->to_email); ?>
		    		<?php foreach ($email as $index => $e){ ?>
		    			<input type="email" class="form-control email-input-plus" placeholder="Email" name="email[]"  value="<?= $e ?>" >
		    		<?php } ?>
		    	<?php }else{ ?>
		    		<input type="email" class="form-control email-input-plus" placeholder="Email" name="email[]" >
		    	<?php } ?>
		    <?php }else{ ?>
		    	<input type="email" class="form-control email-input-plus" placeholder="Email" name="email[]" >
		    <?php } ?>
		    <span id="new-insert-email"></span>
		    <button type="button" class="btn btn-success btn-plus-email"><i class="fa fa-plus-circle"></i></button>
		  </div>
		   <button type="submit" class="btn btn-success btn-save-to-email">Save</button>
		</form>
	</div>
	 <button class="btn btn-primary btn-test-mailer"><i class="fa fa-paper-plane"></i> Test Mail</button>
  </div>