<div id="contact-form-response">
</div>	      
<div id="page-content-wrapper">
	<div class="plugin-1-page">
		  <h2 class="title">Forms <button class="btn btn-primary btn-add-contact-form-show"><i class="fa fa-plus-circle"></i> Add Form</button></h2>
		<?php if($sd->form->all_form() == FALSE) { ?>
		  <div class="boxnote-form">
		  	<h1>You don't have any forms yet, you need to create one!</h1>
		  	<button class="btn btn-primary btn-large btn-add-contact-form-show"><i class="fa fa-plus-circle"></i> Create Form</button>
		  </div>
		<?php } ?>  	
	  	<div class="portlet">
			<div class="portlet-body">
				<div class="table-responsive">
					<table class="table">
					<thead>
					<tr>
						<th>
							 Status
						</th>
						<th>
							 Name
						</th>
						<th>
							Date Created
						</th>
						<th>
							 Created By
						</th>
						<th> &nbsp; </th>
					</tr>
					</thead>
					<tbody>

						<?php $form = $sd->form->all_form(); ?>
				        <?php if($form){ ?>
				        	<?php foreach ($form as $index => $f) { ?>
				        	
				        		<tr class="form-info-<?= $f->id ?>" >
				        			<td>
				        				
										<div class="ddeye-handle eye-active <?= $f->is_main? 'disabled':'' ?> eye-active-<?= $f->id ?>" >
										  <label><input class="ios-switch green tinyswitch" <?= $f->status? "checked":"" ?> type="checkbox">
										  	<div  onclick="switch_status('<?= $f->id ?>')" data-status="<?= $f->status ?>" class="status-form-<?= $f->id ?>" >
										  		<span class="onswitch">On</span><span class="offswitch">Off</span>
										  		<div></div>
										  	</div>
										  </label>
										  <input class="nav-id" value="108" type="hidden">
										</div>
										
									</td>
									<td><?= $f->name ?></td>
									<td><?= date('M d,Y',strtotime($f->create_at)) ?></td>
								 	<td><?= $f->created_by ?></td>
								 	<td>
									  <div class="contactform-container-buttons">
									  	<button class="btn btn-success" onclick="duplicate_form('<?= $f->id ?>')" >Duplicate</button>
									  	<button class="btn btn-success showNextpage" onclick="edit_form('<?= $f->id ?>')">Edit</button>
									  	<button class="btn btn-success main-btn main-btn-<?= $f->id ?>" style="<?= $f->is_main? 'display:none;':'' ?><?= $f->status? '':'display:none;' ?>" onclick="switch_main('<?= $f->id ?>')" >Apply to all pages</button>
									  	<button class="btn btn-danger delete-<?= $f->id ?>" style="<?= $f->status? 'display:none;':'' ?>" onclick="delete_form('<?= $f->id ?>')" >Delete</button>
									  </div>
									  </div>
									</td>

				        			<!-- <td><a href="javascript:switch_status('<?= $f->id ?>');" data-status="<?= $f->status ?>" class="btn btn-info status-form-<?= $f->id ?>"><?= $f->status? "On":"Off" ?></a></td>
						            <td><?= $f->name ?></td>
						            <td><?= date('M d,Y',strtotime($f->create_at)) ?></td>
						             <td><?= $f->created_by ?></td>
						            <td>
						            	<a href="javascript:duplicate_form('<?= $f->id ?>');" class="btn btn-info">Duplicate</a>
						            	<a href="javascript:edit_form('<?= $f->id ?>')" class="btn btn-info">Edit</a>
						            	<a href="javascript:switch_main('<?= $f->id ?>');" class="btn btn-info main-btn main-btn-<?= $f->id ?>" style="<?= $f->is_main? 'display:none;':'' ?>" >Apply all pages</a>
						            	<a href="javascript:delete_form('<?= $f->id ?>');" class="btn btn-info">Delete</a>
						            	
						            </td> -->
						        </tr>
				        	<?php } ?>
				        <?php } ?>
						<tr class="tr-new-contact-form" style="display:none;">
							<td>
								<div class="ddeye-handle eye-active disabled">
								  <label><input class="ios-switch green tinyswitch" checked="" type="checkbox"><div><span class="onswitch">On</span><span class="offswitch">Off</span><div></div></div>
								  </label>
								  <input class="nav-id" value="108" type="hidden">
								</div>
							</td>
							<td>
								 <input id="new-contact-form-name" name="new-contact-form-name" placeholder="Contact Form Title" />					
							</td>
							<td>
								 
							</td>
							<td>
								
							</td>
							<td>
							  <div class="contactform-container-buttons">
							  	<button class="btn btn-success btn-save-new-form">Save</button>
							  	<button class="btn btn-danger delete btn-cancel-new-form">Cancel</button>
							  </div>
							  </div>
							</td>
						</tr>
					
					</tbody>
					</table>
				</div>
			</div>
	  	</div>
	</div>
	<div class="plugin-2-page">
	  	
  		<!-- <div class="load-body-edit">
  		</div> -->
	</div>
</div>
<script src="<?= base_url() ?>sd-assets/js/sd-form-2.js"></script>