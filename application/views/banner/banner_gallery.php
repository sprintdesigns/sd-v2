   <?php
    $media_data = $sd->gallery->process_media_data( $sd->image->get_media() );
?>
<?php if(isset($media_data)) { ?>
  <ul class="image-gallery">
    <?php foreach($media_data as $d) { ?>
     
      <li class="imglicontainer">
        <a href="javascript:create_new_slide('<?php $sd->image->create_full_path_web($d['data']->upload_data->full_path); ?>','<?= $d['id'] ?>');">
          <div class="imagebox">
          	<span>Select</span>
            <div class="centered">
              <img class="lazy-image img-responsive" src="<?php $sd->image->create_full_path_thumb($d['data']->upload_data->full_path); ?>"> 
            </div>
          </div>    
        </a>
      </li>
    <?php  } ?>  
  </ul>
<?php } ?>