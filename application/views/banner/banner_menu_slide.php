 <label>Slides</label>
  <button class="add-item mini btn-add-slide" type="button" ><i class="fa fa-plus-circle"></i> Add Images</button> 
<?php if($slider){ ?>
    <?php $media_data = $sd->gallery->process_media_data( $sd->image->get_media() ); ?>
  <?php foreach ($slider as $index => $slide) { ?>
      <div class="sd-banner-menu-item parent-slider-index-<?= $index; ?>">
          <span class="slide-num"><?= $index + 1; ?></span>
        <?php if(sizeof($slider) > 1){ ?>
          <?php if(sizeof($media_data) > 0){ ?>
            <?php $use = false; ?>
            <?php foreach ($media_data as $index2 => $d) { ?>
              <?php if($sd->image->create_full_path($d['data']->upload_data->full_path,true,true) == $slide->original_url){ ?>
                <?php $use = true; ?>
                <button class="btn btn-danger" onclick="delete_slide('<?= $index2; ?>','<?= $slide->id; ?>','<?= $d["id"] ?>')" ><i class="fa fa-close"></i></button>
              <?php } ?>
            <?php } ?>
            <?php if(!$use){ ?>
              <button class="btn btn-danger" onclick="delete_slide('<?= $index; ?>','<?= $slide->id; ?>','false')" ><i class="fa fa-close"></i></button>
            <?php } ?>
          <?php } ?>
        <?php } ?>
        <a href="javascript:select_slide('<?= $index; ?>','<?= $slide->id; ?>')" class="slider-index-<?= $index; ?> sel-slider <?= $id_selected==$slide->id? 'sd-slider-active':'' ?>" >
          <div class="slider-centered">
            <img src="<?php echo $slide->url ?>" style="width:100%;" />
          </div>
        </a>
      </div>
  <?php } ?>

<?php } ?>

<ul class='custom-menu'>
  <li data-action="first"><i class="fa fa-plus"></i> Add New Slide</li>
</ul>