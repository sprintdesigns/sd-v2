 <!--start carousel -->
<!-- LayerSlider start -->
<div id="layerslider" style="width: 100%; height: 494px; margin: 0 auto;">

  <!-- slide start -->
  <?php if($slider){ ?>
    <?php foreach ($slider as $index => $slide) { ?>
      
    
      <div class="ls-slide ls-slide1" data-ls="">

        <img src="<?php echo $slide->url; ?>" class="ls-bg" alt="Slide background">

        <div class="ls-l ls-title" style="top: 96px; left: 35%; white-space: nowrap;" data-ls="">
          <?php echo $slide->text_title; ?>
        </div>

        <div class="ls-l ls-mini-text" style="top: 338px; left: 35%; white-space: nowrap;" data-ls="">
          <?php echo $slide->sub_title; ?>


          <a href="<?= $slide->button_url; ?>" class="btn btn-warning" ><?= $slide->button_title; ?></a>
        </div>
      </div>

    <?php } ?>
  <?php } ?>
  <!-- slide end -->

</div>
<!-- LayerSlider end -->
  
  <!--end of carousel -->