<div class="row">
  <div class="col-md-12">
    <div class="portlet box white">
        <div class="custom-slide-container relative">
        <button class="btn btn-warning" onclick="change_side_image('<?= $slider[0]->id; ?>')" style="right: 50px; background-color: rgb(92, 184, 92); border-color: rgb(92, 184, 92);">Change Image</button>
        <button class="btn btn-warning" onclick="crop_slide_img('<?= $slider[0]->original_url; ?>')" ><i class="fa fa-crop"></i></button>
          <!-- slide one start -->
        <div class="ls-slide ls-slide1" data-ls="offsetxin: right; slidedelay: 4000; transition2d: 24,25,27,28;">
          <div class="ls-sd-overlay" style="opacity:<?= $slider[0]->opacity; ?>;"></div>
          <img src="<?php echo $slider[0]->url; ?>" class="ls-bg" alt="Slide background" id="sd-slide-image-view" >
          <div id="sd-slide-title-view" class="ls-l ls-title"><?php echo $slider[0]->text_title; ?></div>
          <div id="sd-slide-sub-view" class="ls-l ls-mini-text"><?php echo $slider[0]->sub_title; ?>
          </div>
          <div class="ls-l ls-mini-text">
            
                <a href="javascript:void(0);" id="sd-slide-btn-text-view" class="btn-layerslide" <?= trim($slider[0]->button_title) == ""? 'style="display:none;"':'' ?> ><?php echo $slider[0]->button_title; ?></a>
            
        </div>
        </div>
        
      
        <!-- slide one end -->  
        </div>
    </div>
  </div>
</div>
<div class="row slide-controllers">
  <form id="form-save-create-slide" >
    <input type="hidden" id="sd-slide-opacity" name="sd-slide-opacity" value="<?= $slider[0]->opacity; ?>" >
    <input type="hidden" id="sd-slide-image" name="sd-slide-image" value="<?= $slider[0]->url; ?>" >
    <input type="hidden" id="sd-slide-id" name="sd-slide-id" value="<?= $slider[0]->id; ?>" >
    <div class="col-md-6 col-sm-6">
      <div class="form-group">
    <label>Main Title</label>
    <input class="form-control input-xlarge" placeholder="value text" type="text" id="sd-slide-title" name="sd-slide-title" value="<?= $slider[0]->text_title; ?>" >
    </div>
    <div class="form-group">
    <label>Subtitle</label>
    <input class="form-control input-xlarge" placeholder="value text" type="text" id="sd-slide-sub" name="sd-slide-sub" value="<?= $slider[0]->sub_title; ?>" >
    </div>
    </div>
     <div class="col-md-6 col-sm-6">
        <div class="form-group">
          <label>Button Text</label>
                <input class="form-control input-xlarge" placeholder="Button Text" type="text" id="sd-slide-btn-text" name="sd-slide-btn-text" value="<?= $slider[0]->button_title; ?>" >
        </div>
        <div class="form-group">
          <label>Button Url</label>
                <select class="form-control input-small" id="sd-slide-btn-url" name="sd-slide-btn-url" >
                  <?php $pages = $sd->page->get_pages(); ?>
                  <?php foreach ($pages as $index => $page) { ?>
                    <option value="<?= base_url().$page->page_url ?>" <?= base_url().$page->page_url==$slider[0]->button_url? 'selected':'' ?> ><?= $page->page_title ?></option>
                  <?php } ?>
          </select>
          <!-- <button class="btn btn-primary" type="button"><i class="fa fa-refresh" aria-hidden="true"></i></button> -->
        </div>
    </div>
    
     
    </div>  
    
  </form> 
</div>