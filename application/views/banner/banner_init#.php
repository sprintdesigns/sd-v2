<div id="slider-settings-response"></div>
<div class="portbox relative">
	<div class="slidelist">
	  <label>Slides</label>
	  <button class="add-item mini btn-add-slide" type="button" ><i class="fa fa-plus-circle"></i> Add Images</button> 
	  <?php if($slider){ ?>
	  		<?php $media_data = $sd->gallery->process_media_data( $sd->image->get_media() ); ?>
		  <?php foreach ($slider as $index => $slide) { ?>
		  		<div class="sd-banner-menu-item parent-slider-index-<?= $index; ?>">
		  		    <span class="slide-num"><?= $index + 1; ?></span>
		  			<?php if(sizeof($slider) > 1){ ?>
		  				<?php if(sizeof($media_data) > 0){ ?>
		  					<?php $use = false; ?>
		  					<?php foreach ($media_data as $index2 => $d) { ?>
		  						<?php if($sd->image->create_full_path($d['data']->upload_data->full_path,true,true) == $slide->original_url){ ?>
		  							<?php $use = true; ?>
		  							<button class="btn btn-danger" onclick="delete_slide('<?= $index2; ?>','<?= $slide->id; ?>','<?= $d["id"] ?>')" ><i class="fa fa-close"></i></button>
		  						<?php } ?>
		  					<?php } ?>
		  					<?php if(!$use){ ?>
		  						<button class="btn btn-danger" onclick="delete_slide('<?= $index; ?>','<?= $slide->id; ?>','false')" ><i class="fa fa-close"></i></button>
		  					<?php } ?>
		  				<?php } ?>
			  		<?php } ?>
			  		<a href="javascript:select_slide('<?= $index; ?>','<?= $slide->id; ?>')" class="slider-index-<?= $index; ?> sel-slider <?= $index==0? 'sd-slider-active':'' ?>" >
			  			<div class="slider-centered">
			  				<img src="<?php echo $slide->url ?>" style="width:100%;" />
			  			</div>
		  			</a>
	  			</div>
		  <?php } ?>

	  <?php } ?>
	 
      <ul class='custom-menu'>
		<li data-action="first"><i class="fa fa-plus"></i> Add New Slide</li>
	  </ul>
	</div>
	<div class="slideview">
		<div class="row">
		  <div class="col-md-12">
		  	<div class="portlet box white">
		  	  	<div class="custom-slide-container relative">
  	  				<button class="btn btn-warning" onclick="change_side_image('<?= $slider[0]->id; ?>')" style="right: 50px; background-color: rgb(92, 184, 92); border-color: rgb(92, 184, 92);">Change Image</button>
		  	  	  <button class="btn btn-warning" onclick="crop_slide_img('<?= $slider[0]->original_url; ?>')" ><i class="fa fa-crop"></i></button>
		  	  	  <!-- slide one start -->
		        <div class="ls-slide ls-slide1" data-ls="offsetxin: right; slidedelay: 4000; transition2d: 24,25,27,28;">
		          <div class="ls-sd-overlay" style="opacity:<?= $slider[0]->opacity; ?>;"></div>
		          <img src="<?php echo $slider[0]->url; ?>" class="ls-bg" alt="Slide background" id="sd-slide-image-view" >
		          <div id="sd-slide-title-view" class="ls-l ls-title"><?php echo $slider[0]->text_title; ?></div>
		          <div id="sd-slide-sub-view" class="ls-l ls-mini-text"><?php echo $slider[0]->sub_title; ?>
		          </div>
		          <div class="ls-l ls-mini-text">
		          <a href="javascript:void(0);" id="sd-slide-btn-text-view" class="btn-layerslide" <?= trim($slider[0]->button_title) == ""? 'style="display:none;"':'' ?> ><?php echo $slider[0]->button_title; ?></a>
	          	</div>
		        </div>
		        
		      
		        <!-- slide one end -->	
		  	  	</div>
		  	</div>
		  </div>
		</div>
		<div class="row slide-controllers">
		  <form id="form-save-create-slide" >
		  	<input type="hidden" id="sd-slide-opacity" name="sd-slide-opacity" value="<?= $slider[0]->opacity; ?>" >
		  	<input type="hidden" id="sd-slide-image" name="sd-slide-image" value="<?= $slider[0]->url; ?>" >
		  	<input type="hidden" id="sd-slide-id" name="sd-slide-id" value="<?= $slider[0]->id; ?>" >
	        <div class="col-md-6 col-sm-6">
	      	  <div class="form-group">
				<label>Main Title</label>
				<input class="form-control input-xlarge" placeholder="value text" type="text" id="sd-slide-title" name="sd-slide-title" value="<?= $slider[0]->text_title; ?>" >
			  </div>
			  <div class="form-group">
				<label>Subtitle</label>
				<input class="form-control input-xlarge" placeholder="value text" type="text" id="sd-slide-sub" name="sd-slide-sub" value="<?= $slider[0]->sub_title; ?>" >
			  </div>
	        </div>
	        <div class="col-md-6 col-sm-6">
	      	  	<div class="form-group">
				 	<label>Button Text</label>
		          	<input class="form-control input-xlarge" placeholder="Button Text" type="text" id="sd-slide-btn-text" name="sd-slide-btn-text" value="<?= $slider[0]->button_title; ?>" >
			  	</div>
			  	<div class="form-group">
					<label>Button Url</label>
		          	<select class="form-control input-small" id="sd-slide-btn-url" name="sd-slide-btn-url" >
		          		<?php $pages = $sd->page->get_pages(); ?>
		          		<?php foreach ($pages as $index => $page) { ?>
		          			<option value="<?= base_url().$page->page_url ?>" <?= base_url().$page->page_url==$slider[0]->button_url? 'selected':'' ?> ><?= $page->page_title ?></option>
		          		<?php } ?>
					</select>
					<!-- <button class="btn btn-primary" type="button"><i class="fa fa-refresh" aria-hidden="true"></i></button> -->
				</div>
	        </div>
	        
	         <!--  <div class="form-group disabled-function">
	            <label>Opacity</label>
	      	    <div class="opac-slider slider slider-basic bg-red ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false">
				  <a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 0%;"></a>
		        </div>
		      </div> -->
	        
	        
	      </form>	
		</div>
	</div>

	<div class="slider-gallery-show">
		<h1>Choose Image</h1>
		<button class="slider-gallery-close" ><i class="fa fa-close"></i></button>
		<div class="slider-gallery-preloader">
			<style type='text/css'>@-webkit-keyframes uil-default-anim { 0% { opacity: 1} 100% {opacity: 0} }@keyframes uil-default-anim { 0% { opacity: 1} 100% {opacity: 0} }.uil-default-css > div:nth-of-type(1){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.5s;animation-delay: -0.5s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(2){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.4166666666666667s;animation-delay: -0.4166666666666667s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(3){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.33333333333333337s;animation-delay: -0.33333333333333337s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(4){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.25s;animation-delay: -0.25s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(5){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.16666666666666669s;animation-delay: -0.16666666666666669s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(6){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.08333333333333331s;animation-delay: -0.08333333333333331s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(7){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0s;animation-delay: 0s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(8){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.08333333333333337s;animation-delay: 0.08333333333333337s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(9){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.16666666666666663s;animation-delay: 0.16666666666666663s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(10){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.25s;animation-delay: 0.25s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(11){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.33333333333333337s;animation-delay: 0.33333333333333337s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(12){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.41666666666666663s;animation-delay: 0.41666666666666663s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}</style><div class='uil-default-css' style='transform:scale(0.22);'><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'></div></div>
		</div>
	 	<div class="portlet-body">
	      <div class="row">
	        <div class="col-md-12">
	          <form action="<?php echo base_url();?>index.php/upload/banner_upload" class="dropzone" id="banner_upload" name="banner_upload" enctype="multipart/form-data" method="post">
	          	<input type="hidden" id="slide_id_new_image" name="slide_id_new_image" />
	          	<div class="dragover-image"><div class="dragover-title">Drop Image Here!</div></div>
	          </form>
	        </div>
	      </div>
	    </div>
		<div id="banner-gallery-container" >
		</div>
	</div>
</div>