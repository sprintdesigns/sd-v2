<?php if($sd->user->check_if_login()) { ?>
  <!--check if edit page -->  
  <?php if($sd->edit->mode()) { ?>
    <input type="hidden" value="<?= $tutorial? "true":"false"; ?>" id="check-tutorial" />
    <input type="hidden" value="false" id="check-tutorial-page-manager" />
  	<button class="sd-tutorials" data-target="first-load" data-toggle="tooltip" data-placement="top" title="Show Tutorials"><i class="fa fa-close hidden"></i><img src="<?php echo base_url(); ?>sd-assets/images/tutorials-icon.png"></button>
    <!-- this is for the modal for theme selection-->
    <div class="themeselectionmodal">
      <?php
       
        $theme = $sd->theme->get_all_themes();
        $theme_not_installed = $sd->theme->get_themes_by_folders();
        if (isset($theme_not_installed) && ! $theme) {
          $theme = $theme_not_installed;
        } else if(is_array($theme) && isset($theme_not_installed)) {
          $theme = $sd->theme->super_unique(array_merge($theme,$theme_not_installed));
        } 

        $args = array(
            'title' => 'Theme',
            'object' => $theme,
            'type' => 'slider',
            'display' => array(
              'banner' => 'banner', 
              'description' => 'description', 
              'name' => 'name', 
              'author' => '', 
            ),
           'button' => array( // optional
              'Activate' => array(
                  'type' => 'success',
                  'function' => 'activateTheme',
                ),
              'Live Preview' => array(
                'type' => 'default',
                'function' => 'previewTheme',
              ), 
            )

          );
        $sd->modal->dynamic($args);
      ?>
      <div class="installing-overlay">

        <div class="centerme">
           <div class="notificates">
               <h1>Theme is uploading... Please Wait.</h1>
               <p>This will take a while. You may take a cup of coffee while watching your upload progress.</p>
           </div>
           <div class="progress">
              <div class="themeuploadprogress progress-bar progress-bar-success" role="progressbar" aria-valuenow="0"
              aria-valuemin="0" aria-valuemax="100">
                <span class="numericx">0</span>%
              </div>
            </div>
        </div>

      </div>
    </div>  
    <!-- end for the modal for theme selection-->
  	<div class="overlay-tutorials">
  	  <div class="hole"></div>

  	  <div class="first-cover first">
  	  	<h1>Hey there! This is the walkthrough section!</h1>
  	  	<p>Just relax and let us guide you through the parts of SprintDesigns builder.</p>
  	  	<div class="button-centered">
  	  	  <button data-target="1" class="btn-guide start-guide">Guide me!</button>
  	  	  <button class="closetuts">Nah, I'm good!</button>
  	  	</div>
  	  </div>

      <div class="first-cover-2 page-management-cover">
        <img src="<?php echo base_url(); ?>sd-assets/images/redo-arrow2.png">
        <h1>Add Page</h1>
        <p>Add as pages as you need.Sort them later.</p>
        <div class="button-centered">
          <button data-target="8" class="btn-guide start-guide">Guide me!</button>
          <button class="closetuts">No, I'm good</button>
        </div>
      </div>

  	  <div class="walkthrough-1">
  	    <img src="<?php echo base_url(); ?>sd-assets/images/redo-arrow2.png">
  	  	<h5>Sprintdesigns Logo</h5>
  	  	<p>This little thing here will take you to the sprintdesignsph.com website.</p>
  	  	<button data-target="2" class="btn-guide">Next <i class="fa fa-angle-right"></i></button>
  	  </div>

  	  <div class="walkthrough-2">
  	    <img src="<?php echo base_url(); ?>sd-assets/images/redo-arrow2.png">
  	  	<h5>Your Website Name</h5>
  	  	<p>This will take you to your website’s homepage and allows you to check your website on live.</p>
  	  	<button data-target="1" class="btn-guide"><i class="fa fa-angle-left"></i> Previous</button>
  	  	<button data-target="3" class="btn-guide">Next <i class="fa fa-angle-right"></i></button>
  	  </div>
  
  	  <div class="walkthrough-3">
  	    <img src="<?php echo base_url(); ?>sd-assets/images/redo-arrow2.png">
  	  	<h5>Theme Color Changer</h5>
  	  	<p>With just a single click, you will be able to change the theme color of your website! How easy and convenient, right?</p>
  	  	<button data-target="2" class="btn-guide"><i class="fa fa-angle-left"></i> Previous</button>
  	  	<button data-target="4" class="btn-guide">Next <i class="fa fa-angle-right"></i></button>
  	  </div> 

  	  <div class="walkthrough-4">
  	    <img src="<?php echo base_url(); ?>sd-assets/images/redo-arrow2.png">
  	  	<h5>Menu Panels</h5>
  	  	<p>Drag your mouse to this side panel and be amazed with the useful menu items that you could use to customize your website.</p>
  	  	<button data-target="3" class="btn-guide"><i class="fa fa-angle-left"></i> Previous</button>
  	  	<button data-target="5" class="btn-guide">Next <i class="fa fa-angle-right"></i></button>
  	  </div>
      
      <div class="walkthrough-5">
  	    <img src="<?php echo base_url(); ?>sd-assets/images/redo-arrow2.png">
  	  	<h5>User Dropdown Button</h5>
  	  	<p>This will show you list of functions of your login sessions. You can also log out your session.</p>
  	  	<button data-target="4" class="btn-guide"><i class="fa fa-angle-left"></i> Previous</button>
  	  	<button data-target="6" class="btn-guide">Next <i class="fa fa-angle-right"></i></button>
  	  </div>

      <div class="walkthrough-6">
        <img src="<?php echo base_url(); ?>sd-assets/images/redo-arrow2.png">
        <h5>And finally, Save Website Button</h5>
        <p>This button will save and publish all the changes you made your website content.</p>
        <button data-target="5" class="btn-guide"><i class="fa fa-angle-left"></i> Previous</button>
        <button data-target="7" class="btn-guide">Next <i class="fa fa-angle-right"></i></button>
      </div>

  	  <div class="walkthrough-7">
  	    <img src="<?php echo base_url(); ?>sd-assets/images/redo-arrow2.png">
  	  	<h5>Confused and feeling lost?</h5>
  	  	<p>Just click this button to get back to this quick guide.</p>
  	  	<button data-target="6" class="btn-guide"><i class="fa fa-angle-left"></i> Previous</button>
  	  	<button class="closetuts end-guide">End Tutorials</button>
  	  </div>

      <div class="walkthrough-8">
        <img src="<?php echo base_url(); ?>sd-assets/images/redo-arrow2.png">
        <h5>Published Pages</h5>
        <p>These are your active pages. They are visible on your Navigation Menu.</p>
        <button data-target="9" class="btn-guide">Next <i class="fa fa-angle-right"></i></button>
      </div>

      <div class="walkthrough-9">
        <img src="<?php echo base_url(); ?>sd-assets/images/redo-arrow2.png">
        <h5>Sort Your Pages</h5>
        <p>Drag and drop your pages to sort. You can even drag a page rightward bellow another to create a "Mother > Child Pages".</p>
        <button data-target="8" class="btn-guide"><i class="fa fa-angle-left"></i> Previous</button>
        <button data-target="10" class="btn-guide">Next <i class="fa fa-angle-right"></i></button>
      </div>

      <div class="walkthrough-10">
        <img src="<?php echo base_url(); ?>sd-assets/images/redo-arrow2.png">
        <h5>Draft Pages</h5>
        <p>If you wish to "use" your pages later, drag them here and activate them once needed</p>
        <button data-target="9" class="btn-guide"><i class="fa fa-angle-left"></i> Previous</button>
        <button data-target="11" class="btn-guide">Next <i class="fa fa-angle-right"></i></button>
      </div>

      <div class="walkthrough-11">
        <img src="<?php echo base_url(); ?>sd-assets/images/redo-arrow2.png">
        <h5>View Page</h5>
        <p>Click page title to view and to edit its content.</p>
        <button data-target="10" class="btn-guide"><i class="fa fa-angle-left"></i> Previous</button>
        <button data-target="13" class="btn-guide">Next <i class="fa fa-angle-right"></i></button>
      </div>

      <div class="walkthrough-13">
        <img src="<?php echo base_url(); ?>sd-assets/images/redo-arrow2.png">
        <h5>Need Help?</h5>
        <p>Click here to view these guides again. Happy editing!</p>
        <button data-target="11" class="btn-guide"><i class="fa fa-angle-left"></i> Previous</button>
        <button class="closetuts end-guide">Ok, I'm good!</button>
      </div>

  	</div>

    <div class="se-pre-con">
      <div class="sd-note">
        This may take a while. Please wait.
      </div>
    </div>
    <div class="sd-pannel-out"></div>
    <div id="fullpageloader-response">
      <div class="slider-gallery-preloader show">
          <style type='text/css'>@-webkit-keyframes uil-default-anim { 0% { opacity: 1} 100% {opacity: 0} }@keyframes uil-default-anim { 0% { opacity: 1} 100% {opacity: 0} }.uil-default-css > div:nth-of-type(1){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.5s;animation-delay: -0.5s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(2){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.4166666666666667s;animation-delay: -0.4166666666666667s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(3){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.33333333333333337s;animation-delay: -0.33333333333333337s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(4){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.25s;animation-delay: -0.25s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(5){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.16666666666666669s;animation-delay: -0.16666666666666669s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(6){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.08333333333333331s;animation-delay: -0.08333333333333331s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(7){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0s;animation-delay: 0s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(8){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.08333333333333337s;animation-delay: 0.08333333333333337s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(9){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.16666666666666663s;animation-delay: 0.16666666666666663s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(10){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.25s;animation-delay: 0.25s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(11){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.33333333333333337s;animation-delay: 0.33333333333333337s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(12){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.41666666666666663s;animation-delay: 0.41666666666666663s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}</style><div class='uil-default-css' style='transform:scale(0.22);'><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'></div></div>
      </div>
    </div>
    <div class="sd-loadbar">
      <div class="boxcontainer">
        <div id="box-box" class="relative">  
          <div class="loadlogo"></div>
          <div class="lotext">Loading <div id="drives">Builder</div>: <div id="numprog">0</div>%</div>
          <div class="progress" id="progress_div">
            <input type="hidden" id="progress_width" value="0">
            <div class='bar' id='bar1'></div>
            <div class='percent' id='percent1'></div>
          </div>
        </div>
      </div>
    </div>
    <div class="sdlogin-bar">
      <ul class="maintools">
        <li class="sdicon"><a href="http://sprintdesignsph.com/"><img src="<?php echo base_url(); ?>sd-assets/images/Logo_On_Black.png" /></a></li>  
        <li class="version">Builder <sup><?php echo SD_VERSION;?></sup></li> 
        <li class="websitename hidden-xs"><a href="<?= base_url().$page_url; ?>"><?= $sd->site->get_name(); ?></a></li>
      </ul>
      <ul class="maintools colorpicker">
        <li><button data-color="red" class="colorpaint red">&nbsp;</button></li>
        <li><button data-color="orange" class="colorpaint orange">&nbsp;</button></li>
        <li><button data-color="yellow" class="colorpaint yellow">&nbsp;</button></li>
        <li><button data-color="green" class="colorpaint green">&nbsp;</button></li>
        <li><button data-color="blue" class="colorpaint blue">&nbsp;</button></li>
        <li><button data-color="indigo" class="colorpaint indigo">&nbsp;</button></li>
        <li><button data-color="violet" class="colorpaint violet">&nbsp;</button></li>
        <li><button data-color="default" class="colorpaint default">D</button></li>
      </ul>
      <?php if (SD_Users::isFreeTrial()):?>
        <?php $username = $sd->user->get_current_user()->username;?>
      <ul class="maintools" style="margin-left:200px">
        <li><a class="btn btn-info" target="_blank" href="http://sprintdesignsph.com/pricing_plans/?un=<?php echo $username;?>">Buy this website now!</a></li>
      </ul>
      <?php endif;?>
      <ul class="profile-stat navbar-right">
        <li><a class="btn btn-danger btn-save-website" href="javascript:savePage('<?= $page_url ?>');">Save Website<i class="fa fa-exclamation-circle blink sd-publish" style="display:none;"></i></a></li> 
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">Hello <span><?= (isset($sd->user->get_current_user()->username)) ? $sd->user->get_current_user()->username : 'superadmin';  ?></span> <img class="avatar-circle" src="<?php echo base_url(); ?>/sd-assets/admin/layout4/img/avatar9.jpg"></a>
          <ul class="dropdown-menu">
            <li class="boxed"><img src="<?php echo base_url(); ?>sd-assets/images/Favicon-SprintDesigns.png" /></li>
            <li style="text-align:center;"><?= (isset($sd->user->get_current_user()->username)) ? $sd->user->get_current_user()->username : 'superadmin';  ?></li>
            <!-- <li><a href="#">Edit my Profile</a></li> -->
            <span><li><li style="text-align: left;"><a href="http://localhost/sdv2/advanced/plugins"; ?>Settings</a></li></span>
            <span><li><li style="text-align: right;"><a href="<?php $sd->dashboard->logout(); ?>">Logout</a></li></span>
          </ul>
        </li>    
      </ul>
    </div>

    <div class="edit-menu-left">
      <div class="headbox">
      <!--<a class="btn btn-default btn-transparent" href="<?php echo base_url(); ?>index.php/dashboard"><i class="fa fa-home"></i> Go to Dashboard</a>-->
      <button class="close-edit-menu"><i class="fa fa-bars"></i></button>
      </div>

      <div class="container-x">
        <ul>
          <!--<li class="searchlist">
            <i class="icon-magnifier"></i>
            <input type="text" placeholder="Search"></input>
          </li>-->
          <li>
            <button class="truncate page-open has-layer" data-layer="2">
              <i class="fa fa-file icons"></i> <span class="menuli">Page Manager</span><!--<div class="anglebox"></div>-->
            </button>
          </li> 
          <li>
            <button class="truncate media-open has-layer" data-layer="3">
              <i class="fa fa-picture-o icons"></i> 
              <span class="menuli">Image Gallery</span><!--<div class="anglebox"></div>-->
            </button>
          </li> 
          <li>
            <button class="truncate media-open has-layer" data-layer="4">
              <i class="fa fa-object-group icons"></i> 
              <span class="menuli">Slider Settings</span><!--<div class="anglebox"></div>-->
            </button>
          </li> 
          <li>
            <button class="truncate media-open has-layer" data-layer="5">
              <i class="fa fa-puzzle-piece icons"></i> 
              <span class="menuli">Themes</span><!--<div class="anglebox"></div>-->
            </button>
          </li>  
          <!--<li>
            <button class="truncate media-open has-layer" data-layer="6">
              <i class="fa fa-adjust icons"></i> 
              <span class="menuli">Marketing Banner</span>
            </button>
          </li>-->
          <li>
             <a style="text-decoration: none;" href="<?= base_url().'olstore/index' ?>" class="truncate">
              <i class="fa fa-shopping-cart"></i><span class="menuli"> Online Store</span>
            </a>
          </li>                    
          <li>
            <a style="text-decoration: none;" href="<?= base_url().'advanced/changepassword' ?>" class="truncate">
              <i class="fa fa-puzzle-piece icons"></i><span class="menuli">Other Settings</span>
            </a>
          </li>
          <!--<li><button class="truncate"><i class="fa fa-life-ring icons"></i> <span class="menuli">Help</span></button></li> -->       
        </ul>
      </div>
      <div class="layer-2 all-layer">
        <div class="headerlayer">Pages
         <!-- <button data-toggle="modal" data-target="#modaldynamic" class="add-item mini"><i class="fa fa-plus-circle"></i> Add Page</button> -->
         <button class="add-item mini btn-add-page" type="button" ><i class="fa fa-plus-circle"></i> Add Page</button> 
         <button class="add-item mini page-management-tutorials sd-tutorials" data-target="page-management"><img src="<?php echo base_url(); ?>sd-assets/images/tutorials-icon.png"> Need Help?</button>
         <button class="layer-close" data-panel="2"><i class="fa fa-times"></i></button></div>
        <div class="layer-container-x">
          <!-- <button data-toggle="modal" data-target="#modaldynamic" class="add-item">Add Page</button> -->
          <div class="navsort">
            <div class="menu-list">
              <?php $theme_id = $sd->theme->get_active_theme()[0]->id; ?>
              <?php $sd->menu->menu_sort($theme_id); ?>
            </div>
          </div>
         <!--  <button data-toggle="modal" data-target="#modaldynamic" class="add-item mini">Add Page</button> -->
        </div>
      </div>
      <div class="layer-3 all-layer">
        <?php $sd->gallery->media_gallery($sd); ?>  
      </div>
      <div class="layer-4 all-layer">
        <div class="headerlayer">Slider Settings
         <!-- <button data-toggle="modal" data-target="#modaldynamic" class="add-item mini"><i class="fa fa-plus-circle"></i> Add Page</button> -->
         
         <button class="add-item mini btn-save-slide-changes" type="button" onclick="save_slide_changes()" ><i class="fa fa-save" ></i> Save<i class="fa fa-exclamation-circle blink"></i></button>
         <button class="layer-close" data-panel="4"><i class="fa fa-times"></i></button></div>
        <div class="layer-container-x">
          <!-- <button data-toggle="modal" data-target="#modaldynamic" class="add-item">Add Page</button> -->
              <?php $sd->banner->banner_init($sd); ?>
         <!--  <button data-toggle="modal" data-target="#modaldynamic" class="add-item mini">Add Page</button> -->
        </div>
      </div>

      <div class="layer-5 all-layer">
         <div class="headerlayer">Themes 
            <button class="btn btn-primary open-upload"><i class="fa fa-upload"></i> Upload Theme</button>
            <button onclick="location.href='<?php echo base_url();?>settings/choose_theme'" class="btn btn-primary">Choose More Themes</button>
            <button onclick="update_all_themes()" class="btn btn-primary">Update Themes</button>
            <button class="layer-close" data-panel="5"><i class="fa fa-times"></i></button>       
         </div>

         <div id="message-response">

         </div>
         <div class="layer-container-x upload-layer">
         
            <div class="upload-theme row hidden">
              <p class="install-help">If you have a sdtheme in a .zip format, upload it here and click install now.</p>
              <form id="upload-template-form" class="wp-upload-form sdcore-ux" enctype="multipart/form-data" method="post">
              <input type="file" name="template-file" id="template-file">
              <input type="submit" value="Install Now" class="button" id="install-theme-submit" name="install-theme-submit" disabled>
              </form>
            </div>
            <div class="row">
              <div class="col-md-4 col-sm-5 col-xs-7">
              </div>
            </div>
            <div class="site-wrapper left-aligned">
            <div class="row sandbox">
              <!--the thumb -->
              <div class="col-md-12 mtop20">
                <h4>Uploaded Themes</h4>
              </div>
              <?php $active_theme_id = 0; ?>
              <?php 
                $theme = $sd->theme->get_all_themes();
                $theme_folders = $sd->theme->get_themes_by_folders();
              ?>
              <?php if($theme){ ?> 
                <?php if($sd->theme->get_active_theme()) { ?> 
                <?php $active_theme = $this->sd->theme->get_active_theme(); 
                      $active_theme_id = $active_theme[0]->id;
                      $active_theme_name = $active_theme[0]->name;
                ?>  
                <div class="col-md-3 col-sm-3 col-xs-6 template-thumb">
                  <a class="valtemp modal-dynamic" data-to-pass="<?php echo $active_theme[0]->id ?>">
                    <span class="tag">Active Theme</span>
                    <span class="image-wrapper" style="background-image: url('<?= base_url() ?>sd-includes/themes/<?= str_replace(' ', '-', $active_theme[0]->name)."/".$active_theme[0]->banner ?>');"></span>
                    <span class="template-name"><?php echo $active_theme[0]->name; ?></span>
                  </a>
                </div>
                <?php } ?>
                <!-- show themes not active and not yet installed -->
                <?php
                  $theme = $sd->theme->super_unique(array_merge($theme,$theme_not_installed));
                ?>
                <?php foreach ($theme as $t) { ?>
                    <?php if ($t->name != $active_theme_name):?>
                      <div class="col-md-3 col-sm-3 col-xs-6 template-thumb">
                        <a class="valtemp modal-dynamic" href="#" data-toggle="modal" data-target="#modaldynamic" data-to-pass="<?= property_exists($t,'id')? $t->id:$t->name ?>">
                          <span class="tag">Theme Details</span>
                          <span class="image-wrapper" style="background-image: url('<?= base_url() ?>sd-includes/themes/<?= str_replace(' ', '-', $t->name)."/".$t->banner ?>');"></span>
                          <span class="template-name"><?= $t->name ?></span>
                        </a>
                      </div>
                  <?php endif;} ?>
                <?php } ?>
              </div>
              <!--end of thumb-->
              </div>
            </div>
          </div>
    <div class="layer-6 all-layer">
      <?php $sd->marketing->marketing_layout($sd); ?>
    </div>

    <div class="crop-section"> 
      <!-- <button class="exitCrop">Close</button>
      <h2 class="container">Crop Image</h2> -->
      <div class="cropFunctions">
          <div id="function_photo_gallery" class="function-class-crop" >
            <form id="form-crop-image" data-action="<?= base_url() ?>upload/crop" >
              <input type="hidden" value="" id="crop_x" name="x" />
              <input type="hidden" value="" id="crop_y" name="y" />
              <input type="hidden" value="" id="crop_w" name="w" />
              <input type="hidden" value="" id="crop_h" name="h" />
              <input type="hidden" value="" id="src_img" name="src_img" />
            <button type="submit" class="saveCrop"><i class="fa fa-check-circle"></i> Crop</button>
           </form>
          </div>
          <div id="function_slider_settings" class="function-class-crop" >
            <form id="form-crop-slide" data-action="<?= base_url() ?>upload/crop" >
              <input type="hidden" value="" id="crop_x_slide" name="x" />
              <input type="hidden" value="" id="crop_y_slide" name="y" />
              <input type="hidden" value="" id="crop_w_slide" name="w" />
              <input type="hidden" value="" id="crop_h_slide" name="h" />
              <input type="hidden" value="" id="src_img_slide" name="src_img" />
            <button type="submit" class="saveCrop"><i class="fa fa-check-circle"></i> Crop</button>
           </form>
          </div>
        <button class="cancelCrop"><i class="fa fa-window-close"></i> Cancel</button>
      </div>
      <div class="container" style="position: relative; top: 73px;" >
        <div class="load-for-crop" style="display:none;" >
          <style type='text/css'>@-webkit-keyframes uil-default-anim { 0% { opacity: 1} 100% {opacity: 0} }@keyframes uil-default-anim { 0% { opacity: 1} 100% {opacity: 0} }.uil-default-css > div:nth-of-type(1){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.5s;animation-delay: -0.5s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(2){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.4166666666666667s;animation-delay: -0.4166666666666667s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(3){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.33333333333333337s;animation-delay: -0.33333333333333337s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(4){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.25s;animation-delay: -0.25s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(5){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.16666666666666669s;animation-delay: -0.16666666666666669s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(6){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.08333333333333331s;animation-delay: -0.08333333333333331s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(7){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0s;animation-delay: 0s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(8){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.08333333333333337s;animation-delay: 0.08333333333333337s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(9){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.16666666666666663s;animation-delay: 0.16666666666666663s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(10){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.25s;animation-delay: 0.25s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(11){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.33333333333333337s;animation-delay: 0.33333333333333337s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(12){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.41666666666666663s;animation-delay: 0.41666666666666663s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}</style><div class='uil-default-css' style='transform:scale(0.22);'><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'></div></div>
        </div>
        <div class="row">
          <img id="sd-image-cropper" src="">
        </div>
      </div>
      
    </div>

    <div class="crop-section-core">
        <div class="cropFunctions-core">
            <div id="function_photo_gallery-core" class="function-class-crop" >
                <input type="hidden" value="" class="crop-it-img-src" name="crop-it-img-src" />
                <button type="submit" class="saveCrop-core export"><i class="fa fa-check-circle"></i> Crop</button>
            </div>
            <button class="cancelCrop-core"><i class="fa fa-window-close"></i> Cancel</button>
        </div>
        <div class="container" style="position: relative; top: 73px;" >
            <div class="load-for-crop" style="display:none;" >
                <style type='text/css'>@-webkit-keyframes uil-default-anim { 0% { opacity: 1} 100% {opacity: 0} }@keyframes uil-default-anim { 0% { opacity: 1} 100% {opacity: 0} }.uil-default-css > div:nth-of-type(1){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.5s;animation-delay: -0.5s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(2){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.4166666666666667s;animation-delay: -0.4166666666666667s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(3){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.33333333333333337s;animation-delay: -0.33333333333333337s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(4){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.25s;animation-delay: -0.25s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(5){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.16666666666666669s;animation-delay: -0.16666666666666669s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(6){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.08333333333333331s;animation-delay: -0.08333333333333331s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(7){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0s;animation-delay: 0s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(8){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.08333333333333337s;animation-delay: 0.08333333333333337s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(9){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.16666666666666663s;animation-delay: 0.16666666666666663s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(10){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.25s;animation-delay: 0.25s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(11){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.33333333333333337s;animation-delay: 0.33333333333333337s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(12){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.41666666666666663s;animation-delay: 0.41666666666666663s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}</style><div class='uil-default-css' style='transform:scale(0.22);'><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'></div></div>
                <style type="text/css">.cropit-preview{background-color:#f8f8f8;background-size:cover;border:5px solid #ccc;border-radius:3px;margin-top:7px;width:250px;height:250px}.cropit-preview-image-container{cursor:move}.cropit-preview-background{opacity:.2;cursor:auto}</style>
            </div>
            <div class="row text-center">
                <div class="sd-crop-it-image-editor" style="display: inline-block;">
                    <div class="image-size-label">
                        Resize image
                    </div>
                    <input type="range" class="cropit-image-zoom-input">
                    <div class="cropit-preview"></div>
                </div>
            </div>
        </div>

    </div>

    <!-- small modal -->
    <div id="modaldynamic" class="modal small">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Delete Image</h4>
          </div>
          <div class="modal-body">
            
              <p id="if-use-in-banner"></p>  
              <p>Are you sure to delete this image?</p>
              <label class="checkbox-inline">
                <div class="checker" id="uniform-inlineCheckbox21">
                  <span><input id="direct-delete-check" value="" type="checkbox"></span>
                </div> Don't ask me again. 
              </label>
              
           
          </div>
          <div class="modal-footer">
            <button placeholder="Meta Data" type="submit" class="btn btn-primary btn-gold" onclick="delete_image_action()" >Delete</button>
            <button placeholder="Meta Data" type="submit" class="btn btn-primary btn-gold" data-dismiss="modal" >cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>


    <div id="modal-leave" class="sd-modal modal small">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Delete Image</h4>
          </div>
          <div class="modal-body">
            
              <p id="message-for-leave"></p>  
           
          </div>
          <div class="modal-footer">
            <button placeholder="Meta Data" type="submit" class="btn btn-primary btn-gold" onclick="delete_image_action()" >Leave without saving changes?</button>
            <button placeholder="Meta Data" type="submit" class="btn btn-primary btn-gold" data-dismiss="modal" >don't leave</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>

  <!--end of small modal-->
  <?php } else { ?>
    <div class="sdlogin-bar">
      <ul class="maintools">
       <li class="sdicon"><a href="http://sprintdesignsph.com/"><img src="<?php echo base_url(); ?>sd-assets/images/Logo_On_Black.png" /></a></li>
        <li class="websitename"><?= $sd->site->get_name(); ?></li>
        <li><a class="btn btn-success" href="<?php echo base_url() . 'edit?page=' . $page_url; ?>">Edit this Page</a></li> 
      </ul>
      <ul class="profile-stat">
        <li class="dropdown hidden-xs front-dbar">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">Hello <span><?= (isset($sd->user->get_current_user()->username)) ? $sd->user->get_current_user()->username : 'superadmin';  ?></span> <img class="avatar-circle" src="<?php echo base_url(); ?>/sd-assets/admin/layout4/img/avatar9.jpg"></a>
          <ul class="dropdown-menu">
            <li class="boxed"><img src="<?php echo base_url(); ?>sd-assets/images/Favicon-SprintDesigns.png" /></li>
            <li style="text-align:center;"><?= (isset($sd->user->get_current_user()->username)) ? $sd->user->get_current_user()->username : 'superadmin';  ?></li>
            <!-- li><a href="#">Edit my Profile</a></li> -->
             <span><li><li style="text-align: left;"><a href="http://localhost/sdv2/advanced/plugins"; ?>Settings</a></li></span>
            <span><li><li style="text-align: right;"><a href="<?php $sd->dashboard->logout(); ?>">Logout</a></li></span>
          </ul>
        </li>   
      </ul>
    </div> 
  <?php } ?>
<?php } ?>


<!-- BEGIN_UPDATE_THEMES -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript"> 
function update_all_themes() {
  $.ajax({
    url: "<?php echo base_url(); ?>" + "settings/update_themes"
  });
}
</script>
<!-- END_UPDATE_THEMES -->