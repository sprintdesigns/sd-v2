<nav class="sd-nav">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="pitched">
         <!--  <ul class="sd-nav-pages">
            <?php if($pages){ ?>
              <?php foreach ($pages as $index => $p) { ?>
                <li><a class="sd-nav-page-item" href="<?= $p->page_url=='home'? '':$p->page_url ?>"><?= $p->page_title ?></a></li>    
              <?php } ?>
            <?php } ?>
          </ul> -->  
          <nav class="navbar navbar-inverse" role="navigation">
            <div class="container-fluid">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <!--<a class="navbar-brand" href="#">Brand</a>-->
              </div>

              <!-- Collect the nav links, forms, and other content for toggling -->
              <div id="bs-example-navbar-no-collapse" class="collapse navbar-collapse hidden-xs">
                <ul class="nav navbar-nav">
                  <?php $theme_id = $sd->theme->get_active_theme()[0]->id; ?>
                  <?php $menu = $sd->menu->get_menu($theme_id); ?>
                  <?php if(!$menu){ ?>
                    <?php $page_data = $sd->page->select_page_by_id($i->id); ?>
                      <?php if($page_data[0]->status){ ?>
                       <?php if($sd->edit->mode()) { ?>
                       <li class="active"><a href="<?= base_url() ?>edit?page=home">home</a></li>
                       <?php } else { ?>
                       <li class="active"><a href="<?= base_url() ?>">home</a></li>
                       <?php } ?>
                      <?php } ?>
                  <?php }else { ?>
                      
                      <?php $items = json_decode($menu[0]->orders); ?>
                      <?php foreach ($items as $index => $i) { ?>
                          
                          <?php if(array_key_exists('children', $i)){ ?>
                            <?php $page_data = $sd->page->select_page_by_id($i->id); ?>
                            <?php if($page_data[0]->status){ ?>
                              <li class="dropdown">
                                <?php if($sd->edit->mode()) { ?>
                                <a href="<?= base_url().'edit?page='.$page_data[0]->page_url ?>"><?= $page_data[0]->page_title ?></a>  
                                <?php } else { ?>   
                                <a href="<?= base_url().$page_data[0]->page_url ?>"><?= $page_data[0]->page_title ?></a>
                                <?php } ?>
                                <ul class="dropdown-menu">
                                  <?php foreach ($i->children as $index => $i2) { ?>
                                      <?php $page_data = $sd->page->select_page_by_id($i2->id); ?>
                                      <?php if($page_data[0]->status){ ?>
                                        <?php if($sd->edit->mode()) { ?>
                                        <li><a href="<?= base_url().'edit?page='.$page_data[0]->page_url ?>"><?= $page_data[0]->page_title ?></a></li>
                                        <?php } else { ?>  
                                        <li><a href="<?= base_url().$page_data[0]->page_url ?>"><?= $page_data[0]->page_title ?></a></li>
                                        <?php } ?>
                                      <?php } ?>
                                   <?php } ?>
                                </ul>
                              </li>
                            <?php } ?>
                          <?php }else { ?>
                            <?php $page_data = $sd->page->select_page_by_id($i->id); ?>
                               <?php if($page_data[0]->status){ ?>
                                <?php if($sd->edit->mode()) { ?>
                                <li><a href="<?= base_url().'edit?page='.$page_data[0]->page_url ?>"><?= $page_data[0]->page_title ?></a></li>
                                <?php } else { ?>  
                                <li><a href="<?= base_url().$page_data[0]->page_url ?>"><?= $page_data[0]->page_title ?></a></li>
                                <?php } ?>
                              <?php } ?>
                            <?php } ?>
                       
                      <?php } ?>
                  <?php } ?>
                </ul>
              </div>
              <!-- this is for mobile-->
              <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse hidden-not-xs">
                <ul class="nav navbar-nav">
                 
                  <?php $theme_id = $sd->theme->get_active_theme()[0]->id; ?>
                  <?php $menu = $sd->menu->get_menu($theme_id); ?>
                  <?php if(!$menu){ ?>
                     <li class="active"><a href="<?= base_url() ?>">home</a></li>
                  <?php }else { ?>
                      
                      <?php $items = json_decode($menu[0]->orders); ?>
                      <?php foreach ($items as $index => $i) { ?>
                          
                          <?php if(array_key_exists('children', $i)){ ?>
                            <li class="dropdown">
                              <?php $page_data = $sd->page->select_page_by_id($i->id); ?>
                              <a href="<?= base_url().$page_data[0]->page_url ?>"><?= $page_data[0]->page_title ?></a>
                              <button class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-angle-down"></i></button>
                              <ul class="dropdown-menu">
                                <?php foreach ($i->children as $index => $i2) { ?>
                                    <?php $page_data = $sd->page->select_page_by_id($i2->id); ?>
                                    <li><a href="<?= base_url().$page_data[0]->page_url ?>"><?= $page_data[0]->page_title ?></a></li>
                                 <?php } ?>
                              </ul>
                            </li>
                          <?php }else { ?>
                            <?php $page_data = $sd->page->select_page_by_id($i->id); ?>
                            <li><a href="<?= base_url().$page_data[0]->page_url ?>"><?= $page_data[0]->page_title ?></a></li>
                          <?php } ?>
                       
                      <?php } ?>
                  <?php } ?>
                </ul>
              </div>
              <!--this is for mobile end-->
              <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
          </nav>  

        </div>       
      </div>    
    </div>
  </div>    
</nav>