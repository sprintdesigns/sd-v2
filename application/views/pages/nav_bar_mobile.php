<?php $theme_id = $sd->theme->get_active_theme()[0]->id; ?>
  <?php $menu = $sd->menu->get_menu($theme_id); ?>
  <?php if(!$menu){ ?>
     <li class="active"><a href="<?= base_url() ?>">home</a></li>
  <?php }else { ?>
      
      <?php $items = json_decode($menu[0]->orders); ?>
      <?php foreach ($items as $index => $i) { ?>
          
          <?php if(array_key_exists('children', $i)){ ?>
            <li class="dropdown">
              <?php $page_data = $sd->page->select_page_by_id($i->id); ?>
              <a href="<?= base_url().$page_data[0]->page_url ?>"><?= $page_data[0]->page_title ?></a>
              <button class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-angle-down"></i></button>
              <ul class="dropdown-menu">
                <?php foreach ($i->children as $index => $i2) { ?>
                    <?php $page_data = $sd->page->select_page_by_id($i2->id); ?>
                    <li><a href="<?= base_url().$page_data[0]->page_url ?>"><?= $page_data[0]->page_title ?></a></li>
                 <?php } ?>
              </ul>
            </li>
          <?php }else { ?>
            <?php $page_data = $sd->page->select_page_by_id($i->id); ?>
            <li><a href="<?= base_url().$page_data[0]->page_url ?>"><?= $page_data[0]->page_title ?></a></li>
          <?php } ?>
       
      <?php } ?>
  <?php } ?>