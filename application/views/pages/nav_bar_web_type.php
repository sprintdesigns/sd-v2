<?php $theme_id = $sd->theme->get_active_theme()[0]->id; ?>
<?php $menu = $sd->menu->get_menu($theme_id); ?>
<?php if(!$menu){ ?>
  <?php $page_data = $sd->page->select_page_by_id($i->id); ?>
    <?php if($page_data[0]->status){ ?>
     <?php if($sd->edit->mode()) { ?>
     <li class="active"><a href="<?= base_url() ?>edit?page=home">home</a></li>
     <?php } else { ?>
     <li class="active"><a href="<?= base_url() ?>">home</a></li>
     <?php } ?>
    <?php } ?>
<?php }else { ?>
    
    <?php $items = json_decode($menu[0]->orders); ?>
    <?php foreach ($items as $index => $i) { ?>
        
        <?php if(array_key_exists('children', $i)){ ?>
          <?php $page_data = $sd->page->select_page_by_id($i->id); ?>
          <?php if($page_data[0]->status){ ?>
            <li class="dropdown">
              <?php if($sd->edit->mode()) { ?>
              <a href="<?= base_url().'edit?page='.$page_data[0]->page_url ?>"><?= $page_data[0]->page_title ?></a>  
              <?php } else { ?>   
              <a href="<?= base_url().$page_data[0]->page_url ?>"><?= $page_data[0]->page_title ?></a>
              <?php } ?>
              <ul class="dropdown-menu">
                <?php foreach ($i->children as $index => $i2) { ?>
                    <?php $page_data = $sd->page->select_page_by_id($i2->id); ?>
                    <?php if($page_data[0]->status){ ?>
                      <?php if($sd->edit->mode()) { ?>
                      <li><a href="<?= base_url().'edit?page='.$page_data[0]->page_url ?>"><?= $page_data[0]->page_title ?></a></li>
                      <?php } else { ?>  
                      <li><a href="<?= base_url().$page_data[0]->page_url ?>"><?= $page_data[0]->page_title ?></a></li>
                      <?php } ?>
                    <?php } ?>
                 <?php } ?>
              </ul>
            </li>
          <?php } ?>
        <?php }else { ?>
          <?php $page_data = $sd->page->select_page_by_id($i->id); ?>
             <?php if($page_data[0]->status){ ?>
              <?php if($sd->edit->mode()) { ?>
              <li><a href="<?= base_url().'edit?page='.$page_data[0]->page_url ?>"><?= $page_data[0]->page_title ?></a></li>
              <?php } else { ?>  
              <li><a href="<?= base_url().$page_data[0]->page_url ?>"><?= $page_data[0]->page_title ?></a></li>
              <?php } ?>
            <?php } ?>
          <?php } ?>
     
    <?php } ?>
<?php } ?>