<div class="modal" id="modaldynamic" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="sdremplate">
      <div class="modal-header">
        <!-- for buttons-->
        <?php if($type == "slider"){ ?>
          <div class="slidebuttons">
            <a class="left carousel-control" href="#theme-slides" role="button" data-slide="prev">
              <span class="fa fa-angle-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#theme-slides" role="button" data-slide="next">
              <span class="fa fa-angle-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        <?php } ?>
        <!--end for buttons-->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel"><?= $title ?></h4>
      </div>
      <div class="modal-body">
        <div id="theme-slides" class="carousel slide" data-ride="carousel" data-interval="false">
          <div class="carousel-inner" role="listbox">
          <?php if($sd->theme->get_active_theme()) { 
            $active_theme = $this->sd->theme->get_active_theme(); 
          } else {
            $active_theme = false;
          } ?>  
          <?php if($object){ ?>
            <?php foreach ($object as $index => $o) { ?>
              <?php if (property_exists((object) $o, 'id')) {
                  $id = $o->id;
                } else {
                  $id = $o->name;
                } ?>
              <div class="<?= $active_theme? $id==$active_theme[0]->id? 'activated-theme':'':'' ?> item slider-dynamic dynamic-item <?= $index==0? 'active dynamic-active':'' ?> dynamic-item-<?= $id ?>" data-object-id="<?= $id ?>" data-object-name="<?= $o->name ?>">
                <div class="theme-screenshots">
                  <div class="screenshot">
                    <?php if(array_key_exists('banner',$display)){ ?>
                      <img src="<?= base_url() ?>sd-includes/themes/<?= str_replace(' ', '-', $o->name)."/".$o->$display['banner'] ?>">
                    <?php } ?>
                  </div>
                </div>
                <div class="theme-info">
                  <?=  $active_theme? $id==$active_theme[0]->id? '<button class="btn btn-success btn-sm" style="border: none; pointer-events: none; font-size: 9px; background:#32373c">Activated theme</button>':'':'' ?>
                  <h2 class="theme-name">
                    <?php if(array_key_exists('name',$display)){ ?>
                      <?= $o->$display['name'] ?>
                    <?php } ?>
                    
                    <span class="theme-version">
                      Version: 1.2
                    </span>
                  </h2>
                  <p class="theme-author">By <a class="btn-link" href=""><?= $o->author ?></a></p>
                  <p class="theme-description">
                   <?php if(array_key_exists('description',$display)){ ?>
                      <?= $o->$display['description'] ?>
                    <?php } ?>
                  </p>
                  <p class="theme-tags"><span>Tags:</span><!--  one-column, two-columns, right-sidebar, accessibility-ready, custom-background, custom-colors, custom-header, custom-menu, editor-style, featured-images, flexible-header, microformats, post-formats, rtl-language-support, sticky-post, threaded-comments, translation-ready, blog --></p>
                </div>
              </div>
            <?php } ?>
          <?php } ?>
          </div>
        </div>
      </div>
      <div class="modal-footer">
      <?php if($button){ ?>
        <?php foreach ($button as $index => $b) { ?>
            <a href="javascript:<?= array_key_exists('function', $b)? $b['function']:'' ?>(<?= array_key_exists('parameter', $b)? $b['parameter']:'' ?>);" type="button" class="btn btn-<?= array_key_exists('type', $b)? $b['type']:'default' ?>"><?= $index ?></a>
        <?php } ?>
      <?php } ?>
       <!--  <a href="successful.html" type="button" class="btn btn-success">Activate</a>
        <a href="#" type="button" class="btn btn-primary">Live Preview</a> -->
      </div>
      </div>
    </div>
  </div>
</div>

<script>
function previewTheme() {
  var theme_name = $(".dynamic-active").attr("data-object-name");
  window.open(
    "http://sprintdesignsph.com/store-theme/" + theme_name + "/index.html",
    "_blank" // <-- OPEN IN NEW TAB.
  );
}
</script>