	<style type="text/css">
		#product_carousel {
			position: relative;
			width: 100%;
		}
		.prodimg {
			max-height: 400px;
		}
		.carousel-inner>.item>a>img, .carousel-inner>.item>img, .img-responsive, .thumbnail a>img, .thumbnail>img {
		    margin: 0 auto;
		}
		.preview {
			height: 400px;
			overflow-x: hidden;
			padding-left: 0;
		}
		
	</style>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			$(".img-thumbnail").hover(function() {
				$('html,body').css('cursor','pointer');	
			}, function() {
				$('html,body').css('cursor','default');	
			});
			$(".img-thumbnail").click(function() {
				var lists = $(".carousel-shortcut");
				var imgs = $(".prodimg");
				var id = $(this).attr('id');
				//initate search for correct carousel img
				for(var i = 0; i < <?php echo $product_photos_count ?>; i++) {
					if($(imgs[i]).attr('src') == $(this).attr('src')) {
						for(var j = 0; j < <?php echo $product_photos_count ?>; j++ ) {
							if($(lists[i]).attr('data-slide-to') == i) {
								$(lists[i]).click();
							}
						}
					}
				}
			});
		});

	</script>
	<div id="content-descript">
		<div class="wrap-descript">

		  	<?php if($flashdata) { ?>
		  			<?php if($flashdata['status'] == true) { ?>
							<div class="alert alert-success alert-dismissable">
							  <div style="text-align: right !important; float: right;"></div>
							  <?php echo $flashdata['message']; ?>
							</div>								
					<?php } else { ?>
							<div class="alert alert-danger alert-dismissable">
							  <div style="text-align: right !important; float: right;"></div>
							  <?php echo $flashdata['message']; ?>
							</div>
					<?php } ?>
			<?php } ?>			

			<?php if($product_details) { ?>
			<div class="row wrap-tablet">
				<div class="col-lg-7 col-md-6 col-sm-7 col-xs-12">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-4 preview" id="previewdiv">
							<?php foreach($product_photos as $p) { $ctr = 0; ?>
								<img src="<?php echo base_url(); ?>sd-contents/onlinestore/images/<?php echo $p->image; ?>" class="img-thumbnail" id="<?php echo $ctr; ?>">
							<?php $ctr++; } ?>						   
						</div>				
						<div class="col-lg-9 col-md-9 col-sm-8 main-img">
							<div id="product_carousel" class="carousel slide" data-ride="carousel" data-pause="hover" data-interval="5000">
								<ol class="carousel-indicators">
									<?php for($i = 0; $i < $product_photos_count; $i++) { ?>
										<?php if($i === 0) {?>
									    	<li data-target="#product_carousel" data-slide-to="<?php echo $i; ?>" class="active carousel-shortcut" ></li>
									    <?php } else { ?>
									    	<li data-target="#product_carousel" data-slide-to="<?php echo $i; ?>" class="carousel-shortcut"></li>
									    <?php } ?>				    
									<?php } ?>
							    </ol>
							    <div class="carousel-inner">
									<?php foreach($product_photos as $p) { ?>
											<?php if($p->is_default == "Yes") { ?>
												<div class="item active">
													<img class="prodimg" src="<?php echo base_url(); ?>sd-contents/onlinestore/images/<?php echo $p->image; ?>" >
												</div>
											<?php } else { ?>
												<div class="item">
										        	<img class="prodimg" src="<?php echo base_url(); ?>sd-contents/onlinestore/images/<?php echo $p->image; ?>" >
										      	</div>
										    <?php } ?>
									<?php } ?>
								</div>
								<a class="left carousel-control" href="#product_carousel" data-slide="prev">
								    <span class="glyphicon glyphicon-chevron-left"></span>
								    <span class="sr-only">Previous</span>
							    </a>
							    <a class="right carousel-control" href="#product_carousel" data-slide="next">
							      	<span class="glyphicon glyphicon-chevron-right"></span>
							      	<span class="sr-only">Next</span>
							    </a>
							</div>
						</div>
					</div>
				</div>

				<form id="form_purchase" class="" method="post" action="<?php echo base_url(); ?>store/purchased_product">
					<input type="hidden" id="product_id" name="product_id" value="<?php echo $product_details->id; ?>">
					<div class="col-lg-5 col-md-6 col-sm-5 col-xs-12">
						<h1><?php echo $product_details->name; ?></h1>
						<p class="price">Php <?php echo number_format($product_details->price,2); ?></p>
						<!-- <p class="beforeprice-p">Before: 660.00</p> -->
						<!-- <p class="saveprice-p">You save 12 %</p> -->
						<p class="prod-descript">
							<?php echo $product_details->description; ?>
						</p>

						<p class="stocknum"><?php echo $product_details->quantity ?> in stock</p>
						<div class="centering">
							<p class="quantity-p">Quantity</p>
							<input type="text" id="quantity" class="validate[required] quantity-box" name="quantity" value="">
						</div>
						<div class="centering">
							<p class="size-p">Size</p>
							<select disabled class="size-box">
								<option value="<?php echo $product_details->size; ?>"><?php echo $product_details->size; ?></option>
							</select> 
						</div>
						<div class="centering">
							<p class="color-p">Color: &nbsp; </p>
							<!-- <input type="color" class="color-box"> -->
							<?php echo $product_details->color; ?>
						</div>
						<div class="centering">
							<p class="email-p">Email</p>
							<input type="text" id="email" class="validate[required] email-box" name="email" >
						</div>
						<div class="centering">
							<p class="name-p">Name</p>
							<input type="text" id="cust_name" class="validate[required] name-box" name="cust_name" >
						</div>
						<div class="centering">
							<p class="address-p">Address</p>
							<input type="text" id="cust_address" class="validate[required] address-box" name="cust_address" >
						</div>
						<div class="centering">
							<p class="contact-p">Contact Number</p>
							<input type="text" id="cust_number" class="validate[required] contact-box" name="cust_number" >
						</div>
						<div class="centering">
							<p class="mode-p">Mode of Payment</p>
							<select id="mode_payment" name="mode_payment" class="mode-box">
								<option value="Cash">Cash</option>
								<option value="money_or_bank_deposit" >Money Transfer/Bank Deposit</option>
							</select>
						</div>
						<!--<h2>Category: &nbsp;<span><?php echo $product_details->categoryid; ?></span></h2>-->
						<div class="row div-tags">
							<!-- <div class="col-md-6 col-xs-12 col-sm-12">
								<h3 class="tags">Tags:  &nbsp;<span>Samsung, Galaxy Note, Galaxy</span></h3>
							</div>
							<div class="col-md-6 col-xs-12 col-sm-12">
								 <input type="button" value="PURCHASE" class="btncart">
							</div> -->
						</div>
						<br /><br /><br />
						<div class="col-md-6 col-xs-12 col-sm-12">
							<input type="submit" name="purchase_button" value="PURCHASE" class="btncart">
						</div>
					</div>
				</form>
			</div>
			<?php } else{ ?>
					<div class="row wrap-tablet">
						<h3>NO RECORDS FOUND</h3>
					</div>
			<?php } ?>
		</div>
	</div>