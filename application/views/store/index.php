
<div id="content-categories">
	<div class="container">
		<h2>CATEGORIES</h2>
		<?php $i = 0; ?>
		<div class="row">
		<?php if ($categories != false):?>
		<?php foreach($categories as $ca_key => $ca) { ?>
			<?php
				if($i%5==0) { 
					echo '</div><div class="row margin20">';
				}
			?>
			<div class="col-md-15 col-sm-15 col-lg-15">
				<a href="<?php echo base_url('store/category_items/'.$ca->id); ?>">
					<div class="wrap">
					<div class="img-container red">
						<?php if($ca->category_image != '' || $ca->category_image != null) {?>
					  		<img src="<?php echo base_url(); ?>/sd-contents/onlinestore/images/<?php echo $ca->category_image; ?>" alt="Avatar" class="image_category" style="width:100%">
					  	<?php }else{ ?>
					  		<img src="<?php echo base_url(); ?>/sd-contents/onlinestore/images/gadgets.jpg" alt="Avatar" class="image_category" style="width:100%;">
					  	<?php } ?>
					  	<div class="middle">
					    <div class="text"><?php echo $ca->category_name; ?></div>
					  </div>
					</div></div>
				</a>
			</div>
		<?php $i++; } ?>
		<?php endif;?>
		</div>
	</div>
</div>