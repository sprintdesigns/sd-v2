	<div id="content-refine">
		<div class="container">
			<div class="row">
			<div class="col-md-3">
				<!-- Category -->
				<div class="category">
					<div class="title-bg">
						<h3 class="category-title"><button type="button" class="btnrefine"><i class="fa fa-bars" aria-hidden="true"></i></button><span>Categories</span></h3>
					</div>
						<ul class="list-unstyled">
							<?php if ($categories != false):?>
							<?php foreach($categories as $ca_key => $ca) { ?>
									<?php if(isset($category->id)) { ?>
											<li <?php echo $category->id == $ca->id ? "class='category_active'" : ''; ?> ><a href="<?php echo base_url('store/category_items/'.$ca->id); ?>" title=""><?php echo $ca->category_name; ?></li>
									<?php } else { ?>
											<li><a href="<?php echo base_url('store/category_items/'.$ca->id); ?>" title=""><?php echo $ca->category_name; ?></li>
									<?php } ?>
							<?php } ?>
							<?php endif;?>
						</ul>
			   	</div>
				</div>
				<div class="category-item col-md-9">
					<div class="category-selected">
						<?php if(isset($category->category_name)) {?>
								<h1>[<?php echo strtoupper($category->category_name); ?>]</h1>
						<?php } else { ?>
								<h1>[All Products]</h1>
						<?php } ?>
					</div>
					<div class="row">
						<?php if($products) { ?>
						<div class="item-first-row col-md-12">
							<?php $i = 0; ?>
							<?php foreach($products as $p_key => $p) { ?>
									<?php
										if($i%4==0) { 
											echo '</div><div class="item-first-row col-md-12">';
										}
									?>							
									<div class="items col-md-3 col-sm-3">
										<a href="<?php echo base_url('store/product_details/'.$p->id); ?>">
										<div class="wrap">
											<div class="img-container red">
												<?php if($p->default_product_image != '' || $p->default_product_image != null) { ?>
														<img src="<?php echo base_url(); ?>/sd-contents/onlinestore/images/<?php echo $p->default_product_image; ?>" alt="Avatar" class="image_product" style="width:100%">
												<?php }else{ ?>
														<img src="<?php echo base_url(); ?>/sd-contents/onlinestore/images/gadgets.jpg" alt="Avatar" class="image_product" style="width:100%">
												<?php } ?>
											</div>
										</div>
										<p class="text-center" style="line-height: 20px !important;"><?php echo $p->name ?></p>
										<p class="refine-price">Php <?php echo number_format($p->price,2); ?></p>
										</a>
									</div>
							<?php $i++; } ?>
						<?php }else{ ?>
								<div class="items col-md-3 col-sm-3">
									<p class="text-center">NO RECORDS FOUND</p>
								</div>
						<?php } ?>
				</div>
			</div>
		</div>
	</div>