	<div id="content">
		<div class="content-purchasemessage">

			<div class="purchase-centerpanel">
				<?php if($flashdata) { ?>

						<h1 class="headline">
							<center><?php echo ($purchased_message) ? $purchased_message->thankyou_message : '' ; ?></center>
						</h1>
						<!--<div class="payment-opt">
							<h2>For Payment Options:</h2>
							<div class="row">
								<div class="col-md-6 col-xs-12 panel1">
									<p class="purchase-bankname">BPI (Bank of the Philippine Islands)</p>
									<p class="purchase-banktel">0000 0000 00</p>
									<p class="purchase-bankplace">Cabuyao, Laguna</p>
								</div>
								<div class="col-md-6 col-xs-12 panel2">
									<p class="purchase-bankname">East West Bank</p>
									<p class="purchase-banktel">0000 0000 00</p>
									<p class="purchase-bankplace">Cabuyao, Laguna</p>
								</div>
							</div>
						</div>-->
						<div class="payment-details">
							<h2>Mode of Payment:</h2>
							<div class="row">
								<div class="col-md-12 col-xs-12">
									<p class="payment-invoice">
										<div style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;">
										<?php echo ($purchased_message) ? trim('<pre style="font-size: 15px">' . $purchased_message->purchase_message . '</pre>') : ''; ?>
										</div>
									</p>
								</div>
								<!-- <div class="col-md-6 col-xs-6">
									<p class="lbl-invoice">Insert invoice here</p>
									<p class="lbl-bankname">Insert Bank Name here</p>
									<p class="lbl-date">Insert Date here</p>
									<p class="lbl-amount">Insert Amount Deposit here</p>
								</div> -->
							</div>
						</div>
						<h3 class="note">
							<?php echo ($purchased_message) ? $purchased_message->note_message : ''; ?>
						</h3>						

				<?php } else { ?>
						 
						<br /><br />
						<div class="payment-opt">
							<div class="row">
								<div class="col-md-12 col-xs-12 panel2">
									<p class="payment-invoice">
										<center> Session Expired </center>
									</p>
								</div>
							</div>
						</div>							

				<?php } ?>

			</div>

		</div>
	</div>