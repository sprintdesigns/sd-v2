<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.1
Version: 3.6.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Sort Menu</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/jquery-nestable/jquery.nestable.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="<?php echo base_url(); ?>/sd-assets/pluggables/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>/sd-assets/pluggables/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>/sd-assets/admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
<link id="style_color" href="<?php echo base_url() ?>/sd-assets/admin/layout4/css/themes/light.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>/sd-assets/admin/layout4/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-header-fixed page-sidebar-closed-hide-logo ">

<!-- BEGIN CONTAINER -->
<div class="page-container-navsort" style="margin-top:10px;">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content-navsort">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="note note-info note-shadow">
				<p>
					<span class="label label-danger">
					NOTE! </span>
					<span>
					<span class="bold">
					Nestable List Plugin </span>
					supported in Firefox, Chrome, Opera, Safari, Internet Explorer 10 and Internet Explorer 9 only. Internet Explorer 8 not supported. </span>
				</p>
			</div>
			<div class="portlet light">
				<div class="portlet-body">
					<div class="row">
						<div class="col-md-12">
							<div class="margin-bottom-10" id="nestable_list_menu">
								<button type="button" class="btn" data-action="expand-all">Expand All</button>
								<button type="button" class="btn" data-action="collapse-all">Collapse All</button>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<h3>Serialised Output (per list)</h3>
							<textarea id="nestable_list_1_output" class="form-control col-md-12 margin-bottom-10"></textarea>
							<textarea id="nestable_list_2_output" class="form-control col-md-12 margin-bottom-10"></textarea>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="portlet box blue">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-comments"></i>Nestable List 1
									</div>
									<div class="tools">
										<a href="javascript:;" class="collapse">
										</a>
										<a href="#portlet-config" data-toggle="modal" class="config">
										</a>
										<a href="javascript:;" class="reload">
										</a>
										<a href="javascript:;" class="remove">
										</a>
									</div>
								</div>
								<div class="portlet-body ">
									<div class="dd" id="nestable_list_1">
										<ol class="dd-list">
											<li class="dd-item" data-id="1">
												<div class="dd-handle">
													 Item 1
												</div>
											</li>
											<li class="dd-item" data-id="2">
												<div class="dd-handle">
													 Item 2
												</div>
												<ol class="dd-list">
													<li class="dd-item" data-id="3">
														<div class="dd-handle">
															 Item 3
														</div>
													</li>
													<li class="dd-item" data-id="4">
														<div class="dd-handle">
															 Item 4
														</div>
													</li>
													<li class="dd-item" data-id="5">
														<div class="dd-handle">
															 Item 5
														</div>
														<ol class="dd-list">
															<li class="dd-item" data-id="6">
																<div class="dd-handle">
																	 Item 6
																</div>
															</li>
															<li class="dd-item" data-id="7">
																<div class="dd-handle">
																	 Item 7
																</div>
															</li>
															<li class="dd-item" data-id="8">
																<div class="dd-handle">
																	 Item 8
																</div>
															</li>
														</ol>
													</li>
													<li class="dd-item" data-id="9">
														<div class="dd-handle">
															 Item 9
														</div>
													</li>
													<li class="dd-item" data-id="10">
														<div class="dd-handle">
															 Item 10
														</div>
													</li>
												</ol>
											</li>
											<li class="dd-item" data-id="11">
												<div class="dd-handle">
													 Item 11
												</div>
											</li>
											<li class="dd-item" data-id="12">
												<div class="dd-handle">
													 Item 12
												</div>
											</li>
										</ol>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="portlet box green">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-comments"></i>Nestable List 2
									</div>
									<div class="tools">
										<a href="javascript:;" class="collapse">
										</a>
										<a href="#portlet-config" data-toggle="modal" class="config">
										</a>
										<a href="javascript:;" class="reload">
										</a>
										<a href="javascript:;" class="remove">
										</a>
									</div>
								</div>
								<div class="portlet-body">
									<div class="dd" id="nestable_list_2">
										<ol class="dd-list">
											<li class="dd-item" data-id="13">
												<div class="dd-handle">
													 Item 13
												</div>
											</li>
											<li class="dd-item" data-id="14">
												<div class="dd-handle">
													 Item 14
												</div>
											</li>
											<li class="dd-item" data-id="15">
												<div class="dd-handle">
													 Item 15
												</div>
												<ol class="dd-list">
													<li class="dd-item" data-id="16">
														<div class="dd-handle">
															 Item 16
														</div>
													</li>
													<li class="dd-item" data-id="17">
														<div class="dd-handle">
															 Item 17
														</div>
													</li>
													<li class="dd-item" data-id="18">
														<div class="dd-handle">
															 Item 18
														</div>
													</li>
												</ol>
											</li>
										</ol>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="portlet box yellow">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-comments"></i>Nestable List 3
									</div>
									<div class="tools">
										<a href="javascript:;" class="collapse">
										</a>
										<a href="#portlet-config" data-toggle="modal" class="config">
										</a>
										<a href="javascript:;" class="reload">
										</a>
										<a href="javascript:;" class="remove">
										</a>
									</div>
								</div>
								<div class="portlet-body">
									<div class="dd" id="nestable_list_3">
										<ol class="dd-list">
											<li class="dd-item dd3-item" data-id="13">
												<div class="dd-handle dd3-handle">
												</div>
												<div class="dd3-content">
													 Item 13
												</div>
											</li>
											<li class="dd-item dd3-item" data-id="14">
												<div class="dd-handle dd3-handle">
												</div>
												<div class="dd3-content">
													 Item 14
												</div>
											</li>
											<li class="dd-item dd3-item" data-id="15">
												<div class="dd-handle dd3-handle">
												</div>
												<div class="dd3-content">
													 Item 15
												</div>
												<ol class="dd-list">
													<li class="dd-item dd3-item" data-id="16">
														<div class="dd-handle dd3-handle">
														</div>
														<div class="dd3-content">
															 Item 16
														</div>
													</li>
													<li class="dd-item dd3-item" data-id="17">
														<div class="dd-handle dd3-handle">
														</div>
														<div class="dd3-content">
															 Item 17
														</div>
													</li>
													<li class="dd-item dd3-item" data-id="18">
														<div class="dd-handle dd3-handle">
														</div>
														<div class="dd3-content">
															 Item 18
														</div>
													</li>
												</ol>
											</li>
										</ol>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT -->
		<!-- BEGIN QUICK SIDEBAR -->
		<!--Cooming Soon...-->
		<!-- END QUICK SIDEBAR -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="page-footer">
		<div class="page-footer-inner">
			 2014 &copy; Metronic by keenthemes.
		</div>
		<div class="scroll-to-top">
			<i class="icon-arrow-up"></i>
		</div>
	</div>
	<!-- END FOOTER -->
</div>
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/respond.min.js"></script>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/jquery-nestable/jquery.nestable.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url() ?>/sd-assets/admin/pages/scripts/ui-nestable.js"></script>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>/sd-assets/admin/layout4/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>/sd-assets/admin/layout4/scripts/demo.js" type="text/javascript"></script>
<script>
jQuery(document).ready(function() {       
   // initiate layout and plugins
   Metronic.init(); // init metronic core components
Layout.init(); // init current layout
Demo.init(); // init demo features
   UINestable.init();
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>