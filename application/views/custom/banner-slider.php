<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.1
Version: 3.6.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Slider Banner Settings</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>sd-assets/pluggables/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>sd-assets/pluggables/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>sd-assets/pluggables/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>sd-assets/pluggables/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>sd-assets/pluggables/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="<?php echo base_url(); ?>sd-assets/pluggables/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>sd-assets/pluggables/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>sd-assets/admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
<link id="style_color" href="<?php echo base_url(); ?>sd-assets/admin/layout4/css/themes/light.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>sd-assets/admin/layout4/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->

<!--layer slider -->
<link href="<?php echo base_url(); ?>sd-assets/pluggables/plugins/slider-layer-slider/css/layerslider.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>sd-assets/frontend/pages/css/style-layer-slider.css" rel="stylesheet">
<!--layer slider end -->

<link href="<?php echo base_url(); ?>sd-assets/custom-banner-editor.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-sidebar-closed-hide-logo">
<!-- BEGIN HEADER -->
<div class="page-header navbar">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
	  <div class="container-fluid">
	    <h1>Slider Banner Settings</h1>
	    <button class="btn btn-danger btn-md saveSlide">Save Changes</button>
	  </div>	
	</div>
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content-x container-fluid" style="position:relative;">
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1>Editor <small>Banner Slider</small></h1>
				</div>
				<!-- END PAGE TITLE -->
				<!-- BEGIN PAGE TOOLBAR -->
				<div class="page-toolbar">
					<!-- BEGIN THEME PANEL -->
					<div class="btn-group btn-theme-panel">
						<a href="javascript:;" class="btn dropdown-toggle" data-toggle="dropdown">
						<i class="icon-settings"></i>
						</a>
						<div class="dropdown-menu theme-panel pull-right dropdown-custom hold-on-click">
							<div class="row">
								<div class="col-md-4 col-sm-4 col-xs-12">
									<h3>THEME</h3>
									<ul class="theme-colors">
										<li class="theme-color theme-color-default active" data-theme="default">
											<span class="theme-color-view"></span>
											<span class="theme-color-name">Dark Header</span>
										</li>
										<li class="theme-color theme-color-light" data-theme="light">
											<span class="theme-color-view"></span>
											<span class="theme-color-name">Light Header</span>
										</li>
									</ul>
								</div>
								<div class="col-md-8 col-sm-8 col-xs-12 seperator">
									<h3>LAYOUT</h3>
									<ul class="theme-settings">
										<li>
											 Theme Style
											<select class="layout-style-option form-control input-small input-sm">
												<option value="square" selected="selected">Square corners</option>
												<option value="rounded">Rounded corners</option>
											</select>
										</li>
										<li>
											 Layout
											<select class="layout-option form-control input-small input-sm">
												<option value="fluid" selected="selected">Fluid</option>
												<option value="boxed">Boxed</option>
											</select>
										</li>
										<li>
											 Header
											<select class="page-header-option form-control input-small input-sm">
												<option value="fixed" selected="selected">Fixed</option>
												<option value="default">Default</option>
											</select>
										</li>
										<li>
											 Top Dropdowns
											<select class="page-header-top-dropdown-style-option form-control input-small input-sm">
												<option value="light">Light</option>
												<option value="dark" selected="selected">Dark</option>
											</select>
										</li>
										<li>
											 Sidebar Mode
											<select class="sidebar-option form-control input-small input-sm">
												<option value="fixed">Fixed</option>
												<option value="default" selected="selected">Default</option>
											</select>
										</li>
										<li>
											 Sidebar Menu
											<select class="sidebar-menu-option form-control input-small input-sm">
												<option value="accordion" selected="selected">Accordion</option>
												<option value="hover">Hover</option>
											</select>
										</li>
										<li>
											 Sidebar Position
											<select class="sidebar-pos-option form-control input-small input-sm">
												<option value="left" selected="selected">Left</option>
												<option value="right">Right</option>
											</select>
										</li>
										<li>
											 Footer
											<select class="page-footer-option form-control input-small input-sm">
												<option value="fixed">Fixed</option>
												<option value="default" selected="selected">Default</option>
											</select>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- END THEME PANEL -->
				</div>
				<!-- END PAGE TOOLBAR -->
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="index.html">Dashboard</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="#">Banner Slider</a>
					<i class="fa fa-circle"></i>
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
				 <!-- start banner-->
				 <div class="sd-banner">
				     <div class="col-md-12" style="position:relative;">
                        <h1>Preview</h1>
					    <!-- LayerSlider start -->
					    <div id="layerslider" style="width: 100%; height: 480px; margin: 0 auto;">
					        <!-- slide one start -->
					        <div class="ls-slide ls-slide1" data-ls="offsetxin: right; slidedelay: 4000; transition2d: 24,25,27,28;">

					          <img src="<?php echo $sd->theme->get_theme_path(); ?>assets/images/imageslider/slider-1.jpg" class="ls-bg" alt="Slide background">

					          <!-- <div class="ls-l ls-title" style="top: 96px; left: 35%; white-space: nowrap;" data-ls="
					            fade: true; 
					            fadeout: true; 
					            durationin: 750; 
					            durationout: 750; 
					            easingin: easeOutQuint; 
					            rotatein: 90; 
					            rotateout: -90; 
					            scalein: .5; 
					            scaleout: .5; 
					            showuntil: 4000;
					          "> -->
					          <div class="ls-l ls-title" style="top: 96px; left: 35%; white-space: nowrap;" data-ls="
					            
					          ">Made from <strong>high grade </strong> materials</div>

					         <!--  <div class="ls-l ls-mini-text" style="top: 338px; left: 35%; white-space: nowrap;" data-ls="
					          fade: true; 
					          fadeout: true; 
					          durationout: 750; 
					          easingin: easeOutQuint; 
					          delayin: 300; 
					          showuntil: 4000;
					          "> -->
					           <div class="ls-l ls-mini-text" style="top: 338px; left: 35%; white-space: nowrap;" data-ls="
					         
					          ">Lorem ipsum dolor sit amet  constectetuer diam<br > adipiscing elit euismod ut laoreet dolore.
					          </div>
					        </div>
					        <!-- slide one end -->
					        <!-- slide two start -->
					        
					    </div>
				      <!-- LayerSlider end -->

				     </div> 
				        <!--end of carousel -->
				   </div>
					<!--end banner-->
				</div>
				<div class="slides-container">
				  <div class="slider-controls">
				  	<button id="triggersortSlides">Sort</button>
				  	<button id="triggerdeleteSlides">Delete Slide</button>
				  </div>
				  <div class='slider-container-box' style="position:relative">
				    <!--add slide-->
				    <div class="slider-boxer addslide">
				  	  <button class="slider-buttons ">
				  	    Add
				  	  </button>	
				  	</div>
				    <!--add slide end-->
				    <!--boxes slides-->
				  	<div style="background-image: url('<?php echo $sd->theme->get_theme_path(); ?>assets/images/imageslider/slider-1.jpg');" class="slider-boxer editslide">
				  	  <button class="slider-buttons">
				  	    Edit
				  	  </button>	
				  	  <button class="deleteSlide"><i class="fa fa-close"></i></button>
				  	</div>
				  	<!--end of boxes slides-->
				  	<!--boxes slides-->
				  	<div style="background-image: url('<?php echo $sd->theme->get_theme_path(); ?>assets/images/imageslider/slider-2.jpg');" class="slider-boxer editslide">
				  	  <button class="slider-buttons">
				  	    Edit
				  	  </button>	
				  	  <button class="deleteSlide"><i class="fa fa-close"></i></button>
				  	</div>
				  	<!--end of boxes slides-->
				  	<!--boxes slides-->
				  	<div style="background-image: url('<?php echo $sd->theme->get_theme_path(); ?>assets/images/imageslider/slider-3.jpg');" class="slider-boxer editslide">
				  	  <button class="slider-buttons">
				  	    Edit
				  	  </button>	
				  	  <button class="deleteSlide"><i class="fa fa-close"></i></button>
				  	</div>
				  	<!--end of boxes slides-->
				  </div>	
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<div class="clearfix"></div>

<div class="container"></div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo base_url(); ?>sd-assets/pluggables/plugins/respond.min.js"></script>
<script src="<?php echo base_url(); ?>sd-assets/pluggables/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?php echo base_url(); ?>sd-assets/pluggables/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>sd-assets/pluggables/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo base_url(); ?>sd-assets/pluggables/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>sd-assets/pluggables/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>sd-assets/pluggables/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>sd-assets/pluggables/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>sd-assets/pluggables/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>sd-assets/pluggables/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>sd-assets/pluggables/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>sd-assets/pluggables/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->

<!--start wiggle-->
<script src="<?php echo base_url(); ?>sd-assets/pluggables/plugins/jquery-classywiggle/js/jquery.classywiggle.js" type="text/javascript"></script>
<!--end start wiggle-->

<!--start layerslider-->
<!-- BEGIN LayerSlider -->
<script src="<?php echo base_url(); ?>sd-assets/pluggables/plugins/slider-layer-slider/js/greensock.js" type="text/javascript"></script><!-- External libraries: GreenSock -->
<script src="<?php echo base_url(); ?>sd-assets/pluggables/plugins/slider-layer-slider/js/layerslider.transitions.js" type="text/javascript"></script><!-- LayerSlider script files -->
<script src="<?php echo base_url(); ?>sd-assets/pluggables/plugins/slider-layer-slider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
<script src="<?php echo $sd->theme->get_theme_path(); ?>assets/js/layersliderInit.js" type="text/javascript"></script>

<?php if(isset($scripts)){
	foreach($scripts as $script){
		echo $script;
	}
} ?>

<script>

</script>
<!--end layerslider-->
<script src="<?php echo base_url(); ?>sd-assets/pluggables/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>sd-assets/admin/layout4/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>sd-assets/admin/layout4/scripts/demo.js" type="text/javascript"></script>
<script>
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core components
   Layout.init(); // init current layout
   Demo.init(); // init demo features
   base_url = '<?php echo base_url(); ?>';
   LayersliderInit.initLayerSlider();	
});
</script>
<script>
    $(document).ready(function() {
        var interval = null; 
        $('button#triggerdeleteSlides').click(function() {
        	if ($(this).hasClass('deleteOn')) {
        	  $('div.slider-container-box .editslide').ClassyWiggle('stop');
              $(this).removeClass('deleteOn');
              clearInterval(interval);
        	}
        	else {
              $('div.slider-container-box .editslide').ClassyWiggle('start');
              $(this).addClass('deleteOn');
            }
        });
    });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>