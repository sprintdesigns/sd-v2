<?php if($pages = $sd->page->get_draft_pages()){ ?>
	<ol class="dd-list">
		<?php foreach ($sd->page->get_draft_pages() as $index => $page) { ?>
			<li class="dd-item" data-id="<?= $page->id ?>">
				<div class="dd-handle dd3-handle">
					 
				</div>
				<div class="ddeye-handle  <?= $page->status? 'eye-active':'' ?>">
					<input type="hidden" class="nav-id" value="<?= $page->id; ?>" />											
				</div>
				<div class="ddeye-handle <?= $page->status? 'eye-active':'' ?>">
						  <label><input type="checkbox" class="ios-switch green tinyswitch" <?= $page->status? 'checked':'' ?> /><div><span class="onswitch">On</span><span class="offswitch">Off</span><div></div></div>
						  </label>
						  <input type="hidden" class="nav-id" value="<?= $page->id; ?>" />
						</div>
				<div class="dd-content dd3-content page-<?= $page->page_url ?>">
															
				  <a href="<?= base_url()."edit?page=".$page->page_url ?>">	
                    <?= $page->page_title ?>
                  </a>	
                  <input type="text" class="hidden" value="<?= $page->page_title ?>">
				</div>
				<div class="menu-button-holder">
				  <?php if( strtolower($page->page_url) != "home"){ ?>
				  <button class="btn btn-success sd-page-func rename-page" type="button" data-page="<?= $page->page_url ?>" data-page-id="<?= $page->id ?>" >Rename</button>	
				  	
				<?php } ?>  
					<button class="btn btn-success sd-page-func" type="button" onclick="duplicate_page('<?= $page->id ?>')" >Duplicate</button>
				  <button class="btn btn-success sd-page-func sd-copy-url" type="button" data-clipboard-text="<?= base_url().$page->page_url ?>" >Copy Link</button>	
				<?php if( strtolower($page->page_url) != "home"){ ?> 
				  <button class="btn btn-danger sd-page-func" type="button" onclick="delete_page(<?= $page->id ?>)">Delete</button>	
				<?php } ?>  
				  <button class="btn btn-primary sd-page-func-edit hidden save-page-rename" data-page-id="<?= $page->id ?>" data-page="<?= $page->page_url ?>" type="button" >Save</button>	
					<button class="btn btn-danger sd-page-func-edit hidden cancel-page-rename" data-page="<?= $page->page_url ?>" type="button" >Cancel</button>
				</div>
			</li>
		<?php } ?>
	</ol>
<?php }else{ ?>
	<div class="dd-empty"></div>
<?php } ?>