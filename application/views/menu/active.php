<ol class="dd-list">
	<?php if(!$menu){ ?>
		<div class="dd-empty"></div>
	<?php }else { ?>
		
		<?php $items = json_decode($menu[0]->orders); ?>
		<?php foreach ($items as $index => $i) { ?>
			<?php $page_data = $sd->page->select_page_by_id($i->id); ?>
			<li class="dd-item" data-id="<?= $page_data[0]->id ?>">
				
				<div class="dd-handle dd3-handle">
					
				</div>
				<!-- <div class="ddeye-handle <?= $page_data[0]->status? 'eye-active':'' ?>">
					<input type="hidden" class="nav-id" value="<?= $page_data[0]->id; ?>" />
				</div> -->
				<div class="ddeye-handle <?= $page_data[0]->status? 'eye-active':'' ?>">
				  <label><input type="checkbox" class="ios-switch green tinyswitch" <?= $page_data[0]->status? 'checked':'' ?> /><div><span class="onswitch">On</span><span class="offswitch">Off</span><div></div></div>
				  </label>
				  <input type="hidden" class="nav-id" value="<?= $page_data[0]->id; ?>" />
				</div>
				<div class="dd-content dd3-content page-<?= $page_data[0]->page_url ?>">
				  <a href="<?= base_url()."edit?page=".$page_data[0]->page_url ?>">	
					 <?= $page_data[0]->page_title ?>
				  </a>	
				  <input type="text" class="hidden" value="<?= $page_data[0]->page_title ?>">
				</div>
				<div class="menu-button-holder">
				  <?php if( strtolower($page_data[0]->page_url) != "home"){ ?>
			  		<button class="btn btn-success sd-page-func rename-page" type="button" data-page="<?= $page_data[0]->page_url ?>" >Rename</button>	
			  			
			 	<?php } ?>
			  <button class="btn btn-success sd-page-func" type="button" onclick="duplicate_page('<?= $page_data[0]->id ?>')" >Duplicate</button>

			  <button class="btn btn-success sd-page-func sd-copy-url" type="button" data-clipboard-text="<?= base_url().$page_data[0]->page_url; ?>" >Copy Link</button>	
			  <?php if( strtolower($page_data[0]->page_url) != "home"){ ?>
			  	<button class="btn btn-danger sd-page-func" type="button" onclick="delete_page(<?= $page_data[0]->id ?>)">Delete</button>	
			  <?php } ?>
	   		<button class="btn btn-primary sd-page-func-edit hidden save-page-rename" data-page-id="<?= $page_data[0]->id ?>" data-page="<?= $page_data[0]->page_url ?>" type="button" >Save</button>	
			  <button class="btn btn-danger sd-page-func-edit hidden cancel-page-rename" data-page="<?= $page_data[0]->page_url ?>" type="button" >Cancel</button>	
				</div>
				<?php if(array_key_exists('children', $i)){ ?>
					<ol class="dd-list">
					<?php foreach ($i->children as $index => $i2) { ?>
						<?php $page_data = $sd->page->select_page_by_id($i2->id); ?>
						<li class="dd-item" data-id="<?= $page_data[0]->id ?>">
							
							<div class="dd-handle dd3-handle">
								
							</div>
							<!-- <div class="ddeye-handle <?= $page_data[0]->status? 'eye-active':'' ?>">
									<input type="hidden" class="nav-id" value="<?= $page_data[0]->id; ?>" />
							</div> -->
							<div class="ddeye-handle <?= $page_data[0]->status? 'eye-active':'' ?>">
							  <label><input type="checkbox" class="ios-switch green tinyswitch" <?= $page_data[0]->status? 'checked':'' ?> /><div><span class="onswitch">On</span><span class="offswitch">Off</span><div></div></div>
							  </label>
							  <input type="hidden" class="nav-id" value="<?= $page_data[0]->id; ?>" />
							</div>
							<div class="dd-content dd3-content page-<?= $page_data[0]->page_url ?>">
								
							  <a href="<?= base_url()."edit?page=".$page_data[0]->page_url ?>">	
							    <?= $page_data[0]->page_title ?>
							  </a>	
							  <input type="text" class="hidden" value="<?= $page_data[0]->page_title ?>">
							</div>
							<div class="menu-button-holder">
							  <?php if( strtolower($page_data[0]->page_url) != "home"){ ?>
						  		<button class="btn btn-success sd-page-func rename-page" type="button" data-page="<?= $page_data[0]->page_url ?>" >Rename</button>	
						  			
						 	<?php } ?>
						  <button class="btn btn-success sd-page-func" type="button" onclick="duplicate_page('<?= $page_data[0]->id ?>')" >Duplicate</button>

						  <button class="btn btn-success sd-page-func sd-copy-url" type="button" data-clipboard-text="<?= base_url().$page_data[0]->page_url; ?>" >Copy Link</button>	
						  <?php if( strtolower($page_data[0]->page_url) != "home"){ ?>
						  	<button class="btn btn-danger sd-page-func" type="button" onclick="delete_page(<?= $page_data[0]->id ?>)">Delete</button>	
						  <?php } ?>
				   		<button class="btn btn-primary sd-page-func-edit hidden save-page-rename" data-page-id="<?= $page_data[0]->id ?>" data-page="<?= $page_data[0]->page_url ?>" type="button" >Save</button>	
						  <button class="btn btn-danger sd-page-func-edit hidden cancel-page-rename" data-page="<?= $page_data[0]->page_url ?>" type="button" >Cancel</button>	
							</div>
							<?php if(array_key_exists('children', $i2)){ ?>
								<ol class="dd-list">
								<?php foreach ($i2->children as $index => $i3) { ?>
									<?php $page_data = $sd->page->select_page_by_id($i3->id); ?>
									<li class="dd-item" data-id="<?= $page_data[0]->id ?>">
										
										<div class="dd-handle dd3-handle">
											
										</div>
										<!-- <div class="ddeye-handle <?= $page_data[0]->status? 'eye-active':'' ?>">
												<input type="hidden" class="nav-id" value="<?= $page_data[0]->id; ?>" />
										</div> -->
										<div class="ddeye-handle <?= $page_data[0]->status? 'eye-active':'' ?>">
										  <label><input type="checkbox" class="ios-switch green tinyswitch" <?= $page_data[0]->status? 'checked':'' ?> /><div><span class="onswitch">On</span><span class="offswitch">Off</span><div></div></div>
										  </label>
										  <input type="hidden" class="nav-id" value="<?= $page_data[0]->id; ?>" />
										</div>
										<div class="dd-content dd3-content page-<?= $page_data[0]->page_url ?>">
											
										<a href="<?= base_url()."edit?page=".$page_data[0]->page_url ?>">	
									      <?= $page_data[0]->page_title ?>
									    </a>	
									    <input type="text" class="hidden" value="<?= $page_data[0]->page_title ?>">
										</div>
										<div class="menu-button-holder">
										 <?php if( strtolower($page_data[0]->page_url) != "home"){ ?>
									  		<button class="btn btn-success sd-page-func rename-page" type="button" data-page="<?= $page_data[0]->page_url ?>" >Rename</button>	
									  			
									 	<?php } ?>
									  <button class="btn btn-success sd-page-func" type="button" onclick="duplicate_page('<?= $page_data[0]->id ?>')" >Duplicate</button>

									  <button class="btn btn-success sd-page-func sd-copy-url" type="button" data-clipboard-text="<?= base_url().$page_data[0]->page_url; ?>" >Copy Link</button>	
									  <?php if( strtolower($page_data[0]->page_url) != "home"){ ?>
									  	<button class="btn btn-danger sd-page-func" type="button" onclick="delete_page(<?= $page_data[0]->id ?>)">Delete</button>	
									  <?php } ?>
							   		<button class="btn btn-primary sd-page-func-edit hidden save-page-rename" data-page-id="<?= $page_data[0]->id ?>" data-page="<?= $page_data[0]->page_url ?>" type="button" >Save</button>	
									  <button class="btn btn-danger sd-page-func-edit hidden cancel-page-rename" data-page="<?= $page_data[0]->page_url ?>" type="button" >Cancel</button>	
										</div>
										<?php if(array_key_exists('children', $i3)){ ?>
											<ol class="dd-list">
											<?php foreach ($i3->children as $index => $i4) { ?>
												<?php $page_data = $sd->page->select_page_by_id($i4->id); ?>
												<li class="dd-item" data-id="<?= $page_data[0]->id ?>">
													
													<div class="dd-handle dd3-handle">
														
													</div>
													<!-- <div class="ddeye-handle <?= $page_data[0]->status? 'eye-active':'' ?>">
														<input type="hidden" class="nav-id" value="<?= $page_data[0]->id; ?>" />
												    </div> -->
												    <div class="ddeye-handle <?= $page_data[0]->status? 'eye-active':'' ?>">
													  <label><input type="checkbox" class="ios-switch green tinyswitch" <?= $page_data[0]->status? 'checked':'' ?> /><div><span class="onswitch">On</span><span class="offswitch">Off</span><div></div></div>
													  </label>
													  <input type="hidden" class="nav-id" value="<?= $page_data[0]->id; ?>" />
													</div>
													<div class="dd-content dd3-content page-<?= $page_data[0]->page_url ?>">
														
													  <a href="<?= base_url()."edit?page=".$page_data[0]->page_url ?>">	
							                            <?= $page_data[0]->page_title ?>
							                          </a>	
							                          <input type="text" class="hidden" value="<?= $page_data[0]->page_title ?>">
													</div>
													<div class="menu-button-holder">
													  <?php if( strtolower($page_data[0]->page_url) != "home"){ ?>
													  		<button class="btn btn-success sd-page-func rename-page" type="button" data-page="<?= $page_data[0]->page_url ?>" >Rename</button>	
													  			
													 	<?php } ?>
													  <button class="btn btn-success sd-page-func" type="button" onclick="duplicate_page('<?= $page_data[0]->id ?>')" >Duplicate</button>

													  <button class="btn btn-success sd-page-func sd-copy-url" type="button" data-clipboard-text="<?= base_url().$page_data[0]->page_url; ?>" >Copy Link</button>	
													  <?php if( strtolower($page_data[0]->page_url) != "home"){ ?>
													  	<button class="btn btn-danger sd-page-func" type="button" onclick="delete_page(<?= $page_data[0]->id ?>)">Delete</button>	
													  <?php } ?>
											   		<button class="btn btn-primary sd-page-func-edit hidden save-page-rename" data-page-id="<?= $page_data[0]->id ?>" data-page="<?= $page_data[0]->page_url ?>" type="button" >Save</button>	
													  <button class="btn btn-danger sd-page-func-edit hidden cancel-page-rename" data-page="<?= $page_data[0]->page_url ?>" type="button" >Cancel</button>	
													</div>
												</li>
												<?php if(array_key_exists('children', $i4)){ ?>
													<ol class="dd-list">
													<?php foreach ($i4->children as $index => $i5) { ?>
														<?php $page_data = $sd->page->select_page_by_id($i5->id); ?>
														<li class="dd-item" data-id="<?= $page_data[0]->id ?>">
															
															<div class="dd-handle dd3-handle">
																
															</div>
															<!-- <div class="ddeye-handle <?= $page_data[0]->status? 'eye-active':'' ?>">
																<input type="hidden" class="nav-id" value="<?= $page_data[0]->id; ?>" />
															</div> -->
															<div class="ddeye-handle <?= $page_data[0]->status? 'eye-active':'' ?>">
															  <label><input type="checkbox" class="ios-switch green tinyswitch" <?= $page_data[0]->status? 'checked':'' ?> /><div><span class="onswitch">On</span><span class="offswitch">Off</span><div></div></div>
															  </label>
															  <input type="hidden" class="nav-id" value="<?= $page_data[0]->id; ?>" />
															</div>
															<div class="dd-content dd3-content page-<?= $page_data[0]->page_url ?>">
																
															  <a href="<?= base_url()."edit?page=".$page_data[0]->page_url ?>">	
							                                    <?= $page_data[0]->page_title ?>
							                                  </a>	
							                                  <input type="text" class="hidden" value="<?= $page_data[0]->page_title ?>">
															</div>
															<div class="menu-button-holder">
															 <?php if( strtolower($page_data[0]->page_url) != "home"){ ?>
															  		<button class="btn btn-success sd-page-func rename-page" type="button" data-page="<?= $page_data[0]->page_url ?>" >Rename</button>	
															  			
															 	<?php } ?>
															  <button class="btn btn-success sd-page-func" type="button" onclick="duplicate_page('<?= $page_data[0]->id ?>')" >Duplicate</button>

															  <button class="btn btn-success sd-page-func sd-copy-url" type="button" data-clipboard-text="<?= base_url().$page_data[0]->page_url; ?>" >Copy Link</button>	
															  <?php if( strtolower($page_data[0]->page_url) != "home"){ ?>
															  	<button class="btn btn-danger sd-page-func" type="button" onclick="delete_page(<?= $page_data[0]->id ?>)">Delete</button>	
															  <?php } ?>
													   		<button class="btn btn-primary sd-page-func-edit hidden save-page-rename" data-page-id="<?= $page_data[0]->id ?>" data-page="<?= $page_data[0]->page_url ?>" type="button" >Save</button>	
															  <button class="btn btn-danger sd-page-func-edit hidden cancel-page-rename" data-page="<?= $page_data[0]->page_url ?>" type="button" >Cancel</button>	
															</div>
														</li>
													<?php } ?>
													</ol>
												<?php } ?>
											<?php } ?>
											</ol>
										<?php } ?>
									</li>
								<?php } ?>
								</ol>
							<?php } ?>
						</li>
					<?php } ?>
					</ol>
				<?php } ?>
			</li>
		<?php } ?>
		
	<?php } ?>	
	<li class="dd-item add-page-item">
													
		<div class="dd-handle dd3-handle no-click">
			
		</div>
		
	    <div class="ddeye-handle no-click">
		  <label><input type="checkbox" class="ios-switch green tinyswitch" checked /><div><span class="onswitch">On</span><span class="offswitch">Off</span><div></div></div>
		  </label>
		 
		</div>
		<div class="dd-content dd3-content">
          <input type="text" id="new-page-title" value="" placeholder="Type title" >
		</div>
		<div class="menu-button-holder show-fixed">
		 
		  <button class="btn btn-primary btn-save-new-page" type="button" data-action="<?= base_url().'page/create_page' ?>" >Save</button>	
			<button class="btn btn-danger btn-cancel-new-page" type="button" >Cancel</button>	
		</div>
	</li>
</ol>