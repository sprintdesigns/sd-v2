<!-- BEGIN CONTAINER -->
<div class="page-container-navsort" style="margin-top:10px;">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content-navsort">
			<div class="note note-info note-shadow">
				<p>
					<span class="label label-danger">
					NOTE! </span>
					<span>
					<span class="bold">
					Nestable List Plugin </span>
					supported in Firefox, Chrome, Opera, Safari, Internet Explorer 10 and Internet Explorer 9 only. Internet Explorer 8 not supported. </span>
				</p>
			</div>
			<div class="portlet light">
				<form action="<?= base_url(); ?>menu/save<?= $menu!=false? '/'.$menu[0]->id:''; ?>" method="POST" >
				<input type="hidden" id="nav_json" name="nav_json" value='' />
				<div class="portlet-body">
					
					<div class="row">
						<?php $response = $this->session->flashdata('response'); ?>
						<?php if($response != null){ ?>
							<div class="alert alert-success">
								<button class="close" data-close="alert"></button>
								<span>
								<?= $response['message'] ?>. </span>
							</div>
						<?php } ?>
						<div class="col-md-6">
							<div class="portlet box blue">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-comments"></i>Pages
									</div>
									<div class="tools">
										<a href="javascript:;" class="collapse">
										</a>
										<a href="#portlet-config" data-toggle="modal" class="config">
										</a>
										<a href="javascript:;" class="reload">
										</a>
										<a href="javascript:;" class="remove">
										</a>
									</div>
								</div>
								<div class="portlet-body ">
									<div class="dd" id="nestable_list_1">
										<ol class="dd-list">
											<?php foreach ($sd->page->get_pages() as $index => $page) { ?>
												<li class="dd-item" data-id="<?= $page->page_url ?>">
													<div class="dd-handle">
														 <?= $page->page_title ?>
													</div>
												</li>
											<?php } ?>
											
											
										</ol>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="portlet box green">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-comments"></i>Menus
									</div>
									<div class="tools">
										<a href="javascript:;" class="collapse">
										</a>
										<a href="#portlet-config" data-toggle="modal" class="config">
										</a>
										<a href="javascript:;" class="reload">
										</a>
										<a href="javascript:;" class="remove">
										</a>
									</div>
								</div>
								<div class="portlet-body">
									<div class="dd" id="nestable_list_2">
										<?php if($menu){ ?>
											
											
							
										<?php }else { ?>
											<ol class="dd-list">
											<?php $items = json_decode($menu[0]->orders); ?>
											<?php foreach ($items as $index => $i) { ?>
												<li class="dd-item" data-id="<?= $i->id ?>">
													<div class="dd-handle">
														<?php $page_data = $sd->page->page_exist($i->id); ?>
														 <?= $page_data[0]->page_title ?>
													</div>
													<?php if(array_key_exists('children', $i)){ ?>
														<ol class="dd-list">
														<?php foreach ($i->children as $index => $i2) { ?>
															<li class="dd-item" data-id="<?= $i2->id ?>">
																<div class="dd-handle">
																	<?php $page_data = $sd->page->page_exist($i2->id); ?>
																	 <?= $page_data[0]->page_title ?>
																</div>
																<?php if(array_key_exists('children', $i2)){ ?>
																	<ol class="dd-list">
																	<?php foreach ($i2->children as $index => $i3) { ?>
																		<li class="dd-item" data-id="<?= $i3->id ?>">
																			<div class="dd-handle">
																				<?php $page_data = $sd->page->page_exist($i3->id); ?>
																				 <?= $page_data[0]->page_title ?>
																			</div>
																			<?php if(array_key_exists('children', $i3)){ ?>
																				<ol class="dd-list">
																				<?php foreach ($i3->children as $index => $i4) { ?>
																					<li class="dd-item" data-id="<?= $i4->id ?>">
																						<div class="dd-handle">
																							<?php $page_data = $sd->page->page_exist($i4->id); ?>
																							 <?= $page_data[0]->page_title ?>
																						</div>
																					</li>
																					<?php if(array_key_exists('children', $i4)){ ?>
																						<ol class="dd-list">
																						<?php foreach ($i4->children as $index => $i5) { ?>
																							<li class="dd-item" data-id="<?= $i5->id ?>">
																								<div class="dd-handle">
																									<?php $page_data = $sd->page->page_exist($i5->id); ?>
																									 <?= $page_data[0]->page_title ?>
																								</div>
																							</li>
																						<?php } ?>
																						</ol>
																					<?php } ?>
																				<?php } ?>
																				</ol>
																			<?php } ?>
																		</li>
																	<?php } ?>
																	</ol>
																<?php } ?>
															</li>
														<?php } ?>
														</ol>
													<?php } ?>
												</li>
											<?php } ?>
											</ol>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
						<button type="submit" class="btn btn-primary pull-right">Save</button>
					</div>
					
				</div>
				</form>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT -->
		<!-- BEGIN QUICK SIDEBAR -->
		<!--Cooming Soon...-->
		<!-- END QUICK SIDEBAR -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="page-footer">
		<div class="page-footer-inner">
			 2014 &copy; Metronic by keenthemes.
		</div>
		<div class="scroll-to-top">
			<i class="icon-arrow-up"></i>
		</div>
	</div>
	<!-- END FOOTER -->
</div>
</div>
