<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/respond.min.js"></script>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/jquery-nestable/jquery.nestable.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url() ?>/sd-assets/admin/pages/scripts/ui-nestable.js"></script>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>/sd-assets/admin/layout4/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>/sd-assets/admin/layout4/scripts/demo.js" type="text/javascript"></script>
<script>
jQuery(document).ready(function() {       
   // initiate layout and plugins
   Metronic.init(); // init metronic core components
Layout.init(); // init current layout
Demo.init(); // init demo features
   UINestable.init();
});
</script>
<?php if(isset($menu)){ ?>
	<?php if($menu){ ?>
	<script>
		$(document).ready(function(){
			$("#nav_json").val('<?php echo $menu[0]->orders; ?>');
		});
	</script>
	<?php } ?>
<?php } ?>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>