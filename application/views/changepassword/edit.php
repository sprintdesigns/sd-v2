	<?php include 'header.php';?>
	<?php
		if($this->session->flashdata('success_msg')){
	?>
		<div class="alert alert-success">
			<?php echo $this->session->flashdata('success_msg'); ?>
		</div>
	<?php
		}
	?>

	<?php
		if($this->session->flashdata('error_msg')){
	?>
		<div class="alert alert-success">
			<?php echo $this->session->flashdata('error_msg'); ?>
		</div>
	<?php
		}
	?>
	<form action="<?php echo base_url('advanced/update/') ?>" method="post" class="form-horizontal">
	<input type="hidden" name="txt_hidden" value="<?php echo $profiles->id; ?>">

	<div class="form-inline">
				<input type="hidden" name="action" value="update">
				<div class="form-group">
					<label for="password">Current Password :</label><br>
					<input type="Password" value="" class="form-control" name="txt_password" required="">
				</div>
			</div>
			<br>
			<div class="form-inline">
				<input type="hidden" name="action" value="edit">
				<div class="form-group">
					<label for="newpass_id">New Password :</label><br>
					<input type="Password" value="" class="form-control" name="txt_newpass" required="">
				</div>
			</div>
			<br>
			<div class="form-inline">
				<input type="hidden" name="action" value="edit">
				<div class="form-group">
					<label for="newpass_id">Confirm New Password :</label><br>
					<input type="Password" value="" class="form-control" name="txt_confpassword" required="">
				</div>
			</div>
			<br>
			<button class="btn btn-primary" type="submit">Update Password</button>
	<?php include 'footer.php';?>