<?php include 'header.php';?>
<h3>Profile List</h3>
<?php
		if($this->session->flashdata('success_msg')){
	?>
		<div class="alert alert-success">
			<?php echo $this->session->flashdata('success_msg'); ?>
		</div>
	<?php
		}
	?>

	<?php
		if($this->session->flashdata('error_msg')){
	?>
		<div class="alert alert-success">
			<?php echo $this->session->flashdata('error_msg'); ?>
		</div>
	<?php
		}
	?>
	<table class="table table-bordered table-resposive">
		<thead>
			<tr>
				<td>ID</td>
				<td>Username</td>
				<td>Name</td>
				<td>Email</td>
			</tr>
		</thead>
		<tbody>
		<?php
			if($profiles){
				foreach ($profiles as $profile) {
		?>
			<tr>
				<td><?php echo $profile->id; ?></td>
				<td><?php echo $profile->username; ?></td>
				<td><?php echo $profile->name; ?></td>
				<td><?php echo $profile->email; ?></td>
				<td><center>
					<a href="<?php echo base_url('advanced/edit/'.$profile->id); ?>" class="btn btn-info">Edit</a>
					</center>
				</td>	
			</tr>
			<?php
					}
				}
			?>
		</tbody>
	</table>
<?php include 'footer.php';?>