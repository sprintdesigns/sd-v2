<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Plugins <button class="btn btn-primary showuploadplugin"><i class="fa fa-plus-circle"></i> Add Plugin</button></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		<ul class="page-breadcrumb breadcrumb hide">
			<li>
				<a href="#">Advanced</a><i class="fa fa-circle"></i>
			</li>
			<li class="active">
				 Plugins
			</li>
		</ul>
		<!-- END PAGE BREADCRUMB -->
		<!-- BEGIN PAGE CONTENT INNER -->
		<div class="custom-grid margin-top-20">
		  <div class="plugin-list">
		  	<ul>
		  	  <li class="plugins-item" >
		  	  	<!-- <div class="ddeye-handle eye-active">
				  <label><input class="ios-switch green tinyswitch" checked="" type="checkbox"><div><span class="onswitch">On</span><span class="offswitch">Off</span><div></div></div>
				  </label>
				  <input class="nav-id" value="108" type="hidden">
				</div> -->
				<div class="plugin-name default-plugin" data-plugin-id="default-contact-form" data-action-url="form/index" >Contact Form</div>
				<!--<button class="btn btn-danger btn-delete">Delete</button>-->
		  	  </li>
		  	  <?php foreach ($plugins as $p):?>
		  	  <li class="plugins-item <?php echo ($p->folder == $plugin_folder) ? 'active' : '' ;?>" >
				<a href="<?= base_url() ?>advanced/plugins/<?php echo $p->folder;?>/index.php"><div class="plugin-name" data-plugin-id="<?php echo $p->folder;?>">	<?php echo $p->title;?>
				</div></a>
		  	  </li>
		  	  <?php endforeach;?>	 		  	    	  
		  	</ul>
		  </div>
		  <div class="plugin-settings">
		      <div id="message-response">
		      </div>
		      <?php $response = $this->session->flashdata('response'); ?>
		      <?php if($response != null){ ?>
		        <?php if($response['status']){ ?>
		          <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <strong>Success!</strong> <?= $response['message'] ?>.
		          </div>
		        <?php } ?>
		      <?php } ?>
		      <div class="upload-plugin row hidden">
		        <p class="install-help">If you have a SD Plugin in a .zip format, upload it here and click install now.</p>
		        <form id="upload-plugin-form" class="wp-upload-form" enctype="multipart/form-data" method="post">
		        <input type="file" name="plugin-file" id="plugin-file">
		        <input type="submit" value="Install Now" class="button" id="install-plugin-submit" name="install-plugin-submit" disabled>
		        </form>
		      </div>
		  	<div class="plugin-settings-container"> 	  
		  	  <div class="plugin-1-page">		  	  	
		  	  	<?php if ($is_file_exists):?>
		  	  	  <?php include 'header.php';?>
			  	  <?php include $plugin_path . $file; echo $plugin_path; ?>
			  	  <?php include 'footer.php';?>
			  	<?php endif;?>
			  </div>
		  	</div>
		  </div>  
		</div>
		<!-- END PAGE CONTENT INNER -->
	</div>
</div>
<!-- END CONTENT --> 