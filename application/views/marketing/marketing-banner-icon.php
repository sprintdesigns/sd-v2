<?php $banner = $sd->marketing->get_all_banner(); ?>

<?php foreach ($banner as $index => $b) { ?>
  <li>
    <button class="open-marketing-banner">
       <img class="sd-marketing-banner" src="<?php echo $b->banner!=''? $b->banner:base_url().'sd-assets/images/preloaderimage.png'; ?>" />
        <input type="hidden" class="sd-marketing-description" value="<?= $b->description ?>" >
        <input type="hidden" class="sd-marketing-id" value="<?= $b->id ?>" >
        <input type="hidden" class="sd-marketing-url" value="<?= $b->page_url ?>" >
       <span class="sd-marketing-title" ><?= $b->title; ?></span>
    </button>
  </li>
<?php } ?>