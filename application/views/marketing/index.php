<div class="headerlayer">Marketing Banner
 <!-- <button data-toggle="modal" data-target="#modaldynamic" class="add-item mini"><i class="fa fa-plus-circle"></i> Add Page</button> -->
 <button class="layer-close" data-panel="6"><i class="fa fa-times"></i></button></div>
  <div class="layer-container-x">
    <div id="marketing-banner-response"></div>
    <div class="portbox relative">
      <ul class="marketing-banner-list">

        <?php
          echo $sd->marketing->get_banner_views($sd);
        ?>

      </ul>
      <div class="marketing-banner-settings">
        <div class="row">
          <div class="left-settings">
            <form method="POST" id="marketing-banner-form" >
              <div class="form-group">
                <label for="exampleInputEmail1">Title</label>
                <input type="text" class="form-control" id="marketing-title" name="marketing-title" aria-describedby="marketing-title-help"/>
                <label for="exampleInputEmail1">Landing Page</label>
                <select class="form-control input-small" id="sd-marketing-url" name="sd-marketing-url" >
                        <?php $pages = $sd->page->get_pages(); ?>
                        <?php foreach ($pages as $index => $page) { ?>
                          <option value="<?= base_url().$page->page_url ?>" <?= base_url().$page->page_url==$slider[0]->button_url? 'selected':'' ?> ><?= $page->page_title ?></option>
                        <?php } ?>
                </select>
                <input type="hidden" id="marketing-banner-id" name="marketing-banner-id" value="" >
                <input type="hidden" id="marketing-banner-img" name="marketing-banner-img" value="" >
                <input type="hidden" id="marketing-banner-url" name="marketing-banner-url" value="" >
                <small id="marketing-title-help" class="form-text text-muted">Title of your marketing banner*</small>
              </div>
              <div class="form-group">
                <label for="marketing-description">Marketing Description</label>
                <textarea class="form-control" id="marketing-description" name="marketing-description" rows="3"></textarea>
              </div>
              <button type="button" class="btn btn-primary" id="update-marketing-banner" >Save</button>

              <button id="facebookShareLink" class="pull-right btn" ><i class="fa fa-facebook-official" aria-hidden="true"></i>
</button>
              <button id="tweeterShareLink" class="pull-right btn" ><i class="fa fa-twitter-square" aria-hidden="true"></i>
</button>

            </form>
          </div>
          <div class="right-settings">
            <label>Banner Imager</label>
            <div class="marketing-banner-photo">
              <button class="btn-add-slide marketing-upload pull-right btn btn-success" >upload</button>
              <img src="" />
            </div>
          </div>
        </div>
      </div>
      <div class="slider-gallery-show">
        <h1>Choose Image</h1>
        <button class="slider-gallery-close" ><i class="fa fa-close"></i></button>
        <div class="slider-gallery-preloader">
          <style type='text/css'>@-webkit-keyframes uil-default-anim { 0% { opacity: 1} 100% {opacity: 0} }@keyframes uil-default-anim { 0% { opacity: 1} 100% {opacity: 0} }.uil-default-css > div:nth-of-type(1){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.5s;animation-delay: -0.5s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(2){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.4166666666666667s;animation-delay: -0.4166666666666667s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(3){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.33333333333333337s;animation-delay: -0.33333333333333337s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(4){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.25s;animation-delay: -0.25s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(5){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.16666666666666669s;animation-delay: -0.16666666666666669s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(6){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.08333333333333331s;animation-delay: -0.08333333333333331s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(7){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0s;animation-delay: 0s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(8){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.08333333333333337s;animation-delay: 0.08333333333333337s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(9){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.16666666666666663s;animation-delay: 0.16666666666666663s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(10){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.25s;animation-delay: 0.25s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(11){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.33333333333333337s;animation-delay: 0.33333333333333337s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(12){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.41666666666666663s;animation-delay: 0.41666666666666663s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}</style><div class='uil-default-css' style='transform:scale(0.22);'><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#fff;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'></div></div>
        </div>
        <div class="portlet-body">
            <div class="row">
              <div class="col-md-12">
                <form action="<?php echo base_url();?>index.php/upload/marketing_upload" class="dropzone" id="marketing_upload" name="marketing_upload" enctype="multipart/form-data" method="post">
                  
                  <div class="dragover-image"><div class="dragover-title">Drop Image Here!</div></div>
                </form>
              </div>
            </div>
          </div>
        <div id="marketing-gallery-container" >
        </div>
      </div>
    </div>

  </div>
</div>

