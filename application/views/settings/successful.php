<div class="site-wrapper">

  <div class="site-wrapper-inner">
    <img class="fixedimage hidden-xs" class="animated fadeInRight" src='<?= base_url(); ?>sprintdesigns.png'>
    <div class="container">
      <div class="row">
        <div class="col-md-7">
          <div class="cardbox animated fadeIn">
            
            <h1 class="spaced">Alright! Sprint Designer! Your website is now running! <br>Enjoy sdCMS!</h1>
            <a class='btn btn-success btn-lg btn-spaced' href='<?= base_url(); ?>edit?page=home'>Go to Dashboard</a>
            <a class='btn btn-info btn-lg btn-spaced' target="_blank" href='<?= base_url(); ?>'>View your Website</a>
          </div>  
        </div>
      </div>
    </div>
  </div>  
</div>

