<div class="site-wrapper">

  <div class="site-wrapper-inner">

    <img class="fixedimage hidden-xs" class="animated fadeInRight" src='<?= base_url(); ?>sprintdesigns.png'>
    <div class="container">
      <div class="row">
      	<div class="col-md-7 col-sm-7">
      	  <div class="cardbox animated fadeIn">
            <?php $user=$sd->user->get_current_user(); ?>
      	  	<h1>Hi! <br/>It seems that you don't have theme installed yet. Choose your desired theme.</h1>
      	  	<a class='btn btn-primary btn-lg' href='<?= base_url(); ?>index.php/settings/choose_theme'>Choose Theme</a>
      	  </div>	
      	</div>
      </div>
    </div>
  </div>

</div>

