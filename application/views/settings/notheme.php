<div class="site-wrapper">

  <div class="site-wrapper-inner">
    <!-- <img class="fixedimage hidden-xs" class="animated fadeInRight" src='<?= base_url(); ?>sprintdesigns.png'> -->
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div style="text-align:center;" class="cardbox animated fadeIn">
            
            <h1 class="spaced">Welcome to your website!<br/>It has no content yet so let us help you install your theme. <?php if(!$sd->user->check_if_login()) { ?> Log in to continue.<?php } ?></h1>
            <?php if($sd->user->check_if_login()) { ?>
            <a class='btn btn-success btn-lg btn-spaced w165' href='<?= base_url(); ?>index.php/settings/themes'>Login</a>  
            <?php } else { ?>  
            <a class='btn btn-success btn-lg btn-spaced w165' href='<?= base_url(); ?>index.php/sdlogin'>Login</a>
            <?php  } ?>
           <!--  <a class='btn btn-info btn-lg btn-spaced' target="_blank" href='<?= base_url(); ?>'>View your Website</a> -->
          </div>  
        </div>
      </div>
    </div>
  </div>  
</div>

