<body class="grayed scrollable sdcore-ux">
<div class="site-wrapper left-aligned">
  <div class="container">
    <div class="row heading20 center-aligned">
      <h1>Install your New Theme</h1>
    </div>
    <div class="alert alert-success download-theme hidden" style="text-align:center">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
   <div class="row">
      <div class="col-md-offset-4 col-sm-offset-5 col-xs-offset-7 col-md-8 col-sm-7 col-xs-5">
        <a class="btn btn-default btn-text btn-right" href="<?= base_url() ?>settings/themes/pick"><i class="fa fa-angle-left"></i> Go Back</a>
        </div>
    </div>
    <div class="row mtop35 sandbox ">
      <!--the thumb -->   
      <?php
      foreach ($explodetheme as $theme) {                    	
          if(array_key_exists("zip_file",$theme)) { ?>  
            <div class="col-md-3 col-sm-3 col-xs-6 template-thumb ">

              <div class="template-thumb-holder">
               
                <a class="temp-button" href="javascript:download_theme('<?php echo $theme->theme_folder; ?>', '<?php echo $theme->zip_file; ?>')">Choose</a>
                <a class="temp-button" target="_blank" href="<?php echo $theme->preview ?>">Preview</a>
                <span class="image-wrapper" style="background-image: url('<?php echo $theme->thumbnail ?>');"></span>
                <span class="template-name"><?php echo $theme->theme_folder;?></span>

              </div>
              
            </div>   
          <?php } else { ?>
            <div class="col-md-3 col-sm-3 col-xs-6 template-thumb ">
             <a class="valtemp disabled" href="javascript:void(0);">
                <span class="alert alert-success tagbox">Installed</span>
                <span class="image-wrapper" style="background-image: url('<?= base_url() ?>sd-includes/themes/<?= str_replace(' ', '-', $theme->name)."/".$theme->banner ?>');"></span>
                <span class="template-name"><?php echo $theme->name;?></span>
              </a>
            </div>	
          <?php }	
       } ?>         
      <!--end of thumb-->
    </div>
  </div>
</div>
<br /><br /><br />

<script type="text/javascript">

function download_theme(theme_name, theme_url) {

        if($('.alert').hasClass('hidden')) {
          $('.alert').removeClass('hidden');
        }
        else {
          $('.alert').addClass('hidden');
        }
    $('.installing-overlay.downloading-overlay').addClass('installing');   
    $.ajax({
      type:'GET',
      url:base_url+'index.php/settings/download_and_extract_theme',
      data:{id: theme_name, url: theme_url},
      beforeSend: function() { 
        //$('.download-theme').html('Downloading.....'); );
        $('.themedownloadprogress').css('width', '55%');
      },
      success:function(data) {
        $.ajax({   
          type:'GET',
          url:base_url+'index.php/sdtheme/install_and_activate_theme/' + theme_name,
          data:{},
          beforeSend: function() {
              //$('.download-theme').html('Installing.....');
              $('.installing-overlay.downloading-overlay .notificates h1').html('Installing theme to finish it up...');
              $('.installing-overlay.downloading-overlay .notificates p').html('Just sit back while we activate the theme for you.');
              $('.themedownloadprogress').css('width', '75%');
          },
          success:function(data) {
              $('.themedownloadprogress').css('width', '100%');
              $('.download-theme').html('<strong>Success!</strong> Done installing your new theme!');
              $('.installing-overlay.downloading-overlay .notificates h1').html('Success! Done installing your new theme!');
              $('.installing-overlay.downloading-overlay .notificates p').html('Please wait...');
              $('.circular').remove();
              setTimeout(function(){ 
                $('.installing-overlay.downloading-overlay').removeClass('installing');
                location.href=base_url; 
              }, 3100);      
            }
        });
      }
    });
}
      
</script>  


<!-- footer -->
    <script type="text/javascript">
      /* Global */
        var base_url = '<?php echo base_url(); ?>' + '';
        var site_url = '<?php echo base_url(); ?>' + '';
    </script>

   
    <script src="<?= base_url() ?>sd-assets/js/sdtemplate.js"></script>
    <script src="<?= base_url() ?>sd-assets/js/modal-dynamic.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!--<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->
   
    <script>    
      $('#modaldynamic').on('shown.bs.modal', function() {
       $("body.modal-open").removeAttr("style");
       $(this).removeAttr("style");
       $( this ).css( "display", "block" );
       $( this ).addClass( "fade" );
      });

      $('#modaldynamic').on('hidden.bs.modal', function() {
       $( this ).removeClass( "fade" );
      });      

    </script>
    <!--end for jouen scripts-->
</body>