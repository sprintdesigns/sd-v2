<div class="site-wrapper left-aligned">
  <div class="container">
    <div class="row heading20 center-aligned">
      <h1>Choose your Theme</h1>
      <div id="message-response">
      </div>
      <?php $response = $this->session->flashdata('response'); ?>
      <?php if($response != null){ ?>
        <?php if($response['status']){ ?>
          <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Success!</strong> <?= $response['message'] ?>.
          </div>
        <?php } ?>
      <?php } ?>

      <?php if ($notify_post_max || $notify_upload_max):?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          
          There might be a problem uploading themes. Change your php.ini: <br/>
              <?php if ($notify_post_max):?>
                post_max_size: <?php echo ini_get('post_max_size');?> (Current) | 30M (Recommended)<br />
              <?php endif;?>
              
              <?php if ($notify_upload_max):?>
                upload_max_filesize: <?php echo ini_get('upload_max_filesize');?> (Current) | 30M (Recommended)
              <?php endif;?>
        </div>                         
      <?php endif;?>

    </div>
    <div class="upload-theme row hidden">
      <p class="install-help">If you have a sdtheme in a .zip format, upload it here and click install now.</p>
      <form id="upload-template-form" class="wp-upload-form" enctype="multipart/form-data" method="post">
      <input type="file" name="template-file" id="template-file">
      <input type="submit" value="Install Now" class="button" id="install-theme-submit" name="install-theme-submit" disabled>
      </form>
    </div>
    <div class="row">
      <div class="col-md-8 col-sm-7 col-xs-5">
        <a class="btn btn-default btn-text btn-left" href="<?php echo base_url(); ?>"><i class="fa fa-angle-left"></i> Go to Homepage</a><br /><br />
        <a class="btn-hover btn btn-default" role="button" href="#search"><i class="fa fa-search"></i> Search</a>
      </div>
      <div class="col-md-4 col-sm-5 col-xs-7">
<!-- edited -->
         <a href="<?= base_url() ?>settings/choose_theme"><button class="btn btn-default btn-text btn-right">Choose Themes</button></a>
<!-- edited -->
        <button class="btn btn-default btn-text btn-right open-upload"><i class="fa fa-upload"></i> Upload Theme</button>
      </div>
    </div>
    <div class="row mtop35 sandbox">
      <!--the thumb -->
      <?php $active_theme_id = 0; ?>
      <?php 
        $theme = $sd->theme->get_all_themes();
        $theme_folders = $sd->theme->get_themes_by_folders();
      ?>
      <?php if($theme){ ?> 
        <?php if($sd->theme->get_active_theme()) {
              $active_theme = $this->sd->theme->get_active_theme(); 
              $active_theme_id = $active_theme[0]->id;
              $active_theme_name = $active_theme[0]->name;    
        ?>  
        <div class="col-md-3 col-sm-3 col-xs-6 template-thumb">
          <a class="valtemp modal-dynamic" data-to-pass="<?php echo $active_theme[0]->id ?>">
            <span class="tag">Active Theme</span>
            <span class="image-wrapper" style="background-image: url('<?= base_url() ?>sd-includes/themes/<?= str_replace(' ', '-', $active_theme[0]->name)."/".$active_theme[0]->banner ?>');"></span>
            <span class="template-name"><?php echo $active_theme[0]->name; ?></span>
          </a>
        </div>
        <?php } ?>
        <!-- show themes not active and not yet installed -->
        <?php
          $theme = $sd->theme->super_unique(array_merge($theme,$theme_not_installed));
        ?>
        <?php 
          if (isset($active_theme_name)){ 
            foreach ($theme as $t) { ?>
            <?php if ($t->name != $active_theme_name):?>
              <div class="col-md-3 col-sm-3 col-xs-6 template-thumb">
                <a class="valtemp modal-dynamic" href="#" data-toggle="modal" data-target="#modaldynamic" data-to-pass="<?= property_exists($t,'id')? $t->id:$t->name ?>">
                  <span class="tag">Theme Details</span>
                  <span class="image-wrapper" style="background-image: url('<?= base_url() ?>sd-includes/themes/<?= str_replace(' ', '-', $t->name)."/".$t->banner ?>');"></span>
                  <span class="template-name"><?= $t->name ?></span>
                </a>
              </div>
          <?php endif; 
          } 
        } 
        else {
          $theme = $sd->theme->get_all_themes();
          $theme_folders = $sd->theme->get_themes_by_folders();
          if (isset($theme_not_installed) && ! $theme) {
              $theme = $theme_not_installed;
          } else if(is_array($theme) && isset($theme_not_installed)) {
               $theme = $sd->theme->super_unique(array_merge($theme,$theme_not_installed));
          } 
          foreach ($theme as $t) { ?>
          <?php if ($t->name):?>
            <div class="col-md-3 col-sm-3 col-xs-6 template-thumb">
              <a class="valtemp modal-dynamic" href="#" data-toggle="modal" data-target="#modaldynamic" data-to-pass="<?= property_exists($t,'id')? $t->id:$t->name ?>">
                <span class="tag">Theme Details</span>
                <span class="image-wrapper" style="background-image: url('<?= base_url() ?>sd-includes/themes/<?= str_replace(' ', '-', $t->name)."/".$t->banner ?>');"></span>
                <span class="template-name"><?= $t->name ?></span>
              </a>
            </div>
        <?php endif; }}    
      }
      else {
        $theme = $sd->theme->get_all_themes();
        $theme_folders = $sd->theme->get_themes_by_folders();
        if (isset($theme_not_installed) && ! $theme) {
            $theme = $theme_not_installed;
        } else if(is_array($theme) && isset($theme_not_installed)) {
             $theme = $sd->theme->super_unique(array_merge($theme,$theme_not_installed));
        } 
        foreach ($theme as $t) { ?>
        <?php if ($t->name):?>
          <div class="col-md-3 col-sm-3 col-xs-6 template-thumb">
            <a class="valtemp modal-dynamic" href="#" data-toggle="modal" data-target="#modaldynamic" data-to-pass="<?= property_exists($t,'id')? $t->id:$t->name ?>">
              <span class="tag">Theme Details</span>
              <span class="image-wrapper" style="background-image: url('<?= base_url() ?>sd-includes/themes/<?= str_replace(' ', '-', $t->name)."/".$t->banner ?>');"></span>
              <span class="template-name"><?= $t->name ?></span>
            </a>
          </div>
      <?php endif;} ?>
        <?php } ?>
        </div>
      <!--end of thumb-->
    </div>
  </div>
  <div id="search">
    <button type="button" class="close">&times;</button>
    <form>
        <input type="search" value="" placeholder="What kind of theme do you like?" />
        <button type="submit" class="btn btn-primary">Search</button>
    </form>
  </div>

</div>
<!-- Modal -->

<?php
$theme = $sd->theme->get_all_themes();
$theme_not_installed = $sd->theme->get_themes_by_folders();
if (isset($theme_not_installed) && ! $theme) {
  $theme = $theme_not_installed;
} else if(is_array($theme) && isset($theme_not_installed)) {
  $theme = $sd->theme->super_unique(array_merge($theme,$theme_not_installed));
} 

$args = array(
    'title' => 'Theme',
    'object' => $theme,
    'type' => 'slider',
    'display' => array(
      'banner' => 'banner', 
      'description' => 'description', 
      'name' => 'name', 
      'author' => '', 
    ),
   'button' => array( // optional
      'Activate' => array(
          'type' => 'success',
          'function' => 'activateTheme',
        ),
      'Live Preview' => array(
        'type' => 'default',
        'function' => 'previewTheme',
      ), 
    )

  );
$sd->modal->dynamic($args);
?>
    

