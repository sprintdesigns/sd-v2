<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="">

    <title>SprintDesigns CMS Dashboard</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
    <!-- Bootstrap core CSS -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400" rel="stylesheet"> 
    <link href="<?= base_url(); ?>sd-assets/pluggables/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>sd-assets/themes-flow/animate.css" rel="stylesheet">    

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!--<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">-->

    <!-- Custom styles for this template -->
    <link href="<?= base_url(); ?>sd-assets/themes-flow/cover.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!--<script src="../../assets/js/ie-emulation-modes-warning.js"></script>-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<?= base_url(); ?>sd-assets/pluggables/plugins/jquery.min.js"></script>
     <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
    <script src="<?= base_url(); ?>sd-assets/pluggables/plugins/bootstrap/js/bootstrap.min.js"></script>
    
  </head>
  <?php if($indexpage) { ?>
 <body class="cover">
 <?php } else { ?>  
 <body class="grayed scrollable sdcore-ux">
 <?php } ?>
<div class="installing-overlay uploading-overlay">

<div class="centerme">
   <div class="notificates">
       <h1>Theme is uploading... Please Wait.</h1>
       <p>This will take a while. You may take a cup of coffee while watching your upload progress.</p>
   </div>
   <div class="progress">
      <div class="themeuploadprogress progress-bar progress-bar-success" role="progressbar" aria-valuenow="0"
      aria-valuemin="0" aria-valuemax="100">
        <span class="numericx">0</span>%
      </div>
    </div>
</div>

</div>

<div class="installing-overlay downloading-overlay">
<div class="centerme">
   <div class="notificates">
       <h1>Downloading theme. Please Wait.</h1>
       <p>This will take a while. You may take a cup of coffee while watching your download progress.</p>
   </div>
   <div class="progress">
      <div class="themedownloadprogress progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
    </div>
</div>
</div>