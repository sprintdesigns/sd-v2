    <script type="text/javascript">
      /* Global */
        var base_url = '<?php echo base_url(); ?>' + '';
        var site_url = '<?php echo base_url(); ?>' + '';
    </script>

   
    <script src="<?= base_url() ?>sd-assets/js/sdtemplate.js"></script>
    <script src="<?= base_url() ?>sd-assets/js/modal-dynamic.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!--<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->
    <script>
      $(function () {
        $('a[href="#search"]').on('click', function(event) {
            event.preventDefault();
            $('#search').addClass('open');
            $('#search > form > input[type="search"]').focus();
        });
        
        $('#search, #search button.close').on('click keyup', function(event) {
            if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
                $(this).removeClass('open');
            }
        });
        
        
        //Do not include! This prevents the form from submitting for DEMO purposes only!
        $('form').submit(function(event) {
            event.preventDefault();
            return false;
        })

    });
    </script>
    <!--for jouen scripts-->
    <script>
      $('.sample-btn-lang').click(function(){
        if($('.template-thumb').hasClass('hidden')){
          $('.template-thumb').removeClass('hidden');
        }
        else {
          $('.template-thumb').addClass('hidden');
          $('.sandbox div:first-child').removeClass('hidden');
        }
      });
      $('.valtemp').click(function(event){
        event.preventDefault();
      });
      $('#modaldynamic').on('shown.bs.modal', function() {
       $("body.modal-open").removeAttr("style");
       $(this).removeAttr("style");
       $( this ).css( "display", "block" );
       $( this ).addClass( "fade" );
      });

      $('#modaldynamic').on('hidden.bs.modal', function() {
       $( this ).removeClass( "fade" );
      });
      

      $('.open-upload').click(function(){
        if($('.upload-theme').hasClass('hidden')) {
          $('.upload-theme').removeClass('hidden');
        }
        else {
          $('.upload-theme').addClass('hidden');
        }
        
      });

    </script>
    <!--end for jouen scripts-->
  </body>
</html>