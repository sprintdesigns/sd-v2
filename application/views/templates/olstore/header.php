	<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.1
Version: 3.6.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Online Store</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<link href="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css"/>
<!-- <link href="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css"/> -->
<link href="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/morris/morris.css" rel="stylesheet" type="text/css">
<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN PAGE STYLES -->
<link href="<?php echo base_url() ?>/sd-assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE STYLES -->
<!-- BEGIN THEME STYLES -->
<!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
<link href="<?php echo base_url(); ?>/sd-assets/pluggables/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>/sd-assets/pluggables/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>/sd-assets/admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>/sd-assets/admin/layout4/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="<?php echo base_url(); ?>/sd-assets/css/dropdown.css" rel="stylesheet" type="text/css" >
<!-- notificate-->
<link href="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/jquery-notific8/jquery.notific8.min.css" type="text/css" rel="stylesheet">
<!--end of notificate -->
<link href="<?php echo base_url() ?>/sd-assets/admin/layout4/css/custom.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>sd-assets/custom-advanced.css" rel="stylesheet" type="text/css"/>

<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
<!--favicons-->
<link sizes="32x32" href="http://sprintdesignsph.com/wp-content/themes/twentysixteen-child/img/Favicon-SprintDesigns.png" rel="icon">
<link sizes="192x192" href="http://sprintdesignsph.com/wp-content/themes/twentysixteen-child/img/Favicon-SprintDesigns.png" rel="icon">
<link href="http://sprintdesignsph.com/wp-content/themes/twentysixteen-child/img/Favicon-SprintDesigns.png" rel="apple-touch-icon-precomposed">
<?php 
	if(isset($css)){
		foreach($css as $c){
			echo $c;
		}

	}
?>

<style type="text/css">
/*
	.dropdown {	position: relative; }
	.dropdown-content {position: absolute; left: 100%; top: 50%; margin-top: -2.5em; z-index: 10;} 
	.row {z-index: -1;}
	.row > div {z-index: -1;}
	.custom-grid {z-index: -1;}
	ul {list-style-type: none;}
	#settings_glyph {padding-left: 30%;} 
	*/
</style>

<!--end of favicons-->
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-header-fixed page-sidebar-closed-hide-logo page-sidebar-closed-hide-logo">
<div class="installing-overlay">
<div class="centerme">
   <div class="notificates">
       <h1>Please Wait. Uploading plugin...</h1>
       <p>This will take a while. You may take a cup of coffee while watching your upload progress.</p>
   </div>
   <div class="progress">
      <div class="themeuploadprogress progress-bar progress-bar-success" role="progressbar" aria-valuenow="0"
      aria-valuemin="0" aria-valuemax="100">
        <span class="numericx">0</span>%
      </div>
    </div>
</div>
</div>
<div class="full-loader">
  <div class="slider-gallery-preloader show">
    <div class="loadertext">Loading...</div>
    <img src="<?php echo base_url() ?>sd-assets/images/default.gif">
  </div>
</div>
<!-- BEGIN HEADER -->
<div class="sdlogin-bar">
    <ul class="maintools">
    	<li class="sdicon"><a href="<?php echo base_url(); ?>advanced/"><img src="<?php echo base_url(); ?>sd-assets/images/Logo_On_Black.png" alt="logo" class="logo-default" /></a></li>    	
    	<li>&nbsp;&nbsp;</li>
    	<li>&nbsp;&nbsp;</li>
    </ul>
    <ul class="maintools colorpicker">
        <li><a href="<?php echo base_url(); ?>edit?page=home" class="btn btn-primary"><i class="fa fa-chevron-left" aria-hidden="true"></i> Go back to website</a></li>
        <li><a target="_blank" href="<?php echo base_url(); ?>store" class="btn btn-primary">Go to online store</a></li>
    </ul>
    <ul class="profile-stat navbar-right">
		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">Welcome <span><?= (isset($sd->user->get_current_user()->username)) ? $sd->user->get_current_user()->username : 'superadmin';  ?></span></a>
			<ul class="dropdown-menu">
				<li class="boxed"><img src="<?php echo base_url(); ?>sd-assets/images/Favicon-SprintDesigns.png" /></li>
				<li style="text-align:center;"><?= (isset($sd->user->get_current_user()->username)) ? $sd->user->get_current_user()->username : 'superadmin';  ?></li>
				<!-- <li><a href="#">Edit my Profile</a></li> -->
				<!--<span><li><li style="text-align: left;"><a href="http://localhost/sdv2/advanced/plugins";>Settings</a></li></span>-->
				<li style="text-align: right; color: white"><a href="<?= base_url(); ?>index.php/dashboard/logout">Logout</a></li>
			</ul>
		</li>    
	</ul>
</div>
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar-wrapper">
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<div class="page-sidebar fixed-sidebar">
			<!-- BEGIN SIDEBAR MENU -->
			<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
			<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
			<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
			<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
			<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
			<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
			<ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
				<!-- <li class="start <?= $active_tab=='plugins'? 'active':'' ?>">
					<a href="<?= base_url() ?>advanced/plugins" >
					<span class="title"><i class="fa fa-puzzle-piece"></i> Plugins</span>
					</a> 
				</li>
				 -->

				<div><label id="progress-label" for="progress">Products: </label></div>
				
				<div class="progress">
  					<div class="progress-bar" role="progressbar" id="progress" aria-valuemin="0" aria-valuemax="100">
    					<span class="sr-only"></span>
  					</div>
				</div>
				
				<li class="start <?= $active_tab=='dashboard'? 'active':'' ?>">
					<a href="<?= base_url() ?>olstore/index" >

					<i class="icon-home"></i>
					Dashboard</a>
				</li>
				<li class="start <?= $active_tab=='orders'? 'active':'' ?>">
					<a href="<?= base_url() ?>olstore/orders" >
					<i class="icon-basket"></i>
					Orders</a>
				</li>
				<li class="start <?= $active_tab=='products'? 'active':'' ?>">
					<a href="<?= base_url() ?>olstore/products" >
					<i class="icon-handbag"></i>
					Products</a>
				</li>

				<!-- <li class="start <?= $active_tab=='settings'? 'active':'' ?> dropdown">
					<a href="javascript:;" class="dropbtn" data-toggle="dropdown" >
					<i class="icon-wrench"></i>
					Settings<span class="arrow"></span></a>
					<ul class="dropdown-content">
      					<li><a href="<?= base_url() ?>olstore/page">Purchase Message</a></li>
    				</ul>
				</li> -->
				<li class="start <?= $active_tab=='settings'? 'active':'' ?> dropdown">
					<a href="<?= base_url() ?>olstore/page" >
					<i class="icon-wrench"></i>
					Settings <span id="settings_glyph" class=""></span></a>
					<!--
						<ul class="dropdown-content">
	      					<li><a href="<?= base_url() ?>olstore/page">Purchase Message</a></li>
	      					<li><a href="#">Submenu 2</a></li>
	      					<li><a href="#">Submenu 3</a></li>
	      					<li><a href="#">Submenu 4</a></li>
	      					<li><a href="#">Submenu 5</a></li>      				
	    				</ul>
	    			-->
				</li>
				<!-- <li class="start ">
					<a href="javascript:;">
					<i class="icon-wrench"></i>
					<span class="title">Settings</span>
					<span class="arrow "></span>
					</a>
						<ul class="sub-menu">
							<li class="active">
								<a href="<?= base_url() ?>olstore/page">Purchase Message</a>
							</li>
						</ul>
				</li> -->

			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>
	<!-- END SIDEBAR -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript"> 
$(document).ready(function() {
	$.ajax({
		url: "<?= base_url() ?>olstore/getProductCount",
		type: 'POST',
		success: function(data) {				
			var jsonObj = JSON.parse(data);
			var products = parseInt(jsonObj["products"]);
			var total = parseInt(jsonObj["total"]);

			var percentage = products / total * 100;
			$('#progress').css('width', percentage + '%');
			$('#progress-label').append(products + ' / ' + total);
		}
	});
});
</script>
