<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="container">
	  <div class="row">
	    <div class="white-box">
		    <p>Welcome to sdCMS. Before getting started, we need some information on the database. You will need to know the following items before proceeding.</p>
		    <ol>
			  <li>Database name</li>
			  <li>Database username</li>
			  <li>Database password</li>
			  <li>Database host</li>
		    </ol>
		    <p>We’re going to use this information to create a connection on your database, if this doesn’t work, don’t worry. All this does is fill in the database information to a configuration file. You may also simply open <code>applications/config/config.php</code> in a text editor, fill in your information, and save the changes.
			Need more help? See our <a href="#">tutorials</a>.</p>
		    <p>In all likelihood, these items were supplied to you by your Web Host. If you don’t have this information, then you will need to contact them before you can continue. If you’re all ready…</p>
		    <p class="step"><a href="<?php echo base_url() ?>dashboard/setupdatabase/" class="button button-large">Let’s go!</a></p>
	    </div>
	  </div>
	</div>
</div>
<!-- END CONTENT -->