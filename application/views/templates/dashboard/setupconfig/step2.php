<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="container">
	  <div class="row">
	    <div class="white-box">
		    <form method="post" action="<?php echo base_url(); ?>dashboard/errordatabase">
				<p>Below you should enter your database connection details. If you’re not sure about these, contact your host.</p>
				<table class="form-table">
					<tbody><tr>
						<th scope="row"><label for="dbname">Database Name</label></th>
						<td><input name="dbname" id="dbname" size="25" value="wordpress" type="text"></td>
						<td>The name of the database you want to use with sdCMS.</td>
					</tr>
					<tr>
						<th scope="row"><label for="uname">Username</label></th>
						<td><input name="uname" id="uname" size="25" value="username" type="text"></td>
						<td>Your database username.</td>
					</tr>
					<tr>
						<th scope="row"><label for="pwd">Password</label></th>
						<td><input name="pwd" id="pwd" size="25" value="password" autocomplete="off" type="text"></td>
						<td>Your database password.</td>
					</tr>
					<tr>
						<th scope="row"><label for="dbhost">Database Host</label></th>
						<td><input name="dbhost" id="dbhost" size="25" value="localhost" type="text"></td>
						<td>You should be able to get this info from your web host, if <code>localhost</code> doesn’t work.</td>
					</tr>
				    </tbody>
				</table>
			    <input name="language" value="" type="hidden">
				<p class="step"><input name="submit" value="Submit" class="button button-large" type="submit"></p>
			</form>
	    </div>
	  </div>
	</div>
</div>
<!-- END CONTENT -->