<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="container">
	  <div class="row">
	    <div class="white-box">
			<p>All right, Sprint Designer! You’ve made it through this part of the installation. sdCMS can now communicate with your database. If you are ready, let us...</p>

			<p class="step"><a href="#" class="button button-large">Install your sdCMS site</a></p>
	    </div>
	  </div>
	</div>
</div>
<!-- END CONTENT -->