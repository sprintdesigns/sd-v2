<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="container">
	  <div class="row">
	    <div class="white-box">
	        <h1>Set up done!</h1>
			<p>You can now proceed to logging in to sdCMS platform to set up your theme. It will take you 5 minutes only!</p>

			<p class="step"><a href="<?= base_url() ?>sdlogin" class="button button-large">Let’s Start</a></p>
	    </div>
	  </div>
	</div>
</div>
<!-- END CONTENT -->