<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="container">
	  <div class="row">
	    <div class="white-box">
           <h1>Database has contents in it</h1>
           <p>It seems that the database has tables in it. To proceed, please clear your old database tables first then refresh this page.</p>
            <p class="step"><a href="<?php echo base_url(); ?>/sdlogin.php" class="button button-large">Refresh</a></p>
	    </div>
	  </div>
	</div>
</div>
<!-- END CONTENT -->