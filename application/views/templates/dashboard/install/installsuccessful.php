<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="container">
	  <div class="row">
	    <div class="white-box">
	      <h1>Success!</h1>
			<p>sdCMS has been installed. Thank you, and enjoy your website!</p>
			<table class="form-table install-success">
				<tbody><tr>
					<th>Username</th>
					<td>username here</td>
				</tr>
				<tr>
					<th>Password</th>
					<td>			<p><em>Your chosen password.</em></p>
					</td>
				</tr>
			</tbody></table>
			<p class="step"><a href="<?php echo base_url(); ?>sdlogin/" class="button button-large">Log In</a></p>
	    </div>
	  </div>
	</div>
</div>
<!-- END CONTENT -->