<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="container">
	  <div class="row">
	    <div class="white-box">
	        <div class="note note-danger note-shadow hide">
				<p>
					 You need to setup email address
				</p>
			</div>
			<div class="note note-danger note-shadow hide">
				<p>
					 You need to setup username
				</p>
			</div>
			<div class="note note-danger note-shadow hide">
				<p>
					 You need to setup password
				</p>
			</div>
			<p>Welcome to the 3 minute installation! Just fill in the information below and you’ll be on your way to using the most extendable and user friendly platform in the world.</p>

			<h2>Information needed</h2>
			<p>Please provide the following information. You can change these settings later anyway so don't panic.</p>

			<form id="setup" method="post" action="install.php?step=2" novalidate="novalidate">
				<table class="form-table">
					<tbody><tr>
						<th scope="row"><label for="weblog_title">Site Title</label></th>
						<td><input name="weblog_title" id="weblog_title" size="25" value="" type="text"></td>
					</tr>
					<tr>
						<th scope="row"><label for="user_login">Username</label></th>
						<td>
						<input name="user_name" id="user_login" size="25" value="" type="text">
							<p><span class="description important">Usernames can have only alphanumeric characters, spaces, underscores, hyphens, periods, and the @ symbol.</span></p>
									</td>
					</tr>
							<tr class="form-field form-required user-pass1-wrap">
						<th scope="row">
							<label for="pass1-text">
								Password				
							</label>
						</th>
						<td>
							<div class="form-group">
						        <div class="input-group">
						          <input type="text" class="form-control input-lg" rel="gp" data-size="32" data-character-set="a-z,A-Z,0-9,#">
						          <span class="input-group-btn"><button type="button" class="btn btn-default btn-lg getNewPass"><span class="fa fa-refresh"></span></button></span>
						        </div>
						      </div>
							<p><span class="description important">
							<strong>Important:</strong>
											You will need this password to log&nbsp;in. Please store it in a secure location.</span></p>
						</td>
					</tr>
					<tr class="form-field form-required user-pass2-wrap hide-if-js" style="display: none;">
						<th scope="row">
							<label for="pass2">Repeat Password					<span class="description">(required)</span>
							</label>
						</th>
						<td>
							<input name="admin_password2" id="pass2" autocomplete="off" type="password">
						</td>
					</tr>
					<tr class="pw-weak" style="display: none;">
						<th scope="row">Confirm Password</th>
						<td>
							<label>
								<input name="pw_weak" class="pw-checkbox" type="checkbox">
								Confirm use of weak password				</label>
						</td>
					</tr>
							<tr>
						<th scope="row"><label for="admin_email">Your Email</label></th>
						<td><input name="admin_email" id="admin_email" size="25" value="" type="email">
						<p><span class="description important">Double-check your email address before continuing.</span></p></td>
					</tr>
				</tbody></table>
				<p class="step"><input name="Submit" id="submit" class="button button-large" value="Install sdCMS" type="submit"></p>
				<input name="language" value="en_US" type="hidden">
			</form>
	    </div>
	  </div>
	</div>
</div>
<!-- END CONTENT -->