</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner">
		 2016 &copy; sdCMS by <a href="http://sprintdesignsph.com/"><img src='<?php echo base_url() ?>/sd-assets/admin/layout4/img/logo-sprint.png'></a>
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/respond.min.js"></script>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/excanvas.min.js"></script> 
<![endif]-->

 <?php 
 	if(isset($core_scripts)){
		foreach($core_scripts as $script){
			echo $script;
		}
	}
?>
<!-- END CORE PLUGINS -->
<!--<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/morris/morris.min.js" type="text/javascript"></script>-->
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/morris/raphael-min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->

<?php 
 	if(isset($scripts)){
		foreach($scripts as $script){
			echo $script;
		}
	}
?>
<!-- END PAGE LEVEL SCRIPTS -->
<!--notificate-->
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/jquery-notific8/jquery.notific8.min.js"></script>
<script src="<?php echo base_url() ?>/sd-assets/pluggables/scripts/welcomenotificate.js"></script>


<script>
jQuery(document).ready(function() {    
 UINotific8User.init();
});
</script>

    <script type="text/javascript">
      /* Global */
        var base_url = '<?php echo base_url(); ?>' + '';
        var site_url = '<?php echo base_url(); ?>' + '';
    </script>

   
    <script src="<?= base_url() ?>sd-assets/js/sdtemplate.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!--<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->
    <script>
      $(function () {
        $('a[href="#search"]').on('click', function(event) {
            event.preventDefault();
            $('#search').addClass('open');
            $('#search > form > input[type="search"]').focus();
        });
        
        $('#search, #search button.close').on('click keyup', function(event) {
            if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
                $(this).removeClass('open');
            }
        });
        
        
        //Do not include! This prevents the form from submitting for DEMO purposes only!
        $('form').submit(function(event) {
            event.preventDefault();
            return false;
        })

    });
    </script>
    <!--for jouen scripts-->
    <script>
      $('.sample-btn-lang').click(function(){
        if($('.template-thumb').hasClass('hidden')){
          $('.template-thumb').removeClass('hidden');
        }
        else {
          $('.template-thumb').addClass('hidden');
          $('.sandbox div:first-child').removeClass('hidden');
        }
      });
      $('.valtemp').click(function(event){
        event.preventDefault();
      });
      $('#modaldynamic').on('shown.bs.modal', function() {
       $("body.modal-open").removeAttr("style");
       $(this).removeAttr("style");
       $( this ).css( "display", "block" );
       $( this ).addClass( "fade" );
      });

      $('#modaldynamic').on('hidden.bs.modal', function() {
       $( this ).removeClass( "fade" );
      });
      

      $('.open-upload').click(function(){
        if($('.upload-theme').hasClass('hidden')) {
          $('.upload-theme').removeClass('hidden');
        }
        else {
          $('.upload-theme').addClass('hidden');
        }
        
      });

    </script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>