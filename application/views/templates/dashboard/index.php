<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Dashboard <small>Welcome User!</small></h1>
			</div>
			<!-- END PAGE TITLE -->
			<!-- BEGIN PAGE TOOLBAR -->
			<div class="page-toolbar">
				<!-- BEGIN THEME PANEL -->
				<div class="btn-group btn-theme-panel">
					<a href="javascript:;" class="btn dropdown-toggle" data-toggle="dropdown">
					<i class="icon-settings"></i>
					</a>
					<div class="dropdown-menu theme-panel pull-right dropdown-custom hold-on-click">
						<div class="row">
							<div class="col-md-4 col-sm-4 col-xs-12">
								<h3>THEME</h3>
								<ul class="theme-colors">
									<li class="theme-color theme-color-default" data-theme="default">
										<span class="theme-color-view"></span>
										<span class="theme-color-name">Dark Header</span>
									</li>
									<li class="theme-color theme-color-light active" data-theme="light">
										<span class="theme-color-view"></span>
										<span class="theme-color-name">Light Header</span>
									</li>
								</ul>
							</div>
							<div class="col-md-8 col-sm-8 col-xs-12 seperator">
								<h3>LAYOUT</h3>
								<ul class="theme-settings">
									<li>
										 Theme Style
										<select class="layout-style-option form-control input-small input-sm">
											<option value="square">Square corners</option>
											<option value="rounded" selected="selected">Rounded corners</option>
										</select>
									</li>
									<li>
										 Layout
										<select class="layout-option form-control input-small input-sm">
											<option value="fluid" selected="selected">Fluid</option>
											<option value="boxed">Boxed</option>
										</select>
									</li>
									<li>
										 Header
										<select class="page-header-option form-control input-small input-sm">
											<option value="fixed" selected="selected">Fixed</option>
											<option value="default">Default</option>
										</select>
									</li>
									<li>
										 Top Dropdowns
										<select class="page-header-top-dropdown-style-option form-control input-small input-sm">
											<option value="light">Light</option>
											<option value="dark" selected="selected">Dark</option>
										</select>
									</li>
									<li>
										 Sidebar Mode
										<select class="sidebar-option form-control input-small input-sm">
											<option value="fixed">Fixed</option>
											<option value="default" selected="selected">Default</option>
										</select>
									</li>
									<li>
										 Sidebar Menu
										<select class="sidebar-menu-option form-control input-small input-sm">
											<option value="accordion" selected="selected">Accordion</option>
											<option value="hover">Hover</option>
										</select>
									</li>
									<li>
										 Sidebar Position
										<select class="sidebar-pos-option form-control input-small input-sm">
											<option value="left" selected="selected">Left</option>
											<option value="right">Right</option>
										</select>
									</li>
									<li>
										 Footer
										<select class="page-footer-option form-control input-small input-sm">
											<option value="fixed">Fixed</option>
											<option value="default" selected="selected">Default</option>
										</select>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<!-- END THEME PANEL -->
			</div>
			<!-- END PAGE TOOLBAR -->
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		<ul class="page-breadcrumb breadcrumb hide">
			<li>
				<a href="#">Home</a><i class="fa fa-circle"></i>
			</li>
			<li class="active">
				 Dashboard
			</li>
		</ul>
		<!-- END PAGE BREADCRUMB -->
		<!-- BEGIN PAGE CONTENT INNER -->
		<div class="row margin-top-20">
		    <div class="col-md-12">
		      <div class="note note-info note-shadow bordered">
		        <img src='<?php echo base_url() ?>/sd-assets/admin/layout4/img/logo-sprint.png'>
				<p>
					 Hello! It seems you are not setup your website yet! Start to setup your website here.
				</p>
				<a class="btn btn-primary btn-right" target="_blank" href="#"><i class="icon-magic-wand"></i> Setup using Sprint Builder!</a>
				</div>
		    </div>
		    <div class="col-md-12">
		      <div class="note note-info note-shadow bordered">
		        <img src='<?php echo base_url() ?>/sd-assets/admin/layout4/img/logo-sprint.png'>
				<p>
					 Are you new to sdCMS? No need to worry! Check our free tutorials on how to create a beautiful website.
				</p>
				<a class="btn btn-danger btn-right" target="_blank" href="#"><i class="icon-control-play"></i> See our video tutorials!</a>
				</div>
		    </div>
		</div>
		<div class="tiles">
				<div class="tile double-down bg-blue-hoki">
					<div class="tile-body">
						<i class="fa fa-bell-o"></i>
					</div>
					<div class="tile-object">
						<div class="name">
							 Notifications
						</div>
						<div class="number">
							 6
						</div>
					</div>
				</div>
				<div class="tile bg-red-sunglo">
					<div class="tile-body">
						<i class="fa fa-calendar"></i>
					</div>
					<div class="tile-object">
						<div class="name">
							 Calendar
						</div>
						<div class="number">
							 12
						</div>
					</div>
				</div>
				<div class="tile double selected bg-green-turquoise">
					<div class="corner">
					</div>
					<div class="check">
					</div>
					<div class="tile-body">
						<h4>support@sprintdesignsph.com</h4>
						<p>
							 Re: Sprint Builder v1.2 - Update!
						</p>
						<p>
							Sprint Builder will be available on 24 Dec 2016
						</p>
					</div>
					<div class="tile-object">
						<div class="name">
							<i class="fa fa-envelope"></i>
						</div>
						<div class="number">
							 14
						</div>
					</div>
				</div>
				<div class="tile selected bg-yellow-saffron">
					<div class="corner">
					</div>
					<div class="tile-body">
						<i class="fa fa-user"></i>
					</div>
					<div class="tile-object">
						<div class="name">
							 Users
						</div>
						<div class="number">
							 0
						</div>
					</div>
				</div>
				<div class="tile double bg-blue-madison">
					<div class="tile-body">
						<img alt="" src="<?php echo base_url() ?>/sd-assets/admin/pages/media/profile/photo1.jpg">
						<h4>Announcements</h4>
						<p>
							 Easily style icon color, size, shadow, and anything that's possible with CSS.
						</p>
					</div>
					<div class="tile-object">
						<div class="name">
							 Marlito Dungog - CEO
						</div>
						<div class="number">
							 24 Jan 2013
						</div>
					</div>
				</div>
				<div class="tile image selected">
					<div class="tile-body">
						<img alt="" src="<?php echo base_url() ?>/sd-assets/admin/pages/media/gallery/image2.jpg">
					</div>
					<div class="tile-object">
						<div class="name">
							 Media
						</div>
					</div>
				</div>
				<div class="tile bg-green-meadow">
					<div class="tile-body">
						<i class="fa fa-comments"></i>
					</div>
					<div class="tile-object">
						<div class="name">
							 Comments
						</div>
						<div class="number">
							 12
						</div>
					</div>
				</div>
				<div class="tile double bg-grey-cascade">
					<div class="tile-body">
						<img class="pull-right" alt="" src="<?php echo base_url() ?>/sd-assets/admin/pages/media/profile/photo2.jpg">
						<h3>@karen_polinar</h3>
						<p>
							 I really love this CMS. I look forward to check the next release!
						</p>
					</div>
					<div class="tile-object">
						<div class="name">
							<i class="fa fa-twitter"></i>
						</div>
						<div class="number">
							 10:45PM, 23 Jan
						</div>
					</div>
				</div>
				<div class="tile bg-red-intense">
					<div class="tile-body">
						<i class="fa fa-coffee"></i>
					</div>
					<div class="tile-object">
						<div class="name">
							 New Features
						</div>
						<div class="number">
						</div>
					</div>
				</div>
				<div class="tile bg-green">
					<div class="tile-body">
						<i class="fa fa-bar-chart-o"></i>
					</div>
					<div class="tile-object">
						<div class="name">
							 SEO Reports
						</div>
						<div class="number">
						</div>
					</div>
				</div>
				<div class="tile bg-blue-steel">
					<div class="tile-body">
						<i class="fa fa-briefcase"></i>
					</div>
					<div class="tile-object">
						<div class="name">
							 Sprint Builder News
						</div>
						<div class="number">
							 2
						</div>
					</div>
				</div>
				<div class="tile image double selected">
					<div class="tile-body">
						<img alt="" src="<?php echo base_url() ?>/sd-assets/admin/pages/media/gallery/image4.jpg">
					</div>
					<div class="tile-object">
						<div class="name">
							 Slider Gallery
						</div>
						<div class="number">
							 0
						</div>
					</div>
				</div>
				<div class="tile bg-yellow-lemon selected">
					<div class="corner">
					</div>
					<div class="check">
					</div>
					<div class="tile-body">
						<i class="fa fa-cogs"></i>
					</div>
					<div class="tile-object">
						<div class="name">
							 Settings
						</div>
					</div>
				</div>
				<div class="tile bg-red-sunglo">
					<div class="tile-body">
						<i class="fa fa-plane"></i>
					</div>
					<div class="tile-object">
						<div class="name">
							 Projects
						</div>
						<div class="number">
							 34
						</div>
					</div>
				</div>
			</div>
		<!-- END PAGE CONTENT INNER -->
	</div>
</div>
<!-- END CONTENT -->