<html>
<head>
	<link href="<?php echo base_url(); ?>/sd-assets/css/custom.css" rel="stylesheet" type="text/css"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<link href="<?php echo base_url() ?>/sd-assets/css/validationEngine.jquery.css" rel="stylesheet" type="text/css"/>

	<link href="https://fonts.googleapis.com/css?family=Lato|Ubuntu" rel="stylesheet"> 
	<link href="https://fonts.googleapis.com/css?family=Archivo+Black" rel="stylesheet"> 
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> 
	<link sizes="32x32" href="http://sprintdesignsph.com/wp-content/themes/twentysixteen-child/img/Favicon-SprintDesigns.png" rel="icon">
	<link sizes="192x192" href="http://sprintdesignsph.com/wp-content/themes/twentysixteen-child/img/Favicon-SprintDesigns.png" rel="icon">
	<title>Online Store</title>
</head>
<body class="<?php echo $theme_color;?>">	
	<div id="header">
		<div class="header-graydiv container-fluid header-banner">
			<div class="row">
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="align-img"><!--thisisthe change-->
						<?php echo $sd->page->get_logo($header);?>					
					</div>
				</div>
				<!-- <div class="col-md-7 col-sm-6 col-xs-12">
					<div class="aliging">
					<input type="text" class="searchbox" name="Search" placeholder="Search here...">
					<button type="button" class="btnsearch"><i class="fa fa-search fa-lg" aria-hidden="true"></i></button>
					</div>
				</div>
				<div class="col-md-2 col-sm-3 col-xs-12">
					<div class="aliging">
					<h1 class="adminname">Hello Admin123</h1>
					</div>
				</div> -->
			</div>
		</div>
		<nav class="navbar navbar-inverse">
		  <div class="container-fluid">
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>                        
		      </button>
		      
		    </div>
		    <div class="collapse navbar-collapse" id="myNavbar">
		      <!-- <ul class="nav navbar-nav navbar-center">
		        <li><a href="#">HOME</a></li>
		        <li><a href="#">ABOUT US</a></li>
		        <li><a href="#">PRODUCT'S INFORMATION</a></li>
		        <li><a href="#">CONTACT US</a></li>
		        <li><a href="#">PROMO</a></li>
		        <li><a href="#">STORE</a></li>
		      </ul> -->
		      <ul class="nav navbar-nav navbar-center">
		      	<li><a href="<?php echo base_url('/'); ?>">Home</a></li>
		      	<li><a href="<?php echo base_url('store'); ?>">Categories</a></li>
		      	<li><a href="<?php echo base_url('store/category_items'); ?>">Products</a></li>
		      </ul>
		    </div>
		  </div>
		</nav>
	</div>