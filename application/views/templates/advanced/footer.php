</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->

<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/respond.min.js"></script>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/excanvas.min.js"></script> 
<![endif]-->

 <?php 
 	if(isset($core_scripts)){
		foreach($core_scripts as $script){
			echo $script;
		}
	}
?>
<!-- END CORE PLUGINS -->
<!--<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/morris/morris.min.js" type="text/javascript"></script>-->
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/morris/raphael-min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/sd-assets/pluggables/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->

<?php 
 	if(isset($scripts)){
		foreach($scripts as $script){
			echo $script;
		}
	}
?>
<!-- END PAGE LEVEL SCRIPTS -->
<!--notificate-->
<script src="<?php echo base_url(); ?>sd-assets/pluggables/plugins/jquery-notific8/jquery.notific8.min.js"></script>
<script src="<?php echo base_url() ?>sd-assets/pluggables/scripts/welcomenotificate.js"></script>
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script>
 function preloader(status) {
  if(status == true) {
  	$('.full-loader').addClass('loading');  	
  }
  else {
  	$('.full-loader').removeClass('loading');
  }
}	
</script>

<script>
$(document).ready(function(){
	$('.showuploadplugin').click(function() {
		$('.upload-plugin').removeClass('hidden');
	});
	$("#plugin-file").change(function(){
		message_response('clean','');
		var ext = $('#plugin-file').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['zip']) == -1) {
		    message_response(false,'Invalid File Type');
		}else
		{
			$("#install-plugin-submit").removeAttr('disabled');
		}
	});

	$("#upload-plugin-form").submit(function(){
        $('.installing-overlay').addClass('installing');
		var data = new FormData(document.getElementById('upload-plugin-form'));
        $('#install-plugin-submit').attr('disabled', 'disabled');
		$.ajax({
			xhr: function() {
		        var xhr = new window.XMLHttpRequest();
		        // Upload progress
		        xhr.upload.addEventListener("progress", function(evt){
		            if (evt.lengthComputable) {
		                var percentComplete = evt.loaded / evt.total;
		                //Do something with upload progress
		                console.log(percentComplete);
		                $('.themeuploadprogress').css('width', Math.floor((percentComplete * 100)) + '%');
		                $('.numericx').html(Math.floor((percentComplete * 100)));
		            }
		       }, false);
		       return xhr;
		    },
			type:'POST',
			url:base_url+'advanced/upload_plugin',
			data:data,
			processData: false,
 			contentType: false,
 			dataType:'json',
 			success:function(data)
 			{
 				 if(data.stop == true) {
 				   $('.installing').hide();
 				   message_response(data.status,data.message);
 				 }
 				 
				if(data.stop == false) {
				   $("#install-plugin-submit").removeAttr('disabled');
 				   $('.notificates > h1').html('Plugin Successfully Installed!');
 				   $('.notificates > p').html('Please wait a little more, we are readying the plugin for you. In <span class="counterx"></span>.');
 				   $('.progress').remove();
 				   var counter = 4;
				   var interval = setInterval(function() {
					    counter--;
					    $('.counterx').html(counter);
					    // Display 'counter' wherever you want to display it.
					    if (counter == 0) {
					        // Display a login box
					        clearInterval(interval);
					    }
					}, 1000);	
 				   setTimeout(function(){
 				   	$('.upload-plugin').addClass('hidden');
 				 	location.href=base_url+"advanced/plugins";	
 				   }, 3100);
 				}   				 
 			}
		});

		return false;
	});

	
});

function message_response(status,message)
{
	if(status == 'clean')
	{
		$("#message-response").html('');
	}else
	{
		if(status)
			$("#message-response").html('<div class="alert alert-success"><strong>Success!</strong> '+message+'.</div>');
		else
			$("#message-response").html('<div class="alert alert-danger"><strong>Opps!</strong> '+message+'.</div>');
	}
	
}
</script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>