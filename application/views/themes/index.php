<?php include 'header.php';?>

		<form method="get" action="<?php base_url();?>themes">
			<div class="form-inline">
				<input type="hidden" name="action" value="edit">
				<div class="form-group">
					<label for="theme_id">Select theme to edit:</label>
					<select name="theme_id" class="form-control">	
					<?php foreach ($themes as $t):?>		
						<option value="<?php echo $t->id;?>" <?php echo ($t->id == $active_theme_id) ? 'selected' : '' ;?>>
						<?php echo $t->name;?> <?php echo ($t->id == $active_theme_id) ? '(Active)' : '' ;?>

						</option>		
					<?php endforeach;?>
					</select>
					<button class="btn btn-primary" type="submit">Select</button>
				</div>
			</div>
		</form>

<?php include 'footer.php';?>