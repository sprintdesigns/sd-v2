<?php include 'header.php';?>

    <link rel ="stylesheet" type = "text/css" href = "<?php echo base_url();?>sd-assets/css/edit-page.css">
    <script src="<?php echo base_url();?>sd-assets/js/jquery.min.js"></script>

    <?php //  $action = base_url()."advanced/themes?action=edit&theme_id={$theme_id}"; ?>

    <!--SELECT THEME TO EDIT OPTION -->

    <form class="select-theme-form" method="get" action="<?php base_url();?>themes">
        <div class="form-inline">
            <input type="hidden" name="action" value="edit">
            <div class="form-group">
                <label for="theme_id">Select theme to edit:</label>
                <select name="theme_id" class="form-control">  
                <?php foreach ($themes as $t):?>                           
                    <option value="<?php echo $t->id;?>" <?php echo ($t->id == $theme_id) ? 'selected' : '' ;?>>
                    <?php echo $t->name;?> <?php echo ($t->id == $active_theme_id) ? '(Active)' : '' ;?>              
                    </option>       
                <?php endforeach;?>
                </select>
                <button class="btn btn-primary" type="submit">Select</button>                
            </div>
        </div>
    </form>
    <!--END -->

    <div class="editor-files-container">
        <div class="row">
            <div class="col-sm-3 pull-right">
                <h3 class="editor-file-selected">Files</h3>
                <?php
                $name = $theme->name;
                $dir_cur = getcwd() .'\sd-includes\themes\\' . $name;
                $action = base_url()."advanced/themes?action=edit&theme_id={$theme_id}";
                function newDir($curD, $key) {
                    $newD = $curD."\\".$key;
                    return $newD;
                }
                function scan($curD){
                    $files = scandir($curD);   
                    return $files;
                }
                function disp($files, $curD){
                    echo '<ul class="list-folders">';
                    foreach($files as $key){
                        if($key != "." && $key != ".."){
                            $tempD = newDir($curD, $key);
                            if(is_dir($tempD)){?>
                            <li>
                                <button class ="accordion">
                                    <i class="fa fa-folder" aria-hidden="true"></i>
                                    <i class="fa fa-folder-open" aria-hidden="true"></i>
                                    <?php echo $key; ?>
                                </button>
                                <div class="panel">
                              <?php
                                $filesSec = scan($tempD);
                                disp($filesSec, $tempD);?>
                                </div>
                            </li>
                            <?php } else { ?>
                            <li>
                                <form method="post" action="<?php $action?>" >
                                    <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                       <input class="list-button" type="submit" name="file" value = "<?php echo htmlspecialchars($key); ?>">
                                       <input type="hidden" name="dir" value = "<?php echo htmlspecialchars($tempD)?>">
                                </form>
                            </li>
                            <?php } 
                        } 
                    }
                    echo '</ul>';
                }
                $files = scan($dir_cur);
                disp($files, $dir_cur); 
                $filename = 'None';
                if(isset($_POST['file'])){
                     $directory = $_POST['dir'];
                     $filename = $_POST['file'];
                } ?>

                <script type="text/javascript" src ="<?php echo base_url();?>sd-assets/js/accordion.js"></script>
            </div>
            <div class="col-sm-9">
                <h3 class="editor-file-selected"><?php echo $theme->name;?>: <?php echo "(".$filename.")"?></h3>
                <?php if(isset($_POST['file'])){ $content="<code>".htmlspecialchars(file_get_contents($directory))."</code>"; ?>         
                    <div id="editor" style="height: 500px; width: 100%;"><?php echo $content; ?></div>
                    <form action="<?php $action?>" method="post">
                        <textarea class="col-md-6" name="content" id="content" rows="1" cols="1" enable="enable" style="display: none;"></textarea>
                        <input name="location" type="hidden" value="<?php echo htmlspecialchars($directory) ?>"/>
                        <input class="btn btn-primary" name="save" type="submit" value="Save"/>
                    </form>
                <?php } if(isset($_POST['save'])){?>
                    <?php
                        if(isset($_POST["content"])) {
                            $content1 = $_POST["content"];
                            $file = $_POST["location"];
                            file_put_contents($file, $content1);
                        }
                        $content="<code>".htmlspecialchars(file_get_contents($file))."</code>";
                    ?>
                    <div id="editor" style="height: 500px; width: 100%;"><?php echo htmlspecialchars($content1); ?></div>
                <?php }else{?>
                    <div id="editor" style="height: 500px; width: 100%;"></div>
                <?php } ?> 
                <script src="<?php echo base_url();?>sd-assets/js/ace.js"></script>
                <script type="text/javascript" src ="<?php echo base_url();?>sd-assets/js/ace_editor.js"></script>
            </div>
        </div>
    </div>

<?php include 'footer.php';?>