<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Media <small>Library</small>  

				<small>
					<a href="#" id='btn_aimg' class="btn-sm btn-primary">Add new</a>
					<a href="#" id='btn_cimg' class="btn-sm btn-danger hidden">Cancel</a>
				</small>

				 </h1>
				
			</div>
		</div>

		<!-- start form upload-->
		<div id="upload_container" class="portlet light hidden">
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-12">
						<p>
							<span class="label label-danger">
							NOTE: </span>
							&nbsp; This plugins works only on Latest Chrome, Firefox, Safari, Opera & Internet Explorer 10.
						</p>
						<form action="<?php echo base_url();?>index.php/upload/do_upload" class="dropzone" id="user_upload" name="user_upload" enctype="multipart/form-data" method="post">
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- end of form upload -->

		<?php if(isset($media_data)){ ?>
			
			<?php $ctr = 0;?>
			<?php foreach($media_data as $d){ ?>
				<?php if($ctr == 0){ ?>
					<div class="row">
				<?php }?>
					<div class="col-md-3">
						<a href="<?php $sd->image->create_full_path($d['data']->upload_data->full_path); ?>" target="_blank">
							<img class="img-responsive" src="<?php $sd->image->create_full_path($d['data']->upload_data->full_path); ?>"> 
						</a>
					</div>
					<?php $ctr++;?>
				<?php if($ctr == 4){ ?>
					</div> <br> <?php $ctr=0;?>
				<?php }?>
			<?php }?>
			
		<?php }?>
<!-- 
		<div class="row">
			<div class="col-md-3">
				<a href="images/Chrysanthemum.jpg">
					<img class="img-responsive" src="<?php echo base_url();?>sd-contents/uploads/img/2016/10/Chrysanthemum.jpg">
				</a>
			</div>
			<div class="col-md-3">
				<a href="images/Chrysanthemum.jpg">
					<img  class="img-responsive" src="<?php echo base_url();?>sd-contents/uploads/img/2016/10/Chrysanthemum.jpg">
				</a>
			</div>
			<div class="col-md-3">
				<a href="images/Chrysanthemum.jpg">
					<img class="img-responsive" src="<?php echo base_url();?>sd-contents/uploads/img/2016/10/Chrysanthemum.jpg">
				</a>
			</div>
			<div class="col-md-3">
				<a href="images/Chrysanthemum.jpg">
					<img class="img-responsive" src="<?php echo base_url();?>sd-contents/uploads/img/2016/10/Chrysanthemum.jpg">
				</a>
			</div>
		</div> -->

	</div>
</div>

