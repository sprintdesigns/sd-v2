  <?php
      $slider = $sd->banner->get_slider();
      $media_data = $sd->gallery->process_media_data( $sd->image->get_media() );
  ?>
 <?php if(isset($media_data)) { ?>
       
  <?php foreach($media_data as $d) { ?>
   
   <?php
      $use = 'false';

      if($slider)
      {
        foreach ($slider as $index => $slide) {
            if($sd->image->create_full_path($d['data']->upload_data->full_path,true,true) == $slide->original_url)
            {
              $use = 'true';
            }
        }
      }

   ?>
    <li class="imglicontainer">
      <button class="btn btn-danger btn-delete-image-func gallery-img-<?= $d['id'] ?>" style="position: absolute; top: 0px; right: 0px; width: 0px; height: 22px; padding: 0px 14px 3px 6px;z-index: 1;" onclick="deleteImage(this,'<?= $d['id'] ?>')" data-use="<?= $use ?>" >&times;</button>
      <a href="javascript:viewImage('<?php $sd->image->create_full_path_web($d['data']->upload_data->full_path); ?>');">
        <div class="imagebox">
          <div class="centered">
            <img class="lazy-image img-responsive" src="<?php $sd->image->create_full_path_thumb($d['data']->upload_data->full_path); ?>"> 
          </div>
        </div>    
      </a>
    </li>
  <?php  } ?>  

<?php } ?>