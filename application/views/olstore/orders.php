<style type="text/css">

.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Orders</h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		<div class="page-bar">
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="<?= base_url() ?>olstore">Online Store</a><i class="fa fa-circle"></i>
			</li>
			<li class="active">
				 Orders
			</li>
		</ul>
		</div>
		<!-- END PAGE BREADCRUMB -->
		<!-- BEGIN PAGE CONTENT INNER -->
		<div class="custom-grid margin-top-20">

		  	<?php if($flashdata) { ?>
		  			<?php if($flashdata['status'] == true) { ?>
							<div class="alert alert-success alert-dismissable">
							  <div style="text-align: right !important; float: right;"><a href="#" class="remove" data-dismiss="alert" aria-label="close">&times;</a></div>
							  <?php echo $flashdata['message']; ?>
							</div>								
					<?php } else { ?>
							<div class="alert alert-warning alert-dismissable">
							  <div style="text-align: right !important; float: right;"><a href="#" class="remove" data-dismiss="alert" aria-label="close">&times;</a></div>
							  <?php echo $flashdata['message']; ?>
							</div>
					<?php } ?>
			<?php } ?>		
  
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box grey-cascade">
						<div class="portlet-title">

							<div class="caption">
								<i class="fa fa-shopping-cart"></i>Order Listing
							</div>
							
							<div class="actions">
								<a href="javascript:void(0)" data-toggle="modal" data-target="#add-order-modal" class="btn default yellow-stripe">
								<i class="fa fa-plus"></i>
								<span class="hidden-480">
								New Order </span>
								</a>

								<!-- <div class="btn-group">
									<a class="btn default yellow-stripe dropdown-toggle" href="#" data-toggle="dropdown">
									<i class="fa fa-share"></i>
									<span class="hidden-480">
									Tools </span>
									<i class="fa fa-angle-down"></i>
									</a>
									<ul class="dropdown-menu pull-right">
										<li>
											<a href="#">
											Export to Excel </a>
										</li>
										<li>
											<a href="#">
											Export to CSV </a>
										</li>
										<li>
											<a href="#">
											Export to XML </a>
										</li>
										<li class="divider">
										</li>
										<li>
											<a href="#">
											Print Invoices </a>
										</li>
									</ul>
								</div> -->

							</div>

						</div>
						<div class="portlet-body">

							<table class="table table-striped table-bordered table-hover" id="olstore_dtatable">
							<thead>
							<tr>
								<th>
									Order #
								</th>
								<th>
									Purchased On
								</th>
								<th>
									 Customer
								</th>
								<th>
									 Price
								</th>
								<th>
									 Status
								</th>
								<th>
									Action
								</th>
							</tr>
							</thead>
							<tbody>
							<?php if ($orders != false):?>
							<?php foreach($orders as $o_key => $o) { ?>
							<tr class="odd gradeX">
								<td>
									<?php echo $o->id; ?>
								</td>
								<td>
									<?php date_default_timezone_set("Asia/Manila"); echo date("F d, Y h:i A", strtotime($o->purchase_date)); ?>
								</td>
								<td>
									<?php echo $o->cust_name; ?>
								</td>
								<td class="center">
									PHP <?php echo number_format($o->price * $o->quantity, 2); ?>
								</td>
								<td>
									<input type="hidden" name="oid" id="oid" value="<?php echo $o->id; ?>">
									<?php if($o->status == "Pending") { ?>
									<select class="dropdown_status_<?php echo $o->id; ?> label-primary" name="stat" id="statusdropdown" ><div id="wait" style="display:none;width:69px;height:89px;border:1px solid black;position:absolute;top:50%;left:50%;padding:2px;"><img src='demo_wait.gif' width="64" height="64" /><br>Saving..</div>
										<option class="label label-sm label-primary" value="Pending" selected="selected">PENDING</option>
										<option class="label label-sm label-warning" value="Closed">CLOSED</option>
										<option class="label label-sm label-success" value="Paid">PAID</option>
									</select>
									<?php } ?>
									<?php if($o->status == "Paid") { ?>
									<select class="dropdown_status_<?php echo $o->id; ?> label-success" name="stat" id="statusdropdown" onchange="updateStatus(<?php echo $o->id; ?>, this.value)"><div id="wait" style="display:none;width:69px;height:89px;border:1px solid black;position:absolute;top:50%;left:50%;padding:2px;"><img src='demo_wait.gif' width="64" height="64" /><br>Saving..</div>
										<option class="label label-sm label-primary" value="Pending">PENDING</option>
										<option class="label label-sm label-warning" value="Closed">CLOSED</option>
										<option class="label label-sm label-success" value="Paid" selected="selected">PAID</option>
									</select>
									<?php } ?>
									<?php if($o->status == "Closed") { ?>
									<select class="dropdown_status_<?php echo $o->id; ?> label-warning" name="stat" id="statusdropdown" onchange="updateStatus(<?php echo $o->id; ?>, this.value)"><div id="wait" style="display:none;width:69px;height:89px;border:1px solid black;position:absolute;top:50%;left:50%;padding:2px;"><img src='demo_wait.gif' width="64" height="64" /><br>Saving..</div>
										<option class="label label-sm label-primary" value="Pending">PENDING</option>
										<option class="label label-sm label-warning" value="Closed" selected="selected">CLOSED</option>
										<option class="label label-sm label-success" value="Paid">PAID</option>
									</select>
									<?php } ?>
								</td>
								<td>
									<a href="<?= base_url() ?>olstore/orderView/<?php echo $o->id; ?>" class="btn green btn-xs"><i class="glyphicon glyphicon-eye-open"></i></a>
									<a href="javascript:void(0);" title="Delete Order" data-toggle="modal" data-target="#del-modal-<?php echo $o->id; ?>" class="btn red btn-xs"><i class="glyphicon glyphicon-trash"></i></a>				
									<!-- <a href="#" class="btn yellow btn-xs"><i class="glyphicon glyphicon-edit"></i></a> -->				
								</td>
							</tr>

							<div id="del-modal-<?php echo $o->id; ?>" class="modal fade" role="dialog">
								<form action="<?= base_url() ?>olstore/deleteOrder/<?php echo $o->id; ?>">
								<div class="modal-dialog">
								    <div class="modal-content">
									    	<div class="modal-header">
										        <button type="button" class="close" data-dismiss="modal">&times;</button>
										        <h4 class="modal-title">Confirmation</h4>
									      	</div>

								      		<div class="modal-body">
								        		<p>Are you sure you want to delete this order?</p>
								      		</div>
								      	<div class="modal-footer">
								      		<input type="submit" class="btn btn-default" value="Delete">
								        	<button type="button" class="btn btn-red" data-dismiss="modal">Close</button>
								      	</div>
								    </div>
								</div>
							</form>
							</div>

							<?php } ?>
							<?php endif;?>
							</tbody>
							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
		</div>			
		<!-- END PAGE CONTENT INNER -->
	</div>
</div>
<!-- END CONTENT --> 

<!-- Add New Order Form Popup -->
<div class="modal fade bs-modal-lg" id="add-order-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<form action="<?= base_url() ?>olstore/addOrder" method="post">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Add New Order</h4>
				</div>
				<div class="modal-body">
					<form role="form">
						<div class="form-body">
							<div class="form-group">

								<div class="row">
									<div class="form-group">
										<label class="col-md-4">Customer Email: <span class="required">*</span></label>
										<div class="col-md-12">
											<input type="text" id="customer_email" name="customer_email" class="form-control" placeholder="Customer Email">
										</div>
									</div>
								</div>

								<br />

								<div class="row">
									<div class="form-group">
										<label class="col-md-4">Customer Name: <span class="required">*</span></label>
										<div class="col-md-12">
											<input type="text" id="customer_name" name="customer_name" class="form-control" placeholder="Customer Name">
										</div>
									</div>
								</div>

								<br />

								<div class="row">
									<div class="form-group">
										<label class="col-md-4">Customer Address: <span class="required">*</span></label>
										<div class="col-md-12">
											<textarea id="customer_address" name="customer_address" class="form-control"></textarea>
										</div>
									</div>
								</div>

								<br />

								<div class="row">
									<div class="form-group">
										<label class="col-md-4">Phone Number: <span class="required">*</span></label>
										<div class="col-md-12">
											<input type="text" id="phone_number" name="phone_number" class="form-control" placeholder="Phone Number">
										</div>
									</div>
								</div>
						
								<br />
						
								<div class="row">
									<div class="form-group">
										<label class="col-md-4">Mode of Payment: <span class="required">*</span></label>
										<div class="col-md-12">		
											<select class="form-control" id="mode_of_payment" name="mode_of_payment">
												<option value="Cash">Cash</option>
												<option value="Money Transfer/Bank Deposit">Money Transfer/Bank Deposit</option>
											</select>
										</div>
									</div>
								</div>

								<br />

								<div class="row"> <!-- -->
									<div class="form-group">
										<label class="col-md-4">Product : </label>
										<div class="col-md-12">
											<select class="form-control" id="product_id" name="product_id">
												<option value="0">Choose Product Here</option>
												<?php $query_products = $this->db->select(array('id','name'))->get('products'); ?>
												<?php if ($query_products->num_rows() != 0) { ?>
												  	<?php foreach ($query_products->result() as $product) { ?>
														<option value="<?php echo $product->id; ?>"><?php echo $product->name; ?></option>
													<?php } ?>
												<?php } ?>
											</select><input type="hidden" id="product_name" name="product_name">
										</div>
									</div>
								</div>

								<br />

								<div class="row"> <!-- -->
									<div class="form-group">
										<label class="col-md-4">Product Price: </label>
										<div class="col-md-12">
											<input type="text" id="product_price" name="product_price" class="form-control" readonly>
										</div>
									</div>
								</div>

								<br />

								<div class="row"> <!-- -->
									<div class="form-group">
										<label class="col-md-4">Quantity: <span class="required">*</span></label>
										<div class="col-md-12">		
											<select id="product_quantity" name="product_quantity" class="form-control" readonly>
											</select>
										</div>
									</div>
								</div>
								
								<br />

								<div class="row"> <!-- -->
									<div class="form-group">
										<label class="col-md-4">Product Color: <span class="required">*</span></label>
										<div class="col-md-12">		
											<select id="product_color" name="product_color" class="form-control" readonly>
											</select>
										</div>
									</div>
								</div>

								<br />

								<div class="row"> <!-- -->
									<div class="form-group">
										<label class="col-md-4">Product Size: <span class="required">*</span></label>
										<div class="col-md-12">		
											<select id="product_size" name="product_size" class="form-control" readonly>
											</select>
										</div>
									</div>
								</div>

								<br />

								<div class="row"> <!-- -->
									<div class="form-group">
										<label class="col-md-4">Order Status: <span class="required">*</span></label>
										<div class="col-md-12">		
											<select id="order_status" name="order_status" class="form-control">
												<option value="Pending">PENDING</option>
												<option value="Paid">PAID</option>
												<option value="On Hold">ON HOLD</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn default" data-dismiss="modal">Close</button>
					<input type="submit" class="btn blue" name="submit" value="Add Order">
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
	</form>
	<!-- /.modal-dialog -->
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">

$(document).ready(function() {

	function updateStatus(id, name)
	{
		$.ajax({
			url:"<?php echo base_url(); ?>/Olstore/updateStatus",
			type: "POST",
			data: ({ 'id': id, 'status': name}),
			success:function(data){
				if(data == true)
				{
					//alert("Status Updated!");			
				}
			}
		});	
	}

	$("#product_id").change(function() {

		if($(this).val() != 0)
		{
			$('#product_quantity').attr("readonly", false);
			$('#product_color').attr("readonly", false);
			$('#product_size').attr("readonly", false);

			$.ajax({
				url:     "<?= base_url() ?>olstore/getProduct",
				type:    "POST",
				data:    { id: $(this).val() },
				dataType: "text",
				success: function(data) {

					$('#product_quantity').empty();
					$('#product_color').empty();
					$('#product_size').empty();

					var product_info = JSON.parse(data);
					
					$("#product_name").val(product_info['name']);
					$("#product_price").val(product_info['price']);

					for(var x = product_info['quantity']; x > 0; x--) {

						$('#product_quantity').append($('<option>', {
							value: x,
							text: x,
							selected: true
						}));
					}

					var colors = product_info['color'].split(',');
					var sizes = product_info['size'].split(',');

					for(var x = 0; x < colors.length; x++) {

						$('#product_color').append($('<option>', {
							value: colors[x],
							text: colors[x].toUpperCase()
						}));
					}

					for(var x = 0; x < sizes.length; x++) {

						$('#product_size').append($('<option>', {
							value: sizes[x],
							text: sizes[x]
						}));
					}
				}
			});
		}
		else
		{
			$('#product_price').val("");
			$('#product_quantity').empty();
			$('#product_color').empty();
			$('#product_size').empty();

			$('#product_quantity').attr("readonly", true);
			$('#product_color').attr("readonly", true);
			$('#product_size').attr("readonly", true);	
		}
	});

	$(document).delegate('#statusdropdown', 'change', function(){
		var id = $(this).attr('class').substring(16,17);
		var name = $(this).val();
		var label = $(this).attr('class').substring(18,$(this).attr('class').length);

		updateStatus(id, name);

		if($(this).val() == "Paid"){
			$(this).removeClass(label).addClass('label-success');
		}
		else if($(this).val() == "Closed"){
			$(this).removeClass(label).addClass('label-warning');
		}
		else if($(this).val() == "Pending"){
			$(this).removeClass(label).addClass('label-primary');
		}

		//alert(id);

	});

	for(var i = 0; i < $("#statusdropdown").length; i++){
		if($(this).val() == "Paid"){
			$("." + $(this).attr('class')).css('background-color','#4CAF50');
		}
		else if($(this).val() == "Closed"){
			$("." + $(this).attr('class')).css('background-color','#FFA500');
		}
		else if($(this).val() == "Pending"){
			$("." + $(this).attr('class')).css('background-color','#3B5998');
		}
	}

});
</script> 