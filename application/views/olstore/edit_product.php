<style>
	.btn-circle {
	  width: 30px;
	  height: 30px;
	  text-align: center;
	  padding: 6px 0;
	  font-size: 12px;
	  line-height: 1.428571429;
	  border-radius: 15px;
	  border-color: green;
	}
	.btn-circle.btn-lg {
	  width: 50px;
	  height: 50px;
	  padding: 10px 16px;
	  font-size: 18px;
	  line-height: 1.33;
	  border-radius: 25px;
	}
	.btn-circle.btn-xl {
	  width: 70px;
	  height: 70px;
	  padding: 10px 16px;
	  font-size: 24px;
	  line-height: 1.33;
	  border-radius: 35px;
	}

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		var change = false;
		$("#addImagesBtn").click(function(e) {
			if(change == false)
				e.preventDefault();	
			$("#file_product_img_upload").click();
		});
		$("#file_product_img_upload").change(function() {
			change = true;
			$("#addImagesBtn").click();
		});
		$("#name").prop('required', 'true');
		$("#description").prop('required', 'true');
		$("#price").prop('required', 'true');
		$("#color").prop('required', 'true');
		$("#size").prop('required', 'true');
		$("#quantity").prop('required', 'true');
		$("#categoryid").prop('required', 'true');
		$("#status").prop('required', 'true');
	});
</script>
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Edit Product Details</h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		<div class="page-bar">
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="<?= base_url() ?>olstore">Online Store</a><i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="<?= base_url() ?>olstore/products">Products</a><i class="fa fa-circle"></i> 
			</li>
			<li class="active">
				 Add Product
			</li>
		</ul>
		</div>
		<!-- END PAGE BREADCRUMB -->
		<!-- BEGIN PAGE CONTENT INNER -->
		<div class="custom-grid margin-top-20">

		  	<!-- Flash Session Here.. -->	
		  	<?php if($flashdata) { ?>
		  			<?php if($flashdata['status'] == true) { ?>
							<div class="alert alert-success alert-dismissable">
							  <div style="text-align: right !important; float: right;"><a href="#" class="remove" data-dismiss="alert" aria-label="close">&times;</a></div>
							  <?php echo $flashdata['message']; ?>
							</div>								
					<?php } else { ?>
							<div class="alert alert-warning alert-dismissable">
							  <div style="text-align: right !important; float: right;"><a href="#" class="remove" data-dismiss="alert" aria-label="close">&times;</a></div>
							  <?php echo $flashdata['message']; ?>
							</div>
					<?php } ?>
			<?php } ?>			  	
  
			<div class="row">
				<div class="col-md-12">

					<form action="<?= base_url() ?>olstore/updateProduct" method="post" class="form-horizontal form-row-seperated" enctype="multipart/form-data" id="editform" name="editform">
						<input type="hidden" name="id" value="<?php echo $product->id; ?>">
						<div class="portlet">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-shopping-cart"></i><?php echo $product->name; ?>
								</div>
								<div class="actions btn-set">
									<a href="javascript:void(0)" onclick="window.history.back()" class="btn default"><i class="fa fa-angle-left"></i> Back</a>
									<!-- <button class="btn default"><i class="fa fa-reply"></i> Reset</button> -->
									<button class="btn green"><i class="fa fa-check"></i> Save</button>
									<!-- <button class="btn green"><i class="fa fa-check-circle"></i> Save & Continue Edit</button> -->
									<!-- <button class="btn red"><i class="fa fa-trash-o"></i> Delete</button> -->
									<a href="javascript:void(0);" title="Archived Product" data-toggle="modal" data-target="#del-modal-<?php echo $product->id; ?>" class="btn red btn-xs"><i class="fa fa-trash-o"></i> Archived</a>
									<!-- 
									<div class="btn-group">
										<a class="btn yellow dropdown-toggle" href="#" data-toggle="dropdown">
										<i class="fa fa-share"></i> More <i class="fa fa-angle-down"></i>
										</a>
										<ul class="dropdown-menu pull-right">
											<li>
												<a href="#">
												Duplicate </a>
											</li>
											<li>
												<a href="#">
												Delete </a>
											</li>
											<li class="divider">
											</li>
											<li>
												<a href="#">
												Print </a>
											</li>
										</ul>
									</div>
									-->
								</div>
							</div>
							<div class="portlet-body">
								<div class="tabbable">
									<ul class="nav nav-tabs">
										<li class="active">
											<a href="#tab_details" data-toggle="tab">
											Details </a>
										</li>
										<li>
											<a href="#tab_images" data-toggle="tab">
											Images </a>
										</li>
									</ul>
									<div class="tab-content no-space">
										<div class="tab-pane active" id="tab_details">

											<div class="form-body">
												<div class="form-group">
													<label class="col-md-1 control-label">Name: <span class="required">* </span></label>
													<div class="col-md-8">
														<input type="text" class="form-control" id="name" name="name" value="<?php echo $product->name; ?>" placeholder="">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-1 control-label">Description: <span class="required">*</span>
													</label>
													<div class="col-md-8">
														<textarea class="form-control" id="description" name="description"><?php echo $product->description; ?></textarea>
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-1 control-label">Price: <span class="required">
													* </span>
													</label>
													<div class="col-md-8">
														<input type="text" class="form-control" id="price" name="price" value="<?php echo $product->price; ?>" placeholder="">
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-1 control-label">Color: <span class="required">
													* </span>
													</label>
													<div class="col-md-8">
														<input type="text" class="form-control" id="color" name="color" value="<?php echo $product->color; ?>" placeholder="">
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-1 control-label">Size: <span class="required">
													* </span>
													</label>
													<div class="col-md-8">
														<input type="text" class="form-control" id="size" name="size" value="<?php echo $product->size; ?>" placeholder="">
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-1 control-label">Quantity: <span class="required">
													* </span>
													</label>
													<div class="col-md-8">
														<input type="text" class="form-control" id="quantity" name="quantity" value="<?php echo $product->quantity; ?>" placeholder="">
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-1 control-label">Category: <span class="required">
													* </span>
													</label>
													<div class="col-md-8">
														<select class="table-group-action-input form-control input-medium" name="categoryid" id="categoryid">
															<option value="" disabled>Select...</option>
															<?php foreach($categories as $ckey => $c) { ?>
																		<option <?php echo $product->categoryid == $c->id ? 'selected' : ''; ?> value="<?php echo $c->id; ?>"><?php echo $c->category_name; ?></option>
															<?php } ?>
														</select>
													</div>
												</div>												

												<div class="form-group">
													<label class="col-md-1 control-label">Status: <span class="required">
													* </span>
													</label>
													<div class="col-md-8">
														<select class="table-group-action-input form-control input-medium" id="status" name="status">
															<option value="" disabled>Select...</option>
															<option <?php echo $product->status == "Published" ? 'selected' : ''; ?> value="Published">Published</option>
															<option <?php echo $product->status == "Not Published" ? 'selected' : ''; ?> value="Not Published">Not Published</option>
															<option <?php echo $product->status == "Archived" ? 'selected' : ''; ?> value="Archived">Archived</option>
														</select>
													</div>
												</div>

											</div>

										</div>

										<div class="tab-pane" id="tab_images">
											<!--
											<div class="alert alert-success margin-bottom-10">
												<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
												<i class="fa fa-warning fa-lg"></i> Image type and information need to be specified.
											</div>
											-->
											<div id="tab_images_uploader_container" class="margin-bottom-10">
												<!--
												<a id="tab_images_uploader_pickfiles" href="javascript:;" class="btn yellow">
												<i class="fa fa-plus"></i> Select Files </a>
												<a id="tab_images_uploader_uploadfiles" href="javascript:;" class="btn green">
												<i class="fa fa-share"></i> Upload Files </a>
												-->
												<div class="col-sm-12 col-md-12 col-lg-12" style="padding-bottom: 5px;">
													<input type="file"  multiple="multiple" name="file_product_img_upload[]" id="file_product_img_upload" style="display: none;">
													<div class="col-sm-2 col-md-1 col-lg-1">
														<button class="btn-circle" id="addImagesBtn"> <i class="fa fa-plus fa-lg" style="color: green;"></i>  </button>
													</div>
													<div class="col-sm-10 col-md-11 col-lg-11" style=" padding-left: 0px;">
														<div style="height: 100%; padding-top: 5px; margin-right: 10px;" > <strong> Add More Image </strong> </div>
													</div>
												</div>
											</div>
											<div class="row">
												<div id="tab_images_uploader_filelist" class="col-md-6 col-sm-12">
												</div>
											</div>
											<table class="table table-bordered table-hover" id="imagesTable">
											<thead>
											<tr role="row" class="heading">
												<th width="8%">
													 Image
												</th>
												<th width="25%">
													 &nbsp;
												</th>
												<th width="8%">
													 Is Default
												</th>
												<th width="10%">
												</th>
											</tr>
											</thead>
											<tbody>
											<?php if($images) {  ?>

												<?php foreach($images as $ikey => $im) { ?>
												<tr>
													<td>
														<a class="fancybox" href="<?php echo base_url() ?>sd-contents/onlinestore/images/<?php echo $im->image; ?>" data-fancybox-group="gallery" title="">
															<img class="img-responsive" src="<?php echo base_url() ?>sd-contents/onlinestore/images/<?php echo $im->image; ?>" alt="" />
														</a>
													</td>
													<td>
														<!-- <input type="text" class="form-control" name="label" value="<?php $em->label; ?>"> -->
														&nbsp;
													</td> 
													<td>
														<!--
														<select class="table-group-action-input form-control input-medium" name="is_default" id="is_default">
															<option value="">Select...</option>
																<option <?php echo $im->is_default == "Yes" ? 'selected' : ''; ?> value="Yes"><?php echo $im->is_default; ?></option>
														</select>
														-->
														<input type="radio" name="is_default" value="<?php echo $im->id; ?>" <?php echo $im->is_default == "Yes" ? "checked" : ""; ?>>
														
													</td>
													<td>
														<a href="javascript:void(0);" title="Remove Image" data-toggle="modal" data-target="#del-modal-img-<?php echo $im->id; ?>" class="btn default btn-sm"><i class="fa fa-times"></i> Remove </a>
													</td>
												</tr>
													<div id=modals>
														<div id="del-modal-img-<?php echo $im->id; ?>" class="modal fade" role="dialog">
															<!-- <form method="post" action="<?= base_url() ?>olstore/deleteImage/<?php echo $im->id; ?>"> -->
																<!-- <input type="hidden" name="id" value="<?php echo $im->id; ?>"> -->	
																<div class="modal-dialog">
																    <div class="modal-content">
																	    	<div class="modal-header">
																		        <button type="button" class="close" data-dismiss="modal">&times;</button>
																		        <h4 class="modal-title">Confirmation</h4>
																	      	</div>

																      		<div class="modal-body">
																        		<p>Are you sure you want to remove this image?</p>
																      		</div>
																      	<div class="modal-footer">
																      		<input type="button" class="btn red" value="Remove" onclick="window.location = '<?= base_url() ?>olstore/deleteImage/<?php echo $im->id; ?>/<?php echo $product->id ?>';">
																        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																      	</div>
																    </div>
																</div>
															<!-- </form> -->
														</div>

														<div id="del-modal-dynamicimg" title="Remove Image" class="modal fade" role="dialog">
															<div class="modal-dialog">
																<div class="modal-content">
																	   	<div class="modal-header">
																	        <button type="button" class="close" data-dismiss="modal">&times;</button>
																	        <h4 class="modal-title">Confirmation</h4>
																	   	</div>

																     	<div class="modal-body">
																      		<p>Are you sure you want to remove this image?</p>
																    	</div>
																    <div class="modal-footer">
																      		<input id="removeDynamicImg" type="button" class="btn red" value="Remove">
																    	<button id="dynmodalclose" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																    	<button id="checker" type="button" class="btn btn-primary" value="check"> check image</button>
																  	</div>
																</div>
															</div>
														</div>

													</div>
												<?php } ?>
											<?php } ?>
											</tbody>
											</table>

										</div>

									</div>
								</div>
							</div>
						</div>
					</form>

				</div>
			</div>
		</div>			
		<!-- END PAGE CONTENT INNER -->

		<div id="del-modal-<?php echo $product->id; ?>" class="modal fade" role="dialog">
			<form method="post" action="<?= base_url() ?>olstore/archiveProduct">
				<input type="hidden" name="id" value="<?php echo $product->id; ?>">	
				<div class="modal-dialog">
				    <div class="modal-content">
					    	<div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal">&times;</button>
						        <h4 class="modal-title">Confirmation</h4>
					      	</div>

				      		<div class="modal-body">
				        		<p>Are you sure you want to archived this product?</p>
				      		</div>
				      	<div class="modal-footer">
				      		<input type="submit" class="btn red" value="Archived">
				        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				      	</div>
				    </div>
				</div>
			</form>
		</div>			