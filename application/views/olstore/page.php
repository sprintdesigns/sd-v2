<style type="text/css">
	#tabs > ul > li {
		border-radius: 0;
	    display: unset;
	    margin-top: 4px;
	    font-size: 15px !important;
	    padding: 3px 14px;
	}


	#tabs > div {
		padding-left: 2%;
	}

	#tabs .ui-state-active {
		background: #428BCA !important;

	}

	#tabs .ui-state-active a {
		color: #FFFFFF;
		text-decoration:  none;
	}
</style>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
   <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script> 
	$( function() {
    $( "#tabs" ).tabs();
  } );


</script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Settings</h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		<div class="page-bar">
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="#">Online Store</a><i class="fa fa-circle"></i>
			</li>
			<li class="active">
				 Settings
			</li>
		</ul>
		</div>
		<!-- END PAGE BREADCRUMB -->
		<!-- BEGIN PAGE CONTENT INNER -->
		<div class="custom-grid margin-top-20">

			  	<!-- Flash Session Here.. -->	
			  	<?php if($flashdata) { ?>
			  			<?php if($flashdata['status'] == true) { ?>
								<div class="alert alert-success alert-dismissable">
								  <div style="text-align: right !important; float: right;"><a href="#" class="remove" data-dismiss="alert" aria-label="close">&times;</a></div>
								  <?php echo $flashdata['message']; ?>
								</div>								
						<?php } else { ?>
								<div class="alert alert-success alert-dismissable">
								  <div style="text-align: right !important; float: right;"><a href="#" class="remove" data-dismiss="alert" aria-label="close">&times;</a></div>
								  <?php echo $flashdata['message']; ?>
								</div>
						<?php } ?>
				<?php } ?>	  

			<div class="row">
				<div class="col-md-12">
					<form class="form-horizontal form-row-seperated" action="<?php echo base_url(); ?>olstore/addMessage" method="post">
						<div class="portlet">

							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-shopping-cart"></i>
								</div>
								<div class="actions btn-set">
									<a href="javascript:void(0)" onclick="window.history.back()" class="btn default"><i class="fa fa-angle-left"></i> Back</a>
									<!-- <button class="btn default"><i class="fa fa-reply"></i> Reset</button> -->
									<button class="btn green" ><i class="fa fa-check"></i> Save</button>
									</div>
							</div>

							<div id="tabs">
								<ul>
									<li> <a href="#purchase_tab"> Purchase Message</a> </li>
									<li> <a href="#tab_2"> Tab 2</a> </li>
									<li> <a href="#tab_3"> Tab 3</a> </li>
									<li> <a href="#tab_4"> Tab 4</a> </li>
								</ul>

								<div id="purchase_tab">
									<div class="portlet-body">
										<div class="form-body">
											<?php if($msg == false) { ?>
												<input type="hidden" value="" name="id" id="id">
											<?php } else { ?>
												<input type="hidden" value="<?php echo $msg[0]->id; ?>" name="id" id="id">
											<?php } ?>

											<div class="form-group">
												<label class="col-md-2 control-label">Thank You Message: <span class="required">* </span></label>
												<div class="col-md-8">
													<?php if($msg == false) { ?>
														<textarea rows="5" class="form-control" name="thankyou_message" id="thankyou_message"></textarea>
													<?php } else { ?>
														<textarea rows="5" class="form-control" name="thankyou_message" id="thankyou_message"><?php echo $msg[0]->thankyou_message; ?></textarea>
													<?php } ?>
												</div>
											</div>									
											<div class="form-group">
												<label class="col-md-2 control-label">Note: <span class="required">*</span></label>
												<div class="col-md-8">
													<?php if($msg == false) { ?>
													<textarea rows="5" class="form-control" name="note_message" id="note_message"></textarea>
													<?php } else { ?>
													<textarea rows="5" class="form-control" name="note_message" id="note_message"><?php echo $msg[0]->note_message; ?></textarea>
													<?php } ?>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-2 control-label">Purchase Message: <span class="required">*</span></label>
												<div class="col-md-8">
													<?php if($msg == false) { ?>
													<textarea rows="5" class="form-control" name="purchase_message" id="purchase_message"></textarea>
													<?php } else { ?>
													<textarea rows="5" class="form-control" name="purchase_message" id="purchase_message"><?php echo $msg[0]->purchase_message; ?></textarea>
													<?php } ?>
												</div>
											</div>

										</div>
									</div>
								</div>

								<div id="tab_2">
									<!-- Tab 2 content -->
								</div>

								<div id="tab_3">
									<!-- Tab 3 content -->
								</div>

								<div id="tab_4">
									<!-- Tab 4 content -->
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>

		</div>
		<!-- END PAGE CONTENT INNER -->
	</div>
</div>
<!-- END CONTENT --> 