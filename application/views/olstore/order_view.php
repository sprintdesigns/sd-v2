<style type="text/css">

</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Order Details</h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		<div class="page-bar">
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="<?= base_url() ?>olstore">Online Store</a><i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="<?= base_url() ?>olstore/orders">Orders</a><i class="fa fa-circle"></i> 
			</li>
			<li class="active">
				 Details
			</li>
		</ul>
		</div>
		<!-- END PAGE BREADCRUMB -->
		<!-- BEGIN PAGE CONTENT INNER -->
		<div class="custom-grid margin-top-20">

		  	<!-- Flash Session Here.. -->	
		  	<?php if($flashdata) { ?>
		  			<?php if($flashdata['status'] == true) { ?>
							<div class="alert alert-success alert-dismissable">
							  <div style="text-align: right !important; float: right;"><a href="#" class="remove" data-dismiss="alert" aria-label="close">&times;</a></div>
							  <?php echo $flashdata['message']; ?>
							</div>								
					<?php } else { ?>
							<div class="alert alert-warning alert-dismissable">
							  <div style="text-align: right !important; float: right;"><a href="#" class="remove" data-dismiss="alert" aria-label="close">&times;</a></div>
							  <?php echo $flashdata['message']; ?>
							</div>
					<?php } ?>
			<?php } ?>			  	
  
			<div class="row">
				<div class="col-md-12">
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-shopping-cart"></i>Order #<?php echo $order_info->id; ?> <span class="hidden-480"> | <?php date_default_timezone_set("Asia/Manila"); echo date("F d, Y h:i A", strtotime($order_info->purchase_date)); ?> </span>
							</div>
							<div class="actions">
								<a id="edit_details" href="javascript:void(0)" data-toggle="modal" data-target="#update-order-modal" class="btn default blue-stripe">
								<i class="fa fa-angle-left"></i>
								<span class="hidden-480">Edit Details</span>	
								</a>
								&nbsp; 
								<a href="javascript:void(0)" onclick="window.history.back()" class="btn default yellow-stripe">
								<i class="fa fa-angle-left"></i>
								<span class="hidden-480">Back </span>
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="tabbable">
								<ul class="nav nav-tabs nav-tabs-lg">
									<li class="active">
										<a href="#tab_1" data-toggle="tab">Details </a>
									</li>
									<li>
										<a href="#tab_3" data-toggle="tab">Others </a>
									</li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="tab_1">
										<div class="row">
											<div class="col-md-6 col-sm-12">
												<div class="portlet yellow-crusta box">
													<div class="portlet-title">
														<div class="caption">
															<i class="fa fa-cogs"></i>Order Details
														</div>
														<!-- <div class="actions">
															<a href="#" class="btn btn-default btn-sm">
															<i class="fa fa-pencil"></i> Edit </a>
														</div> -->
													</div>
													<div class="portlet-body">
														<div class="row static-info">
															<div class="col-md-5 name">
																 Order #:
															</div>
															<div class="col-md-7 value">
																<?php echo $order_info->id; ?> <!-- <span class="label label-info label-sm">
																Email confirmation was sent </span> -->
															</div>
														</div>
														<div class="row static-info">
															<div class="col-md-5 name">
																 Product Name:
															</div>
															<div class="col-md-7 value">
																<?php echo $order_info->prodname; ?>
															</div>
														</div>
														<div class="row static-info">
															<div class="col-md-5 name">
																 Order Date &amp; Time:
															</div>
															<div class="col-md-7 value">
																<?php date_default_timezone_set("Asia/Manila"); echo date("F d, Y h:i A", strtotime($order_info->purchase_date)); ?>
															</div>
														</div>
														<div class="row static-info">
															<div class="col-md-5 name">
																 Order Status:
															</div>
															<div class="col-md-7 value">
																<?php if(strtoupper($order_info->status) === 'PENDING') { ?>

																	<span class="label label-sm label-primary"><?php echo strtoupper($order_info->status); ?></span>

																<?php } else if(strtoupper($order_info->status) === 'ON HOLD') { ?>

																	<span class="label label-sm label-warning"><?php echo strtoupper($order_info->status); ?></span>

																<?php } else { ?>

																	<span class="label label-sm label-success"><?php echo strtoupper($order_info->status); ?></span>
																
																<?php } ?>
															</div>
														</div>
														<div class="row static-info">
															<div class="col-md-5 name">
																 Quantity:
															</div>
															<div class="col-md-7 value">
																<?php echo $order_info->quantity; ?>
															</div>
														</div>
														<div class="row static-info">
															<div class="col-md-5 name">
																 Price:
															</div>
															<div class="col-md-7 value">
																PHP <?php echo number_format($order_info->price, 2); ?>
															</div>
														</div>
														<div class="row static-info">
															<div class="col-md-5 name">
																 Payment Information:
															</div>
															<div class="col-md-7 value">
																<?php echo strtoupper($order_info->mode_payment); ?>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-6 col-sm-12">
												<div class="portlet blue-hoki box">
													<div class="portlet-title">
														<div class="caption">
															<i class="fa fa-cogs"></i>Customer Information
														</div>
													</div>
													<div class="portlet-body">
														<div class="row static-info">
															<div class="col-md-5 name">
																 Customer Name:
															</div>
															<div class="col-md-7 value">
																<?php echo $order_info->cust_name; ?>
															</div>
														</div>
														<div class="row static-info">
															<div class="col-md-5 name">
																 Email:
															</div>
															<div class="col-md-7 value">
																<?php echo $order_info->email; ?>
															</div>
														</div>
														<div class="row static-info">
															<div class="col-md-5 name">
																 Contact Number:
															</div>
															<div class="col-md-7 value">
																<?php echo $order_info->cust_number; ?>
															</div>
														</div>
														<div class="row static-info">
															<div class="col-md-5 name">
																 Address:
															</div>
															<div class="col-md-7 value">
																<?php echo $order_info->cust_address; ?>
															</div>
														</div>
														<div class="row static-info">
															<div class="col-md-5 name">
																 Size / Color:
															</div>
															<div class="col-md-7 value">
																<?php echo $order_info->size . ' / ' . strtoupper($order_info->color); ?> 
															</div>
														</div>
														<div class="row static-info">
															&nbsp;
														</div>
														<div class="row static-info">
															&nbsp;
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="well">
													<div class="row static-info align-reverse">
														<div class="col-md-8 name">
															 Grand Total:
														</div>
														<div class="col-md-3 value">
															PHP <?php echo number_format($order_info->price * $order_info->quantity, 2); ?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									
									<div class="tab-pane" id="tab_3">
										<div class="row">
										...
										</div>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>			
		<!-- END PAGE CONTENT INNER -->

		<div class="modal fade bs-modal-lg" id="update-order-modal" tabindex="-1" role="dialog" aria-hidden="true">
			<form action="<?= base_url() ?>olstore/updateOrder/<?php echo $order_info->id; ?>" method="post">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Edit Order</h4>
						</div>
						<div class="modal-body">
							<form role="form">
								<div class="form-body">
									<div class="form-group">

										<div class="row">
											<div class="form-group">
												<label class="col-md-4">Customer Email: <span class="required">*</span></label>
												<div class="col-md-12">
													<input type="text" id="customer_email" name="customer_email" class="form-control" placeholder="Customer Email">
												</div>
											</div>
										</div>

										<br />

										<div class="row">
											<div class="form-group">
												<label class="col-md-4">Customer Name: <span class="required">*</span></label>
												<div class="col-md-12">
													<input type="text" id="customer_name" name="customer_name" class="form-control" placeholder="Customer Name">
												</div>
											</div>
										</div>

										<br />

										<div class="row">
											<div class="form-group">
												<label class="col-md-4">Customer Address: <span class="required">*</span></label>
												<div class="col-md-12">
													<textarea id="customer_address" name="customer_address" class="form-control"></textarea>
												</div>
											</div>
										</div>

										<br />

										<div class="row">
											<div class="form-group">
												<label class="col-md-4">Phone Number: <span class="required">*</span></label>
												<div class="col-md-12">
													<input type="text" id="phone_number" name="phone_number" class="form-control" placeholder="Phone Number">
												</div>
											</div>
										</div>
								
										<br />
								
										<div class="row">
											<div class="form-group">
												<label class="col-md-4">Mode of Payment: <span class="required">*</span></label>
												<div class="col-md-12">		
													<select class="form-control" id="mode_of_payment" name="mode_of_payment">
														<option value="Cash">Cash</option>
														<option value="Money Transfer/Bank Deposit">Money Transfer/Bank Deposit</option>
													</select>
												</div>
											</div>
										</div>

										<br />

										<div class="row"> <!-- -->
											<div class="form-group">
												<label class="col-md-4">Product : </label>
												<div class="col-md-12">
													<select class="form-control" id="product_id" name="product_id">
														<option value="0">Choose Product Here</option>
														<?php $query_products = $this->db->select(array('id','name'))->get('products'); ?>
														<?php if ($query_products->num_rows() != 0) { ?>
															<?php foreach ($query_products->result() as $product) { ?>
																<option value="<?php echo $product->id; ?>"><?php echo $product->name; ?></option>
															<?php } ?>
														<?php } ?>															
													</select><input type="hidden" id="product_name" name="product_name">
												</div>
											</div>
										</div>

										<br />

										<div class="row"> <!-- -->
											<div class="form-group">
												<label class="col-md-4">Product Price: </label>
												<div class="col-md-12">
													<input type="text" id="product_price" name="product_price" class="form-control" readonly>
												</div>
											</div>
										</div>

										<br />

										<div class="row"> <!-- -->
											<div class="form-group">
												<label class="col-md-4">Quantity: <span class="required">*</span></label>
												<div class="col-md-12">		
													<select id="product_quantity" name="product_quantity" class="form-control" readonly>
													</select>
												</div>
											</div>
										</div>
										
										<br />

										<div class="row"> <!-- -->
											<div class="form-group">
												<label class="col-md-4">Product Color: <span class="required">*</span></label>
												<div class="col-md-12">		
													<select id="product_color" name="product_color" class="form-control" readonly>
													</select>
												</div>
											</div>
										</div>

										<br />

										<div class="row"> <!-- -->
											<div class="form-group">
												<label class="col-md-4">Product Size: <span class="required">*</span></label>
												<div class="col-md-12">		
													<select id="product_size" name="product_size" class="form-control" readonly>
													</select>
												</div>
											</div>
										</div>

										<br />

										<div class="row"> <!-- -->
											<div class="form-group">
												<label class="col-md-4">Order Status: <span class="required">*</span></label>
												<div class="col-md-12">		
													<select id="order_status" name="order_status" class="form-control">
														<option value="Pending">PENDING</option>
														<option value="Paid">PAID</option>
														<option value="Closed">CLOSED</option>
													</select>
												</div>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
							<input type="submit" class="btn blue" name="submit" value="Save Changes">
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
			</form>
			<!-- /.modal-dialog -->
		</div>			

	</div>
</div>
<!-- END CONTENT --> 

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript"> 
$(document).ready(function() {

	$("#edit_details").click(function() {

		$("#customer_email").val("<?= $order_info->email; ?>");
		$("#customer_name").val("<?= $order_info->cust_name; ?>");
		$("#customer_address").val("<?= $order_info->cust_address; ?>");
		$("#phone_number").val("<?= $order_info->cust_number; ?>");
		$("#mode_of_payment option[value='<?= $order_info->mode_payment; ?>']").prop('selected', true);

		$("#product_id option[value='<?= $order_info->product_id; ?>']").prop('selected', true);
		$("#product_name").val("<?= $order_info->prodname; ?>");
		
		$('#product_quantity').attr("readonly", false);
		$('#product_color').attr("readonly", false);
		$('#product_size').attr("readonly", false);

		$.ajax({
			url: "<?= base_url() ?>olstore/getProduct",
			type: "POST",
			data: { id: <?= $order_info->product_id; ?> },
			dataType: "text",
			success: function(data) {

				$('#product_quantity').empty();
				$('#product_color').empty();
				$('#product_size').empty();

				var product_info = JSON.parse(data);
				
				$("#product_name").val(product_info['name']);
				$("#product_price").val(product_info['price']);

				for(var x = product_info['quantity']; x > 0; x--) {

					$('#product_quantity').append($('<option>', {
						value: x,
						text: x,
						selected: true
					}));
				}

				var colors = product_info['color'].split(',');
				var sizes = product_info['size'].split(',');

				for(var x = 0; x < colors.length; x++) {

					$('#product_color').append($('<option>', {
						value: colors[x],
						text: colors[x].toUpperCase()
					}));
				}

				for(var x = 0; x < sizes.length; x++) {

					$('#product_size').append($('<option>', {
						value: sizes[x],
						text: sizes[x]
					}));
				}

				$("#product_quantity option[value='<?= $order_info->quantity; ?>']").prop('selected', true);
				$("#product_color option[value='<?= $order_info->color; ?>']").prop('selected', true);
				$("#product_size option[value='<?= $order_info->size; ?>']").prop('selected', true);
				$("#order_status option[value='<?= $order_info->status; ?>']").prop('selected', true);
			}
		});
	});

	$("#product_id").change(function() {

		if($(this).val() != 0) {

			$('#product_quantity').attr("readonly", false);
			$('#product_color').attr("readonly", false);
			$('#product_size').attr("readonly", false);

			$.ajax({
				url: "<?= base_url() ?>olstore/getProduct",
				type: "POST",
				data: { id: $(this).val() },
				dataType: "text",
				success: function(data) {

					$('#product_quantity').empty();
					$('#product_color').empty();
					$('#product_size').empty();

					var product_info = JSON.parse(data);
					
					$("#product_name").val(product_info['name']);
					$("#product_price").val(product_info['price']);

					for(var x = product_info['quantity']; x > 0; x--) {

						$('#product_quantity').append($('<option>', {
							value: x,
							text: x,
							selected: true
						}));
					}

					var colors = product_info['color'].split(',');
					var sizes = product_info['size'].split(',');

					for(var x = 0; x < colors.length; x++) {

						$('#product_color').append($('<option>', {
							value: colors[x],
							text: colors[x].toUpperCase()
						}));
					}

					for(var x = 0; x < sizes.length; x++) {

						$('#product_size').append($('<option>', {
							value: sizes[x],
							text: sizes[x]
						}));
					}
				}
			});
		}
		else {

			$('#product_price').val("");
			$('#product_quantity').empty();
			$('#product_color').empty();
			$('#product_size').empty();

			$('#product_quantity').attr("readonly", true);
			$('#product_color').attr("readonly", true);
			$('#product_size').attr("readonly", true);	
		}
	});
});
</script> 
