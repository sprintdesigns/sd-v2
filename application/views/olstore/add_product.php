<style>
	.images {
	    border: 1px solid #ddd;
	    border-radius: 4px;
	    padding: 5px;
	    width: 200px;
	}
	#image-holder {
		padding-top: 10px;
	}
	.inputbox {
		border: 1px solid #ddd;
	    border-radius: 4px;
	    height: 200px;
	    width: 100%;
	}
	#file_product_img_upload {
		 opacity: 0; 
	    border: none;
	    border-radius: 3px;
	    background: grey;
	    position: absolute;
	    left: 0px;    
	    width: 100%;
	    top: 0px;
	    height: 100%;
	}
	#inputtext {
		position: absolute;
	    color: #000000;
	    top: 50%;
	    left: 50%;
	    transform: translate(-50%, -50%);
	    font-size: 25px;
	}
	#editImages {
		position: absolute;
		top: 85%;
	    left: 92%;
	    height: 30px;
	    width: 30px;
	    padding-top: 5%;

	}
	.errormsg {
		padding-top: 2%;
		padding-left: 5%;
		position: absolute;
	}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
	$(document).ready(function() {
		var debug = [];
		var length = 0;
		$("#file_product_img_upload").change(function(e) {
		    length = $(this).get(0).files.length;
			var x = 0;
			for(var i = 0; i < length; i++) {
				var imgPath = $(this)[0].value;
			    var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();

			    if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
			        if (typeof (FileReader) != "undefined") {

			            var image_holder = $("#image-holder");
			            image_holder.empty();

			            var reader = new FileReader();
			            reader.onload = function (e) {
			                $("<img />", {
			                    "src": e.target.result,
			                    "class": "images",
			                    "id" : x
			                }).appendTo(image_holder);
			                x++;
			                
			            }	
			            image_holder.show();
			            
			            reader.readAsDataURL($(this)[0].files[i]);
			        }
			        else {
			            alert("This browser does not support FileReader.");
			        }
			    } 
			    else {
			        alert("Please select only images");
			        break;
			    }
			    $(".inputbox").css('display', 'none');
			}
			$("#editImages").css('display', 'block');
			$(".errormsg").css('display', 'none');
		});
		$($("#editImages").before()).hover(function() {
			$(this).css( 'cursor', 'pointer' );
		});
		$($("#editImages").before()).click(function() {
			$("#file_product_img_upload").click();
		});

		$("#name").prop('required', 'true');
		$("#description").prop('required', 'true');
		$("#price").prop('required', 'true');
		$("#status").prop('required', 'true');
		
		$("#savebtn").click(function(e) {
			var forminputs = $("input");
			forminputs.push($("select[id='categoryid']"));
			forminputs.push($("select[id='status']"));

			for (var i = 0; i < forminputs.length; i++) {
				if(forminputs[i]['id'] == 'file_product_img_upload') {
					if($("#file_product_img_upload").get(0).files.length === 0) {
						$('.nav-tabs a[href="#' + 'tab_images' + '"]').tab('show');	
						$("#file_product_img_upload").prop('required', 'true');
						$(".errormsg").css('display', 'block');
						e.preventDefault();				 
					}
					else {
						$('.nav-tabs a[href="#' + 'tab_details' + '"]').tab('show');
						if(!$("#name").length || !$("#description") || !$("#price") || !$("status")) {
							e.preventDefault();	
						}
					}			
				}
			}

		});


		
	});
</script> 
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Add New Product</h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		<div class="page-bar">
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="<?= base_url() ?>olstore">Online Store</a><i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="<?= base_url() ?>olstore/products">Products</a><i class="fa fa-circle"></i> 
			</li>
			<li class="active">
				 Add Product
			</li>
		</ul>
		</div>
		<!-- END PAGE BREADCRUMB -->
		<!-- BEGIN PAGE CONTENT INNER -->
		<div class="custom-grid margin-top-20">

		  	<!-- Flash Session Here.. -->	
		  	<?php if($flashdata) { ?>
		  			<?php if($flashdata['status'] == true) { ?>
							<div class="alert alert-success alert-dismissable">
							  <div style="text-align: right !important; float: right;"><a href="#" class="remove" data-dismiss="alert" aria-label="close">&times;</a></div>
							  <?php echo $flashdata['message']; ?>
							</div>								
					<?php } else { ?>
							<div class="alert alert-warning alert-dismissable">
							  <div style="text-align: right !important; float: right;"><a href="#" class="remove" data-dismiss="alert" aria-label="close">&times;</a></div>
							  <?php echo $flashdata['message']; ?>
							</div>
					<?php } ?>
			<?php } ?>			  	
  
			<div class="row">
				<div class="col-md-12">

					<form action="<?= base_url() ?>olstore/saveProduct" method="post" class="form-horizontal form-row-seperated" enctype="multipart/form-data">
						<div class="portlet">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-shopping-cart"></i>
								</div>
								<div class="actions btn-set">
									<a href="javascript:void(0)" onclick="window.history.back()" class="btn default"><i class="fa fa-angle-left"></i> Back</a>
									<button class="btn green" id="savebtn"><i class="fa fa-check"></i> Save</button>
									<!-- <button class="btn default"><i class="fa fa-reply"></i> Reset</button> -->
									<!-- <button class="btn green"><i class="fa fa-check-circle"></i> Save & Continue Edit</button> -->
									<!-- <button class="btn red"><i class="fa fa-trash-o"></i> Delete</button> -->
									<!-- 
									<div class="btn-group">
										<a class="btn yellow dropdown-toggle" href="#" data-toggle="dropdown">
										<i class="fa fa-share"></i> More <i class="fa fa-angle-down"></i>
										</a>
										<ul class="dropdown-menu pull-right">
											<li>
												<a href="#">
												Duplicate </a>
											</li>
											<li>
												<a href="#">
												Delete </a>
											</li>
											<li class="divider">
											</li>
											<li>
												<a href="#">
												Print </a>
											</li>
										</ul>
									</div>
									-->
								</div>
							</div>
							<div class="portlet-body">
								<div class="tabbable">
									<ul class="nav nav-tabs">
										<li class="active">
											<a href="#tab_details" data-toggle="tab">
											Details </a>
										</li>
										<li>
											<a href="#tab_images" data-toggle="tab">
											Images </a>
										</li>
									</ul>
									<div class="tab-content no-space">
										<div class="tab-pane active" id="tab_details">

											<div class="form-body">
												<div class="form-group">
													<label class="col-md-1 control-label">Name: <span class="required">* </span></label>
													<div class="col-md-8">
														<input type="text" class="form-control" id="name" name="name" value="" placeholder="">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-1 control-label">Description: <span class="required">*</span>
													</label>
													<div class="col-md-8">
														<textarea class="form-control" id="description" name="description"></textarea>
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-1 control-label">Price: <span class="required">
													* </span>
													</label>
													<div class="col-md-8">
														<input type="text" class="form-control" id="price" name="price" value="" placeholder="">
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-1 control-label">Color:</label>
													<div class="col-md-8">
														<input type="text" class="form-control" id="color" name="color" value="" placeholder="">
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-1 control-label">Size:</label>
													<div class="col-md-8">
														<input type="text" class="form-control" id="size" name="size" value="" placeholder="">
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-1 control-label">Quantity:</label>
													<div class="col-md-8">
														<input type="text" class="form-control" id="quantity" name="quantity" value="" placeholder="">
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-1 control-label">Category:</label>
													<div class="col-md-8">
														<select class="table-group-action-input form-control input-medium" name="categoryid" id="categoryid">
															<option value="">Select...</option>
															<?php foreach($categories as $ckey => $c) { ?>
																		<option value="<?php echo $c->id; ?>"><?php echo $c->category_name; ?></option>
															<?php } ?>
														</select>
													</div>
												</div>												

												<div class="form-group">
													<label class="col-md-1 control-label">Status: <span class="required">
													* </span>
													</label>
													<div class="col-md-8">
														<select class="table-group-action-input form-control input-medium" id="status" name="status">
															<option value="">Select...</option>
															<option value="Published">Published</option>
															<option value="Not Published">Not Published</option>
															<option value="Archived">Archived</option>
														</select>
													</div>
												</div>

											</div>

										</div>

										<div class="tab-pane" id="tab_images">
											<!--
											<div class="alert alert-success margin-bottom-10">
												<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
												<i class="fa fa-warning fa-lg"></i> Image type and information need to be specified.
											</div>
											-->
											<div id="tab_images_uploader_container" class="text-align-reverse margin-bottom-10">
												<!-- <a id="tab_images_uploader_pickfiles" href="javascript:;" class="btn yellow">
												<i class="fa fa-plus"></i> Select Files </a>
												<a id="tab_images_uploader_uploadfiles" href="javascript:;" class="btn green">
												<i class="fa fa-share"></i> Upload Files </a> -->
												<div class="col-sm-12 col-md-12 col-lg-12"> 
													<div class="inputbox"> 
														<div id="inputtext"> Drag or Click here to add images </div>
														<input type="file" multiple="multiple" name="file_product_img_upload[]" id="file_product_img_upload">
													</div>
												</div>
												<div id="image-holder" style="margin: 0 auto; text-align: center;"></div>
												<i class="fa fa-pencil-square-o fa-3x" id="editImages" style="text-align: right; display: none;"> </i>
												
											</div>
											<div class="row">
												<div id="tab_images_uploader_filelist" class="col-md-6 col-sm-12">
													<div class="errormsg fa fa-warning" style="display: none;"> Please select image(s) for your product. </div>
												</div>
											</div>
											<!--
											<table class="table table-bordered table-hover">
											<thead>
											<tr role="row" class="heading">
												<th width="8%">
													 Image
												</th>
												<th width="25%">
													 Label
												</th>
												<th width="8%">
													 Sort Order
												</th>
												<th width="10%">
												</th>
											</tr>
											</thead>
											<tbody>
											<tr>
												<td>
													<a class="fancybox" href="<?php echo base_url() ?>sd-contents/onlinestore/images/Lighthouse.jpg" data-fancybox-group="gallery" title="">
														<img class="img-responsive" src="<?php echo base_url() ?>sd-contents/onlinestore/images/Lighthouse.jpg" alt="" />
													</a>
												</td>
												<td>
													<input type="text" class="form-control" name="product_img" value="">
												</td>
												<td>
													<input type="text" class="form-control" name="product_sort" value="1">
												</td>
												<td>
													<a href="javascript:;" class="btn default btn-sm">
													<i class="fa fa-times"></i> Remove </a>
												</td>
											</tr>
											</tbody>
											</table>
											-->
										</div>

									</div>
								</div>
							</div>
						</div>
					</form>

				</div>
			</div>
		</div>			
		<!-- END PAGE CONTENT INNER -->