<style type="text/css">

</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Products</h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		<div class="page-bar">
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="<?= base_url() ?>olstore">Online Store</a><i class="fa fa-circle"></i>
			</li>
			<li class="active">
				 Products
			</li>
		</ul>
		</div>
		<!-- END PAGE BREADCRUMB -->
		<!-- BEGIN PAGE CONTENT INNER -->
		<div class="custom-grid margin-top-20">

		  	<?php if($flashdata) { ?>
		  			<?php if($flashdata['status'] == true) { ?>
							<div class="alert alert-success alert-dismissable">
							  <div style="text-align: right !important; float: right;"><a href="#" class="remove" data-dismiss="alert" aria-label="close">&times;</a></div>
							  <?php echo $flashdata['message']; ?>
							</div>								
					<?php } else { ?>
							<div class="alert alert-warning alert-dismissable">
							  <div style="text-align: right !important; float: right;"><a href="#" class="remove" data-dismiss="alert" aria-label="close">&times;</a></div>
							  <?php echo $flashdata['message']; ?>
							</div>
					<?php } ?>
			<?php } ?>				
  
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box grey-cascade">
						<div class="portlet-title">


							<div class="caption">
								<i class="fa fa-shopping-cart"></i>Product Listing
							</div>
							<div class="actions">

								<a href="<?= base_url() ?>olstore/categories" class="btn default yellow-stripe">
								<i class="fa fa-list"></i>
								<span class="hidden-480">
								Category List </span>
								</a>

								<!-- <a href="javascript:void(0)" data-toggle="modal" data-target="#add-category-modal" class="btn default yellow-stripee">	
								<i class="fa fa-plus"></i>
								<span class="hidden-480">
								Add Category </span>
								</a> -->

								<a href="<?= base_url() ?>olstore/addProduct" class="btn default yellow-stripe">
								<i class="fa fa-plus"></i>
								<span class="hidden-480">
								Add Product </span>
								</a>
								
							</div>

						</div>
						<div class="portlet-body">

							<table class="table table-striped table-bordered table-hover" id="olstore_dtatable">
							<thead>
							<tr>
								<th>
									Id
								</th>
								<th>
									Product Name
								</th>
								<th>
									Category
								</th>
								<th>
									Price
								</th>
								<th>
									QTY Stocks
								</th>
								<th>
									Status
								</th>
								<th>
									Actions
								</th>
							</tr>
							</thead>
							<tbody>
							<?php if ($products != false):?>
							<?php foreach( $products as $pkey => $p ) { ?>
									<tr class="odd gradeX">
										<td>
											<?php echo $p->id; ?>
										</td>
										<td>
											<?php echo $p->name; ?>
										</td>
										<td>
											<?php echo $p->category_name; ?>
										</td>
										<td class="center">
											Php <?php echo number_format($p->price,2); ?>
										</td>
										<td>
											<?php echo $p->quantity; ?>
										</td>
											<!-- 
												- pulished
												- deleted or archived
												- not published
											-->
										<td>
											<?php if($p->status == "Archived") { ?>
													<span class="label label-sm label-danger"><?php echo $p->status; ?></span>
											<?php }elseif($p->status == "Published") { ?>
													<span class="label label-sm label-success"><?php echo $p->status; ?></span>
											<?php }elseif($p->status == "Not Published") { ?>
													<span class="label label-sm label-info"><?php echo $p->status; ?></span>
											<?php } ?>
										</td>
										<td>
											<!-- <a href="<?= base_url() ?>olstore/orderView/<?php echo $p->id; ?>" title="View Details" class="btn green btn-xs"><i class="glyphicon glyphicon-eye-open"></i></a> -->
											<a href="<?= base_url() ?>olstore/editProduct/<?php echo $p->id; ?>" title="Edit Details" class="btn blue btn-xs"><i class="glyphicon glyphicon-pencil"></i></a>
											<a href="javascript:void(0);" title="Delete Product" data-toggle="modal" data-target="#del-modal-<?php echo $p->id; ?>" class="btn red btn-xs"><i class="glyphicon glyphicon-trash"></i></a>
										</td>
									</tr>	

									<div id="del-modal-<?php echo $p->id; ?>" class="modal fade" role="dialog">
										<form method="post" action="<?= base_url() ?>olstore/archiveProduct">
										<input type="hidden" name="id" value="<?php echo $p->id; ?>">	
										<div class="modal-dialog">
										    <div class="modal-content">
											    	<div class="modal-header">
												        <button type="button" class="close" data-dismiss="modal">&times;</button>
												        <h4 class="modal-title">Confirmation</h4>
											      	</div>

										      		<div class="modal-body">
										        		<p>Are you sure you want to archived this product?</p>
										      		</div>
										      	<div class="modal-footer">
										      		<input type="submit" class="btn red" value="Archived">
										        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										      	</div>
										    </div>
										</div>
									</form>
									</div>		
																
							<?php } ?>
							<?php endif;?>
							</tbody>
							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
		</div>
		<!-- END PAGE CONTENT INNER -->
	</div>
</div>
<!-- END CONTENT --> 

<!-- Add New Order Form Popup -->
<div class="modal fade bs-modal-sm" id="add-category-modal" tabindex="-1" role="dialog" aria-hidden="true">
	
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Add Category</h4>
			</div>
			<div class="modal-body">
				<form action="<?= base_url() ?>olstore/addOrder" method="post">
					<div class="form-body">
						<div class="form-group">
							<label>Name</label>
							<input type="text" class="form-control" placeholder="Category Name">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
				<input type="submit" class="btn blue" name="submit" value="Add">
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	
	<!-- /.modal-dialog -->
</div>