<style type="text/css">

</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Product - Categories</h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		<div class="page-bar">
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="<?= base_url() ?>olstore">Online Store</a><i class="fa fa-circle"></i>
			</li>
			<li class="active">
				 Categories
			</li>
		</ul>
		</div>
		<!-- END PAGE BREADCRUMB -->
		<!-- BEGIN PAGE CONTENT INNER -->
		<div class="custom-grid margin-top-20">

		  	<?php if($flashdata) { ?>
		  			<?php if($flashdata['status'] == true) { ?>
							<div class="alert alert-success alert-dismissable">
							  <div style="text-align: right !important; float: right;"><a href="#" class="remove" data-dismiss="alert" aria-label="close">&times;</a></div>
							  <?php echo $flashdata['message']; ?>
							</div>								
					<?php } else { ?>
							<div class="alert alert-warning alert-dismissable">
							  <div style="text-align: right !important; float: right;"><a href="#" class="remove" data-dismiss="alert" aria-label="close">&times;</a></div>
							  <?php echo $flashdata['message']; ?>
							</div>
					<?php } ?>
			<?php } ?>		
  
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box grey-cascade">
						<div class="portlet-title">

							<div class="caption">
								<i class="fa fa-shopping-cart"></i>Category Listing
							</div>
							
							<div class="actions">
								<a href="<?= base_url() ?>olstore/products" class="btn default yellow-stripe">
								<i class="fa fa-list"></i>
								<span class="hidden-480">
								Product Listing </span>
								</a>

								<a href="javascript:void(0)" data-toggle="modal" data-target="#add-category-modal" class="btn default yellow-stripe">
								<i class="fa fa-plus"></i>
								<span class="hidden-480">
								New Category </span>
								</a>

							</div>

						</div>
						<div class="portlet-body">

							<table class="table table-striped table-bordered table-hover" id="olstore_dtatable">
							<thead>
							<tr>
								<th>Image</th>
								<th>Name</th>
								<th>Action</th>
							</tr>
							</thead>
							<tbody>
							<?php if ($categories != false):?>
							<?php foreach($categories as $o_key => $c) { ?>
							<tr class="odd gradeX">
								<td><img style="width: 100px;" class="img-responsive" src ="<?php echo base_url(); ?>sd-contents/onlinestore/images/<?php echo $c->category_image; ?>" alt="" /></td>
								<td>
									<?php echo $c->category_name; ?>
								</td>
								<td>
									<a href="javascript:void(0);" title="Delete Category" data-toggle="modal" data-target="#del-modal-<?php echo $c->id; ?>" class="btn red btn-xs"><i class="glyphicon glyphicon-trash"></i></a>	
									<a href="javascript:void(0)" data-toggle="modal" data-target="#edit-category-modal-<?php echo $c->id; ?>" class="btn blue btn-xs"><i class="glyphicon glyphicon-pencil"></i></a>
								</td>
							</tr>

							<div id="del-modal-<?php echo $c->id; ?>" class="modal fade" role="dialog">
								<form action="<?= base_url() ?>olstore/deleteCategory/<?php echo $c->id; ?>">
								<div class="modal-dialog">
								    <div class="modal-content">
									    	<div class="modal-header">
										        <button type="button" class="close" data-dismiss="modal">&times;</button>
										        <h4 class="modal-title">Confirmation</h4>
									      	</div>

								      		<div class="modal-body">
								        		<p>Are you sure you want to delete this category?</p>
								      		</div>
								      	<div class="modal-footer">
								      		<input type="submit" class="btn red" value="Delete">
								        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								      	</div>
								    </div>
								</div>
							</form>
							</div>

							<!-- Update Popup -->
							<div class="modal fade bs-modal-lg" id="edit-category-modal-<?php echo $c->id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
								<form action="<?= base_url() ?>olstore/updateCategory" method="post" enctype="multipart/form-data">
								<input type="hidden" name="id" value="<?php echo $c->id; ?>">
									<div class="modal-dialog modal-lg">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
												<h4 class="modal-title">Edit Category</h4>
											</div>
											<div class="modal-body">
												<div class="form-body">
													<div class="form-group">
														<label>Name</label>
														<input type="text" id="category_name" name="category_name" value="<?php echo $c->category_name; ?>" class="form-control" placeholder="">
													</div>
													<div class="form-group">
														<label>Upload Image</label>
														<input type="hidden" name="size" value="10000000">
														<input type="file" name="cat_image" id="cat_image" multiple style="color: black;">
														
													</div>													
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn default" data-dismiss="modal">Close</button>
												<input type="submit" class="btn blue" name="submit" value="Update">
											</div>
										</div>
										<!-- /.modal-content -->
									</div>
								</form>
								<!-- /.modal-dialog -->
							</div>
							<?php } ?>
							<?php endif;?>
							</tbody>
							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
		</div>			
		<!-- END PAGE CONTENT INNER -->
	</div>
</div>
<!-- END CONTENT --> 

<!-- Add New Form Popup -->
<div class="modal fade bs-modal-lg" id="add-category-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<form action="<?= base_url() ?>olstore/addCategory" method="post" enctype="multipart/form-data">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Add New Category</h4>
				</div>
				<div class="modal-body">
					<form role="form">
						<div class="form-body">
							<div class="form-group">
								<label>Name</label>
								<input type="text" id="category_name" name="category_name" value="" class="form-control" placeholder="">
							</div>
							<div class="form-group">
								<label>Upload Image</label>
								<input type="hidden" name="size" value="10000000">
								<input type="file" name="image[]" id="catimg" multiple style="color: black;">
								
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn default" data-dismiss="modal">Close</button>
					<input type="submit" class="btn blue" name="submit" value="Add">
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
	</form>
	<!-- /.modal-dialog -->
</div>

