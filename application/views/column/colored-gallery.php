<section class="ui-state-default section-appender colored-gallery">
		<div class="container-fluid tagline-header">
			<div class="row">
				<div class="col-md-12">
					<h2 class="text-only sd-content-title">Have a look on our</h2>
				</div>
			</div>
			<div class="row">			
				<div class="col-md-12">
					<h1 class="text-only sd-content-title">Furniture Gallery</h1>
				</div>
			</div>
		</div>			
		<div class="container">
			<div class="row no-gutters">
				<div class="col-sm-4">
					<div class="service-content cabinets">
					  <button class="galleryShow"><i class="fa fa-image"></i></button>
						<div class="services-text">
							<h1>Cabinets</h1>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="service-content outdoor-tables">
					  <button class="galleryShow"><i class="fa fa-image"></i></button>
						<div class="services-text">
							<h1>Outdoor Tables</h1>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="service-content table">
					  <button class="galleryShow"><i class="fa fa-image"></i></button>
						<div class="services-text">
							<h1>Table</h1>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="service-content diner">
					  <button class="galleryShow"><i class="fa fa-image"></i></button>
						<div class="services-text">
							<h1>Diner</h1>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="service-content chairs">
					  <button class="galleryShow"><i class="fa fa-image"></i></button>
						<div class="services-text">
							<h1>Chairs</h1>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="service-content decor">
					  <button class="galleryShow"><i class="fa fa-image"></i></button>
						<div class="services-text">
							<h1>Decor</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php $sd->column->bar($sd); ?>
</section>