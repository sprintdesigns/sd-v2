<section class="ui-state-default section-appender">
	<div class="container-fluid">
		<div class="row">
		    <h1 class="text-only">Section Title</h1>
			<?php $col_class = 12/intval($col); ?>
			<?php for ($i=0; $i < $col; $i++) { ?>
				<div class="col-md-<?= $col_class ?>">
					<?php $sd->form->load($sd); ?>
				</div>
			<?php } ?>
		</div>
	</div>
	<?php $sd->column->bar($sd); ?>
</section>