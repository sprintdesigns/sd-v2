<?php if($sd->edit->mode()) { ?>         
<div class="after-section sd-column-shell">
  <?php if($type == 'content'){ ?>
    <button class="btn btn-danger" style="position: absolute;" onclick="deleteColumn(this)" ><i class="fa fa-trash"></i></button>
  <?php } ?>
  <section class="addable">
    <div class="hoverable-trigger">
    </div>    
     <?php $slide_btn_class = $sd->column->class_bar(); ?>
    <button class="btn btn-primary slidecx <?= $slide_btn_class ?>" title="Add new Section"><i class="fa fa-plus"></i></button>
    <div class="pickRow">
      <i class="wnd-insert-arrow"></i>
      <div class="controw"><h2 class="col-md-12">ADD SECTION</h2></div>
      <div class="relative">
        <?php 
          $carousel_class = $sd->column->class_bar(); 
          $section = $sd->column->get_theme_section($sd);
          $core_sections = $sd->column->get_core_section($sd);

          $section = array_merge($section, $core_sections);
        ?>
   
        <div class="slidemebar carousel slide <?= $carousel_class ?>" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <?php 
              $mo_sec = sizeof($section) % 6;

              if($mo_sec == 0)
              {

                $li_sec = floor(sizeof($section) / 6);

              }else
              {
                $li_sec = floor(sizeof($section) / 6) + 1;
              }

            ?>
            <?php for ($i=0; $i < $li_sec; $i++) { ?>
              <li data-target=".<?= $carousel_class ?>" data-slide-to="<?= $i ?>" class="<?= $i==0? 'active':'' ?>"></li>
            <?php } ?>
            
          </ol>
          <div class="carousel-inner" role="listbox">
             <?php $sec_index = 0; ?>
            <?php for ($i=0; $i < $li_sec; $i++) { ?>
              <?php $parent_div = 0; ?>
              <div class="item <?= $i==0? 'active':'' ?>">
                <?php while ( $parent_div%6 != 0 || $parent_div == 0) { ?>
                  <?php $child_div = 0; ?>
                  <div class="controw">
                  <?php while ( $child_div%3 != 0 || $child_div == 0) { ?>
                      <?php if(array_key_exists($sec_index, $section)){ ?>                    
                        <div class="col-md-4 col-sm-4 col-xs-4">
                          <button class="<?= $section[$sec_index]->class_img ?> sd-add-column" data-id="<?= $section[$sec_index]->id ?>" ><?= $section[$sec_index]->title ?></button>
                        </div>
                      <?php } ?>
                    <?php $sec_index++; ?>
                    <?php $child_div++; ?>
                    <?php $parent_div++; ?>
                  <?php } ?>                 
                  </div>     

                 <?php } ?>

              </div>

            <?php } ?>

          </div>
        </div>
      </div>  
    </div>
  </section>
  <script>
    var slide_btn_class = '<?php echo $slide_btn_class; ?>';
    $(document).ready(function(){
      $('.'+slide_btn_class).click(function(){
        if($(this).parent().hasClass('open')) {
            $this = $(this);
            $this.parent().removeClass('open');   
            setTimeout(function() {   
              $this.parent().parent().removeAttr('style');
            }, 300);

          }  
          else {
            $this = $(this);
            $this.parent().toggleClass('open');
          }

      });
    });
  </script>
</div>
<?php } ?>



