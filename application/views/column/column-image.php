<section class="ui-state-default section-appender">
	<div class="container">
		<div class="row">
		  	<div class="col-md-12 fl-rich-text">
				<h2 class="text-only sd-content-title">Welcome to</h2>
				<h1 class="text-only sd-content-title"><span id="first"><span id="second">Company</span>&nbsp;Name</span></h1>
			</div>
			<div class="col-md-12 fl-module fl-module-rich-text fl-node-577a255ac120a">
				<div class="fl-module-content fl-node-content">
					<div class="fl-rich-text sd-col-content">
						<div class="alignleft wp-image-24 service-content" style="background-image:url(http://design.sprintdesignsph.com/furniture1/wp-content/uploads/2016/07/img01-1.jpg)">
							  <button class="galleryShow"><i class="fa fa-image"></i></button>
						</div>
						<div class="editable">
							This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitaae erat consequat auctor eu in elit. 
							Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.
							This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitaae erat consequat au
							Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</span>
						</div>
					</div>
				</div>
			</div>

		<!--THIRD PART-->
			<div class="col-md-12" id="my-third-part">
				<a class="fl-button" href="http://design.sprintdesignsph.com/furniture1/about-us/" target="_self" role="button"><button id="btn1" type="button">MORE ABOUT US</button></a>
				<a class="fl-button" href="http://design.sprintdesignsph.com/furniture1/contact-us/ " target="_self" role="button"><button id="btn2" type="button">CONTACT US</button></a>
			</div>
		</div>
	</div>
	<?php $sd->column->bar($sd); ?>
</section>