<?php

	defined('BASEPATH') OR exit('No direct script access allowed');

	class Changepass_m extends CI_Model{

		public function getProfile(){
			$this->db->order_by('id', 'desc');
			$query = $this->db->get('sd_login');
			if($query->num_rows() > 0){
				return $query->result();
			}
			else{
				return false;
			}
		}

		public function getProfileById($id){
			$this->db->where('id', $id);
			$query = $this->db->get('sd_login');
			if($query->num_rows() > 0){
				return $query->row();
			}
			else{
				return false;
			}
		}

		public function updatepassword($password, $id){
			$id = $this->input->post('txt_hidden');
			$field = array(
				'password'=> $password
				);
			$this->db->where('id', $id);
			$this->db->update('sd_login', $field);
			if($this->db->affected_rows() > 0){
				return true;
			}
			else{
				return false;
			}
		}

		public function delete($id){
			$this->db->where('id', $id);
			$this->db->delete('sd_login');
			if($this->db->affected_rows() > 0){
				return true;
			}
			else{
				return false;
			}
		}
}
?>