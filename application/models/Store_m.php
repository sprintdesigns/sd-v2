<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store_m extends CI_Model
{
	function __construct(){
		parent:: __construct();
		$this->load->database();
		
	}

	public function getAllOrders(){
		$this->db->order_by('id', 'asc');
		$query = $this->db->get('orders');

		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	public function getAllOrdersbyID($id){
		$this->db->where('id', $id);
		$query = $this->db->get('orders');
		if($query->num_rows() > 0){
			return $query->row();
		} else {
			return false;
		}
	}

	public function countOrders($id)
    {
        $this->db->where('id' , $id);
        $result = $this->db->get('orders');
        return $result->num_rows();
    }

    public function getAllMessages()
    {
    	$this->db->order_by('id', 'asc');
		$query = $this->db->get('messages');

		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
    }

    public function getMessage()
    {
		$query = $this->db->get('messages');

		if($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}
    }

	public function getPurchasedMessage($id = 0)
	{
		$result = $this->db->get_where('messages', array('id' => $id));
	
		if($result->num_rows() > 0){
			$return = $result->row();
			return $return;
		}else{
			return false;
		} 		
	}    

	public function getAllCategories(){
		$this->db->order_by('id', 'asc');
		$query = $this->db->get('categories');

		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}	

	public function getCategory($id = ''){

		$result = $this->db->get_where('categories', array('id' => $id));
	
		if($result->num_rows() > 0){
			$return = $result->row();
			return $return;
		}else{
			return false;
		}
	
	}	

	public function getAllProducts(){

		$this->db->select('products.id as id, products.name as name, categories.category_name as category_name, products.price as price, products.quantity as quantity, products.status as status');
		$this->db->from('products');
		$this->db->join('categories', 'categories.id = products.categoryid', 'left');
		$this->db->order_by('products.id', 'asc');
		$query = $this->db->get();

		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	
	}	

	public function getAllItemsByCategory($category_id){

		$this->db->select('products.id as id, products.name as name, categories.category_name as category_name, products.price as price, products.quantity as quantity, products.status as status, photos.image as default_product_image');
		$this->db->from('products');
		$this->db->join('categories', 'categories.id = products.categoryid', 'left');
		$this->db->join('photos', 'photos.prodid = products.id', 'left');
	
		if($category_id != '') {
			$where = array(
				'products.categoryid' => $category_id, 
				'products.status' => "Published", 
				'photos.is_default' => 'Yes',
			);
			$this->db->where($where);			
		} else {
			$where = array(
				'products.status' => "Published", 
			);
			$this->db->where($where);				
		}

		$this->db->group_by('products.id'); 
		$this->db->order_by('products.id', 'asc');
		$query = $this->db->get();

		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	
	}
	

	public function getAllImagesByProduct($id = ''){

		$this->db->order_by('id', 'asc');
		$query = $this->db->get_where('photos', array('prodid' => $id));

		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}	

	}

	public function getProduct($id = ''){

		$result = $this->db->get_where('products', array('id' => $id));
	
		if($result->num_rows() > 0){
			//$return = $result->result_array();
			$return = $result->row();
			return $return;
		}else{
			return false;
		}
	
	}	

	public function getPublishedProduct($id = ''){


		$this->db->join('categories', 'categories.id = products.categoryid', 'left');
		$result = $this->db->get_where('products', array('products.id' => $id, 'status' => "Published"));
	
		if($result->num_rows() > 0){
			$return = $result->row();
			return $return;
		}else{
			return false;
		}
	
	}		

	public function getProductPhotos($product_id = '') {
		$result = $this->db->get_where('photos', array('prodid' => $product_id));
		echo "<script> console.log('Product ID: ' + " . $product_id . "); </script>";
		
		if($result->num_rows() > 0){
			$return = $result->result();
			return $return;
		}else{
			return false;
		}

	}	

	public function getProductPhotosCount($product_id = '') {
		$this->db->select('count(image) as image_count');
		$result = $this->db->get_where('photos', array('prodid' => $product_id));


		if($result->num_rows() > 0) {
			return $result->result()[0]->image_count;
		}
		else {		
			return false;
		}
	}

	public function addCategory(){

		$data  = $this->input->post();
		$file  = $_FILES;
		$imagename = $_FILES['image']['name'][0];

		if($data['category_name'] == '') {
			return false; 
			exit;
		}

		if($file['image']['error'][0] == 0) {
			$field = array(
						'category_name'  => $data['category_name'],
						'category_image' => $imagename
					);			
		} else {
			$field = array(
						'category_name'  => $data['category_name']
					);
		}

		$this->db->insert('categories', $field);

		if($file['image']['error'][0] == 0) {
			move_uploaded_file($_FILES['image']['tmp_name'][0],"sd-contents/onlinestore/images/".$imagename);
		}

		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}	

	public function addProduct(){

		$data  = $this->input->post();
		$file  = $_FILES;
		$file_count = count($file['file_product_img_upload']['name']);

		if( $data['name'] == '' || $data['price'] == '' || $data['status'] == '' ) {
			return false; 
			exit;
		}

		$field = array(
					'name' 			=> $data['name'],
					'description' 	=> $data['description'],
					'price' 		=> $data['price'],
					'categoryid' 	=> $data['categoryid'],
					'quantity' 		=> $data['quantity'],
					'color' 		=> $data['color'],
					'size' 			=> $data['size'],
					'status' 		=> $data['status'],
				);

		$this->db->insert('products', $field);
		$inserted_id = $this->db->insert_id();
 
		for ($i = 0; $i < $file_count; $i++) {
			if($file['file_product_img_upload']['error'][$i] == 0) {
				$name 	= $file['file_product_img_upload']['name'][$i];
				$imname = $file['file_product_img_upload']['tmp_name'][$i];
				move_uploaded_file($imname,"sd-contents/onlinestore/images/".$name);
				if($i == 0) 
					$response_img = $this->db->insert('photos', array('image' => $name, 'prodid' => $inserted_id, 'is_default' => 'Yes'));
				else
					$response_img = $this->db->insert('photos', array('image' => $name, 'prodid' => $inserted_id, 'is_default' => 'No'));	
			}
		}

		if($this->db->affected_rows() > 0){
			return true;
		} else {
			return false;
		}
	}


	public function updateProduct(){
		$data  = $this->input->post();
		$file  = $_FILES;
		$file_count = count($file['file_product_img_upload']['name']);

		if( $data['name'] == '' || $data['price'] == '' || $data['status'] == '' ) {
			return false; 
			exit;
		}

		$field = array(
					'name' 			=> $data['name'],
					'description' 	=> $data['description'],
					'price' 		=> $data['price'],
					'categoryid' 	=> $data['categoryid'],
					'quantity' 		=> $data['quantity'],
					'color' 		=> $data['color'],
					'size' 			=> $data['size'],
					'status' 		=> $data['status'],
				);

		$this->db->trans_start();

		$this->db->where('id', $data['id']);
		$result = $this->db->update('products', $field);

		$this->db->trans_complete();

		if($data['is_default'] != '') {

			/*
			 * 'No' all 'is_dafault' values
			*/
			$null_field = array(
					'is_default' => "No"
				);

			$this->db->where('prodid', $data['id']);
			$result = $this->db->update('photos', $null_field);	

			/*
			 * Make the image default
			*/



			$is_default_field = array(
					'is_default' => "Yes"
				);

			$this->db->where('id', $data['is_default']);
			$result = $this->db->update('photos', $is_default_field);			

		}

		for($i = 0; $i < $file_count; $i++) {
			if($file['file_product_img_upload']['error'][$i] == 0) {
				$name 	= $file['file_product_img_upload']['name'][$i];
				$imname = $file['file_product_img_upload']['tmp_name'][$i];
				move_uploaded_file($imname,"sd-contents/onlinestore/images/".$name);
				$response_img = $this->db->insert('photos', array('image' => $name, 'prodid' => $data['id'], 'is_default' => 'No'));
			}
		}

		
		if($this->db->trans_status() === FALSE)
		{
		    $response = array(
		        'message' => "Unable to edit user",
		        'status' => false
		    );
		}
		else
		{
		    if($this->db->affected_rows() > 0)
		    {
		        $response = array(
		            'message' => "User edited successfully",
		            'status' => true
		        );
		    }
		    else
		    {
		        $response = array(
		            'message' => "User edited successfully, but it looks like you haven't updated anything!",
		            'status' => true
		        );
		    }
		}

		if($response['status'] == true) {
			return true;
		}
		

		if($this->db->affected_rows() > 0){
			return $result;
		} else { return false; }		

	}

	public function updateImageTable() {
		$data = $this->input->post();
		$file = $_FILES;
		
	}
	
	public function updateCategory(){

		$data  = $this->input->post();
		$file  = $_FILES;
		$imagename = $_FILES['cat_image']['name'];

		if($data['category_name'] == '') {
			return false; 
			exit;
		}		

		if($file['cat_image']['error'][0] == 0) {

			$field = array(
						'category_name' => $data['category_name'],
						'category_image' => $imagename
					);		

		} else {

			$field = array(
						'category_name' => $data['category_name']
					);					

		}

		$this->db->where('id', $data['id']);
		$result = $this->db->update('categories', $field);

		if($file['cat_image']['error'][0] == 0) {
			move_uploaded_file($_FILES['cat_image']['tmp_name'],"sd-contents/onlinestore/images/".$imagename);
		}

		if($this->db->affected_rows() > 0){
			return $result;
		} else { return false; }

	}

	public function ArchivedProduct(){

		$data  = $this->input->post();
		$field = array(
			'status'=> "Archived",
		);

		$this->db->where('id', $data['id']);
		$result = $this->db->update('products', $field);
		if($this->db->affected_rows() > 0){
			return $result;
		} else { return false; }

	}	

	public function deleteOrder($id){
		$this->db->where('id', $id);
		$this->db->delete('orders');
		if($this->db->affected_rows() > 0){
			return true;
		}else{ return false; }
	}	

	public function deleteCategory($id){
		$this->db->where('id', $id);
		$this->db->delete('categories');
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}

	public function deleteImage($id){
		$this->db->where('id', $id);
		$this->db->delete('photos');
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}

	public function get_all_orders_ob_date() {

		$this->db->order_by('purchase_date', 'desc');
		$query = $this->db->get('orders');

		if($query->num_rows() > 0) {
			return $query->result();
		}
		else {
			return false;
		}
	}

	public function updateMessage($id)
	{
		$field = array(
				'thankyou_message' 	=> $this->input->post('thankyou_message'),
				'note_message' 		=> $this->input->post('note_message'),
				'purchase_message' 	=> $this->input->post('purchase_message')
			);

			$this->db->where('id', $id);
			$result = $this->db->update('messages', $field);
			if($this->db->affected_rows() > 0){
				return $result;
			}
			else{
				return false;
			}
	}

	public function addMessage()
	{
		$id 				= $this->input->post('id');
		$thankyou_message 	= $this->input->post('thankyou_message');
		$note_message 		= $this->input->post('note_message');
		$purchase_message 	= $this->input->post('purchase_message');
		$result 			= $this->getMessage();

		if($result){

			$this->updateMessage($id);
			
		} else {

			$field = array(
				'thankyou_message'	=> $thankyou_message,
				'note_message'		=> $note_message,
				'purchase_message' 	=> $purchase_message
			);
			$this->db->insert('messages', $field);
			if($this->db->affected_rows() > 0){
				return true;
			} else {
				return false;
			}

		}
	}

}