<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store_m extends CI_Model
{
	function __construct(){
		parent:: __construct();
		$this->load->database();
	}

	public function getCategory(){
		$this->db->order_by('id', 'asc');
		$query = $this->db->get('categories');
		if($query->num_rows() > 0){
			return $query->result();
		}
		else{
			return false;
		}
	}

	public function getCategorybyID($id){
		$this->db->where('id', $id);
		$query = $this->db->get('categories');
		if($query->num_rows() > 0){
			return $query->row();
		} else {
			return false;
		}
	}

	public function getProductbyCategoryID($id){
		$this->db->where('id', $id);
		$query = $this->db->get('products');
		if($query->num_rows() > 0){
			return $query->row();
		} else {
			return false;
		}
	}

	public function submit(){
		$field = array(
			'category_name'=>$this->input->post('txt_category')
			);
		$this->db->insert('categories', $field);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}

	public function addProduct()
	{
        // $db = mysqli_connect("localhost", "root", "", "sprintd");

        for($i = 0; $i<count($_FILES["image"]["name"]); $i++)
        {
        	$cid = $_POST['categoryid'];
          	$prodname = $_POST['name'];
        	$desc=$_POST['desc'];
        	$price=$_POST['price'];
        	$quantity=$_POST['quantity'];
        	$color = $_POST['color'];
        	$name=$_FILES['image']['name'][$i];
        	$ftype=$_FILES['image']['type'][$i];
        	$size = $_POST['size'];
        	$type=array("image/jpg","image/png","image/jpeg","image/gif");

        	if(!$name)
        	{
          		echo "<script>alert(\"Please Select a File\")</script>";
          		//redirect('Advanced/defaultcategory/'.$cid);
        	}
        	else
        	{
          		$f=0;
          		for($a=0;$a<4;$a++)
          		{
            		if($type[$a]==$ftype)
            		{
              			$f=1;
              			// $this->load->database();
              			$response = $this->db->insert('products', array('name' => $prodname,
                		'description' => $desc,
                		'price' => $price,
                		'image' => $name,
                		'categoryid' => $cid,
                		'quantity' => $quantity,
                		'color' => $color,
                		'size' => $size));

              		move_uploaded_file($_FILES['image']['tmp_name'][$i],"sd-contents/onlinestore/images/".$name);
              		echo "<script>alert(\"Image Uploaded\")</script>";
            		}
          		}
          		if($f==0)
          		{
            		echo "<script>alert(\"Invalid File type.\")</script>";
          		}
          		return $_POST['categoryid'];
        	}
      	}
	}

	public function addPhoto()
	{

      //$db = mysqli_connect("localhost", "root", "", "sprintd");

      	for($i = 0; $i<count($_FILES["image"]["name"]); $i++)
      	{
      		$pid = $_POST['prodID'];
          	$name=$_FILES['image']['name'][$i];
          	$ftype=$_FILES['image']['type'][$i];
          	$type=array("image/jpg","image/png","image/jpeg","image/gif");

          	if(!$name)
	        {
	           echo "<script>alert(\"Please Select a File.\")</script>";
	        }
	        else
	        {
	        	$f=0;
	            for($a=0;$a<4;$a++)
	            {
	            	if($type[$a]==$ftype)
	              	{
	                	$f=1;
	                	//$this->load->database();
	                	$response = $this->db->insert('photos', array('image' => $name, 'prodid' => $pid));

	                	move_uploaded_file($_FILES['image']['tmp_name'][$i],"sd-contents/onlinestore/images/".$name);
	                	//redirect('Advanced/prod/'.$pid);
	              	}
	            }
	            if($f==0)
	            {
	              echo "<script>alert(\"Invalid File type.\")</script>";
	            }
	            return $pid;
	        }
    	}
	}

	public function getItemDetails($id)
	{
		$result = $this->db->get_where('products',array('id' => $id));

		return $result->result_array();
	}

	public function showAllOrder(){
	$this->db->order_by('id', 'asc');
	$query = $this->db->get('orderinfo');
	if($query->num_rows() > 0){
		return $query->result();
	}else{
		return false;
	}
}

	public function getPhotoDetails($id)
	{
		$result = $this->db->get_where('photos',array('prodid' => $id));

		return $result->result_array();
	}

	public function retrieveItemPhotos($id)
	{
		
		$result = $this->db->get_where('photos', array('prodid' => $id));

		return $result->result_array();
	}

	public function getProductsByCategory($id)
	{
		$result = $this->db->get_where('products', array('categoryid' => $id));

		return $result->result_array();
	}

	public function updateProducts(){
		$id = $this->input->post('txt_hidden');
		$name=$_FILES['image']['name'][0];

		$field = array(
			'name'=>$this->input->post('name'),
			'description'=>$this->input->post('desc'),
			'price' =>$this->input->post('price'),
			'quantity' =>$this->input->post('quantity'),
			'color' => $this->input->post('color'),
			'size' => $this->input->post('size'),
			'image' => $name
			);
		$this->db->where('id', $id);
		$result = $this->db->update('products', $field);
		if($this->db->affected_rows() > 0){
			return $result;
		}
		else{
			return false;
		}
	}

	public function updateProdPhoto($id){
		$field = array(
			'image'=>"shopbag.png"
			);
		$this->db->where('id', $id);
		$result = $this->db->update('products', $field);
		if($this->db->affected_rows() > 0){
			return $result;
		}
		else{
			return false;
		}
	}

	public function deleteProducts($id){
		$this->db->where('id', $id);
		$this->db->delete('products');
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}

	public function deletePhotos($id){
		$this->db->where('prodid', $id);
		$this->db->delete('photos');
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}

	public function deleteCategories($id){
		$this->db->where('id',$id);
		$this->db->delete('categories');
		if($this->db->affected_rows() > 0){
			return true;
		} else {
			return false;
		}
	}

	public function updateCategory(){
		$id = $this->input->post('txt_hidden');
		$field = array(
			'category_name'=>$this->input->post('txt_category'),
			);
		$this->db->where('id', $id);
		$result = $this->db->update('categories', $field);
		if($this->db->affected_rows() > 0){
			return $result;
		}
		else{
			return false;
		}
	}

	public function updateQuantity(){
		$id = $this->input->post('txt_hidden');
		$field = array(
			'quantity'=>$this->input->post('quantity'),
			);
		$this->db->where('id', $id);
		$result = $this->db->update('products', $field);
		if($this->db->affected_rows() > 0){
			return $result;
		}
		else{
			return false;
		}
	}

	public function countProducts($id)
	{
		$this->db->where('categoryid' , $id);
		$result = $this->db->get('products');
		return $result->num_rows();
	}

	public function submitOrder()
	{
		$field = array(
			'email' => $this->input->post('email'),
			'cust_name' => $this->input->post('name'),
			'cust_address' => $this->input->post('address'),
			'cust_number' => $this->input->post('contactnumber'),
			'mode_payment' => $this->input->post('modepayment'),
			'quantity' => $this->input->post('quantity'),
			'color' => $this->input->post('color'),
			'size' => $this->input->post('size'),
			'prodname' => $this->input->post('prodname'),
			'price' => $this->input->post('prodprice'),
			'purchase_date'=>date('Y-m-d')
			);

		$this->db->insert('orderinfo', $field);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}

	public function viewOrder()
	{
		$id = $this->input->get('id');
		$this->db->where('id', $id);
		$query = $this->db->get('orderinfo');
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}

}