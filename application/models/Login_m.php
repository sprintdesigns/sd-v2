<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login_m extends CI_Model  {
	
	public function __construct()
	{
		parent::__construct();
	}	


	public function get_all_login()
	{
		$this->db->select('*');
		$query = $this->db->get('sd_login');

		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}
		
	}

	public function check_login($params = null)
	{

		$this->db->select('*');
		$where = array(
			'username' => $params['username'], 
			'password' => $params['password'], 
		);
		$this->db->where($where);
		$query = $this->db->get('sd_login');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}
		
	}

	
}
?>