<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Sdplugin_m extends CI_Model  {
	
	private $plugin_table;
	private $pages_table;

	public function __construct()
	{
		parent::__construct();

		$this->plugin_table='sd_incs';
	}	


	public  function get_all_plugins()
	{
		$this->db->select('*');
		$query = $this->db->get($this->plugin_table);

		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}
	}

	public  function get_all_themes()
	{
		$this->db->select('*');
		$this->db->from($this->plugin_table);

		$where = array(
			'type' => 'theme', 
		);
		$this->db->where($where);
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}
	}
	

	public function register_theme($theme)
	{
		$data = array(
		   'name' => $theme->name,
		   'type' => $theme->type,
		   'banner' => $theme->banner,
		   'description' =>$theme->description,
		   'status' => 0
		);

		$this->db->insert($this->plugin_table, $data); 
	}

	
}
?>