<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$sd_json = file_get_contents('corefile/sd.json');
$sd_json = json_decode($sd_json);


$active_group = 'default';
$query_builder = TRUE;

$db['default'] = array(
	'dsn'	=> '',
	'hostname' => $sd_json->dbhost,
	'username' => $sd_json->username,
	'password' => $sd_json->password,
	'database' => $sd_json->dbname,
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);
