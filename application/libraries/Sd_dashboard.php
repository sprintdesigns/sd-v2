<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SD_Dashboard {

	private $CI;

	public function __construct()
	{
       $this->CI =& get_instance();
	}

	public function bar($sd)
	{
		$data['sd'] = $sd;

		echo $this->CI->load->view('dashboard/dashboard_bar',$data,true);
	}

	public function logout()
	{
		echo $this->CI->config->base_url().'dashboard/logout';
	}

}

?>