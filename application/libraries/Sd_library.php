<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

require 'PHPMailer-master/PHPMailerAutoload.php';
/*
* UnzipFile class for CI
* 
*/

class SD_Library {
        
    private $library_class;

    public function __construct()
    {
        $this->library_class = array(
            'Rx_Utilities' => 'utilities', 
            'SD_Plugin' => 'plugin', 
            'SD_UnzipFile' => 'unzipfile', 
            'SD_Users_Manager' => 'user', 
            'SD_Theme' => 'theme', 
            'SD_Assets' => 'asset', 
            'SD_Modal' => 'modal', 
            'SD_Uploader' => 'uploader', 
            'SD_Page' => 'page', 
            'SD_Site' => 'site', 
            'SD_Edit' => 'edit',
            'SD_Forms' => 'form',
            'SD_Gallery' => 'gallery',
            'SD_Image' => 'image',
            'SD_Js' => 'js',
            'SD_Css' => 'css', 
            'SD_Dashboard' => 'dashboard', 
            'SD_Column' => 'column', 
            'SD_Dircheck' => 'dircheck',
            'SD_Benchmark' => 'benchmark', 
            'SD_Email' => 'email', 
            'SD_Menu' => 'menu',
            'SD_Banner' => 'banner', 
            'SD_Social' => 'social', 
            'SD_Marketing' => 'marketing',
            'SD_Dynamicsection' => 'dynamicsection' 
        );
    }

    public function loadLibraries()
    {
        $libraries = new stdClass();
       
        foreach ($this->library_class as $class => $instance) {
            $libraries->$instance = new $class();
        }

        return $libraries;
    }



}

?>
