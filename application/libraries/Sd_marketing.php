<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SD_Marketing {

	private $CI;
	private $plugin_table;
	
	public function __construct()
	{
       $this->CI =& get_instance();
       $this->plugin_table = "sd_marketing";
       
	}

	public function marketing_layout($sd = null)
	{
		if($sd == null)
		{
			echo "";
			return;
		}

		$data['sd'] = $sd;
		echo $this->CI->load->view('marketing/index',$data,true);
	}

	public function get_all_banner($id = null)
	{
		$this->CI->db->select('*');
		$this->CI->db->from($this->plugin_table);
		if($id != null)
		{
			$this->CI->db->where('id',$id);
		}
		$query = $this->CI->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}
	}

	public function add_banner($form = null)
	{

		$this->CI->db->insert($this->plugin_table, $form); 

		return $this->CI->db->insert_id();
	}

	public function update_banner($data = null,$id = null)
	{
		$this->CI->db->set($data);
		$this->CI->db->where('id', $id);
		$this->CI->db->update($this->plugin_table);
	}

	public function get_banner_views($sd = null)
	{
		if($sd == null)
		{
			echo "missing sd inizialize";
			return;
		}

		$data['sd'] = $sd;
		return $this->CI->load->view('marketing/marketing-banner-icon',$data,true);
	}

}

?>