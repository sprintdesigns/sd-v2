<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SD_Column {

	private $CI;
	private $plugin_table;

	public function __construct()
	{
       $this->CI =& get_instance();
       $this->plugin_table = 'sd_column';
	}

	public function bar($sd,$type = null)
	{
		if($type == null)
			$type='content';

		$data['sd'] = $sd;
		$data['type'] = $type;
		echo $this->CI->load->view('column/bar',$data,true);
	}

	public function section_core($sd = null, $section_name = null)
	{
		if($sd == null || $section_name == null)
		{
			echo "Invalid argument";
		}

		$data['sd'] = $sd;

		$filename = $section_name .'.php';

		echo $this->CI->load->view('../../sd-assets/php/sections/'. $filename, $data, true);		
	}

	public function section($sd = null,$id = null)
	{
		if($sd == null || $id == null)
		{
			echo "Invalid argument";
		}
		
		$theme = $sd->theme->get_active_theme();
		$theme_name = str_replace(' ', '-', $theme[0]->name);

		$section = $this->get_section($id);

		$data['sd'] = $sd;

		echo $this->CI->load->view('../../sd-includes/themes/'.$theme_name.'/section/'.$section[0]->file,$data,true);
		
	}

	public function class_bar($length = 5)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $class_length = strlen($characters);
	    $random_class = '';
	    for ($i = 0; $i < $length; $i++) {
	        $random_class .= $characters[rand(0, $class_length - 1)];
	    }
	    
		return $random_class;
	}

	public function get_core_section($sd = null) 
	{
		if($sd == null)
		{
			return "sd is not initialize";
		}

		$other_data['2-square-image'] = array('title' => '2-Square Images', 'class_img' => 'two-col-sq-img');
		$other_data['one-column'] = array('title' => '1-Column Paragraph', 'class_img' => 'one-col');
		$other_data['two-column'] = array('title' => '2-Column Paragraph', 'class_img' => 'two-col');
		$other_data['3-circle-image'] = array('title' => '3-Column Circle Images', 'class_img' => 'three-col-circ-img');
		$other_data['content-only'] = array('title' => 'Content Only', 'class_img' => 'one-col');
		$other_data['image-text-left'] = array('title' => 'Image and Text ', 'class_img' => 'img-lefttext');
		$other_data['photo-title'] = array('title' => 'Photo Gallery Tiled', 'class_img' => 'photo-gallery-col');
		$other_data['slick-slider-gallery'] = array('title' => 'Photo Gallery with Navigation', 'class_img' => '');
		$other_data['three-column'] = array('title' => '3-Column with Photos', 'class_img' => 'three-col');

		$core_sections = array();
		$section_files = $this->get_folder_files('sd-assets/php/sections');

		foreach ($section_files as $section_name) {
			list($section_name) = explode('.', $section_name); // remove the ".php"

			if (isset($other_data[$section_name])) {
				$title = $other_data[$section_name]['title'];
				$class_img = $other_data[$section_name]['class_img'];
			} else {
				$title = str_replace('-', ' ', $section_name);
				$class_img = '';
			}
			$core_sections[] = (object) array(
				'id' => $section_name,
				'title' => $title,
				'class_img' => $class_img
			);			
		}
		return $core_sections;
	}

	public function get_folder_files($folder_path) {
        $files = scandir($folder_path);
        unset($files[0]); // remove '.'
        unset($files[1]); // remove '..'
        return $files;
	}


	public function get_theme_section($sd = null)
	{
		if($sd == null)
		{
			return "sd is not initialize";
		}

		$theme = $sd->theme->get_active_theme();

		$this->CI->db->select('*');
		$this->CI->db->from($this->plugin_table);
		$this->CI->db->where('theme_id',$theme[0]->id);

		$query = $this->CI->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}
	}

	public function get_section($id = null)
	{
		if($id == null)
		{
			return "invalid section id";
		}

		$this->CI->db->select('*');
		$this->CI->db->from($this->plugin_table);
		$this->CI->db->where('id',$id);

		$query = $this->CI->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}

	}

	public function register_section($data = null)
	{
		if($data == null)
		{
			return "Invalid data to add";
		}

		$this->CI->db->insert($this->plugin_table, $data); 

		return $this->CI->db->insert_id();
	}


}

?>