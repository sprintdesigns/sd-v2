<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/*
* UnzipFile class for CI
* 
*/
class SD_Theme  {
        
    private $CI;
    private $plugin_table;
    private $theme_path;

    public function __construct()
    {
    	$this->CI =& get_instance();
    	$this->plugin_table='sd_incs';
    	$this->template_table='sd_template';
    	$this->theme_path='sd-includes/themes/';
    }

    public function check_if_has_theme()
	{
		$theme = $this->get_all_themes();
		$check = false;

		if($theme)
		{
			foreach ($theme as $p) {	
				if($p->status == true)
				{
					$check=true;
				}
			}

		}

		return $check;
	}

	function super_unique($obj)
    {
      $result = [];

      foreach ($obj as $index => $value) {
      	$status = true;
      	$value = (object)$value;
      	foreach ($result as $new => $arr) {
      		if($value->name == $arr->name)
      		{
      			$status=false;
      		}
      	}
      	if($status)
      	{
      		array_push($result, $value);
      	}
      }

      return $result;

    }

	public function get_all_themes()
	{
		$this->CI->db->select('*');
		$this->CI->db->from($this->plugin_table);

		$where = array(
			'type' => 'theme', 
		);
		$this->CI->db->where($where);
		$query = $this->CI->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}
	}

	public function get_theme_by_id($id)
	{
		$this->CI->db->select('*');
		$this->CI->db->from($this->plugin_table);

		$where = array(
			'id' => $id
		);
		$this->CI->db->where($where);
		$query = $this->CI->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}
	}	

	public function get_themes_by_folders()
	{
        $folders = scandir('sd-includes/themes');
        $install_file = 'install.json';
        $themes = array();

        foreach ($folders as $folder) {
            $file = "sd-includes/themes/{$folder}/{$install_file}";
            if (file_exists($file)) {
                $json = file_get_contents($file);
                $json = json_decode($json);
                $themes[] = $json;           
            }
        }

        return $themes;
	}		

	public function get_active_theme()
	{
		$this->CI->db->select('*');
		$this->CI->db->from($this->plugin_table);

		$where = array(
			'type' => 'theme', 
			'status' => true, 
		);
		$this->CI->db->where($where);
		$query = $this->CI->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}
	}

	public function activate_theme($id = null)
	{
		$this->CI->db->set('status', false);
		$this->CI->db->where('status', true);
		$this->CI->db->update($this->plugin_table);

		$this->CI->db->set('status', true);
		$this->CI->db->where('id', $id);
		$this->CI->db->update($this->plugin_table);
	}

	public function update_theme($id = null,$data = null)
	{
		$this->CI->db->set($data);
		$this->CI->db->where('id', $id);
		$this->CI->db->update($this->plugin_table);
	}

	public function register_theme($theme)
	{
		$data = array(
		   'name' => $theme->name,
		   'type' => $theme->type,
		   'banner' => $theme->banner,
		   'description' =>$theme->description,
		   'author' => $theme->author,
		   'status' => 0
		);
		
		$this->CI->db->insert($this->plugin_table, $data); 

		$theme_id = $this->CI->db->insert_id();

		$this->register_template($theme->template,$theme_id);

		return $theme_id;
	}

	public function get_theme_path($theme_name = null)
	{
		$theme = $this->get_active_theme();

		if($theme_name == null){
			$theme_name = str_replace(' ', '-', $theme[0]->name);
		}
		else
		{
			$theme_name = str_replace(' ', '-', $theme_name);
		}

		$theme_path = $this->theme_path;
		return $this->CI->config->base_url().$theme_path.$theme_name.'/';
	}

	/*
	* Get install.json data
	*
	*/
	public function get_install_data($theme_name) {
        $folder = $theme_name;
        $install_file = 'install.json';

        $file = "sd-includes/themes/{$folder}/{$install_file}";
        if (file_exists($file)) {
            $json = file_get_contents($file);
            $json = json_decode($json);
            return $json;
        } else {
        	return false;
        }
	}

	public function install_theme($theme_name) {
		$libraries = new SD_Library();
		$this->sd = $libraries->loadLibraries();		

		$theme = $this->get_install_data($theme_name);

		if (! $theme) {
			return false;
		}		

		$result = $theme;
		$theme_id = $this->register_theme($result);

    	//register slide
    	if(array_key_exists('slider', $result)){
	    	foreach ($result->slider as $index => $slide) {

	    		$data = array(
	    			'url' => base_url().'sd-includes/themes/'.$slide, 
	    			'original_url' => base_url().'sd-includes/themes/'.$slide, 
	    			'text_title' => $index, 
	    			'sub_title' => $index, 
	    			'opacity' => '0', 
	    			'button_title' => 'learn more', 
	    			'button_url' => "#", 
	    			'theme_id' => $theme_id, 
	    		);

	    		$insert_slide = $this->sd->banner->create_slide($data);	
	    	}
	    }

	    //register section
	    if(array_key_exists('section', $result)){
	    	foreach ($result->section as $index => $section) {

	    		$data = array(
	    			'title' => $section->title, 
	    			'file' => $section->file, 
	    			'class_img' => $section->class_img,  
	    			'theme_id' => $theme_id
	    		);

	    		$insert_section = $this->sd->column->register_section($data);	
	    	}
    	}

    	//register pages
    	$menu_id = $this->sd->menu->create_new_menu($theme_id);
    	if(array_key_exists('pages', $result))
    	{
    		
    		foreach ($result->pages as $name => $data) {
    			$new_name = str_replace('_', ' ', $name);

    			$page_content="";
    			for ($i=0; $i < sizeof($data->sections); $i++) { 
    				$this->data['sd'] = $this->sd;
    				$section = $this->CI->load->view('../../sd-includes/themes/'.$result->name.'/section/'.$data->sections[$i],$this->data,true);
    					
    				$page_content .= $section;    				 
    			}
    			if(array_key_exists('status', $data))
    			{
    				$status = $data->status;
    			}else
    			{
    				$status = true;
    			}
    			$data = array(
					'page_title' => $new_name, 
					'page_url' =>preg_replace('/\s+/', '-',strtolower($new_name)),
					'template_name' => $data->template, 
					'meta_description' => '', 
					'page_content' => $page_content, 
					'theme_id' => $theme_id, 
					'type' => true, 
					'status' => $status, 
				);

				if($id = $this->sd->page->create($data))
				{
					
					$menu = $this->sd->menu->get_menu($theme_id);				
					if($menu[0]->orders == null)
					{
						$nav = [];
					}else
					{
						$nav = json_decode($menu[0]->orders);
					}
					
					array_push($nav,(object) array('id' => $id ));
					$data = array(
						'orders' => json_encode($nav), 
					);
					$menu_response = $this->sd->menu->update_menu($theme_id,$data);
				}
    		}
    	}
    	$social_id = $this->sd->social->add_social_init($theme_id);

    	return $theme_id;		    		
	}

	public function register_template($template,$theme_id)
	{
		foreach ($template as $name => $file) {
			
			$data = array(
				'template_name' => $name, 
				'file' => $file,
				'theme_id' => $theme_id 
			);
	
			$this->CI->db->insert($this->template_table, $data); 

		}
	}
	public function get_template()
	{
		$theme = $this->get_active_theme();


		$this->CI->db->select('*');
		$this->CI->db->from($this->template_table);

		$where = array(
			'theme_id' => $theme[0]->id
		);
		$this->CI->db->where($where);
		$query = $this->CI->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	public function get_template_by_id($template_id = null)
	{
		if($template_id == null)
		{
			return "Invalid template id";
		}

		$this->CI->db->select('*');
		$this->CI->db->from($this->template_table);

		$where = array(
			'id' => $template_id
		);
		$this->CI->db->where($where);
		$query = $this->CI->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}

	}

	public function download_install_theme($folder, $link)
	{
		if ( isset($_GET['id']) || isset($_GET['url']) ) 
		{
			$folder = $_GET['id'];
        	$link = $_GET['url'];
		}		
		
		ini_set('max_execution_time', 0); 

		$url = $link;
		$zipFile = "sd-includes/themes/" . $folder . ".zip"; /* LOCAL ZIP FILE PATH */
		$zipResource = fopen($zipFile, "w");
		
		/* GET THE ZIP FILE FROM THE SERVER */
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 600); /* 10 minute, maximum number of seconds to allow cURL functions to execute. */
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); 
		curl_setopt($ch, CURLOPT_FILE, $zipResource);
		$page = curl_exec($ch);
		
		if(!$page) 
		{
		 	echo "Error : ".curl_error($ch);
		}

		curl_close($ch);
		fclose($zipResource);

		/* OPEN ZIP FILE */
		$zip = new ZipArchive;
		$extractPath = "sd-includes/themes";
		
		if($zip->open($zipFile) != "true")
		{
		 	echo "Error : Unable to open the Zip File";
		} 
		
		/* EXTRACT ZIP FILE */
		$zip->extractTo($extractPath);
		$zip->close();

		/* DELETE ZIP FILE */
		unlink($zipFile); 
	}

	public function update_all_themes($theme_names, $links) 
	{
		ini_set('max_execution_time', 0);
		
		for($x = 0; $x < count($theme_names); $x++)
		{
			$zip_file = "sd-includes/themes/" . $theme_names[$x] . ".zip"; // LOCAL ZIP FILES PATH
			$zip_resource = fopen($zip_file, "w");
			
			// GET FOLDER MODIFIED DATE
			$folder_stat = stat("sd-includes/themes/{$theme_names[$x]}");
			$folder_mod_date = date("d-m-Y H:i:s", $folder_stat['mtime']);

			// GET THE ZIP FILES FROM THE SERVER 
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $links[$x]);
			curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
			curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
			curl_setopt($ch, CURLOPT_BINARYTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_TIMEOUT, 600); // 10 minute, maximum number of seconds to allow cURL functions to execute. 
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); 
			curl_setopt($ch, CURLOPT_FILE, $zip_resource);
			curl_setopt($ch, CURLOPT_FILETIME, TRUE);
			$page = curl_exec($ch);
			
			// GET RAR MODIFIED DATE
			$info = curl_getinfo($ch);
			$rar_mod_date = date("d-m-Y H:i:s", $info['filetime']);

			curl_close($ch);
			fclose($zip_resource);

			// CONVERT TO DATETIME
			$rar_date = new DateTime($rar_mod_date);
			$folder_date = new DateTime($folder_mod_date);

			if($rar_date > $folder_date)
			{
				$zip = new ZipArchive;
			
				if($zip->open($zip_file) === TRUE) 
				{
					$zip->extractTo("sd-includes/themes");				
					$zip->close();
					unlink($zip_file);
				}
			}
			else
			{
				unlink($zip_file);
				
			}
		}
	}
}