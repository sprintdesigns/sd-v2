<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SD_Benchmark {

	private $CI;

	public function __construct()
	{
       $this->CI =& get_instance();
	}


	public function escape($value = null)
	{
		if($value = null)
		{
			echo "the given value must not contain null";
			return ;
		}
		

		return $this->CI->db->escape_str($value);
	}


}

?>