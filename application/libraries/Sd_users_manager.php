<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/*
* UnzipFile class for CI
* 
*/
class SD_Users_Manager extends SD_Users {
        
    private $CI;

    public function __construct()
    {
    	
        parent::__construct();

        $this->CI =& get_instance();
     
    }


    public function check_if_login()
	{
		
		$user_data = $this->CI->session->userdata('user_data');
		if($user_data != null)
		{
			return true;
		}else
		{
			return false;
		}
	}

	public function checkUser($params = null)
	{

		$this->CI->db->select('*');
		$where = array(
			'username' => $params['username'], 
			'password' => $params['password'], 
		);
		$this->CI->db->where($where);
		$query = $this->CI->db->get('sd_login');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}
	}

	public function checkUsername($username)
	{
		$this->CI->db->select('*');
		$where = array(
			'username' => $username
		);
		$this->CI->db->where($where);
		$query = $this->CI->db->get('sd_login');
		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}
	}

	public function get_current_user()
	{
		return $this->CI->session->userdata('user_data');
	}

	public function site_name()
	{
		$user_data = $this->get_current_user();
		return $user_data->name;
	}
        
}

?>
