<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SD_Dynamicsection {

	private $CI;
	private $allowed_functions = array('sd_layer_slider');
	private $data;

	public function __construct()
	{
       $this->CI =& get_instance(); 
	}

	public function apply_dynamic_sections($content) {
	    ob_start();
	    echo $content;
	    $the_content = ob_get_clean();

	    $allowedFunctions = $this->allowed_functions;

	    $replaced = preg_replace_callback("~(".implode("|",$allowedFunctions).")\(\)~",
	        function ($function_found) {
	            return $this->$function_found[1]();
	        }, $the_content);

	    return $replaced;		
	}

	public function sd_layer_slider() {
		$theme = new SD_Theme;
		$theme_id = $theme->get_active_theme()[0]->id;

		$s = new SD_Banner;
		$slider = $s->get_slider(null, $theme_id);

		$data = [];
		$data['sliders'] = $slider;

		return $this->CI->load->view('../../sd-assets/php/dynamic_sections/layer-slider.php', $data, true);
		
	}
}

?>