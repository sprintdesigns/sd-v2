<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

/*
* UnzipFile class for CI
* 
*/
class SD_Users  {
    
    private $CI;
    private $id;
    private $username;
    private $password;
    private $email;
    private $name;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->id = '';
        $this->username = '';
        $this->password = '';
        $this->email = '';
        $this->name = '';
    }

    public static function isFreeTrial() {
      if (file_exists('corefile/free-trial.json')) {
        $json_data = file_get_contents('corefile/free-trial.json');
        $data = json_decode($json_data);

        if ($data->free_trial) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }      
    }

    public function getUserEmail() {
        $this->CI->db->select('*');
        $this->CI->db->from('sd_login');
        $query = $this->CI->db->get();

        if($query->num_rows() > 0)
        {
          return $query->result();
        }else
        {
          return false;
        }
    }

    //setter

   public function setId($id)
   {
        $this->id = $id;
   }

   public function setUsername($username)
   {
        $this->username = $username;
   }

   public function setPassword($password)
   {
        $this->password = $password;
   }

   public function setEmail($email)
   {
        $this->email = $email;
   }

   public function setName($name)
   {
        $this->name = $name;
   }

   //end setter

   //getter

   public function getId()
   {
        return $this->id;
   }

   public function getUsername()
   {
        return $this->username;
   }

   public function getPassword()
   {
        return $this->password;
   }

   public function getEmail()
   {
        return $this->email;
   }

   public function getName()
   {
        return $this->name;
   }

   //end getter

    




}

?>
