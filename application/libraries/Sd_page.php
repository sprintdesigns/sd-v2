<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SD_Page {

	private $CI;
	private $plugin_table;
	private $header_table;

	public function __construct()
	{
       $this->CI =& get_instance();
       $this->plugin_table='sd_pages';
       $this->header_table='sd_header';   
	}

	public function show_content($content, $sd = '') {
		$d = new SD_Dynamicsection;
		$content = $d->apply_dynamic_sections($content);
		return $this->change_link($content, $sd);
	}

	private function change_link($content, $sd = '') {
		if($sd && $sd->edit->mode()) {
			return preg_replace("~({base_url})~", base_url() . 'edit?page=', $content);
		} else {
			return preg_replace("~({base_url})~", base_url(), $content);
		}   		
	}

	/*
	 * Value of $contents variable
	 *
        Array(
            [0] => Array(
                    [variable] => reservation
                    [content] => For Reservations and I nquiry, Please Call: 0917-1233-123
	        )
            [1] => Array(
                    [variable] => first_slogan
                    [content] => wonderful field of bodywork
	        )
        )
	 */
	public function update_editable_text($contents) {
        $json_file = 'editable_text.json';
        $file = "sd-contents/{$json_file}";

        if (file_exists($file)) {
            $existing_content = file_get_contents($file);
            $existing_content = json_decode($existing_content, true);

            foreach ($contents as $content) {
                $existing_content[$content['variable']] = $content['content'];
            }
            $existing_content = json_encode($existing_content);
            file_put_contents($file, $existing_content);
        }
    }

	public function editable_text($variable, $default_content = '') {
        $json_file = 'editable_text.json';
        $file = "sd-contents/{$json_file}";

        if (file_exists($file)) {
            $content = file_get_contents($file);
            $json = json_decode($content, true);
            if (isset($json[$variable])) {
                return "<span class='_sd-editable-text text-only' id='_". $variable ."'>". $json[$variable] . "</span>";
            } else {
                $json[$variable] = $default_content;
                $content = json_encode($json);
                file_put_contents($file, $content);
                return "<span class='_sd-editable-text text-only' id='_". $variable ."'>". $json[$variable] . "</span>";
            }
        } else {
            $content = array($variable => $default_content);
            $content = json_encode($content);
            file_put_contents($file, $content);
            return "<span class='_sd-editable-text text-only' id='_". $variable ."'>". $default_content . "</span>";
        }
    }

	public function get_logo($header) {
        if (isset($header)) {
            if ($header->page_logo != '') {
            	return $header->page_logo;
                //return '<img src="'.base_url().'sd-assets/images/logo.png" >';
            } else {
                return '<img src="'.base_url().'sd-assets/images/logo.png" >';
            }
        } else {
            return '<img src="'.base_url().'sd-assets/logo.png">';
        }
    }

    public function page_link($page_name, $sd) {
     if($sd->edit->mode()) {
     	return base_url() . 'edit?page='. $page_name;
     } else {
     	return base_url() . $page_name;
     }    	
    }

	/*
	* This must be called to header.php of all themes inside <head> tag 
	*/
	public function include_header($sd = null, $page_tools) {
		//if (isset($page_tools['css'])) {
		//	$css = $page_tools['css'];
		//}
		$theme_color = '';
		if (isset($page_tools['theme_color'])) {
			$theme_color = $page_tools['theme_color'];
		}		

		foreach($sd->css->get_css_view('core_css') as $global_css) {
			echo $global_css;
		}

		foreach($sd->css->get_css_view('slick_gallery') as $slick_gallery) {
			echo $slick_gallery;
		}

		foreach($sd->css->get_css_view('layer_slider') as $layer_slider) {
			echo $layer_slider;
		}

		foreach($sd->css->get_css_view('datetime_picker') as $datetime_picker) {
			echo $datetime_picker;
		}							
		
		echo '<script>
			var base_url = "'. base_url() .'";
			</script>';

        // Kindly set this to false if you want the slider settings input to be disabled,
		// This should be on theme js, for customizing core crucial for themes.	
		echo '<script>
		    var sd_slide_title = true;
		    var sd_slide_btn_text = true;
		    var sd_slide_sub = true;
		  </script>';		

		  if($sd->edit->mode()) {
		      echo '<input type="hidden" id="body-color-template" value="'.$theme_color.'">';
		  }		  

    	$core_css[] = "<link type='text/css' rel='stylesheet' href='".base_url()."sd-assets/pluggables/plugins/font-awesome/css/font-awesome.min.css'>"; 
    	$core_css[] = "<link type='text/css' rel='stylesheet' href='".base_url()."sd-assets/pluggables/plugins/simple-line-icons/simple-line-icons.min.css'>";     	

	    if($sd->edit->mode()) {
	    	$core_css[] = "<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Oswald:300,400,700'>"; 
	    	$core_css[] = "<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Cookie'>"; 

			if(isset($core_css)){
	        	foreach($core_css as $core){
	          		echo $core;
	        	}
	      	}		    	
			foreach($sd->css->get_css_view('edit') as $css_edit) {
				echo $css_edit;
			}	    	
		}

    	if($sd->user->check_if_login()) {
    		$custom[] = "<link rel='stylesheet' href='".base_url()."sd-assets/themes-flow/cover.css'>"; 
    		$custom[] = "<link rel='stylesheet' href='".base_url()."sd-assets/custom-admin.css'>"; 

			if(isset($custom)){
	        	foreach($custom as $c){
	          		echo $c;
	        	}
	      	}	    		
    	}		
	}

	public function include_footer($sd = null, $page_tools = '') {
		if (isset($page_tools['scripts'])) { // See this on (Controller:Edit.php)
			$scripts = $page_tools['scripts'];
		}		
		
		$core_scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/jquery.min.js'></script>";
		$core_scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/bootstrap.min.js'></script>";		

		if($sd->edit->mode()) {
			$core_scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/modal-dynamic.js'></script>";
			$core_scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/modernizr.js'></script>";
			$core_scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/sdtemplatedashboard.js'></script>";
			$core_scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/sd-page.js'></script>";
		 }		

		if(isset($core_scripts)){
			foreach($core_scripts as $core){
				echo $core;
			}
		} 	
		
		if(isset($scripts)){
			foreach($scripts as $script){
				echo $script;
			}
		} 

		foreach($sd->js->get_scripts_view('slick_gallery') as $slick_gallery) {
			echo $slick_gallery;
		}	

		foreach($sd->js->get_scripts_view('layer_slider') as $layer_slider) {
			echo $layer_slider;
		}

		foreach($sd->js->get_scripts_view('datetime_picker') as $datetime_picker) {
			echo $datetime_picker;
		}

		foreach($sd->js->get_scripts_view('global_js') as $global_js) {
			echo $global_js;
		}								

		if(!$sd->edit->mode()) {
			echo '<script>
				$(document).ready(function(){
				  $(".galleryShow").remove();	
				});	
			</script>';
		}	
	}

	public function include_body($sd = null) {
		$sd->dashboard->bar($sd);
	}

	public function get_body_class($sd, $page_tools, $default_theme_color = '') {
		$page_url = $page_tools['page_url'];
		$template = $page_tools['template'];		
		$theme_color = $page_tools['theme_color'];

		if ($theme_color == '' || $theme_color == 'default') {
			if ($default_theme_color != '') {
				$theme_color = $default_theme_color;
			}
		}

		$class = '';
		if ($sd->user->check_if_login()) { 
			$class = 'logged-in';
		}

		if($sd->page->is_not_frontPage($page_url)) { 
			$class .= ' inner-page';
		} else { 
			$class .= ' front-page'; 
		}
		$class .= ' page-'.$page_url.' '.str_replace('.php', '',$template);

		if($sd->edit->mode()) { 
			$class .= ' noscroll';
		}

		$class .= " {$theme_color}";

		return $class;
	}

	public function social_icons($sd = null) {
		include 'sd-assets/php/social_icons.php';
	}

	public function get_content($page = null,$theme_id = null)
	{
		$this->CI->db->select('*');
		$this->CI->db->from($this->plugin_table);

		$where = array(
			'page_url' => $page, 
			'theme_id' => $theme_id, 
		);
		$this->CI->db->where($where);
		$query = $this->CI->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}
	}

	public function page_exist($page = null,$theme_id = null)
	{
		if($page == null)
		{
			preg_match("/[^\/]+$/",$_SERVER['REQUEST_URI'], $page);
		}
		else
		{
			$page = [$page];
		}

		if(!empty($page)){
			$page_data = $this->get_content($page[0],$theme_id);
		}
		else{
			$page_data = $this->get_content('home',$theme_id);
		}

		return $page_data;
	}

	public function get_pages()
	{
		$this->CI->db->select('*');
		$this->CI->db->order_by("id", "asc");
		$this->CI->db->from($this->plugin_table);

		$query = $this->CI->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}
	}

	public function nav_bar($sd = null)
	{
		$pages = $this->get_pages();
		$data['pages'] = $pages;
		if($sd != null)
		{
			$data['sd'] = $sd;
		}

		return $this->CI->load->view('pages/nav_bar',$data,true); 

	}

	public function nav_bar_web($sd = null)
	{
		$pages = $this->get_pages();
		$data['pages'] = $pages;
		if($sd != null)
		{
			$data['sd'] = $sd;
		}

		return $this->CI->load->view('pages/nav_bar_web',$data,true); 

	}

	public function nav_bar_mobile($sd = null)
	{
		$pages = $this->get_pages();
		$data['pages'] = $pages;
		if($sd != null)
		{
			$data['sd'] = $sd;
		}

		return $this->CI->load->view('pages/nav_bar_mobile',$data,true); 

	}

	public function save_page($id = null,$data = null)
	{
		if($id == null)
		{
			return 742;
		}

		$this->CI->db->where('id', $id);
		$this->CI->db->update($this->plugin_table, $data);

		if($this->CI->db->affected_rows() > 0)
		{
			return $this->CI->db->affected_rows();
		}else
		{
			return false;
		}
	}

	public function all_page_with_title($title = null,$data = null)
	{
		if($title == null)
		{
			return 742;
		}

		$pages = $this->get_pages();

		foreach ($pages as $index => $page) {
			if(preg_replace('/\s+/', '',strtolower($page->page_title)) == preg_replace('/\s+/', '',strtolower($title)))
			{
				$this->save_page($page->id,$data);
			}
		}
	}
	

	public function create($data)
	{

		$this->CI->db->insert($this->plugin_table, $data); 
		return $this->CI->db->insert_id();

	}

	public function is_not_frontpage($page_url)
	{
		if($page_url !== 'home')
		{
			return true;
		}

		return false;
	}

	public function delete_page($page_id)
	{
		$this->CI->db->where('id', $page_id);
		$this->CI->db->delete($this->plugin_table);
	}

	public function get_header()
	{
		$this->CI->db->select('*');
		$this->CI->db->from($this->header_table);

		$query = $this->CI->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}
	}

	public function save_header($header = null)
	{
		if($header == null)
		{
			return false;
		}

		$current_header= $this->get_header();
		
		if(sizeof($current_header) > 0)
		{
			
			$result = $this->update_header($current_header[0]->id,$header);
			
			return $result;
			
		}else
		{
			$this->CI->db->insert($this->header_table, $header); 
			return $this->CI->db->insert_id();
		}
	}

	public function update_header($id = null,$data = null)
	{
		if($id == null)
		{
			return 742;
		}

		$this->CI->db->where('id', $id);
		$this->CI->db->update($this->header_table, $data);
		
		if($this->CI->db->affected_rows() > 0)
		{
			return true;
		}else
		{
			return true;
		}
	}

	public function select_page_by_id($id) {

	    $this->CI->db->select('*');
		$this->CI->db->from($this->plugin_table);
        $this->CI->db->where('id', $id);
		$query = $this->CI->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}	
	}

	public function get_draft_pages()
	{
		$this->CI->db->select('*');
		$this->CI->db->order_by("id", "asc");
		$this->CI->db->where('type',false);
		$this->CI->db->from($this->plugin_table);

		$query = $this->CI->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}
	}


}

?>