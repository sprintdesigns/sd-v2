<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SD_Menu {

	private $CI;
	private $plugin_table;

	public function __construct()
	{
       $this->CI =& get_instance();
       $this->plugin_table='sd_nav_menus';

	}

	public function save_menu($data = null)
	{
		$this->CI->db->insert($this->plugin_table, $data); 
		return $this->CI->db->insert_id();
	}

	public function get_menu($id = null)
	{
		if($id == null)
		{
			return 'Invalid menu id';
		}

		$this->CI->db->select('*');
		$this->CI->db->where('theme_id', $id);
		$this->CI->db->from($this->plugin_table);

		$query = $this->CI->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}
	}

	public function update_menu($id = null,$menu = null)
	{
		if($id == null)
		{
			return 742;
		}

		$this->CI->db->where('theme_id', $id);
		$this->CI->db->update($this->plugin_table, $menu);
		
		if($this->CI->db->affected_rows() > 0)
		{
			return true;
		}else
		{
			return true;
		}
	}

	public function delete_menu_item($page_url,$sd)
	{

		$theme_id = $sd->theme->get_active_theme()[0]->id;

        $menu = $this->get_menu($theme_id);

        $my_menu = json_decode($menu[0]->orders, true);
        //unset($my_menu[$page_url]);
        foreach ($my_menu as $ix => $menu3) {
          if($menu3['id'] == $page_url) {

      	 	if(array_key_exists('children', $menu3)) {
          		foreach($menu3['children'] as $ix2 => $menu2) {
          			$data = array(
						'type' => false , 
					);
	             	$update = $sd->page->save_page($menu2['id'],$data);
	            }  		
          	}
          	unset($my_menu[$ix]);
            
          }
        }
        foreach ($my_menu as $ix => $menu) {
          if(array_key_exists('children', $menu)) {
            foreach($menu['children'] as $ix2 => $menu2) {
              if($menu2['id'] == $page_url) {
              	unset($my_menu[$ix]['children'][$ix2]);
              	if(sizeof($my_menu[$ix]['children']) < 1) {
              	  unset($my_menu[$ix]['children']);
              	}
              }
            }
          }
        }
        $json = json_encode($my_menu);
        $data = array('orders' => $json );
        $theme_id = $sd->theme->get_active_theme()[0]->id;
        $this->update_menu($theme_id, $data);
	}

	public function menu_sort($theme_id)
	{
		$data['menu'] = $this->get_menu($theme_id);
		echo $this->CI->load->view('menu/menu',$data,true);
	}

	public function menu_draft($sd = null)
	{
		$data['sd'] = $sd;

		return $this->CI->load->view('menu/draft',$data,true); 

	}

	public function menu_active($sd = null)
	{
		$theme_id = $sd->theme->get_active_theme()[0]->id;
		$data['menu'] = $this->get_menu($theme_id);
		$data['sd'] = $sd;

		return $this->CI->load->view('menu/active',$data,true); 

	}

	public function create_new_menu($theme_id)
	{
		
		$data = array(
			'name' => 'default', 
			'orders' => '[]', 
			'theme_id' => $theme_id
		);

		$this->CI->db->insert($this->plugin_table, $data); 
		return $this->CI->db->insert_id();
	}

	
}

?>