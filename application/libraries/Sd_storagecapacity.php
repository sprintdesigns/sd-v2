<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/*
* UnzipFile class for CI
* 
*/
class SD_Storagecapacity {
        
    private $CI;
    private $plugin_table;
    private $theme_path;

    public function __construct()
    {
    	$this->CI =& get_instance();
    	$this->plugin_table='sd_incs';
    	$this->template_table='sd_template';
    	$this->theme_path='sd-includes/themes/';
    }
    
    public function getStorageCapacity() {
      $pathx = $_SERVER['DOCUMENT_ROOT'];  
      $SIZE_LIMIT = 3221225472; // 3 GB
      $disk_used = $this->foldersize($pathx);

      $disk_remaining = $SIZE_LIMIT - $disk_used;
      $percentagex = ($disk_used / $SIZE_LIMIT) * 100;  
      $disk_used_converted = $this->format_size($disk_used);
      $disk_remaining_converted = $this->format_size($disk_remaining);
      $result = array('size_limit'=> $SIZE_LIMIT, 'disk_remaining' => $disk_remaining_converted , 'percentagex' => $percentagex, 'disk_used' => $disk_used_converted  );

      return $result;
    }
        
	  public function foldersize($path) {
        $total_size = 0;
        $files = scandir($path);
        $cleanPath = rtrim($path, '/'). '/';

        foreach($files as $t) {
            if ($t<>"." && $t<>"..") {
                $currentFile = $cleanPath . $t;
                if (is_dir($currentFile)) {
                    $size = $this->foldersize($currentFile);
                    $total_size += $size;
                }
                else {
                    $size = filesize($currentFile);
                    $total_size += $size;
                }
            }   
        }

        return $total_size;
    }

    public function format_size($size) {
          $units = explode(' ', 'B KB MB GB TB PB');
          
          $mod = 1024;

          for ($i = 0; $size > $mod; $i++) {
              $size /= $mod;
          }

          $endIndex = strpos($size, ".")+3;
          return substr( $size, 0, $endIndex).' '.$units[$i];
      }      
}

?>
