<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SD_Gallery {

	private $CI;

	public function __construct()
	{
       $this->CI =& get_instance();
	}


	public function process_media_data($data,$order="reverse"){

		$media_data = array();
		if($data){
			foreach($data as $d){
				$arr = array(
					'id' => $d->id,
					'data' => json_decode($d->image_data),
					'create_at' => $d->create_at
				);

				$media_data[] = $arr;
			}

			if($order == "reverse"){
				$reversed = array_reverse($media_data);
				return $reversed;
			}else{
				return $media_data;
			}
		}
		
	}

	public function media_gallery($sd = null)
	{
		if($sd == null)
		{
			echo 'invalid arguments'; 
		}

		$data['sd'] = $sd;

		echo $this->CI->load->view('dashboard/media_gallery',$data,true); 
	}

	public function load_gallery($sd = null)
	{
		$data['sd'] = $sd;

		return $this->CI->load->view('gallery/images',$data,true); 
	}


}

?>