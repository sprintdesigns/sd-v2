<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SD_Image {

	private $CI;
	private $plugin_table;
	public $img_dir =  'localhost/sdv1.1/sd-contents/uploads/img/2016/10/';
	public $thumb_dir = 'sd-contents/uploads/thumbs/2016/10';
	public $thumbs_width = 200;

	public function __construct()
	{
       $this->CI =& get_instance();
       $this->plugin_table = "sd_gallery";
	}

	/* function:  generates thumbnail */
	function make_thumb($src,$dest,$desired_width,$ext) {
		/* read the source image */
		if($ext == 'png'){
			$source_image = imagecreatefrompng($src);
		}else{
			$source_image = imagecreatefromjpeg($src);
		}

		$width = imagesx($source_image);
		$height = imagesy($source_image);
		/* find the "desired height" of this thumbnail, relative to the desired width  */
		$desired_height = floor($height*($desired_width/$width));
		/* create a new, "virtual" image */
		$virtual_image = imagecreatetruecolor($desired_width,$desired_height);
		/* copy source image at a resized size */
		imagecopyresized($virtual_image,$source_image,0,0,0,0,$desired_width,$desired_height,$width,$height);
		/* create the physical thumbnail image to its destination */
		imagejpeg($virtual_image,$dest);
	}

	/* function:  returns files from dir */
	function get_files($images_dir='sd-contents/uploads/img/2016/10',$exts = array('jpg','png')) {

		if( $this->CI->sd->dircheck->check_dir( $images_dir ) ){
				
			$ex = $this->CI->sd->dircheck->get_dir_content( $images_dir ) ;
			
			$files = array();
			if($handle = opendir($images_dir)) {
				while(false !== ($file = readdir($handle))) {
					$extension = strtolower($this->get_file_extension($file));
					if($extension && in_array($extension,$exts)) {
						$files[] = $file;
					}
				}
				closedir($handle);
			}
			return $ex;
		}else{
			return "";
		}
		
	}

	/* function:  returns a file's extension */
	function get_file_extension($file_name) {
		return substr(strrchr($file_name,'.'),1);
	}


	function save_images($data){
		$arr = array("image_data" => $data);
		$this->CI->db->insert($this->plugin_table , $arr);
		return $this->CI->db->insert_id();
	}

	function get_media(){
		$this->CI->db->select('*');
		$this->CI->db->from($this->plugin_table);

		$query = $this->CI->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}
	}

	function create_full_path($path,$raw = false,$full = false)
	{
		$path_arr = explode('/', $path);
		$new_path="";
		$check_path=false;
		

		for ($i=sizeof($path_arr); $i > 0; $i--) { 
			if(!$check_path)
			{
				if($path_arr[$i-1] == 'sd-contents')
				{
					$new_path = $path_arr[$i-1].$new_path;
					$check_path=true;
				}else
				{
					$new_path = '/'.$path_arr[$i-1].$new_path;
				}
			}
			
		}

		if(!$raw)
		{
			echo $this->CI->config->base_url().$new_path;
		}
		else
		{
			if($full)
			{
				return $this->CI->config->base_url().$new_path;
			}else
			{
				return $new_path;
			}
			
		}

	}

	function create_full_path_thumb($path,$raw = false)
	{
		$path_arr = explode('/', $path);
		$new_path="";
		$check_path=false;
		

		for ($i=sizeof($path_arr); $i > 0; $i--) { 
			if(!$check_path)
			{
				if($path_arr[$i-1] == 'sd-contents')
				{
					$new_path = $path_arr[$i-1].$new_path;
					$check_path=true;
				}else
				{
					if($i == sizeof($path_arr))
						$new_path = '/thumb_'.$path_arr[$i-1].$new_path;
					else
						$new_path = '/'.$path_arr[$i-1].$new_path;
				}
			}
			
		}
		
		if(!$raw)
			echo $this->CI->config->base_url().$new_path;
		else
			return $this->CI->config->base_url().$new_path;

	}

	function create_full_path_web($path,$raw = false)
	{
		$path_arr = explode('/', $path);
		$new_path="";
		$check_path=false;
		

		for ($i=sizeof($path_arr); $i > 0; $i--) { 
			if(!$check_path)
			{
				if($path_arr[$i-1] == 'sd-contents')
				{
					$new_path = $path_arr[$i-1].$new_path;
					$check_path=true;
				}else
				{
					if($i == sizeof($path_arr))
						$new_path = '/web_'.$path_arr[$i-1].$new_path;
					else
						$new_path = '/'.$path_arr[$i-1].$new_path;
				}
			}
			
		}
		
		if(!$raw)
			echo $this->CI->config->base_url().$new_path;
		else
			return $this->CI->config->base_url().$new_path;

	}

	function image_by_id($id = null)
	{
		if($id == null)
		{
			return "id is null";
		}

		$this->CI->db->select('*');
		$this->CI->db->where('id',$id);
		$this->CI->db->from($this->plugin_table);

		$query = $this->CI->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}

	}

	function delete_image($id = null)
	{
		if($id == null)
		{
			return "Invalid argument";
		}

		$image = $this->image_by_id($id);

		if($image)
		{
			$image_data = json_decode($image[0]->image_data);
			unlink($this->create_full_path($image_data->upload_data->full_path,true));
			$this->CI->db->where('id', $id);
			$this->CI->db->delete($this->plugin_table);

		}else
		{
			return 'Invalid image id';
		}


	}

}

?>