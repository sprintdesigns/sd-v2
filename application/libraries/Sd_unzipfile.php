<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

/*
* UnzipFile class for CI
* 
*/

class SD_UnzipFile {
        
    private $file;
    private $path;

    public function __construct()
    {
        $this->file = '';
        $this->path = '';
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFile($file)
    {
        $this->file = $file;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function setPath($path)
    {
        $this->path = $path;
    }

    public function unzipFile($filename = 'install.json')
    {
        $zip = new ZipArchive;
        if ($zip->open($this->file) === TRUE) {
            $zip->extractTo($this->path);
            $zip->close();
            unlink($this->file);

           $response = $this->getInstall($filename);


        } else {
           $response = 'Problem in oppening the zip file';
        }

        return $response;
    }

    public function getInstall($filename)
    {
        $file = explode('.', $this->file);
        $install = file_get_contents($file[0].'/' .$filename);
        $install = json_decode($install);

        //unlink($file[0].'/install.json');

        return $install;


    }





}

?>
