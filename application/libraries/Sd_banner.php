<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SD_Banner {

	private $CI;
	private $plugin_table;

	public function __construct()
	{
       $this->CI =& get_instance();
       $this->plugin_table = "sd_slider";
	}

	public function banner_init($sd = null)
	{
		if($sd == null)
		{
			echo 'invalid arguments'; 
		}
		$theme = $sd->theme->get_active_theme()[0]->id;
		$data['sd'] = $sd;
		$data['slider'] = $this->get_slider(null,$theme);

		echo $this->CI->load->view('banner/banner_init',$data,true); 
	}

	public function banner_slider($sd = null)
	{
		if($sd == null)
		{
			echo "sd is not initialized";
			return;
		}
		$theme = $sd->theme->get_active_theme()[0]->id;
		$data['sd'] = $sd;
		$data['slider'] = $this->get_slider(null,$theme);

		echo $this->CI->load->view('banner/banner_slider',$data,true);
	}

	public function get_slider($id = null,$theme_id = null)
	{
		if($id == null)
		{
			$this->CI->db->select('*');
			$this->CI->db->order_by("id", "asc");
			$where = array('theme_id' => $theme_id, );
			$this->CI->db->where($where);
			$this->CI->db->from($this->plugin_table);

		}else
		{
			$this->CI->db->select('*');
			$this->CI->db->from($this->plugin_table);
			$where = array('id' => $id );
			$this->CI->db->where($where);
		}

		$query = $this->CI->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}
	}

	public function load_slide($id = null,$sd = null,$text = true)
	{
		if($id == null || $sd == null)
		{
			echo "Invalid argument";
		}
		$theme = $sd->theme->get_active_theme()[0]->id;
		$slide = $this->get_slider($id,$theme);

		if($slide)
		{
			$data['slider'] = $slide;
			$data['sd'] = $sd;

			if($text)
			{
				return $this->CI->load->view('banner/slide_view',$data,true);		
			}else
			{
				echo $this->CI->load->view('banner/slide_view',$data,true);
			}	
		}else
		{
			if($text)
			{
				return false;		
			}else
			{
				echo 'slide not exist';
			}	
		}
		

	}

	public function save_slide($id = null,$data = null)
	{
		if($id == null || $data == null)
		{
			return "Id or data is not initialize";
		}

		$this->CI->db->set($data);
		$this->CI->db->where('id', $id);
		$this->CI->db->update($this->plugin_table);

		//return $this->CI->db->affected_rows();
		return true;
	}

	public function create_slide($data = null)
	{
		if($data == null)
		{
			return "data to insert is not initialized";
		}
		
		$this->CI->db->insert($this->plugin_table, $data); 

		return $this->CI->db->insert_id();
	}

	public function gallery($sd = null)
	{
		if($sd == null)
		{
			echo "SD is not initialize";
		}

		$data['sd'] = $sd;

		echo $this->CI->load->view('banner/banner_gallery',$data,true);
	}

	public function banner_menu_slide($id_selected = null,$sd = null)
	{
		$theme = $sd->theme->get_active_theme()[0]->id;
		$data['slider'] = $this->get_slider(null,$theme);
		$data['id_selected'] = $id_selected;
		$data['sd'] = $sd;

		return $this->CI->load->view('banner/banner_menu_slide',$data,true);
	}

	function delete_slide($id = null)
	{
		if($id == null)
		{
			return "Invalid argument";
		}

		$slide = $this->get_slider($id);

		if($slide)
		{
			$this->CI->db->where('id', $id);
			$this->CI->db->delete($this->plugin_table);

			return true;

		}else
		{
			return false;
		}


	}
}

?>