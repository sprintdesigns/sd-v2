<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SD_Edit {

	private $CI;
	private $editMode;

	public function __construct()
	{
       $this->CI =& get_instance();
       $this->editMode =false;
	}


	public function mode($mode = null)
	{
		
		if($mode == null)
			return $this->editMode;

		if(!is_bool($mode))
		{
			echo "Invalid argiment: not a boolean";
			return;
		}

		$this->editMode = $mode;

	}


}

?>