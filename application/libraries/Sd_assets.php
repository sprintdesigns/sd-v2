<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

/*
* UnzipFile class for CI
* 
*/

class SD_Assets {


     private $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
    }

    function add_js($file='')
    {
        $str = '';
        $ci = &get_instance();
        $header_js  = $ci->config->item('header_js');
 
        if(empty($file)){
            return;
        }
 
        if(is_array($file)){
            if(!is_array($file) && count($file) <= 0){
                return;
            }
            foreach($file AS $item){
                $header_js[] = $item;
            }
            $ci->config->set_item('header_js',$header_js);
        }else{
            $str = $file;
            $header_js[] = $str;
            $ci->config->set_item('header_js',$header_js);
        }
    }
    

    



}

?>
