<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SD_Email {

	private $CI;
	private $email_to;
	private $subject;
	private $message;
	private $email_from;

	public function __construct()
	{
       $this->CI =& get_instance();
       $this->email_to='';
       $this->subject='';
       $this->message='';
       $this->email_from='';
	}

	public function send()
	{
		$this->CI->load->library('email');

		$result = $this->CI->email
		        ->from($this->email_from)
		        ->to($this->email_to)
		        ->subject($this->subject)
		        ->message($this->message)
		        ->send();

		return $result;
	}

	public function set_from($settings = null,$form_data = null,$data = null)
	{	
		if($settings == null || $form_data == null || $data == null)
		{
			return 'invalid prameter';
		}

		$str = $this->set_up_string($settings,$form_data,$data);

		$this->email_from = $str;

	}
	public function set_to($settings = null,$form_data = null,$data = null)
	{	
		if($settings == null || $form_data == null || $data == null)
		{
			return 'invalid prameter';
		}

		$str = $this->set_up_string($settings,$form_data,$data);

		$this->email_to = $str;
		
	}

	public function set_subject($settings = null,$form_data = null,$data = null)
	{	
		if($settings == null || $form_data == null || $data == null)
		{
			return 'invalid prameter';
		}

		$str = $this->set_up_string($settings,$form_data,$data);

		
		$this->subject = $str;

	}

	public function set_message($settings = null,$form_data = null,$data = null)
	{
		if($settings == null || $form_data == null || $data == null)
		{
			return 'invalid prameter';
		}

		$str = $this->set_up_string($settings,$form_data,$data);

		$this->message = $str;
	}

	public function set_up_string($settings = null,$form_data = null,$data = null)
	{

		
		$new_str=$settings;
		foreach ($form_data as $index => $fdata) {
			
			$new_str = str_replace('['.$fdata->name.']',array_key_exists($fdata->name.'-'.$fdata->id, $data)? $data[$fdata->name.'-'.$fdata->id]:'', $new_str);
			
		}

		return $new_str;
	}



}

?>