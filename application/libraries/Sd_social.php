<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/*
* UnzipFile class for CI
* 
*/
class SD_Social {
        
    private $CI;
    private $plugin_table;

    public function __construct()
    {
    	$this->CI =& get_instance();
    	$this->plugin_table='sd_social';

    }


   	public function get_social($sd = null)
   	{

   		if($sd == null)
   		{
   			return "SD isntance is needed";
   		}
   		$theme_id = $sd->theme->get_active_theme()[0]->id;
   		$this->CI->db->select('*');
   		$where = array('theme_id' => $theme_id );
   		$this->CI->db->where($where);
		$this->CI->db->from($this->plugin_table);

		$query = $this->CI->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}
   	}

   	public function update_social($theme_id = null,$social = null)
	{

		$this->CI->db->set($social);
		$this->CI->db->where('theme_id', $theme_id);
		$this->CI->db->update($this->plugin_table);


	}

  public function add_social_init($theme_id = null)
  {
    $this->CI->db->insert($this->plugin_table,array('theme_id' => $theme_id,'social' => '[]' )); 
    return $this->CI->db->insert_id();
  }
        
}

?>
