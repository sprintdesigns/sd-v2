<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SD_Bandwidth {

	private $CI;

	public function __construct()
	{
       $this->CI =& get_instance();
       
	}


	public function cal_bandwidth()
	{
		
		$int="eth0";
	   
	    $rx[] = @file_get_contents("/sys/class/net/$int/statistics/rx_bytes");
	    $tx[] = @file_get_contents("/sys/class/net/$int/statistics/tx_bytes");
	    sleep(1);
	    $rx[] = @file_get_contents("/sys/class/net/$int/statistics/rx_bytes");
	    $tx[] = @file_get_contents("/sys/class/net/$int/statistics/tx_bytes");
	    
	    $tbps = $tx[1] - $tx[0];
	    $rbps = $rx[1] - $rx[0];
	    
	    $round_rx=round($rbps/1024, 2);
	    $round_tx=round($tbps/1024, 2);
	    
	    $time=date("U")."000";
	    $_SESSION['rx'][] = "[$time, $round_rx]";
	    $_SESSION['tx'][] = "[$time, $round_tx]";
	    $data['label'] = $int;
	    $data['data'] = $_SESSION['rx'];
	    # to make sure that the graph shows only the
	    # last minute (saves some bandwitch to)
	    if (count($_SESSION['rx'])>60)
	    {
	        $x = min(array_keys($_SESSION['rx']));
	        unset($_SESSION['rx'][$x]);
	    }
	    
	    //var_dump($round_tx);
	    # json_encode didnt work, if you found a workarround pls write me
	    # echo json_encode($data, JSON_FORCE_OBJECT);

	    $result = array('tbps' => $round_tx,'rbps' => $round_rx );

    	return $result;

	}


}

?>