<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SD_Forms {

	private $CI;
	private $plugin_table;
	private $form_data_table;

	public function __construct()
	{
       $this->CI =& get_instance();
       $this->plugin_table = "sd_forms";
       $this->form_data_table = "sd_form_data";
       $this->settings_data_table = "sd_form_settings";
	}


	public function all_form()
	{
		$this->CI->db->select('*');
		$this->CI->db->from($this->plugin_table);

		$query = $this->CI->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}
	}

	public function get_select_form($sd)
	{
		$data['sd'] = $sd;
		return $this->CI->load->view('forms/select_form',$data,true);
	}

	// new function
	public function form_list($sd)
	{
		$data['sd'] = $sd;
		return $this->CI->load->view('forms/list_form',$data,true);
	}

	public function get_form_by_name($name = null)
	{
		if($name == null)
		{
			echo "name is not defined";
			return;
		}

		$all_form = $this->all_form();

		if(!$all_form)
			return false;

		foreach ($all_form as $index => $f) {
			if(strcmp($name,$f->name) == 0)
				return $f;
		}

		return false;

	}

	public function register_contact_form($form = null)
	{

		$this->CI->db->insert($this->plugin_table, $form); 

		return $this->CI->db->insert_id();
	}

	public function get_form($sd,$id = null,$public = false)
	{
		if($id == null)
		{
			echo "id is not initialized";
			return;
		}

		if(!$this->form($id))
		{
			echo "";
			return;
		}

		$data['fields'] = $this->get_form_data($id);
		$data['sd'] = $sd;
		$data['id'] = $id;
		$data['public'] = $public;

		echo $this->CI->load->view('forms/form',$data,true);

	}

	public function get_form_data($id = null)
	{
		if($id == null)
		{
			echo "id is not initialized";
			return;
		}

		$this->CI->db->select('*');
		$this->CI->db->from($this->form_data_table);
		$where = array('form_id' => $id );
		$this->CI->db->where($where);
		$query = $this->CI->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}

	}	


	public function form($id = null)
	{
		if($id == null)
		{
			echo "id is not initialized";
			return;
		}

		$this->CI->db->select('*');
		$this->CI->db->from($this->plugin_table);
		$this->CI->db->order_by('id','asc');
		$where = array('id' => $id );
		$this->CI->db->where($where);
		
		$query = $this->CI->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}
	}

	public function create_field($data = null)
	{
		if($data == null)
		{
			echo "Invalid parameter";
			return;
		}
		
		$form_data = $this->get_form_data( $data['form']);

		if(!$form_data)
		{
			$position = 1;
		}else
		{
			$position = sizeof($form_data) + 1;
		}
		$field_name = $this->gen_name(3);
		$data = array(
		   'name' => strtolower(str_replace(' ', '-', $data['label'])).'-form-'.$data['form'].$field_name,
		   'label' => $data['label'],
		   'type' => $data['type'],
		   'position' => $position,
		   'form_id' => $data['form'],
		   'status' => $data['status'],
		   'required' => $data['required'],
		   'clone_from' => $data['clone_from']
		);

		$this->CI->db->insert($this->form_data_table, $data); 
	}

	public function field($fields = null,$args = null)
	{
		if($fields == null)
		{
			//echo "No fields";
			return;
		}

		if(!$fields)
		{
			//echo "No fields";
			return;
		}

		$data['fields'] = $fields;
		$data['args'] = $args;
		echo $this->CI->load->view("forms/fields",$data,true);

	}

	public function load($sd)
	{
		$data['sd'] = $sd;
		echo $this->CI->load->view('forms/contact_form',$data,true);
	}

	public function main()
	{
		
		$this->CI->db->select('*');
		$this->CI->db->from($this->plugin_table);
		$where = array('is_main' => true );
		$this->CI->db->where($where);
		$query = $this->CI->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}
		
	}

	public function remove_main()
	{

		$this->CI->db->set('is_main', false);
		$this->CI->db->where('is_main', true);
		$this->CI->db->update($this->plugin_table);


	}

	public function update_field($data = null,$id = null)
	{
		if($id == null || $data ==  null)
		{
			return false;
		}

		$this->CI->db->set($data);
		$this->CI->db->where('id', $id);
		$this->CI->db->update($this->form_data_table);

		return true;
	}

	public function set_main($form_id = null)
	{
		if($form_id == null)
		{
			echo "no form id given";
			return ;
		}

		$this->remove_main();

		$this->CI->db->set('is_main', true);
		$this->CI->db->where('id', $form_id);
		$this->CI->db->update($this->plugin_table);
	}

	public function clear_fields($form_id = null)
	{
		if($form_id == null)
			return false;

		$this->CI->db->where('form_id', $form_id);
		$this->CI->db->delete($this->form_data_table);

		return true;
	}

	public function gen_name($length = 5)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $name_len = strlen($characters);
	    $random_name = '';
	    for ($i = 0; $i < $length; $i++) {
	        $random_name .= $characters[rand(0, $name_len - 1)];
	    }
	    
		return $random_name;
	}

	public function set_settings($data = null)
	{
		if($data == null)
		{
			echo "Invalid data given";
			return ;
		}

		$this->CI->db->insert($this->settings_data_table, $data); 

		return $this->CI->db->insert_id();

	}

	public function settings($id = null)
	{
		if($id == null)
		{
			echo "no id given";
			return ;
		}

		$this->CI->db->select('*');
		$where = array('form_id' => $id );
		$this->CI->db->where($where);
		$this->CI->db->from($this->settings_data_table);

		$query = $this->CI->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}


	}

	public function update_settings($data = null,$id = null)
	{
		if($data == null)
		{
			echo "Invalid data given";
			return ;
		}

		if($id == null)
		{
			echo "form id must not contain null value";
			return ;
		}

		$this->CI->db->set($data);
		$this->CI->db->where('id', $id);
		$this->CI->db->update($this->settings_data_table);

		return $this->CI->db->affected_rows();

	}

	public function subject($form_id = null)
	{
		if($form_id == null)
		{
			return "form id is null";
		}

		$this->CI->db->select('subject');
		$this->CI->db->from($this->settings_data_table);
		$where = array('form_id' => $form_id );
		$this->CI->db->where($where);
		$query = $this->CI->db->get();

		if($query->num_rows() > 0)
		{
			$subject = $query->result();

			print_r($subject);
		}else
		{
			return false;
		}

	}


	// new function

	public function set_status($id = null)
	{
		if($id == null)
		{
			return false;
		}

		$form = $this->form($id);

		$data = array(
			'status' => !$form[0]->status, 
		);

		$this->CI->db->set($data);
		$this->CI->db->where('id', $id);
		$this->CI->db->update($this->plugin_table);

		return $this->CI->db->affected_rows();


	}

	public function get_field($id = null)
	{
		if($id == null)
		{
			
			return false;
		}

		$this->CI->db->select('*');
		$this->CI->db->from($this->form_data_table);
		$where = array('id' => $id );
		$this->CI->db->where($where);
		$query = $this->CI->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}
	}

	public function set_status_field($id = null)
	{
		if($id == null)
		{
			return false;
		}

		$field = $this->get_field($id);

		$data = array(
			'status' => !$field[0]->status, 
		);

		$this->CI->db->set($data);
		$this->CI->db->where('id', $id);
		$this->CI->db->update($this->form_data_table);

		return $this->CI->db->affected_rows();


	}

	public function set_required_field($id = null)
	{
		if($id == null)
		{
			return false;
		}

		$field = $this->get_field($id);

		$data = array(
			'required' => !$field[0]->required, 
		);

		$this->CI->db->set($data);
		$this->CI->db->where('id', $id);
		$this->CI->db->update($this->form_data_table);

		return $this->CI->db->affected_rows();


	}

	// new function
	public function delete_form($id = null)
	{
		if($id == null)
		{
			return false;
		}

		$this->CI->db->where('id', $id);
		$this->CI->db->delete($this->plugin_table);

		return true;
	}

	public function delete_field($id = null)
	{
		if($id == null)
		{
			return false;
		}

		$this->CI->db->where('id', $id);
		$this->CI->db->delete($this->form_data_table);

		return true;
	}

	// new function
	public function count_clone($id = null)
	{
		if($id == null)
		{
			return false;
		}

		$this->CI->db->select('*');
		$this->CI->db->from($this->plugin_table);
		$this->CI->db->where('clone_from',$id);

		$query = $this->CI->db->get();

		return $query->num_rows();
		
	}

	public function count_clone_field($id = null)
	{
		if($id == null)
		{
			return false;
		}

		$this->CI->db->select('*');
		$this->CI->db->from($this->form_data_table);
		$this->CI->db->where('clone_from',$id);

		$query = $this->CI->db->get();

		return $query->num_rows();
		
	}

	

	
}

?>