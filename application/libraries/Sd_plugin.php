<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

/*
* UnzipFile class for CI
* 
*/

class SD_Plugin {
        
    private $description;
    private $name;
    private $type;
    private $banner;
    private $pages;

    public function __construct()
    {
        $this->description = '';
        $this->name = '';
        $this->type = '';
        $this->banner = '';
        $this->pages = [];
    }

    public function setPlugin($install)
    {
        $this->description = $install->description;
        $this->name = $install->name;
        $this->type = $install->type;
        $this->banner = $install->banner;
        $this->pages = $install->pages;

    }

    public static function getJsonValue($plugin_folder, $name) {
        $plugin_file = 'plugin.json';
        $file = "sd-includes/plugins/{$plugin_folder}/{$plugin_file}";

        if (file_exists($file)) {
            $json = file_get_contents($file);
            $json = json_decode($json, true);
            return $json[$name];
        }
    }

    public static function getPlugin($plugin_folder) {
        $plugin_file = 'plugin.json';
        $file = "sd-includes/plugins/{$plugin_folder}/{$plugin_file}";

        if (file_exists($file)) {
            $json = file_get_contents($file);
            $json = json_decode($json);
            return $json;
        }
    }

    public static function getPlugins() {
        $folders = scandir('sd-includes/plugins');
        $install_file = 'plugin.json';
        $plugins = array();

        foreach ($folders as $folder) {
            $file = "sd-includes/plugins/{$folder}/{$install_file}";
            if (file_exists($file)) {
                $json = file_get_contents($file);
                $json = json_decode($json);
                if ($json->enable) {
                    $plugins[] = $json;
                }                
            }
        }

        return $plugins;
    }
}

?>
