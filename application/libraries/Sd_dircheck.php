<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SD_Dircheck {

	private $upload_dir = "uploads";
	private $current_yr;
	private $current_month;
	private $type;

	public function __construct()
	{
       $this->CI =& get_instance();
	}

	public function dir_init(){
		$this->set_dir_type();
		$this->set_current_year();
		$this->set_current_month();
		$this->has_uploads_dir();
		// $this->has_uploads_dir_type();
		// $this->has_uploads_cur_year();
		// $this->has_uploads_curr_month();
	}

	public function has_uploads_dir(){

		if(! is_dir('./sd-contents/'.$this->upload_dir) ){
			mkdir('./sd-contents/'.$this->upload_dir, 0777, true);
			// echo "Successfully Created Directory.";
			return;
		}else{
			// echo "Directory Already Exist.";
			return;
		}
	}


	public function has_uploads_dir_type($type = 'img'){
		if( !is_dir('./sd-contents/'.$this->upload_dir.'/'. $this->get_dir_type() ) ){
			mkdir('./sd-contents/'.$this->upload_dir.'/'. $this->get_dir_type() , 0777, true);
			// echo "Successfully Created Directory.";
			return;
		}else{
			// echo "Directory Already Exist.";
			return;
		}
	}

	public function has_uploads_cur_year(){
		if( !is_dir('./sd-contents/'.$this->upload_dir.'/'.$this->get_dir_type().'/'. $this->get_current_year() ) ){
			mkdir('./sd-contents/'.$this->upload_dir.'/'.$this->get_dir_type().'/'. $this->get_current_year(), 0777, true);
			// echo "Successfully Created Directory";
			return;
		}else{
			// echo "Directory Already Exist.";
			return;
		}
	}

	public function has_uploads_curr_month(){
		if( !is_dir('./sd-contents/'.$this->upload_dir.'/'.$this->get_dir_type().'/'.$this->get_current_year().'/'. $this->get_currnet_month() ) ){
			mkdir('./sd-contents/'.$this->upload_dir.'/'.$this->get_dir_type().'/'.$this->get_current_year().'/'. $this->get_currnet_month() ,0777,true);
			// echo "Successfully Created Directory";
			return;
		}else{
			// echo "Directory Already Exist.";
			return;
		}
	}

	public function check_dir($path){
		if(is_dir($path)){
			return true;
		}else{
			return false;
		}
	}

	public function get_dir_content($path){
		$files = scandir($path);
		// $files = array_diff(scandir($path), array('.', '..'));

		//working for getting images
		// foreach( glob($path.'/*.*')  as $file ){
		// 	$data[] = $file;
		// }
		//end of glob
		// if ($handle = opendir($path)) {
		//     while (false !== ($file = readdir($handle))) {
		//         $data[] = $file;
		//     }
		//     closedir($handle);
		// }
		// $data = array_diff(scandir($path), array('.', '..'));
		$dir = $this->get_sub_dir_list($path);
		$dir_sub = array();
		$contents = array();
		//example : sd-contents\uploads\img\2016\10
		foreach($dir as $key => $value){
			$dir_sub[$value] = $this->get_sub_dir_list($path.$value);
			// $dir_sub[] = array_fill_keys($dir,$this->get_sub_dir_list($path.'/'.$value));
		}
		
		foreach($dir_sub as $year => $yr){
			foreach($yr as $month => $mo){
				$contents[$year][] = $this->get_sub_dir_contents($path.$year.'/'.$mo);
			}
		}
		return $contents;
	}

	public function get_sub_dir_list($path){
		$dir = array();
		if ($handle = opendir($path)) {
		    while (false !== ($file = readdir($handle))) {
		        $dir[] = $file;
		    }
		    closedir($handle);
		}
		$dir = array_diff(scandir($path), array('.', '..'));
		return $dir;
	}

	public function get_sub_dir_contents($path){
		$data = array();
		foreach( glob($path.'/*.{jpg,gif,png}', GLOB_BRACE)  as $file ){
			$data[] = $file;
		}
		return $data;
	}


	public function set_dir_type($type="img"){
		$this->type = ($type != "img")? $type: 'img';
	}

	public function get_dir_type(){
		return $this->type;
	}

	public function set_current_year($yr = "" ){
		$this->current_yr = ($yr != "")? $yr: (new DateTime)->format("Y");
	}

	public function get_current_year(){
		return $this->current_yr;
	}

	public function set_current_month($month = ""){
		$this->current_month = ($month != "") ? $month : (new DateTime)->format("m");
	}

	public function get_currnet_month(){
		return $this->current_month;
	}

	public function get_current_full_path(){
		return 'sd-contents/'.$this->upload_dir.'/'.$this->get_dir_type().'/'.$this->get_current_year().'/'. $this->get_currnet_month();
	}

	public function get_upload_full_path(){
		return './sd-contents/'.$this->upload_dir.'/'.$this->get_dir_type().'/'.$this->get_current_year().'/'. $this->get_currnet_month();
	}


}

?>