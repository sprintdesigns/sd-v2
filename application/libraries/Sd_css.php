<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SD_Css {

	private $CI;

	public function __construct()
	{
       $this->CI =& get_instance();
	}

	public function get_css_view($cont, $view="index"){

		$links = array();
		if($cont == 'core_css') {
          $links[] = '<link href="https://fonts.googleapis.com/css?family=Acme|Bad+Script|Bree+Serif|Courgette|Dancing+Script|Didact+Gothic|Gloria+Hallelujah|Great+Vibes|Indie+Flower|Josefin+Sans|Lobster|Lobster+Two|Merriweather|Pacifico|Roboto+Slab|Roboto:100,100i,300i|Sacramento|Satisfy|Slabo+27px|Ubuntu|Spicy+Rice" rel="stylesheet">'; 
          $links[] = "<link type='text/css' rel='stylesheet' href='".base_url()."sd-assets/css/social-icons.css'>";
          $links[] = "<link type='text/css' rel='stylesheet' href='".base_url()."sd-assets/css/sections.css'>";          
        }
        if ($cont == 'slick_gallery') {
        	$links[] = "<link type='text/css' rel='stylesheet' href='".base_url()."sd-assets/slick/slick.css'>"; 
        	$links[] = "<link type='text/css' rel='stylesheet' href='".base_url()."sd-assets/slick/slick-theme.css'>";         	
        }
        if ($cont == 'layer_slider') {
        	$links[] = "<link type='text/css' rel='stylesheet' href='".base_url()."sd-assets/pluggables/plugins/slider-layer-slider/css/layerslider.css'>"; 
        	$links[] = "<link type='text/css' rel='stylesheet' href='".base_url()."sd-assets/frontend/pages/css/style-layer-slider.css'>";         	
        } 
        if ($cont == 'datetime_picker') {
        	$links[] = "<link type='text/css' rel='stylesheet' href='".base_url()."sd-assets/css/datepicker.css'>";  	
        }    
		if($cont == 'gallery' && $view == 'index'){
			$links[] = "<link rel='stylesheet' href='".base_url()."sd-assets/pluggables/plugins/jcrop/css/jquery.Jcrop.min.css'>"; 
			$links[] = "<link rel='stylesheet' href='".base_url()."sd-assets/admin/pages/css/image-crop.css'>"; 

			$links[] = "<link rel='stylesheet' href='".base_url()."sd-assets/css/smoothbox.css'>"; 
			$links[] = "<link rel='stylesheet' href='".base_url()."sd-assets/css/gallery.css'>"; 
			$links[] = "<link rel='stylesheet' href='".base_url()."sd-assets/css/dropzone.css'>"; 
		}
		if($cont == 'edit' && $view == 'index'){
			$links[] = "<link rel='stylesheet' href='".base_url()."sd-assets/pluggables/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.css'>"; 
			$links[] = "<link rel='stylesheet' href='".base_url()."sd-assets/pluggables/plugins/jcrop/css/jquery.Jcrop.min.css'>"; 
			$links[] = "<link rel='stylesheet' href='".base_url()."sd-assets/admin/pages/css/image-crop.css'>"; 
			$links[] = "<link rel='stylesheet' href='".base_url()."sd-assets/pluggables/plugins/jquery-nestable/jquery.nestable.css'>"; 

			$links[] = "<link rel='stylesheet' href='".base_url()."sd-assets/css/smoothbox.css'>"; 
			$links[] = "<link rel='stylesheet' href='".base_url()."sd-assets/css/gallery.css'>"; 
			$links[] = "<link rel='stylesheet' href='".base_url()."sd-assets/css/dropzone.css'>"; 
		}
		if($cont == 'fancybox'){
			$links[] = "<link rel='stylesheet' href='".base_url()."sd-assets/pluggables/plugins/fancybox/source/jquery.fancybox.css'>"; 
		}
		return $links;
	}

}

?>