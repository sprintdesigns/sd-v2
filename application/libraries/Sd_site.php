<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SD_Site {

	private $CI;
	private $plugin_table;

	public function __construct()
	{
       $this->CI =& get_instance();
       $this->plugin_table='sd_site';
       
	}


	public function get_name()
	{
		$this->CI->db->select('*');
		$this->CI->db->from($this->plugin_table);
		$query = $this->CI->db->get();

		if($query->num_rows() > 0)
		{
			$result = $query->result();
			return $result[0]->name;
		}else
		{
			return false;
		}
	}

}

?>