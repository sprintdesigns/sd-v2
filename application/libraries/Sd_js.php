<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SD_Js {

	private $CI;

	public function __construct()
	{
       $this->CI =& get_instance();
	}

	public function get_scripts_view($cont, $view="index"){

		$scripts = array();

		if ($cont == 'slick_gallery') {
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/slick/slick.min.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/custom-slick.js'></script>";
			$scripts[] = "<script type='text/javascript' > $(document).ready(function(){  initSlick(); }); </script>"; 
		}

		if ($cont == 'datetime_picker') {
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/bootstrap-datepicker.js'></script>";
			$scripts[] = "<script type='text/javascript'> $('.sd-date').datepicker({
							autoclose: true,
							templates: {
							    leftArrow: '&laquo;',
							    rightArrow: '&raquo;'
							}
						});</script>"; 	 		
		}

		if ($cont == 'global_js') {
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/sd.js'></script>";	
		}		
	
		if ($cont == 'layer_slider') {
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/pluggables/plugins/slider-layer-slider/js/greensock.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/pluggables/plugins/slider-layer-slider/js/layerslider.transitions.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/pluggables/plugins/slider-layer-slider/js/layerslider.kreaturamedia.jquery.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/layersliderInit.js'></script>";
			$scripts[] = "<script type='text/javascript' > $(document).ready(function(){  LayersliderInit.initLayerSlider(); }); </script>"; 		
		}		

		if($cont == 'gallery' && $view == 'index'){
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/pluggables/plugins/jcrop/js/jquery.color.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/pluggables/plugins/jcrop/js/jquery.Jcrop.min.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/pluggables/scripts/metronic.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/admin/layout4/scripts/layout.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/admin/layout4/scripts/demo.js'></script>";
	
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/smoothbox.js'></script>"; 
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/dropzone.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/form-dropzone.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/sd-gallery.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/sd-gallery-crop.js'></script>";
			$scripts[] = "<script type='text/javascript' > FormDropzone.init(); </script>"; 
			$scripts[] = "<script type='text/javascript' > $(document).ready(function(){  Metronic.init();Layout.init(); Demo.init();FormImageCrop.init(); }); </script>"; 


		}
		if($cont == 'edit' && $view == 'index')
		{
			$scripts[] = "<script type='text/javascript' > var base_rl ='".$this->CI->config->base_url()."'; </script>"; 
			$scripts[] = "<script type='text/javascript' > var load_banner_gallery = true; </script>"; 
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/pluggables/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/pluggables/plugins/jquery-cookie/jquery.cookie.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/pluggables/plugins/mousetrap-master/mousetrap.min.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/pluggables/plugins/jquery.lazyload.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/sd-progress.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/sd-tutorials.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/pluggables/plugins/tinymce/tinymce.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/pluggables/plugins/jcrop/js/jquery.color.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/pluggables/plugins/jcrop/js/jquery.Jcrop.min.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/pluggables/plugins/jquery-nestable/jquery.nestable.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/pluggables/scripts/metronic.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/admin/layout4/scripts/layout.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/admin/layout4/scripts/demo.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/clipboard.min.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/admin/pages/scripts/components-jqueryui-sliders.js'></script>";

			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/sd-tooltip.js'></script>";

			
			

			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/sd-column.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/sd-form.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/sd-save-page.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/smoothbox.js'></script>"; 
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/dropzone.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/form-dropzone.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/sd-gallery.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/sd-gallery-crop.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/sd-front-function.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/sd-sortable.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/sd-banner.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/sd-social.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/sd-marketing-banner.js'></script>";
            $scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/jquery.cropit.js'></script>";

			
			
			$scripts[] = "<script type='text/javascript' > FormDropzone.init(); </script>"; 
			$scripts[] = "<script type='text/javascript' > $(document).ready(function(){  Metronic.init();Layout.init(); Demo.init();FormImageCrop.init();UINestable.init(); ComponentsjQueryUISliders.init(); }); </script>"; 
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/pluggables/plugins/mousetrap-master/mousetrap.run.js'></script>";

 
		}
		if($cont == 'dashboard' && $view == 'index')
		{
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/pluggables/scripts/metronic.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/admin/layout4/scripts/layout.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/admin/layout4/scripts/demo.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/admin/pages/scripts/index3.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/admin/pages/scripts/tasks.js'></script>";
			$scripts[] = "<script type='text/javascript' > $(document).ready(function(){  Metronic.init();Layout.init(); Demo.init();Tasks.initDashboardWidget(); }); </script>"; 


		}
		if($cont == "plugins")
		{
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/sd-plugins.js'></script>";
		}

		if($cont == "olstore_datatable") {
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/jquery.dataTables.min.js'></script>";
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/dataTables.bootstrap.js'></script>";	
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/ol-store.js'></script>";	
		}

		if($cont == "fancybox") {
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/pluggables/plugins/fancybox/source/jquery.fancybox.pack.js'></script>";	
			$scripts[] = "<script type='text/javascript' src='". base_url() . "sd-assets/js/ol-store-fancy.js'></script>";	
		}

		return $scripts;
	}

	public function core_script()
	{
		$scripts = array();

		$scripts[] = "<script type='text/javascript' > var base_url ='".$this->CI->config->base_url()."'; </script>"; 
		$scripts[] = '<script src="'. base_url() .'sd-assets/pluggables/plugins/jquery.min.js" type="text/javascript"></script>';
		$scripts[] = '<script src="'. base_url() .'sd-assets/pluggables/plugins/jquery-migrate.min.js" type="text/javascript"></script>';
		$scripts[] = '<script src="'. base_url() .'sd-assets/pluggables/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>';
		$scripts[] = '<script src="'. base_url() .'sd-assets/pluggables/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>';
		$scripts[] = '<script src="'. base_url() .'sd-assets/pluggables/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>';
		$scripts[] = '<script src="'. base_url() .'sd-assets/pluggables/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>';
		$scripts[] = '<script src="'. base_url() .'sd-assets/pluggables/plugins/jquery.blockui.min.js" type="text/javascript"></script>';
		$scripts[] = '<script src="'. base_url() .'sd-assets/pluggables/plugins/jquery.cokie.min.js" type="text/javascript"></script>';
		$scripts[] = '<script src="'. base_url() .'sd-assets/pluggables/plugins/uniform/jquery.uniform.min.js"></script>';
		$scripts[] = '<script src="'. base_url() .'sd-assets/pluggables/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>';

		return $scripts;
	}

}

?>