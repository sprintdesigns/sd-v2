<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

/*
* UnzipFile class for CI
* 
*/

class SD_Modal {


    private $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
    }
    

    public function dynamic($args = null)
    {

        if($args == null)
        {
            echo "SDmodal error: modal dynamic need the object,type,title,display";
            return ;
        }

        $check = ['object','type','title','display'];
        $check_args = $this->check_modal_args($args,$check);

        
        if(!$check_args['status'])
        {
            echo "SDmodal error: ".$check_args['index']." is not defined";
            return ;
        }

        //set data to pass
        $data['object'] = $args['object'];
        $data['type'] = $args['type'];
        $data['title'] = $args['title'];
        $data['display'] = $args['display'];
        $data['button'] = array_key_exists('button',$args)? $args['button']:false;

        // echo the modal
        echo $this->CI->load->view('modal/dynamic',$data,true); 

       
    }

    public function check_modal_args($args,$check)
    {
        foreach ($check as $key => $value) {
            if(!array_key_exists($value,$args))
            {
                $response = array('status' => false,'index' => $value );
                return $response;
            }   
        }

         $response = array('status' => true);

         return $response;
    }



}

?>
