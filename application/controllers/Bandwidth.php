<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Bandwidth extends CI_Controller {

	public $sd;

	function __construct()
	{
		parent::__construct();

		$libraries = new SD_Library();
		$this->sd = $libraries->loadLibraries();
		$this->data['sd'] = $this->sd;
		$this->sd->dircheck->dir_init();
	}


	public function update()
	{
		$result = $this->sd->bandwidth->cal_bandwidth();
		
		echo json_encode($result);
	}
}

?>