<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends CI_Controller {

	public $sd;

	function __construct()
	{
		parent::__construct();

		$libraries = new SD_Library();
		$this->sd = $libraries->loadLibraries();
		$this->data['sd'] = $this->sd;
		
	}


	public function send($form_id = null)
	{
		
		$this->load->library('email');
		
		$email_settingis = $this->sd->form->settings($form_id);
		$form_data = $this->sd->form->get_form_data($form_id);

		if($email_settingis)
		{
			if($email_settingis[0]->from_email != "" && $email_settingis[0]->to_email != "")
			{
				$email_from = json_decode($email_settingis[0]->from_email);
				$this->email->set_smtp_user($email_from->email);
				$this->email->set_smtp_pass($email_from->password);
				$this->email->from($email_from->email);

				$email_to = implode(',',json_decode($email_settingis[0]->to_email));
				$this->email->to($email_to);

				$this->email->subject($email_settingis[0]->subject);

				$body = "";
				foreach ($form_data as $index => $data) {
					if($data->type != "radio")
					{
						$body .= $data->label.': '.$_POST[$data->name.'-'.$data->id].'<br/><br/>';
					}
					
				}

				if(array_key_exists('radio-btn-'.$form_id, $_POST))
				{
					$body .= 'Type: '.$_POST['radio-btn-'.$form_id].'<br/><br/>';
				}
				
				$this->email->message($body);

				$result = $this->email->send();
				
				if($result)
				{

					$response = array('status' => true, 'message' => "Message Sent Successfully");
					$this->session->set_flashdata('response',$response);

					redirect($_SERVER['HTTP_REFERER']);
				}
			        

			}else
			{
				$response = array('status' => false,'message' => "Please give the webmail and email receivers to test the email" );
				$this->session->set_flashdata('response',$response);
				redirect($_SERVER['HTTP_REFERER']);
			} 	
				
		}else
		{
			$response = array('status' => false,'message' => "Your mailer is not setup yet" );
			$this->session->set_flashdata('response',$response);
			redirect($_SERVER['HTTP_REFERER']);
		}

		
	
	}

	public function test_send()
	{
		$this->load->library('email');
		$form_id = $_POST['form_id'];
		$email_settingis = $this->sd->form->settings($form_id);

		if($email_settingis)
		{
			if($email_settingis[0]->from_email != "" && $email_settingis[0]->to_email != "")
			{
				$email_from = json_decode($email_settingis[0]->from_email);
				$this->email->set_smtp_user($email_from->email);
				$this->email->set_smtp_pass($email_from->password);
				$this->email->from($email_from->email);
				$email_to = implode(',',json_decode($email_settingis[0]->to_email));
				$this->email->to($email_to);

				$this->email->subject($email_settingis[0]->subject);

				$this->email->message("this is a test email success!");

				$result = $this->email->send();
				
				if($result)
				{
					$response = array('status' => true,'message' => "Test email successfully send, check your email to see the result" );
				}else
				{
					$response = array('status' => false,'message' => "Test email sending failed","result" => $result );
				}
			        

			}else
			{
				$response = array('status' => false,'message' => "Please give the webmail and email receivers to test the email" );
			} 	
				
		}else
		{
			$response = array('status' => false,'message' => "Your mailer is not setup yet" );
		}

		 
		echo json_encode($response);
	}

	
	
	
}
?>