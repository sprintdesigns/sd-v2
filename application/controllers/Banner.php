<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends CI_Controller {

	public $sd;

	function __construct()
	{
		parent::__construct();

		$libraries = new SD_Library();
		$this->sd = $libraries->loadLibraries();
		$this->data['sd'] = $this->sd;
		
	}
	

	public function get_slide()
	{
		
 		$slide = $this->sd->banner->load_slide($_POST['slide_id'],$this->sd);

 		if($slide)
 		{
 			$response = array('status' => true,'message' => 'slide load successfully ','slide' => $slide );
 		}else
 		{
 			$response = array('status' => false,'message' => "slide doesn't exist" );
 		}
 		
 		echo json_encode($response);
	}

	public function save_slide()
	{

		
		$data = array(
			'url' => $_POST['sd-slide-image'], 
			'text_title' => $_POST['sd-slide-title'], 
			'sub_title' => $_POST['sd-slide-sub'], 
			'opacity' => $_POST['sd-slide-opacity'], 
			'button_title' => $_POST['sd-slide-btn-text'], 
			'button_url' => $_POST['sd-slide-btn-url'], 
		);

		$update_slide = $this->sd->banner->save_slide($_POST['sd-slide-id'],$data);

		if($update_slide)
		{
			$response = array('status' => true ,'message' => '<a href="#" class="close" data-dismiss="alert" aria-label="close"><span class="text text-primary got-it">got it</span></a>Slide saved, To see your changes you must <button class="btn btn-danger" onclick="history.go(0)">refresh</button> your page.' );
		}else
		{
			$response = array('status' => false ,'message' => 'Something went wrong' );
		}

		echo json_encode($response);
	}

	public function banner_images()
	{
		$this->sd->banner->gallery($this->sd);
	}

	public function create_slide()
	{
		$theme_id = $this->sd->theme->get_active_theme()[0]->id;

		$data = array(
			'url' => $_POST['url'], 
			'original_url' => $_POST['url'], 
			'text_title' => 'New slide title', 
			'sub_title' => 'New slide sub title', 
			'opacity' => '0', 
			'button_title' => 'learn more', 
			'button_url' => '#', 
			'theme_id' => $theme_id, 
		);

		$new_slide = $this->sd->banner->create_slide($data);

		$menu_slide = $this->sd->banner->banner_menu_slide($new_slide,$this->sd);

		$slide = $this->sd->banner->load_slide($new_slide,$this->sd);

		$response = array('status' => true,'message' => 'New slide successfully created','menu_slide' => $menu_slide, 'slide' => $slide );

		echo json_encode($response);

	}

	public function delete_slide()
	{
		$theme_id = $this->sd->theme->get_active_theme()[0]->id;
		$slider  = $this->sd->banner->get_slider(null,$theme_id);
		if(sizeof($slider) > 1)
		{
			$delete_slide = $this->sd->banner->delete_slide($_POST['slide_id']);

			if($delete_slide)
			{
				$slider = $this->sd->banner->get_slider(null,$theme_id);
				$menu_slide = $this->sd->banner->banner_menu_slide($slider[0]->id,$this->sd);

				$slide = $this->sd->banner->load_slide($slider[0]->id,$this->sd);



				$response = array('status' => true,'message' => 'Slide successfully deleted','menu_slide' => $menu_slide, 'slide' => $slide );

			}else
			{
				$response = array('status' => false,'message' => 'Invalid slide id');
			}	
		}else
		{
			$response = array('status' => false,'message' => 'Invalid action, you must at least leave one slide for the slider');
		}
		

		echo json_encode($response);
	}


	function change_slide_image()
	{
		$data = array(
			'url' => $_POST['url'],
			'original_url' => $_POST['url'], 
		);

		$update_slide = $this->sd->banner->save_slide($_POST['slide_id'],$data);

		$menu_slide = $this->sd->banner->banner_menu_slide($_POST['slide_id'],$this->sd);

		$slide = $this->sd->banner->load_slide($_POST['slide_id'],$this->sd);

		$response = array('status' => true,'message' => 'Change image slide successful','menu_slide' => $menu_slide, 'slide' => $slide );

		echo json_encode($response);

	}
	
	
}
?>