<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form extends CI_Controller {

	public $sd;

	function __construct()
	{
		parent::__construct();

		$libraries = new SD_Library();
		$this->sd = $libraries->loadLibraries();
		$this->data['sd'] = $this->sd;
		
	}

	// new function
	public function index()
	{
		//$this->sd->edit->mode(true);
		
		echo $this->load->view('forms/index.php',$this->data,true);
	}

	// new function
	public function add_contact_form()
	{
		$form = $this->sd->form->get_form_by_name($_POST['form_name']);

		if(!$form)
		{
			$user_data = $this->session->userdata()['user_data'];
			$data = array(
				'name' => $_POST['form_name'], 
				'created_by' => $user_data->username,
				'is_main' => false,
				'status' => true
			);
			$form_id = $this->sd->form->register_contact_form($data);

			$default_field = array(
				'text' => 'Full Name', 
				'email' => 'Email Address', 
				'number' => 'phone Number', 
				'textarea' => 'Message', 
			);

			foreach ($default_field as $type => $name) {
				$field = array(
					'label' => $name, 
					'type' => $type,
					'form' => $form_id,
					'status' => true,
					'required' => false,
					'clone_from' => 0
				);

				$this->sd->form->create_field($field);
			}

			

			$form_list = $this->sd->form->form_list($this->sd);

			$response = array('status' => true, 'message' => 'Contact Form Successfully Added', 'form_list' => $form_list );
		}else
		{
			$response = array('status' => false, 'message' => 'Contact Form name already Exist' );
		}

		echo json_encode($response);
	}

	public function use_form()
	{
		$id = $this->sd->utilities->decrypt($_POST['id']);

		$this->sd->form->get_form($this->sd,$id);
	}

	public function add_field()
	{
		$field = [
			(object) array(
				'label' => $_POST['name'], 
				'type' => $_POST['type'],
			)
		];

		
		$this->sd->form->field($field);

	}

	// new function
	public function edit()
	{
		$id = $_POST['form_id'];
		$this->data['form_id'] = $id;
		
		
		echo $this->load->view('forms/edit.php',$this->data,true);
		

	}

	public function settings($id)
	{
		$this->data['form_data'] = $this->sd->form->get_form_data($id);
		$this->data['id'] = $id;
		$this->data['settings'] = $this->sd->form->settings($id);
		$this->sd->edit->mode(true);
		$this->load->view('forms/header.php',$this->data);
		$this->load->view('forms/settings.php',$this->data);
		$this->load->view('forms/footer.php',$this->data);
	}


	public function add_email()
	{
		echo json_encode($_POST['email-contact-form']);
		
	}

	public function switch_main()
	{
		$id = $_POST['form_id'];

		$this->sd->form->set_main($id);

		$form = $this->sd->form->form($id);

		$form_list = $this->sd->form->form_list($this->sd);

		$response = array('status' => true,'message' => $form[0]->name.' is now set to your pages','form_list' => $form_list );

		echo json_encode($response);
	}

	public function save_edit()
	{
		
		$result = $this->sd->form->clear_fields($_POST['form_id']);

		if($result)
		{
			foreach ($_POST['name'] as $index => $fname) {
				$field = array(

						'label' => $fname, 
						'type' => $_POST['type'][$index],
						'form' => $_POST['form_id']

				);

				$this->sd->form->create_field($field);
			}
			$response = array('status' => true, 'message' => "Form save Successfully");
			$this->session->set_flashdata('response',$response);
		}
		
	}

	public function save_settings($id)
	{

		$settings_data = array(
			'to_email' => $_POST['email-form-to'], 
			'from_email' => $_POST['email-form-from'], 
			'subject' => $_POST['email-form-subject'], 
			'message' => $_POST['email-form-message'], 
			'type' => $_POST['email-form-type'], 
			'form_id' => $id, 
		);

		if(!empty($_POST['email-form-id']))
			$result = $this->sd->form->update_settings($settings_data,$_POST['email-form-id']);
		else
			$result = $this->sd->form->set_settings($settings_data);


		var_dump($result);
		if($result)
		{
			$response = array('status' => true, 'message' => "Saving success");
			$this->session->set_flashdata('response',$response);
		}else
		{
			$response = array('status' => false, 'message' => "Something went wrong");
			$this->session->set_flashdata('response',$response);
		}

		redirect(base_url()."form/settings/".$id);
		


	}

	
	public function email()
	{
		$sd_json = file_get_contents('corefile/sd.json');
		$sd_json = json_decode($sd_json);

		$this->data['data_email'] = array_key_exists('admin_email', $sd_json)? $sd_json->admin_email:false;

		$this->sd->edit->mode(true);
		$this->load->view('forms/header.php',$this->data);
		$this->load->view('forms/email.php',$this->data);
		$this->load->view('forms/footer.php',$this->data);
	}

	public function save_email()
	{
		$sd_json = file_get_contents('corefile/sd.json');
		$sd_json = json_decode($sd_json);

		$admin_email = array(
				'email' => $_POST['form-email'], 
				'password' => $_POST['form-password'], 
				'host' => $_POST['form-host'], 
				'protocol' => $_POST['form-protocol'], 
				'port' => $_POST['form-port'], 
				'encryption' => $_POST['form-crypto'], 
			);
		$sd_json->admin_email = $admin_email;


		$jsonData = json_encode($sd_json);
		file_put_contents('corefile/sd.json', $jsonData);

		$response = array('status' => true, 'message' => "Email Successfully Saved");
		$this->session->set_flashdata('response',$response);

		redirect(base_url()."form/email");
	}

	// new function

	public function switch_status()
	{
		$form_id = $_POST['form_id'];

		$update_form = $this->sd->form->set_status($form_id);

		$form = $this->sd->form->form($form_id);


		if($update_form)
		{
			if($form[0]->status)
			{
				$response = array('status' => true,'message' => 'Form '.$form[0]->name.' successfully switch to on','form_status' => $form[0]->status );
			}else
			{
				$response = array('status' => true,'message' => 'Form '.$form[0]->name.' successfully switch to off','form_status' => $form[0]->status );
			}
		}else
		{
			$response = array('status' => false,'message' => 'Something went wrong' );
		}

		

		echo json_encode($response);
	}

	// new function
	public function delete_form()
	{
		$id = $_POST['form_id'];

		$form = $this->sd->form->form($id);

		$delete_form = $this->sd->form->delete_form($id);

		if($delete_form)
		{
			$result = $this->sd->form->clear_fields($id);

			$response = array('status' => true,'message' => $form[0]->name.' is successfully deleted' );
		}else
		{
			$response = array('status' => false,'message' => 'Something went wrong while deleting form '.$form[0]->name );
		}

		echo json_encode($response);
	}

	// new function
	public function duplicate_form()
	{
		$id = $_POST['form_id'];
		$form = $this->sd->form->form($id);
	
		$num_clone = $this->sd->form->count_clone($id);
	
		if(is_int($num_clone))
		{
			$user_data = $this->session->userdata()['user_data'];
			$num_clone++;

			$data = array(
				'name' => $form[0]->name.'-'.$num_clone,
				'created_by' => $user_data->username,
				'is_main' => false,
				'clone_from' => $id,
				'status' => $form[0]->status
			);

			$form_id = $this->sd->form->register_contact_form($data);

			$form_data = $this->sd->form->get_form_data($id);

			$result = $this->sd->form->clear_fields($form_id);

			if($form_data)
			{
				foreach ($form_data as $index => $data){
					$field = array(

							'label' => $data->label, 
							'type' => $data->type,
							'form' => $form_id,
							'status' => $data->status,
							'required' => $data->required,
							'clone_from' => 0

					);

					$this->sd->form->create_field($field);
				}
			}
			
			$form_list = $this->sd->form->form_list($this->sd);

			$response = array('status' => true,'message' => $form[0]->name.' is successfully duplicated','form_list' => $form_list );


		}else
		{
			$response  = array('status' => false,'message' => 'Something went wrong' );
		}

		echo json_encode($response);
	}


	public function switch_status_field()
	{
		$field_id = $_POST['field_id'];

		$update_field = $this->sd->form->set_status_field($field_id);

		$field = $this->sd->form->get_field($field_id);


		if($update_field)
		{
			if($field[0]->status)
			{
				$response = array('status' => true,'message' => 'Successfully switch to on' );
			}else
			{
				$response = array('status' => true,'message' => 'Successfully switch to off' );
			}
		}else
		{
			$response = array('status' => false,'message' => 'Something went wrong' );
		}

		

		echo json_encode($response);
	}

	public function switch_required_field()
	{
		$field_id = $_POST['field_id'];

		$update_field = $this->sd->form->set_required_field($field_id);

		$field = $this->sd->form->get_field($field_id);


		if($update_field)
		{
			if($field[0]->status)
			{
				$response = array('status' => true,'message' => 'Successfully change to required' );
			}else
			{
				$response = array('status' => true,'message' => 'Successfully change to not required' );
			}
		}else
		{
			$response = array('status' => false,'message' => 'Something went wrong' );
		}

		

		echo json_encode($response);
	}

	


	public function duplicate_field()
	{
		$id = $_POST['field_id'];

		$field = $this->sd->form->get_field($id);

		$num_clone = $this->sd->form->count_clone_field($id);

		if(is_int($num_clone))
		{

			$num_clone++;

			$field_data = array(
				'label' => $field[0]->label.'-'.$num_clone, 
				'type' => $field[0]->type,
				'form' => $field[0]->form_id,
				'status' => $field[0]->form_id,
				'required' => $field[0]->required,
				'clone_from' => $id
			);

			$this->sd->form->create_field($field_data);

			$this->data['form_id'] = $field[0]->form_id;

			$field_table = $this->load->view('forms/edit_fields_table',$this->data,true);

			$response = array('status' => true,'message' => 'Successfully cloned', 'field_table' => $field_table );

		}else
		{
			$response = array('status' => false,'message' => 'Something went wrong' );
		}

		echo json_encode($response);
	}

	public function delete_field()
	{
		$id = $_POST['field_id'];

		$field = $this->sd->form->get_field($id);

		$deleted_field = $this->sd->form->delete_field($id);

		$this->data['form_id'] = $field[0]->form_id;

		$field_table = $this->load->view('forms/edit_fields_table',$this->data,true);

		$response = array('status' => true,'message' => 'Field successfully deleted', 'field_table' => $field_table );

		echo json_encode($response);


	}

	public function update_field()
	{
		$type = $_POST['type'];
		$name = $_POST['name'];
		$id = $_POST['id'];

		$field = $this->sd->form->get_field($id);

		$field_data = array(
			'type' => $type, 
			'label' => $name, 
		);

		$update_field = $this->sd->form->update_field($field_data,$id);

		if($update_field)
		{
			$this->data['form_id'] = $field[0]->form_id;
			$field_table = $this->load->view('forms/edit_fields_table',$this->data,true);

			$response = array('status' => true,'message' => 'Field successfully updated', 'field_table' => $field_table );
		}else
		{
			$response = array('status' => false,'message' => 'Something went wrong' );
		}
		

		echo json_encode($response);
	}

	
	public function create_field()
	{
		$label = $_POST['label'];
		$type = $_POST['type'];
		$form_id = $_POST['id'];

		$field = array(
			'label' => $label, 
			'type' => $type,
			'form' => $form_id,
			'status' => true,
			'required' => false,
			'clone_from' => 0
		);

		$this->sd->form->create_field($field);

		$this->data['form_id'] = $form_id;
		$field_table = $this->load->view('forms/edit_fields_table',$this->data,true);

		$response = array('status' => true,'message' => 'Field successfully updated', 'field_table' => $field_table );

		echo json_encode($response);
	}


	public function save_mailer()
	{
		$id = $_POST['id'];
		$email = $_POST['email'];
		$pass = $_POST['pass'];
		$form_id = $_POST['form_id'];

		$email = array(
			'email' => $email, 
			'password' => $pass 
		);

		

		if($id == '')
		{
			$data = array(
				'from_email' => json_encode($email) , 
				'form_id' => $form_id
			);
			$id = $this->sd->form->set_settings($data);
		}else
		{
			$data = array(
				'from_email' => json_encode($email) , 
			);
			$this->sd->form->update_settings($data,$id);
		}

		

		$response = array('status' => true,'message' => 'Webmail successfully saved', 'id' => $id );

		echo json_encode($response);


	}

	public function save_subject()
	{
		$id = $_POST['id'];
		$subject = $_POST['subject'];
		$form_id = $_POST['form_id'];
		

		

		if($id == '')
		{
			$data = array(
				'subject' => $subject , 
				'form_id' => $form_id
			);
			$id = $this->sd->form->set_settings($data);
		}else
		{
			$data = array(
				'subject' => $subject , 
			);
			$this->sd->form->update_settings($data,$id);
		}

		$response = array('status' => true,'message' => 'Subject successfully saved', 'id' => $id );

		echo json_encode($response);


	}

	public function save_to_email()
	{
		$email = $_POST['email'];
		$id = $_POST['id'];
		$form_id = $_POST['form_id'];

		for ($i=0; $i < sizeof($email) ; $i++) { 

			if(trim($email[$i]) == "" && trim($email[$i]) == null )
				 unset($email[$i]);
				
		}

		if($id == '')
		{
			$data = array(
				'to_email' => json_encode($email) , 
				'form_id' => $form_id
			);
			$id = $this->sd->form->set_settings($data);
		}else
		{
			$data = array(
				'to_email' => json_encode($email) , 
			);
			$this->sd->form->update_settings($data,$id);
		}

		$response = array('status' => true,'message' => 'Subject successfully saved', 'id' => $id );

		echo json_encode($response);
	}

	
	function submit_form_ajax() {
		date_default_timezone_set("Asia/Manila");
		
		$form_title = $_POST['form_title'];

		$u = new SD_Users;
		$user = $u->getUserEmail();
		$to_email = $user[0]->email;		

		$body = "Hello, <p>There is an inquiry from your form <strong>{$form_title}</strong>:</p>";
		$body .= "<p>Submitted on: ". date('F j, Y, g:i a') ."</p>";
		foreach ($_POST as $field => $value) {
			$field = str_replace('_', ' ', $field);
			$field = ucfirst($field);

			$body .= "{$field}: {$value} <br />";
		}

        $mail = new PHPMailer;
        $mail->isSMTP();  
        $mail->isHTML();                          // Set mailer to use SMTP
        //$mail->Host = 'smtp.gmail.com'; 
        $mail->Host = 'mail.sprintdesignsph.com';            // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                     // Enable SMTP authentication
        $mail->Username = 'mailer@sprintdesignsph.com';                 // SMTP username
		$mail->Password = 'K-o$kKhK{FaE';
        //$mail->Username = 'sprintdesignsmail@gmail.com';          // SMTP username
        //$mail->Password = 'M5nsJHp6cNUH'; // SMTP password
        $mail->SMTPSecure = 'tls';                  // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;

        //$mail->setFrom('info@sprintdesignsph.com', 'SprintDesigns Team');
        $mail->setFrom('mailer@sprintdesignsph.com', 'SprintDesigns');
		$mail->addAddress($to_email);
		$mail->Subject  = "{$form_title}: New inquiry";
		$mail->Body     = $body;

		if($mail->send()) {	
			$response['is_sent'] = true;
			$response['message'] = 'Your inquiry is successfully sent.';		
		} else {
		    $is_valid = $this->submit_plain_email($to_email, $mail->Subject, $body);
		    
		    if ($is_valid) {
    			$response['is_sent'] = true;
    			$response['message'] = 'Your inquiry is successfully sent.';
		    } else {
			    $response['is_sent'] = false;
			    $response['message'] = 'Something went wrong please contact the administrator.';
		    }
		}	

		echo json_encode($response);
	}

	function submit_plain_email($to_email, $subject, $body) {
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        $to      = $to_email;
        $subject = $subject;
        $message = $body;
        $headers .= 'From: mailer@sprintdesignsph.com' . "\r\n" .
            'Reply-To: info@sprintdesignsph.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        
        $x = mail($to, $subject, $message, $headers);	    
        if ($x) {
            return true;
        } else {
            return false;
        }	    
	}	
}
?>