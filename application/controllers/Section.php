<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Section extends CI_Controller {

	public $sd;

	function __construct()
	{
		parent::__construct();

		$libraries = new SD_Library();
		$this->sd = $libraries->loadLibraries();
		$this->data['sd'] = $this->sd;
		
	}

	public function apply_dynamic_section_ajax()
	{
		$content = $_POST['content'];
		$d = new SD_Dynamicsection;
		echo $d->apply_dynamic_sections($content);
	}	
}

?>