<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Marketing extends CI_Controller {

	public $sd;

	function __construct()
	{
		parent::__construct();

		$libraries = new SD_Library();
		$this->sd = $libraries->loadLibraries();
		$this->data['sd'] = $this->sd;
		
	}
	
		
	public function update_banner()
	{

		$data = array(
			'title' => $_POST['marketing-title'], 
			'banner' => $_POST['marketing-banner-img'],
			'description' => $_POST['marketing-description'],
			'page_url' => $_POST['sd-marketing-url'],
		);

		$banner_id = $this->sd->marketing->update_banner($data,$_POST['marketing-banner-id']);

		$banner_view = $this->sd->marketing->get_banner_views($this->sd);

		$result = array('status' => true,'message' => 'banner successfuly created','banner_view' => $banner_view);

		echo json_encode($result);
	}
	
}
?>