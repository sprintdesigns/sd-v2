<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontpage extends CI_Controller {

	public $sd;

	function __construct()
	{
		parent::__construct();

		$libraries = new SD_Library();
		$this->sd = $libraries->loadLibraries();
		$this->data['sd'] = $this->sd;
	}
	
	

	public function index()
	{
		if(!$this->sd->theme->check_if_has_theme())
		{
			$this->data['indexpage'] = true;
			$this->load->view('settings/header',$this->data);
			$this->load->view('settings/notheme',$this->data);
			$this->load->view('settings/footer',$this->data);
		}else
		{

			$theme = $this->sd->theme->get_active_theme();

			$theme_name = str_replace(' ', '-', $theme[0]->name);
			$template = 'frontpage.php';
			$theme_color = $theme[0]->color;
			$meta = "";
			$content = "";
			$page_url = "";

			$page = $this->sd->page->page_exist(null,$theme[0]->id);
			if($page != false && $page[0]->type != false)
			{
				$content = $page[0]->page_content;
				$template = $page[0]->template_name;
				$meta = $page[0]->meta_description;
				$page_url = $page[0]->page_url;
				$page = $page[0]->page_title;				

				$this->data['template'] = $this->data['page_tools']['template'] = $template;
				$this->data['content'] = $this->data['page_tools']['content'] = $content;
				$this->data['page'] = $this->data['page_tools']['page'] = $page;
				$this->data['meta'] = $this->data['page_tools']['meta'] = $meta;
				$this->data['page_url'] = $this->data['page_tools']['page_url'] = $page_url;
				$this->data['theme_color'] = $this->data['page_tools']['theme_color'] = $theme_color;
				$header = $this->sd->page->get_header();
                $this->data['header'] = $header[0];

                $this->load->view('../../sd-includes/themes/'.$theme_name.'/index',$this->data);

			}else
			{
				$this->load->view('errors/404');
			}
		}

		
 		
	}
	
}
?>