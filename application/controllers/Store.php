<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends CI_Controller {

	public $sd;
	public $plugins;

	function __construct()
	{
		parent::__construct();

		$libraries = new SD_Library();
		$this->sd = $libraries->loadLibraries();
		$this->load->library('session');
		$this->data['sd'] = $this->sd;
		$header = $this->sd->page->get_header();
        $this->data['header'] = $header[0];

        $theme = $this->sd->theme->get_active_theme();
        $this->data['theme_color'] = $theme[0]->color;
        $this->data['folder'] = $theme[0]->name;       

		$this->load->model('store_m');
	}	

	public function index()
	{
		$this->data['categories']   = $this->store_m->getAllCategories();

		$this->load->view('templates/store/header',$this->data);
		$this->load->view('store/index',$this->data);
		$this->load->view('templates/store/footer',$this->data);
	}

	public function category_items($id = '')
	{
		$this->data['categories'] = $this->store_m->getAllCategories();
		$this->data['products']   = $this->store_m->getAllItemsByCategory($id);
		$this->data['category']   = $c = $this->store_m->getCategory($id);

		$this->load->view('templates/store/header',$this->data);
		$this->load->view('store/category_items',$this->data);
		$this->load->view('templates/store/footer',$this->data);				
	}

	public function product_details($id = '')
	{
		$this->data['product_details']   = $this->store_m->getPublishedProduct($id);
		$this->data['product_photos']    = $photos = $this->store_m->getProductPhotos($id);
		$this->data['product_photos_count'] = $this->store_m->getProductPhotosCount($id);
		
		$user_flash_session 	  = $this->session->flashdata('user_flash_data');
		$this->data['flashdata']  = $user_flash_session;			

		$this->load->view('templates/store/header',$this->data);
		$this->load->view('store/product_details',$this->data);
		$this->load->view('templates/store/footer',$this->data);			
	}

	public function purchased_product() 
	{
		$data  = $this->input->post();
		$product_details = $this->store_m->getProduct($data['product_id']);

		date_default_timezone_set("Asia/Manila");
		if($product_details) {

			$product_id    = $product_details->id;
			$product_name  = $product_details->name;
			$product_price = $product_details->price;
			$product_color = isset($product_details->color) ? $product_details->color : 'NA';
			$product_size  = isset($product_details->size) ? $product_details->size : 'NA';			

			$fields = array( 'product_id'	=> $product_id,
							'email' 		=> $data['email'],
							'cust_name' 	=> $data['cust_name'],
							'cust_address' 	=> $data['cust_address'],
							'cust_number' 	=> $data['cust_number'],
							'mode_payment' 	=> $data['mode_payment'],
							'prodname' 		=> $product_name,
							'price' 		=> $product_price,
							'quantity' 		=> $data['quantity'],
							'color'	 		=> $product_color,
							'size' 			=> $product_size,
							'status' 		=> "PENDING",
							'purchase_date' => date('Y-m-d H:i:s')
			);

		} else {

			$fields = array();

		}

		foreach($fields as $field) {

			if(!isset($field) || empty($field)) {

				$flash_data = array(
					'status'  => false,
					'message' => '<strong>Please!</strong> fill in all data.'
				);
			
				$this->session->set_flashdata('user_flash_data', $flash_data);	
				redirect('store/product_details/' . $data['product_id']);
				exit;
			}
		}


		$this->db->insert('orders', $fields);
		
		if($this->db->affected_rows() != 0) {
			
			$flash_data = array(
				'status'  => true,
				'email'   => $data['email'],
				'product_name' 	=> $product_name,
				'message' => '<strong>Success!</strong> adding new order.'
			);
		} else {

			$flash_data = array(
				'status'  		=> false,
				'product_name' 	=> 'NA',
				'email' 		=> 'NA',
				'message' 		=> '<strong>Error!</strong> adding new order.'
			);
			redirect('store/product_details/' . $data['product_id']);
			exit;			
		}	

		$this->session->set_flashdata('user_flash_data', $flash_data);
       	//redirect('store/product_details/' . $data['product_id']);	
       	redirect('store/purchased_successful');	
       	
	}

	public function purchased_successful() 
	{
		$user_flash_session 	  = $this->session->flashdata('user_flash_data');
		$this->data['flashdata']  = $user_flash_session;			

		$this->data['purchased_message'] = $this->store_m->getPurchasedMessage(1);		

		if($user_flash_session) {
			$this->notify_purchase($user_flash_session['email'], $user_flash_session['product_name']);
		}

		$this->load->view('templates/store/header',$this->data);
		$this->load->view('store/purchased_successful',$this->data);
		$this->load->view('templates/store/footer',$this->data);
	}	

	function notify_purchase($email, $product_name)
	{
		$mail = new PHPMailer();
		$mail->isSMTP();
		$mail->SMTPDebug  = SMTP::DEBUG_SERVER;
		$mail->SMTPDebug  = 2;
        $mail->SMTPSecure = 'ssl';
        $mail->SMTPAuth   = true;
        
        $mail->Host 	= 'mail.sprintdesignsph.com';
        $mail->Port 	= 465;
        $mail->Username = 'mailer@sprintdesignsph.com';
        $mail->Password = 'K-o$kKhK{FaE';

        $mail->setFrom('mailer@sprintdesignsph.com', 'SprintDesigns');
        $mail->addAddress($email, 'Info');
        $mail->addReplyTo($email, 'Info');
        $mail->addBCC('bryann.revina@gmail.com');
        $mail->isHTML(true);	

        $body = "You have purchased ". $product_name;

        $mail->Subject = "Item purchased";
        $mail->Body    = $body;
        $mail->AltBody = $body;
	}	


}
?>