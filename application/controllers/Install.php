<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Install extends CI_Controller {

	public $sd;

	function __construct()
	{
		parent::__construct();

		$libraries = new SD_Library();
		$this->sd = $libraries->loadLibraries();
		$this->data['sd'] = $this->sd;
		
	}

	public function success()
	{
		$this->data['indexpage'] = true;
		$this->load->view('settings/header',$this->data);
		$this->load->view('settings/successful',$this->data);
		$this->load->view('settings/footer',$this->data);
	}
}

?>