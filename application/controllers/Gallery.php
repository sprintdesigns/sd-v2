<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

	public $sd;

	function __construct()
	{
		parent::__construct();

		$libraries = new SD_Library();
		$this->sd = $libraries->loadLibraries();
		$this->data['sd'] = $this->sd;
		$this->sd->dircheck->dir_init();
	}


	public function check_if_login()
	{
		$user_data = $this->session->userdata('user_data');
		if($user_data == null)
		{
			redirect('sdlogin');
		}
	}

	public function index()
	{
	
		if($this->sd->user->check_if_login())
		{
			if(!$this->sd->theme->check_if_has_theme())
			{
				redirect('settings/themes');
			}

			redirect('gallery/view_gallery');

		}else
		{
			redirect('sdlogin');
		}
	}

	public function view_gallery(){

		// $this->sd->dircheck->check_dir( base_url().'/sd-contents/uploads/img/');
		// exit();
		if($this->sd->user->check_if_login())
		{
			if(!$this->sd->theme->check_if_has_theme())
			{
				redirect('settings/themes');
			}


			$this->data['css'] = $this->sd->css->get_css_view('gallery');
			$this->data['scripts'] = $this->sd->js->get_scripts_view('gallery');
			$this->data['core_scripts'] = $this->sd->js->core_script();
			$this->data['media_data'] = $this->sd->gallery->process_media_data( $this->sd->image->get_media() );
			// echo '<pre>';
			// print_r($this->data['media_data']);
			// echo '</pre>';
			// exit();
			$this->load->view('templates/dashboard/header',$this->data);
			$this->load->view('gallery/index',$this->data);
			$this->load->view('templates/dashboard/footer',$this->data);

		}else
		{
			redirect('sdlogin');
		}
	
	}

	public function crop()
	{
		$this->data['css'] = $this->sd->css->get_css_view('gallery');
		$this->data['scripts'] = $this->sd->js->get_scripts_view('gallery');
		$this->data['core_scripts'] = $this->sd->js->core_script();

		$this->load->view('templates/dashboard/header',$this->data);
		$this->load->view('gallery/crop',$this->data);
		$this->load->view('templates/dashboard/footer',$this->data);

	}

	public function crop_it() {
	    $image_source = $_POST['image_source'];
        $code = $image_source;
        list($type, $code) = explode(';', $code);
        list(, $code)      = explode(',', $code);
        $code = base64_decode($code);

        $unique_id = uniqid();
        $image_file = "image-". $unique_id .".jpg";
        file_put_contents('sd-contents/cropped_images/'. $image_file, $code);

        $response = array('image_filename' => base_url() . 'sd-contents/cropped_images/'. $image_file);
        echo json_encode($response);
    }


	public function delete()
	{
		$this->sd->image->delete_image($_POST['img_id']);
	
		$response = array('status' => true,'message' => 'Image successfully deleted');

		echo json_encode($response);
	}

	public function load_images()
	{
		$images = $this->sd->gallery->load_gallery($this->sd);

		$response = array('status' => true,'images' => $images );

		echo json_encode($response);
	}
}

?>