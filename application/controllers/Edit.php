<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Edit extends CI_Controller {

	public $sd;

	function __construct()
	{
		parent::__construct();

		$libraries = new SD_Library();
		$this->sd = $libraries->loadLibraries();
		$this->data['sd'] = $this->sd;
		
	}
	
		
	public function index()
	{
		

		if(!$this->sd->theme->check_if_has_theme())
		{
			//redirect('sdlogin');
		}

		if($this->sd->user->check_if_login()){

			$user_data = $this->sd->user->get_current_user();

			$tutorial = $user_data->tutorial;
			if($user_data->tutorial)
			{
				$user_data->tutorial = false;
				$this->session->set_userdata('user_data',$user_data);
			}



			
			$theme = $this->sd->theme->get_active_theme();

			$theme_name = str_replace(' ', '-', $theme[0]->name);
			$theme_color = $theme[0]->color;
			$template = 'frontpage.php';
			$meta = "";
			$content = "";
			$page_url = "";

			$page = $this->sd->page->page_exist($_GET['page'],$theme[0]->id);

			if($page)
			{
				$content = $page[0]->page_content;
				$template = $page[0]->template_name;
				$meta = $page[0]->meta_description;
				$page_url = $page[0]->page_url;
				$page = $page[0]->page_title;
				

				$this->data['template'] = $this->data['page_tools']['template'] = $template;
				$this->data['content'] = $this->data['page_tools']['content'] = $content;
				$this->data['page'] = $this->data['page_tools']['page'] = $page;
				$this->data['meta'] = $this->data['page_tools']['meta'] = $meta;
				$this->data['tutorial'] = $this->data['page_tools']['tutorial'] = $tutorial;
				$this->data['page_url'] = $this->data['page_tools']['page_url'] = $page_url;
				$this->data['theme_color'] = $this->data['page_tools']['theme_color'] = $theme_color;
				$this->data['css'] = $this->data['page_tools']['css'] = $this->sd->css->get_css_view('edit');
				$this->data['scripts'] = $this->data['page_tools']['scripts'] = $this->sd->js->get_scripts_view('edit');
                $header = $this->sd->page->get_header();
                $this->data['header'] = $header[0];

				$this->sd->edit->mode(true);
				
				$this->load->view('../../sd-includes/themes/'.$theme_name.'/index',$this->data);

			}else
			{
				$this->load->view('errors/404');
			}
		}
	}
	
}
?>