<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Themes extends CI_Controller {

	public $sd;
	function __construct()
	{
		parent::__construct();

		$libraries = new SD_Library();
		$this->sd = $libraries->loadLibraries();
		$this->data['sd'] = $this->sd;

	}

	function choose_theme()
	{
		$this->data['indexpage'] = true;
		$themes = file_get_contents('http://sprintdesignsph.com/store-theme/get_themes.php');
		$themes = json_decode($themes, true);
		$themedb = $this->sd->theme->get_all_themes();
		if (! $themedb) {
			$explodetheme = $this->sd->theme->super_unique($themes);
		} else {
			$explodetheme = $this->sd->theme->super_unique(array_merge($themedb,$themes));
		}		

		$this->data['explodetheme'] = $explodetheme;
		$this->load->view('settings/header',$this->data);
		$this->load->view('settings/view_themes',$this->data);
		$this->load->view('modal/dynamic',$this->data);	

	}
}