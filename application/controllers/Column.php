<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Column extends CI_Controller {

	public $sd;

	function __construct()
	{
		parent::__construct();

		$libraries = new SD_Library();
		$this->sd = $libraries->loadLibraries();
		$this->data['sd'] = $this->sd;		
	}

	public function load()
	{
		$this->sd->edit->mode(true);

		$this->sd->column->bar($this->sd,$_POST['type']);
	}

	public function add_section()
	{
		$section_id = $_POST['section_id'];
		$this->sd->edit->mode(true);

		if (is_numeric($section_id)) {
			$this->sd->column->section($this->sd, $section_id);
		} else {
			$this->sd->column->section_core($this->sd, $section_id);
		}				
	}	
}
?>