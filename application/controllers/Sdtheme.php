<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sdtheme extends CI_Controller {

	public $sd;

	function __construct()
	{
		parent::__construct();

		$libraries = new SD_Library();
		$this->sd = $libraries->loadLibraries();
		$this->data['sd'] = $this->sd;

	}
	

	public function upload_theme()
	{

		$target_dir = "sd-includes/themes/";
		$target_file = $target_dir . basename($_FILES["template-file"]["name"]);
		$uploadOk = 1;
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
	
		// Check if file already exists
		$temp_check_file = explode('.', $target_file);
		if (file_exists($temp_check_file[0])) {
		   $response = array('status' => false,'message' => 'template already exists' );
		    $uploadOk = 0;
		}
		// Allow certain file formats
		if($imageFileType != "zip") {
		    $response = array('status' => false,'message' => 'invalid file type must be zip file' );
		    $uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
		// if everything is ok, try to upload file
		} else {
		    if (move_uploaded_file($_FILES["template-file"]["tmp_name"], $target_file)) {

		    	$this->sd->unzipfile->setFile($target_file);
		    	$this->sd->unzipfile->setPath($target_dir);
		    	$result = $this->sd->unzipfile->unzipFile();

		    	$theme_id = $this->sd->theme->register_theme($result);

		    	//register slide
		    	if(array_key_exists('slider', $result)){
			    	foreach ($result->slider as $index => $slide) {

			    		$data = array(
			    			'url' => base_url().'sd-includes/themes/'.$slide, 
			    			'original_url' => base_url().'sd-includes/themes/'.$slide, 
			    			'text_title' => $index, 
			    			'sub_title' => $index, 
			    			'opacity' => '0', 
			    			'button_title' => 'learn more', 
			    			'button_url' => "#", 
			    			'theme_id' => $theme_id, 
			    		);

			    		$insert_slide = $this->sd->banner->create_slide($data);	
			    	}
			    }

			    //register section
			    if(array_key_exists('section', $result)){
			    	foreach ($result->section as $index => $section) {

			    		$data = array(
			    			'title' => $section->title, 
			    			'file' => $section->file, 
			    			'class_img' => $section->class_img,  
			    			'theme_id' => $theme_id
			    		);

			    		$insert_section = $this->sd->column->register_section($data);	
			    	}
		    	}

		    	//register pages
		    	$menu_id = $this->sd->menu->create_new_menu($theme_id);
		    	if(array_key_exists('pages', $result))
		    	{
		    		

		    		foreach ($result->pages as $name => $data) {
		    			
		    			
		    			$new_name = str_replace('_', ' ', $name);

		    			$page_content="";
		    			for ($i=0; $i < sizeof($data->sections); $i++) { 
		    				$this->data['sd'] = $this->sd;
		    				$section = $this->load->view('../../sd-includes/themes/'.str_replace('.zip','',$_FILES["template-file"]["name"]).'/section/'.$data->sections[$i],$this->data,true);
		    					
		    				

		    				$page_content .= $section;

		    				 
		    			}
		    			if(array_key_exists('status', $data))
		    			{
		    				$status = $data->status;
		    			}else
		    			{
		    				$status = true;
		    			}
		    			$data = array(
							'page_title' => $new_name, 
							'page_url' =>preg_replace('/\s+/', '-',strtolower($new_name)),
							'template_name' => $data->template, 
							'meta_description' => '', 
							'page_content' => $page_content, 
							'theme_id' => $theme_id, 
							'type' => true, 
							'status' => $status, 
						);

						if($id = $this->sd->page->create($data))
						{
							
							$menu = $this->sd->menu->get_menu($theme_id);

						
							if($menu[0]->orders == null)
							{
								$nav = [];
							}else
							{
								$nav = json_decode($menu[0]->orders);
							}
							
							array_push($nav,(object) array('id' => $id ));
							$data = array(
								'orders' => json_encode($nav), 
							);
							$menu_response = $this->sd->menu->update_menu($theme_id,$data);

						}
		    		}
		    	}
		    	$social_id = $this->sd->social->add_social_init($theme_id);	
		    	
		    	
		       	$response = array('status' => true,'message' => 'Template Successfully Added' );
		    } else {
		        $response = array('status' => false,'message' => 'something went wrong, refresh the page and try it again' );
		    }
		}

		echo json_encode($response);
	}


	function activate_theme($id = null)
	{
		if($this->sd->theme->get_active_theme())
		{
			$redirect ="/sdlogin";
		}else
		{
			$redirect ="settings/success";
		}

		$this->sd->theme->activate_theme($id);

		$response = array(
				'status' => true,
				'message' => "Theme Successfully Activated.",
			);

		$this->session->set_flashdata('response',$response);

		redirect($redirect);
	}

	function install_and_activate_theme($theme_name = null) {
		$theme_id = $this->sd->theme->install_theme($theme_name);
		$this->activate_theme($theme_id);
	}
	
}
?>