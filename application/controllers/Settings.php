<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {

	public $sd;
	
	function __construct()
	{
		parent::__construct();

		$libraries = new SD_Library();
		$this->sd = $libraries->loadLibraries();
		$this->data['sd'] = $this->sd;
	}

	public function themes($pick = null)
	{	
		if($this->sd->user->check_if_login())
		{
			$this->data['notify_post_max'] = false;
			$this->data['notify_upload_max'] = false;

			if(!$this->sd->theme->check_if_has_theme())
			{							
				if($pick == null)
				{
                    $this->data['indexpage'] = true;
					$this->load->view('settings/header',$this->data);
					$this->load->view('settings/index',$this->data);
					$this->load->view('settings/footer',$this->data);
				}
				else
				{
					$theme_folders = $this->sd->theme->get_themes_by_folders();

					$this->data['theme_not_installed'] = $theme_folders;
                    $this->data['indexpage'] = false;
                    $this->data['post_max_size'] = $post_max_size = ini_get('post_max_size');
                    $this->data['upload_max_filesize'] = $upload_max_filesize = ini_get('upload_max_filesize');

                    $post_max = (int) $post_max_size;
                    $upload_max = (int) $upload_max_filesize;
                    $this->data['notify_post_max'] = false;
                    $this->data['notify_upload_max'] = false;

					if ($post_max < 30) 
					{
                    	$this->data['notify_post_max'] = true;
                    }

					if ($upload_max < 30) 
					{
                    	$this->data['notify_upload_max'] = true;
                    }

					$this->load->view('settings/header',$this->data);
					$this->load->view('settings/pick_template',$this->data);
					$this->load->view('settings/footer',$this->data);
				}
			}
			else
			{
				$theme = $this->sd->theme->get_active_theme();
				$theme_folders = $this->sd->theme->get_themes_by_folders();
				$theme_installed = $this->sd->theme->get_all_themes();

				$theme_not_installed = array();
				foreach ($theme_installed as $installed) 
				{
					$themes[$installed->name] = $installed;	
				}

				foreach ($theme_folders as $folder) 
				{
					$folders[$folder->name] = $folder;
				}

				$theme_not_installed = array_diff_key($folders,$themes);

				$this->data['theme_not_installed'] = $theme_not_installed;
				$this->data['active_theme'] = $theme[0];
				$this->data['indexpage'] = false;
				$this->load->view('settings/header',$this->data);
				$this->load->view('settings/pick_template',$this->data);
				$this->load->view('settings/footer',$this->data);
			}
		}
		else
		{
			redirect('sdlogin');
		}
	}

	function success()
	{
		$this->data['indexpage'] = true;
		$this->load->view('settings/header',$this->data);
		$this->load->view('settings/successful',$this->data);
		$this->load->view('settings/footer',$this->data);
	}

	function download_and_extract_theme()
	{
		$this->load->library('sd_theme');
		$this->sd_theme->download_install_theme();
	}

	function choose_theme()
	{
		$this->data['indexpage'] = true;
		$themes = file_get_contents('http://sprintdesignsph.com/store-theme/get_themes.php');
		$themes = json_decode($themes, true);
		$themedb = $this->sd->theme->get_all_themes();
		if (! $themedb) 
		{
			$explodetheme = $this->sd->theme->super_unique($themes);
		} 
		else 
		{
			$explodetheme = $this->sd->theme->super_unique(array_merge($themedb,$themes));
		}		

		$this->data['explodetheme'] = $explodetheme;
		$this->load->view('settings/header',$this->data);
		$this->load->view('settings/choose_theme',$this->data);
		$this->load->view('modal/dynamic',$this->data);	
	}

	function update_themes() 
	{	
		$themes = file_get_contents('http://sprintdesignsph.com/store-theme/get_themes.php');
		$themes = json_decode($themes, true);
		$explodetheme = $this->sd->theme->super_unique($themes);
		$active_theme = $this->sd->theme->get_themes_by_folders();
		
		$theme_names = array();
		$links = array();

		foreach ($explodetheme as $theme) {

			if(array_key_exists("zip_file", $theme)) {

				foreach($active_theme as $at) {

			  		if($theme->name == $at->name) {
				
						$theme_names[] = $theme->theme_folder;
						$links[] = $theme->zip_file;
			  		}
				} 
		  	} 
		}

		$this->load->library('sd_theme');
		$this->sd_theme->update_all_themes($theme_names, $links);
		
	}
}
?>