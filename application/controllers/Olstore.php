<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Olstore extends CI_Controller {

	public $sd;
	public $plugins;

	function __construct()
	{
		parent::__construct();

		$libraries = new SD_Library();
		$this->sd = $libraries->loadLibraries();
		$this->load->library('session');
		$this->data['sd'] = $this->sd;

		$this->load->model('store_m');
		//$this->plugins = $this->data['plugins'] = SD_Plugin::getPlugins();		
	}

	public function is_login()
	{
		if($this->sd->user->check_if_login())
		{
			if(!$this->sd->theme->check_if_has_theme())
			{
				redirect('settings/themes');
			}

		} else {
			redirect('sdlogin');
		}
	}		

	public function index()
	{
		$this->is_login();
		
		$this->data['scripts'] 		= $this->sd->js->get_scripts_view('dashboard');
		$this->data['core_scripts'] = $this->sd->js->core_script();
		$this->data['active_tab'] 	= "dashboard";

		$orders = $this->store_m->getAllOrders();

		$this->data['countOrder'] = ($orders == false) ? 0 : count($orders);
		$grandtotal = 0;

		if ($orders != false) {
			foreach ($orders as $ord) {
				if($ord->status == "Paid")
				{
					$grandtotal += floatval($ord->quantity) * floatval($ord->price);
				}
			}			
		}
		
		$this->data['totalsales'] = $grandtotal;

		$dailytotal = 0;
		$datetoday_str = date("Y/m/d");
		$datetoday_obj = date_create($datetoday_str);

		if ($orders != false) {
			foreach($orders as $or){
				$purchasedate = date_create($or->purchase_date);
				$temp = date_create(date_format($purchasedate, "Y/m/d"));

				if($datetoday_obj == $temp)
				{
					if($or->status == "Paid")
					{
						$dailytotal += floatval($or->quantity) * floatval($or->price);
					}
				}
			}
		}
		$this->data['dailysales'] = $dailytotal;
		
		$products = $this->store_m->getAllProducts();
		$this->data['countProduct'] = ($products == false) ? 0 : count($products);

		$this->load->view('templates/olstore/header',$this->data);
		$this->load->view('olstore/dashboard',$this->data);
		$this->load->view('templates/olstore/footer',$this->data);
	}

	public function page()
	{
		$this->is_login();
		
		$this->data['scripts'] 		= $this->sd->js->get_scripts_view('dashboard');
		$this->data['core_scripts'] = $this->sd->js->core_script();

		$this->data['active_tab'] 	= "settings";

		$user_flash_session 	  = $this->session->flashdata('user_flash_data');
		$this->data['flashdata']  = $user_flash_session;

		$this->data['msg'] = $this->store_m->getAllMessages();

		$this->load->view('templates/olstore/header',$this->data);
		$this->load->view('olstore/page',$this->data);
		$this->load->view('templates/olstore/footer',$this->data);
	}

	public function orders()
	{
		$this->is_login();
	
		$this->data['scripts'] 		= $this->sd->js->get_scripts_view('olstore_datatable');
		$this->data['core_scripts'] = $this->sd->js->core_script();

		$this->data['active_tab'] 	= "orders";	
		$this->data['orders']       = $this->store_m->get_all_orders_ob_date();

		$user_flash_session 	  = $this->session->flashdata('user_flash_data');
		$this->data['flashdata']  = $user_flash_session;		

		$this->load->view('templates/olstore/header',$this->data);
		$this->load->view('olstore/orders',$this->data);
		$this->load->view('templates/olstore/footer',$this->data);
	}

	public function orderView($id = null)
	{
		$this->is_login();
	
		$this->data['scripts'] 		= $this->sd->js->get_scripts_view('olstore_datatable');
		$this->data['core_scripts'] = $this->sd->js->core_script();

		$this->data['active_tab'] 	= "orders";
		$this->data['order_info'] 	= $this->store_m->getAllOrdersbyID($this->uri->segment(3));

		//$this->data['order_id']     = $this->uri->segment(3);

		$user_flash_session 	    = $this->session->flashdata('user_flash_data');
		$this->data['flashdata']    = $user_flash_session;

		$this->data['order'] = $this->store_m->getAllOrdersbyID($id);

		$this->load->view('templates/olstore/header', $this->data);
		$this->load->view('olstore/order_view', $this->data);
		$this->load->view('templates/olstore/footer', $this->data);
	}

	public function updateOrder($id = null)
	{
		$data  = $this->input->post();
		
		$fields = array( 'product_id'	=> $data['product_id'],
						'email' 		=> $data['customer_email'],
						'cust_name' 	=> $data['customer_name'],
						'cust_address' 	=> $data['customer_address'],
						'cust_number' 	=> $data['phone_number'],
						'mode_payment' 	=> $data['mode_of_payment'],
						'prodname' 		=> $data['product_name'],
						'price' 		=> $data['product_price'],
						'quantity' 		=> $data['product_quantity'],
						'color'	 		=> $data['product_color'],
						'size' 			=> $data['product_size'],
						'status' 		=> $data['order_status']
		);

		foreach($fields as $field) {

			if(!isset($field) || empty($field)) {

				$flash_data = array(
					'status'  => true,
					'message' => '<strong>Please!</strong> fill in all data.'
				);
			
				$this->session->set_flashdata('user_flash_data', $flash_data);
				redirect('olstore/orderView/' . $id);
			}
		}

		$this->db->where('id', $id);
		$this->db->update('orders', $fields); 
		
		if($this->db->affected_rows() != 0) {
			
			$flash_data = array(
				'status'  => true,
				'message' => '<strong>Success!</strong> updating new order.'
			);
		}
		else {

			$flash_data = array(
				'status'  => true,
				'message' => '<strong>Error!</strong> updating new order.'
			);
		}

		$this->session->set_flashdata('user_flash_data', $flash_data);
		redirect('olstore/orderView/' . $id);
	}

	public function addOrder($id = null)
	{
		$data  = $this->input->post();
		date_default_timezone_set("Asia/Manila");

		$fields = array( 'product_id'	=> $data['product_id'],
						'email' 		=> $data['customer_email'],
						'cust_name' 	=> $data['customer_name'],
						'cust_address' 	=> $data['customer_address'],
						'cust_number' 	=> $data['phone_number'],
						'mode_payment' 	=> $data['mode_of_payment'],
						'prodname' 		=> $data['product_name'],
						'price' 		=> $data['product_price'],
						'quantity' 		=> $data['product_quantity'],
						'color'	 		=> $data['product_color'],
						'size' 			=> $data['product_size'],
						'status' 		=> $data['order_status'],
						'purchase_date' => date('Y-m-d H:i:s')
		);

		foreach($fields as $field) {

			if(!isset($field) || empty($field)) {

				$flash_data = array(
					'status'  => true,
					'message' => '<strong>Please!</strong> fill in all data.'
				);
			
				$this->session->set_flashdata('user_flash_data', $flash_data);
	
				redirect('olstore/orders');
			}
		}

		$this->db->insert('orders', $fields);
		
		if($this->db->affected_rows() != 0) {
			
			$flash_data = array(
				'status'  => true,
				'message' => '<strong>Success!</strong> adding new order.'
			);
		}
		else {

			$flash_data = array(
				'status'  => true,
				'message' => '<strong>Error!</strong> adding new order.'
			);
		}

		$this->session->set_flashdata('user_flash_data', $flash_data);
       	redirect('olstore/orders');	
	}	

	public function products()
	{
		$this->is_login();
		
		$this->data['scripts'] 		= $this->sd->js->get_scripts_view('olstore_datatable');
		$this->data['core_scripts'] = $this->sd->js->core_script();
		
		$this->data['active_tab'] 	= "products";	
		$this->data['products']       = $this->store_m->getAllProducts();

		$user_flash_session 	  	= $this->session->flashdata('user_flash_data');
		$this->data['flashdata']  	= $user_flash_session;		

		$this->load->view('templates/olstore/header',$this->data);
		$this->load->view('olstore/products',$this->data);
		$this->load->view('templates/olstore/footer',$this->data);
	}

	public function addProduct()
	{
		$this->is_login();

		$this->data['css'] 			= $this->sd->css->get_css_view('fancybox');
		$this->data['scripts'] 		= $this->sd->js->get_scripts_view('olstore_datatable');
		$this->data['scripts'] 		= $this->sd->js->get_scripts_view('fancybox');
		$this->data['core_scripts'] = $this->sd->js->core_script();
		$this->data['active_tab'] 	= "products";	
		$this->data['categories']   = $this->store_m->getAllCategories();

		$user_flash_session 	  = $this->session->flashdata('user_flash_data');
		$this->data['flashdata']  = $user_flash_session;					

		$this->load->view('templates/olstore/header',$this->data);
		$this->load->view('olstore/add_product',$this->data);
		$this->load->view('templates/olstore/footer',$this->data);		
	}

	public function editProduct($id)
	{
		$this->is_login();

		$this->data['css'] 			= $this->sd->css->get_css_view('fancybox');
		$this->data['scripts'] 		= $this->sd->js->get_scripts_view('olstore_datatable');
		$this->data['scripts'] 		= $this->sd->js->get_scripts_view('fancybox');
		$this->data['core_scripts'] = $this->sd->js->core_script();
		$this->data['active_tab'] 	= "products";	
		$this->data['product']	    = $this->store_m->getProduct($id);
		$this->data['categories']   = $this->store_m->getAllCategories();
		$this->data['images']   	= $images = $this->store_m->getAllImagesByProduct($id);

		$user_flash_session 	  = $this->session->flashdata('user_flash_data');
		$this->data['flashdata']  = $user_flash_session;					

		$this->load->view('templates/olstore/header',$this->data);
		$this->load->view('olstore/edit_product',$this->data);
		$this->load->view('templates/olstore/footer',$this->data);	
	}	

	public function categories()
	{
		$this->is_login();
	
		$this->data['scripts'] 		= $this->sd->js->get_scripts_view('olstore_datatable');
		$this->data['core_scripts'] = $this->sd->js->core_script();

		$this->data['active_tab'] 	= "products";	
		$this->data['categories']   = $this->store_m->getAllCategories();

		$user_flash_session 	  = $this->session->flashdata('user_flash_data');
		$this->data['flashdata']  = $user_flash_session;		

		$this->load->view('templates/olstore/header',$this->data);
		$this->load->view('olstore/categories',$this->data);
		$this->load->view('templates/olstore/footer',$this->data);		
	}

	public function saveProduct()
	{
		$this->is_login();
		$result = $this->store_m->addProduct();
		if($result) {

			$flash_data = array(
	       			'status'  => true,
	       			'message' => '<strong>Success!</strong> adding new product.'
	       		);			

	       	$this->session->set_flashdata('user_flash_data', $flash_data);	
	       	redirect('olstore/addProduct');				

		} else {

			$flash_data = array(
	       			'status'  => false,
	       			'message' => '<strong>Failed!</strong> adding new product.'
	       		);			

		       	$this->session->set_flashdata('user_flash_data', $flash_data);	
		       	redirect('olstore/addProduct');				

		}

			
	}

	public function updateProduct()
	{
		$this->is_login();
		$result = $this->store_m->updateProduct();
		if($result) {

			$flash_data = array(
	       			'status'  => true,
	       			'message' => '<strong>Success!</strong> update product.'
	       		);			

	       	$this->session->set_flashdata('user_flash_data', $flash_data);	
	       	redirect('olstore/editProduct/' . $_POST['id']);	       	

		} else {

			$flash_data = array(
	       			'status'  => false,
	       			'message' => '<strong>Failed!</strong> update product.'
	       		);		

	       	$this->session->set_flashdata('user_flash_data', $flash_data);	
	       	redirect('olstore/editProduct/' . $_POST['id']);				       			

		}
	
	}

	public function addCategory()
	{
		$result = $this->store_m->addCategory();
		if($result) {

			$flash_data = array(
	       			'status'  => true,
	       			'message' => '<strong>Success!</strong> adding new category.'
	       		);			
			
		} else {

			$flash_data = array(
	       			'status'  => false,
	       			'message' => '<strong>Failed!</strong> adding new category.'
	       		);
			
		}

       	$this->session->set_flashdata('user_flash_data', $flash_data);	
       	redirect('olstore/categories');			
	}

	public function updateCategory()
	{
		$result = $this->store_m->updateCategory();
		if($result) {

			$flash_data = array(
	       			'status'  => true,
	       			'message' => '<strong>Success!</strong> update category.'
	       		);			
			
		} else {

			$flash_data = array(
	       			'status'  => false,
	       			'message' => '<strong>Failed!</strong> update category.'
	       		);
			
		}

       	$this->session->set_flashdata('user_flash_data', $flash_data);	
       	redirect('olstore/categories');			
	}		

	/*
	******
	*/

	public function deleteOrder($id)
	{
		$del = $this->store_m->deleteOrder($id);
		if($del) {

			$flash_data = array(
	       			'status'  => true,
	       			'message' => '<strong>Success!</strong> deleting order.'
	       		);
	       	$this->session->set_flashdata('user_flash_data', $flash_data);	

			redirect('olstore/orders');	
		} else {
			redirect('olstore/orders');
		}
	}	

	public function deleteCategory($id)
	{
		$del = $this->store_m->deleteCategory($id);
		if($del) {
			$flash_data = array(
	       			'status'  => true,
	       			'message' => '<strong>Success!</strong> deleting category.'
	       		);
	       	$this->session->set_flashdata('user_flash_data', $flash_data);	
			redirect('olstore/categories');	
		} else {
			redirect('olstore/categories');
		}
	}


	public function deleteImage($id, $product_id)
	{
		$del = $this->store_m->deleteImage($id);
		if($del) {
			$flash_data = array(
	       			'status'  => true,
	       			'message' => '<strong>Success!</strong> deleting image.'
	       		);
	       	$this->session->set_flashdata('user_flash_data', $flash_data);	
			redirect('olstore/editProduct/' . $product_id );	
		} else {
			$flash_data = array(
	       			'status'  => false,
	       			'message' => '<strong>Failed!</strong> deleting image.'
	       		);
	       	$this->session->set_flashdata('user_flash_data', $flash_data);				
			redirect('olstore/editProduct/' . $product_id);
		}		
	}

	public function addMessage()
	{
		$result = $this->store_m->addMessage();
		if($result) {

			$flash_data = array(
	       			'status'  => true,
	       			'message' => '<strong>Success!</strong> adding new messages.'
	       		);				

		} else {

			$flash_data = array(
	       			'status'  => false,
	       			'message' => '<strong>Success!</strong> update messages.'
	       		);

		}

       	$this->session->set_flashdata('user_flash_data', $flash_data);	
       	redirect('olstore/page');
	}		

	public function archiveProduct()
	{
		$result = $this->store_m->ArchivedProduct();
		if($result) {

			$flash_data = array(
	       			'status'  => true,
	       			'message' => '<strong>Success!</strong> archived product.'
	       		);			
			
		} else {

			$flash_data = array(
	       			'status'  => false,
	       			'message' => '<strong>Failed!</strong> archived product.'
	       		);
			
		}

       	$this->session->set_flashdata('user_flash_data', $flash_data);	
       	redirect('olstore/products');	
	}		

	public function updateStatus()
	{
		$prod_id = $_POST['id'];
		$prod_status = $_POST['status'];
		$this->db->where('id',$prod_id);
		$this->db->set('status',$prod_status);

		if($this->db->update('orders')){
			echo true;
		}
		
	}

	public function getProduct($id = null) {

		$id = $this->input->post('id');
		$query_product = $this->db->where('id', $id)->get('products');
		
		if ($query_product->num_rows() != 0) {
		 
			echo json_encode($query_product->row_array());
		}
	}
	
	public function getProductCount()
	{
		$products = $this->store_m->getAllProducts();
		$corevalues = json_decode(file_get_contents('sd-contents/corefile/products.json'), true);

		$product_info = array('products' => count($products), 'total' => $corevalues['products']);
		echo json_encode($product_info);
	}
}
?>