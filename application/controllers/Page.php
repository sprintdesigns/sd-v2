<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	public $sd;

	function __construct()
	{
		parent::__construct();

		$libraries = new SD_Library();
		$this->sd = $libraries->loadLibraries();
		$this->data['sd'] = $this->sd;
		
	}

	public function removeJSCodes($content) {
		return preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $content);
	}

	public function trimCodes($content) {
		return trim($content);
	}

	public function save()
	{
		$theme_id =  $this->sd->theme->get_active_theme()[0]->id;
		$page = $this->sd->page->page_exist($_POST['pageName'],$theme_id);
		if($page)
		{
		    if (isset($_POST['editable_text'])) {
                $extra_contents = json_decode($_POST['editable_text'], true);
                $this->sd->page->update_editable_text($extra_contents);
            }

			$content = $_POST['content'];
			$content = $this->removeJSCodes($content);
			$content = $this->trimCodes($content);

			$data = array(
				'page_content' => $content , 
			);			
			
			$update_page = $this->sd->page->save_page($page[0]->id,$data);

			if($update_page == 742)
			{
				$response = array('status' => false,'mesasge' => 'Invalid Page ID' );
			}
			else if($update_page)
			{
				$header = array(
					'page_logo' => $_POST['page_logo'], 
					'page_top_right' => $_POST['page_top_right'], 
					'page_bottom_right' => $_POST['page_bottom_right'],
					'site_title' => $_POST['site_title']
				);

				if($this->sd->page->save_header($header))
				{

					$active_theme = $this->sd->theme->get_active_theme();

					$data_theme = array(
						'color' => $_POST['theme_color'], 
					);

					$this->sd->theme->update_theme($active_theme[0]->id,$data_theme);

					
					if(array_key_exists('social_item', $_POST))
					{
						$data_social = array(
							'social' => $_POST['social_item'] 
						);

						$this->sd->social->update_social($active_theme[0]->id,$data_social);

					}
					

					$response = array('status' => true,'message' => 'Page Successfully Updated' );
				}else
				{
					$response = array('status' => false,'message' => 'Something went wrong in saving header' );
				}

				
			}else
			{
				$response = array('status' => false,'message' => 'Something went wrong while updating the page' );
			}


		}else
		{
			$response = array('status' => false,'message' => 'Page not exist' );
		}

		echo json_encode($response);
	}

	public function create_page()
	{
		if(!$this->sd->page->page_exist(preg_replace('/\s+/', '-',strtolower($_POST['title']))))
		{
			$theme_id = $this->sd->theme->get_active_theme()[0]->id;

			$remove[] = "'";
			$remove[] = '"';
			$page_url 	    = str_replace( $remove, "", $_POST['title'] );			

			$data = array(
					'page_title' => $_POST['title'], 
					//'page_url' =>preg_replace('/\s+/', '-',strtolower($_POST['title'])), 
					'page_url' =>preg_replace('/\s+/', '-',strtolower($page_url)), 
					'template_name' => 'fullwidth.php', 
					'meta_description' => '', 
					'page_content' => '', 
					'theme_id' => $theme_id,
					'type' => true, 
					'status' => true, 
				);

			if($id = $this->sd->page->create($data))
			{
				$theme_id = $this->sd->theme->get_active_theme()[0]->id;
				$menu = $this->sd->menu->get_menu($theme_id);
				$nav = json_decode($menu[0]->orders,true);
				array_push($nav,(object) array('id' => $id ));
				$data = array(
					'orders' => json_encode($nav), 
				);
				$theme_id = $this->sd->theme->get_active_theme()[0]->id;
				$menu_response = $this->sd->menu->update_menu($theme_id,$data);
				$this->sd->edit->mode(true);
				$response = array('status' => true, 'message'=> 'page Successfully created','menu_web' => $this->sd->page->nav_bar_web($this->sd),'menu_mobile' => $this->sd->page->nav_bar_mobile($this->sd), 'menu_active' => $this->sd->menu->menu_active($this->sd), 'menu_data_active' => $this->sd->menu->get_menu($theme_id)[0]->orders, 'menu_draft' => $this->sd->menu->menu_draft($this->sd) );
			}else
			{
				$response = array('status' => false, 'message'=> 'Something went wrong' );
			}
			
		}else
		{
			$page = $this->sd->page->page_exist(preg_replace('/\s+/', '-',strtolower($_POST['title'])));
			$count = $page[0]->clone + 1;
			$sub = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);

			$data = array(
				'page_title' => $_POST['title'], 
				'page_url' =>preg_replace('/\s+/', '-',strtolower($_POST['title'])).'-'.$count,
				'template_name' => 'fullwith.php', 
				'meta_description' => '', 
				'page_content' => '', 
				'type' => true, 
				'status' => true, 
			);
			

			if($id = $this->sd->page->create($data))
			{
				$data = array(
					'clone' => $count , 
				);
				
				$update_page = $this->sd->page->all_page_with_title($page[0]->page_title,$data);
				$theme_id = $this->sd->theme->get_active_theme()[0]->id;
				$menu = $this->sd->menu->get_menu($theme_id);
				$nav = json_decode($menu[0]->orders,true);
				array_push($nav,(object) array('id' => $id ));
				$data = array(
					'orders' => json_encode($nav), 
				);
				$theme_id = $this->sd->theme->get_active_theme()[0]->id;
				$menu_response = $this->sd->menu->update_menu($theme_id,$data);
				$this->sd->edit->mode(true);
				$response = array('status' => true, 'message'=> 'page Successfully created','menu_web' => $this->sd->page->nav_bar_web($this->sd),'menu_mobile' => $this->sd->page->nav_bar_mobile($this->sd), 'menu_active' => $this->sd->menu->menu_active($this->sd), 'menu_data_active' => $this->sd->menu->get_menu($theme_id)[0]->orders, 'menu_draft' => $this->sd->menu->menu_draft($this->sd) );
			}else
			{
				$response = array('status' => false, 'message'=> 'Something went wrong' );
			}

		}

		echo json_encode($response);
		//$template = $this->sd->theme->get_template_by_id($_POST['template']);
			
	}

	public function delete()
	{   
		$page = $this->sd->page->select_page_by_id($_POST['page_id']);
		$this->sd->page->delete_page($_POST['page_id']);
		$this->sd->menu->delete_menu_item($page[0]->id,$this->sd);
		$this->sd->edit->mode(true);
		$theme_id = $this->sd->theme->get_active_theme()[0]->id;
		$response = array('status' => true,'message' => "Page successfully deleted",'menu_web' => $this->sd->page->nav_bar_web($this->sd),'menu_mobile' => $this->sd->page->nav_bar_mobile($this->sd),'menu_data' => $this->sd->menu->get_menu($theme_id)[0]->orders,'menu_draft' => $this->sd->menu->menu_draft($this->sd), 'menu_active' => $this->sd->menu->menu_active($this->sd) );

		echo json_encode($response);
	}

	public function save_rename()
	{
		if(preg_replace('/\s+/', '-',strtolower($_POST['new_name'])) != $_POST['old_name'])
		{
			$page = $this->sd->page->page_exist(preg_replace('/\s+/', '-',strtolower($_POST['new_name'])));

			if(!$page)
			{
				$theme_id = $this->sd->theme->get_active_theme()[0]->id;
				$page = $this->sd->page->page_exist($_POST['old_name'],$theme_id);

				$data = array(
					'page_title' => $_POST['new_name'], 
					'page_url' =>preg_replace('/\s+/', '-',strtolower($_POST['new_name'])), 
				);
				
				if($this->sd->page->save_page($page[0]->id,$data))
				{

					$this->sd->edit->mode(true);
					$response = array('status' => true,'message' => "Page successfully renamed",'new_name' => preg_replace('/\s+/', '-',strtolower($_POST['new_name'])),'menu_web' => $this->sd->page->nav_bar_web($this->sd),'menu_mobile' => $this->sd->page->nav_bar_mobile($this->sd) );
				}else
				{
					$response = array('status' => false,'message' => "Error in saving changes" );
				}
				
			}else
			{
				$count = $page[0]->clone + 1;
				$sub = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);

			
				$data = array(
					'page_title' => $_POST['new_name'], 
					'page_url' =>preg_replace('/\s+/', '-',strtolower($_POST['new_name'])).'-'.$count, 
				);
				
				if($this->sd->page->save_page($_POST['page_id'],$data))
				{
					$data = array(
					'clone' => $count , 
					);
					
					$update_page = $this->sd->page->all_page_with_title($page[0]->page_title,$data);

					$this->sd->edit->mode(true);
					$response = array('status' => true,'message' => "Page successfully renamed",'new_name' => preg_replace('/\s+/', '-',strtolower($_POST['new_name'])).'-'.$count,'menu_web' => $this->sd->page->nav_bar_web($this->sd),'menu_mobile' => $this->sd->page->nav_bar_mobile($this->sd) );
				}else
				{
					$response = array('status' => false,'message' => "Error in saving changes" );
				}
			}
		}else
		{
			$theme_id = $this->sd->theme->get_active_theme()[0]->id;
			$page = $this->sd->page->page_exist($_POST['old_name'],$theme_id);
			$data = array(
				'page_title' => $_POST['new_name'], 
				'page_url' =>preg_replace('/\s+/', '-',strtolower($_POST['new_name'])), 
			);
			
			if($this->sd->page->save_page($page[0]->id,$data))
			{

				$this->sd->edit->mode(true);
				$response = array('status' => true,'message' => "Page successfully renamed",'new_name' => preg_replace('/\s+/', '-',strtolower($_POST['new_name'])),'menu_web' => $this->sd->page->nav_bar_web($this->sd),'menu_mobile' => $this->sd->page->nav_bar_mobile($this->sd) );
			}else
			{
				$response = array('status' => false,'message' => "Please make changes first." );
			}
		}

		echo json_encode($response);
	}
	
	
	public function duplicate()
	{
		$page = $this->sd->page->select_page_by_id($_POST['page_id']);
		$count = $page[0]->clone + 1;
		$sub = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);

		$data = array(
				'page_title' => $page[0]->page_title, 
				'page_url' =>preg_replace('/\s+/', '-',strtolower($page[0]->page_title)).'-'.$count, 
				'template_name' => $page[0]->template_name, 
				'meta_description' => $page[0]->meta_description, 
				'page_content' => $page[0]->page_content, 
				'theme_id' => $page[0]->theme_id, 
				'type' => $page[0]->type, 
				'status' => $page[0]->status
			);

		if($id = $this->sd->page->create($data))
		{
			$data = array(
				'clone' => $count , 
			);
			
			$update_page = $this->sd->page->all_page_with_title($page[0]->page_title,$data);

			if($page[0]->type)
			{
				$theme_id = $this->sd->theme->get_active_theme()[0]->id; 
				$menu = $this->sd->menu->get_menu($theme_id);
				$nav = json_decode($menu[0]->orders,true);
				array_push($nav,(object) array('id' => $id ));
				$data = array(
					'orders' => json_encode($nav), 
				);
				
				$menu_response = $this->sd->menu->update_menu($theme_id,$data);
			}
			
			$this->sd->edit->mode(true);
			$response = array('status' => true, 'message'=> 'page Successfully cloned','menu_web' => $this->sd->page->nav_bar_web($this->sd),'menu_mobile' => $this->sd->page->nav_bar_mobile($this->sd), 'menu_active' => $this->sd->menu->menu_active($this->sd), 'menu_data_active' => $this->sd->menu->get_menu($theme_id)[0]->orders,'menu_draft' => $this->sd->menu->menu_draft($this->sd) );
		}else
		{
			$response = array('status' => false, 'message'=> 'Something went wrong' );
		}


		echo json_encode($response);
	} 
}
?>