<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller {

	public $sd;

	function __construct()
	{
		parent::__construct();

		$libraries = new SD_Library();
		$this->sd = $libraries->loadLibraries();
		$this->data['sd'] = $this->sd;
		
	}

	public function do_upload()
   	{
   		if (!empty($_FILES)) {

   			$this->sd->dircheck->dir_init();
   			$this->sd->dircheck->has_uploads_dir_type();
   			$this->sd->dircheck->has_uploads_cur_year();
   			$this->sd->dircheck->has_uploads_curr_month();

   			
   			
   			$date = new DateTime();
        	$date->setTimezone(new DateTimeZone('Asia/Manila'));

	        // $config['upload_path']          = $this->sd->dircheck->get_upload_full_path();
	        // $config['allowed_types']        = 'gif|jpg|png';
	        // $config['encrypt_name'] 		= true;
	        // $config['quality'] = 70;
	        $upload_url = $this->sd->dircheck->get_upload_full_path().'/';

	        foreach ($_FILES as $index => $value)
        	{

        		if ($value['name'] != '')
           		{
           			
           			$file_name = $this->generateRandomString(32);

           			
           			$this->create_thumb($value['tmp_name'],$upload_url,75,$value['name'],$value['size'],$file_name,0.25,0.25);
           			$this->create_thumb($value['tmp_name'],$upload_url,90,$value['name'],$value['size'],$file_name,1,1,true);
           			$upload_image = $this->compress_image($value['tmp_name'],$upload_url,100,$value['name'],$value['size'],$file_name);
           			
                	if (!is_array($upload_image))
	        		{
		                $error = array('error' => "Something whent wrong while uploading");

		                // $this->load->view('user_upload', $error);
		                // var_dump($_FILES);
		                echo json_encode(array("message"=>$error));
			        }
			        else
			        {
		                $data = array('upload_data' => $upload_image);

		                // $this->load->view('user_upload', $data);
		                $img_id = $this->sd->image->save_images(json_encode($data));

		                $full_path_web = $this->sd->image->create_full_path_web($data['upload_data']['full_path'],true);
		                $full_path_thumb = $this->sd->image->create_full_path_thumb($data['upload_data']['full_path'],true);

		                $response = array('full_path_web' => $full_path_web, 'full_path_thumb' => $full_path_thumb, 'img_id' => $img_id );

		                echo json_encode($response);
	        		}

           		}

        	}

	        

    	}
    }

    public function crop()
    {
    	$targ_w = $_GET['w'] ; $targ_h = $_GET['h'];
		$jpeg_quality = 90;

		$src = $_GET['src_img'];

		$img_r = imagecreatefromjpeg($src);

		$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

		imagecopyresampled($dst_r,$img_r,0,0,intval($_GET['x']),intval($_GET['y']), $targ_w,$targ_h, intval($_GET['w']),intval($_GET['h']));

		header('Content-type: image/jpeg');
		imagejpeg($dst_r,null,$jpeg_quality);
    }

    public function crop_test()
    {
    	$targ_w = $_GET['w'] ; $targ_h = $_GET['h'];

		$jpeg_quality = 50;

		$src = $_GET['src_img'];

		$img_r = imagecreatefromjpeg($src);

		$size = getimagesize($src);

		$dst_r = ImageCreateTrueColor( 100,100 );

		imagecopyresampled($dst_r,$img_r,0,0,0,0,100,100, $size[0],$size[1]);

		header('Content-type: image/jpeg');
		imagejpeg($dst_r,null,$jpeg_quality);
    }

    function compress_image($source_url, $destination_url, $quality,$original_name,$file_size,$file_name) {

		$info = getimagesize($source_url);

		
		$extension="";
		// $file_name = $this->generateRandomString(32);

    		if ($info['mime'] == 'image/jpeg'){
        			$image = imagecreatefromjpeg($source_url);
        			$extension="jpg";


    		}elseif ($info['mime'] == 'image/gif'){
        			$image = imagecreatefromgif($source_url);
        			$extension="gif";

   		}elseif ($info['mime'] == 'image/png'){
        			$image = imagecreatefrompng($source_url);
        			$extension="png";
        }
    		imagejpeg($image, $destination_url.$file_name.'.'.$extension, $quality);


    	$data = array(
			'file_name'		=> $file_name.'.'.$extension,
			'file_type'		=> $info['mime'],
			'file_path'		=> $destination_url,
			'full_path'		=> $destination_url.$file_name.'.'.$extension,
			'raw_name'		=> $file_name,
			'orig_name'		=> $original_name,
			'client_name'		=> $original_name,
			'file_ext'		=> $extension,
			'file_size'		=> $file_size,
			'is_image'		=> true,
			'image_width'		=> $info[0],
			'image_height'		=> $info[1],
			'image_type'		=> $extension,
			'image_size_str'	=> $info[3],
		);
		return $data;
	}

	function create_thumb($source_url, $destination_url, $quality,$original_name,$file_size,$file_name,$width_per,$height_per,$global = false) {

		

		$info = getimagesize($source_url);
		$sub_name = "thumb_";

		if(!$global)
		{
			$new_width=floor($info[0] * $width_per);
			$new_height=floor($info[1] * $height_per);
		}else
		{
			$sub_name = "web_";
			if((int) $info[0] > 1024 )
			{
				$new_width = 1024;
				$origin_width = (int) $info[0];
				$percent = floor((($origin_width - 1024) / $origin_width) * 100 );

				$origin_height =(int) $info[1];

				$new_height=$origin_height - floor(($percent / 100)*$origin_height);


			}else
			{
				$new_width=(int) $info[0];
				$new_height=(int) $info[1];
			}

			
		}
		

		$extension="";
		

		if ($info['mime'] == 'image/jpeg'){
    			$image = imagecreatefromjpeg($source_url);
    			$extension="jpg";


		}elseif ($info['mime'] == 'image/gif'){
    			$image = imagecreatefromgif($source_url);
    			$extension="gif";

   		}elseif ($info['mime'] == 'image/png'){
        			$image = imagecreatefrompng($source_url);
        			$extension="png";
        }

        $thumb = imagecreatetruecolor($new_width,$new_height);
        imagealphablending($thumb, false);
        imagecopyresized($thumb, $image, 0, 0, 0, 0, $new_width, $new_height, $info[0], $info[1]);
		imagejpeg($thumb, $destination_url.$sub_name.$file_name.'.'.$extension, $quality);

	}

	function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	function banner_upload()
	{
		if (!empty($_FILES)) {

   			$this->sd->dircheck->dir_init();
   			$this->sd->dircheck->has_uploads_dir_type();
   			$this->sd->dircheck->has_uploads_cur_year();
   			$this->sd->dircheck->has_uploads_curr_month();

   			
   			
   			$date = new DateTime();
        	$date->setTimezone(new DateTimeZone('Asia/Manila'));

	        // $config['upload_path']          = $this->sd->dircheck->get_upload_full_path();
	        // $config['allowed_types']        = 'gif|jpg|png';
	        // $config['encrypt_name'] 		= true;
	        // $config['quality'] = 70;
	        $upload_url = $this->sd->dircheck->get_upload_full_path().'/';

	        foreach ($_FILES as $index => $value)
        	{

        		if ($value['name'] != '')
           		{
           			
           			// $this->load->library('upload', $config);
              //   	$this->upload->initialize($config);
           			
           			$file_name = $this->generateRandomString(32);
           			$this->create_thumb($value['tmp_name'],$upload_url,75,$value['name'],$value['size'],$file_name,0.25,0.25);
           			$this->create_thumb($value['tmp_name'],$upload_url,90,$value['name'],$value['size'],$file_name,1,1,true);
           			$upload_image = $this->compress_image($value['tmp_name'],$upload_url,100,$value['name'],$value['size'],$file_name);
           			
                	if (!is_array($upload_image))
	        		{
		                $error = array('error' => "Something whent wrong while uploading");

		                // $this->load->view('user_upload', $error);
		                // var_dump($_FILES);
		                echo json_encode(array("message"=>$error));
			        }
			        else
			        {
		                $data = array('upload_data' => $upload_image);

		                // $this->load->view('user_upload', $data);
		                $img_id = $this->sd->image->save_images(json_encode($data));

		                $full_path = $this->sd->image->create_full_path_web($data['upload_data']['full_path'],true);

		                $theme_id = $this->sd->theme->get_active_theme()[0]->id;

		               	if($_POST['slide_id_new_image'] == 'none')
		               	{
		               		$data = array(
								'url' => $full_path, 
								'original_url' => $full_path, 
								'text_title' => 'New slide title', 
								'sub_title' => 'New slide sub title', 
								'opacity' => '0', 
								'button_title' => 'learn more', 
								'button_url' => '#', 
								'theme_id' => $theme_id, 
							);

							$new_slide = $this->sd->banner->create_slide($data);

							$menu_slide = $this->sd->banner->banner_menu_slide($new_slide,$this->sd);

							$slide = $this->sd->banner->load_slide($new_slide,$this->sd);

							$response = array('status' => true,'message' => 'New slide successfully created','menu_slide' => $menu_slide, 'slide' => $slide ,'img_id' => $img_id ,'full_path' => $full_path );
		               	}else
		               	{
		               		$data = array(
								'url' => $full_path,
								'original_url' => $full_path, 
							);

							$update_slide = $this->sd->banner->save_slide($_POST['slide_id_new_image'],$data);

							$menu_slide = $this->sd->banner->banner_menu_slide($_POST['slide_id_new_image'],$this->sd);

							$slide = $this->sd->banner->load_slide($_POST['slide_id_new_image'],$this->sd);

							$response = array('status' => true,'message' => 'Change image slide successful','menu_slide' => $menu_slide, 'slide' => $slide ,'img_id' => $img_id ,'full_path' => $full_path );
		               	}
		                

						echo json_encode($response);
	        		}

           		}

        	}

	        

    	}
	}

	public function marketing_upload()
	{
		if (!empty($_FILES)) {

   			$this->sd->dircheck->dir_init();
   			$this->sd->dircheck->has_uploads_dir_type();
   			$this->sd->dircheck->has_uploads_cur_year();
   			$this->sd->dircheck->has_uploads_curr_month();

   			
   			
   			$date = new DateTime();
        	$date->setTimezone(new DateTimeZone('Asia/Manila'));

	        // $config['upload_path']          = $this->sd->dircheck->get_upload_full_path();
	        // $config['allowed_types']        = 'gif|jpg|png';
	        // $config['encrypt_name'] 		= true;
	        // $config['quality'] = 70;
	        $upload_url = $this->sd->dircheck->get_upload_full_path().'/';

	        foreach ($_FILES as $index => $value)
        	{

        		if ($value['name'] != '')
           		{
           			
           			$file_name = $this->generateRandomString(32);

           			
           			$this->create_thumb($value['tmp_name'],$upload_url,75,$value['name'],$value['size'],$file_name,0.25,0.25);
           			$this->create_thumb($value['tmp_name'],$upload_url,90,$value['name'],$value['size'],$file_name,1,1,true);
           			$upload_image = $this->compress_image($value['tmp_name'],$upload_url,100,$value['name'],$value['size'],$file_name);
           			
                	if (!is_array($upload_image))
	        		{
		                $error = array('error' => "Something whent wrong while uploading");

		                // $this->load->view('user_upload', $error);
		                // var_dump($_FILES);
		                echo json_encode(array("message"=>$error));
			        }
			        else
			        {
		                $data = array('upload_data' => $upload_image);

		                // $this->load->view('user_upload', $data);
		                $img_id = $this->sd->image->save_images(json_encode($data));

		                $full_path_web = $this->sd->image->create_full_path_web($data['upload_data']['full_path'],true);
		                $full_path_thumb = $this->sd->image->create_full_path_thumb($data['upload_data']['full_path'],true);

		                $response = array('full_path_web' => $full_path_web, 'full_path_thumb' => $full_path_thumb, 'img_id' => $img_id );

		                echo json_encode($response);
	        		}

           		}

        	}

	        

    	}
	}

}

?>