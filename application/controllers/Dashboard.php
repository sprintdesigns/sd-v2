<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public $sd;

	function __construct()
	{
		parent::__construct();

		$libraries = new SD_Library();
		$this->sd = $libraries->loadLibraries();
		$this->data['sd'] = $this->sd;
		
	}
	
		
	public function check_if_login()
	{
		$user_data = $this->session->userdata('user_data');
		if($user_data == null)
		{
			redirect('sdlogin');
		}
	}

	public function index()
	{
	
		if($this->sd->user->check_if_login())
		{
			if(!$this->sd->theme->check_if_has_theme())
			{
				redirect('settings/themes');
			}

			// end checking template
			$this->data['scripts'] = $this->sd->js->get_scripts_view('dashboard');
			$this->data['core_scripts'] = $this->sd->js->core_script();
			$this->load->view('templates/dashboard/header',$this->data);
			$this->load->view('templates/dashboard/index',$this->data);
			$this->load->view('templates/dashboard/footer',$this->data);

		}else
		{
			redirect('sdlogin');
		}
		
 		
	}
	public function logout()
	{
		$this->check_if_login();

		$this->session->unset_userdata('user_data');
		redirect('sdlogin');
	}
	
}
?>