<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

	public $sd;

	function __construct()
	{
		parent::__construct();

		$libraries = new SD_Library();
		$this->sd = $libraries->loadLibraries();
		$this->data['sd'] = $this->sd;
		
	}

	
	public function index()
	{
		$theme_id = $this->sd->theme->get_active_theme()[0]->id;
		$this->data['menu'] = $this->sd->menu->get_menu($theme_id);
		
		$this->load->view('menu/header',$this->data);
		$this->load->view('menu/index.php',$this->data);
		$this->load->view('menu/footer',$this->data);
	}

	public function save($id = null)
	{
		
		$menu = array(
			'name' => 'default',
			'orders' => $_POST['nav_json']
		);
		if($id == null)
			$this->sd->menu->save_menu($menu);
		else{
			$theme_id = $this->sd->theme->get_active_theme()[0]->id;
			$this->sd->menu->update_menu($theme_id,$menu);
			$menu_draft = json_decode($_POST['nav_json_draft']);
			$menu_active = json_decode($_POST['nav_json']);

			if(!empty($menu_active))
			{
				foreach ($menu_active as $index => $menu) {
					$page = $this->sd->page->select_page_by_id($menu->id);
					$data = array(
						'type' => true, 
					);
					$this->sd->page->save_page($page[0]->id,$data);
				}
			}

			if(!empty($menu_draft))
			{
				foreach ($menu_draft as $index => $menu) {
					$page = $this->sd->page->select_page_by_id($menu->id);
					$data = array(
						'type' => false, 
					);
					$this->sd->page->save_page($page[0]->id,$data);
				}
			}



		}
		$this->sd->edit->mode(true);

		$response = array('status' => true, 'message' => "Menu save successfully",'menu_web' => $this->sd->page->nav_bar_web($this->sd),'menu_mobile' => $this->sd->page->nav_bar_mobile($this->sd) );
		
		echo json_encode($response);
		
	}

	public function menu_active_status()
	{
		$id = $_POST['id'];
		$status = $_POST['status'];
		$data = array(
			'status' => $status, 
		);
		$this->sd->page->save_page($id,$data);

		if($status == 1)
		{
			$class='eye-active';
		}else
		{
			$class="";
		}
		$this->sd->edit->mode(true);
		$response = array('status' => true,'message' => 'nav menu status save successfully','class' => $class,'menu_web' => $this->sd->page->nav_bar_web($this->sd),'menu_mobile' => $this->sd->page->nav_bar_mobile($this->sd) );

		echo json_encode($response);

	}
	
	
	
}
?>