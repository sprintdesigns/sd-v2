<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Advanced extends CI_Controller {

	public $sd;
	public $plugins;

	function __construct()
	{
		parent::__construct();

		$libraries = new SD_Library();
		$this->sd = $libraries->loadLibraries();
		$this->data['sd'] = $this->sd;

		$this->plugins = $this->data['plugins'] = SD_Plugin::getPlugins();
		$this->load->model('changepass_m', 'm');		
	}
	
		
	public function check_if_login()
	{
		if($this->sd->user->check_if_login())
		{
			if(!$this->sd->theme->check_if_has_theme())
			{
				redirect('settings/themes');
			}

		}else
		{
			redirect('sdlogin');
		}
	}

	public function index()
	{
		$this->check_if_login();
		

		// end checking template
		$this->data['scripts'] = $this->sd->js->get_scripts_view('dsahboard');
		$this->data['core_scripts'] = $this->sd->js->core_script();
		$this->data['active_tab'] ="";

		$this->load->view('templates/advanced/header',$this->data);
		$this->load->view('templates/advanced/index',$this->data);
		$this->load->view('templates/advanced/footer',$this->data);
	}

	public function themes() {
		$this->check_if_login();

		$this->data['scripts'] = $this->sd->js->get_scripts_view('plugins');
		$this->data['core_scripts'] = $this->sd->js->core_script();
		$this->data['active_tab'] ="themes";

		$themes = $this->sd->theme->get_all_themes();
		if ($themes) {
			$this->data['themes'] = $themes;
			$active = $this->sd->theme->get_active_theme();

			if ($active) {
				$this->data['active_theme_id'] = $active[0]->id;
			}
	
		}	
		if (isset($_GET['action'])) {
			$action = $_GET['action'];
			if ($action == 'edit') {
				if (isset($_GET['theme_id'])) {
					$theme_id = $_GET['theme_id'];
				}
				$theme = $this->sd->theme->get_theme_by_id($theme_id);
				if ($theme[0]) {
					$this->data['theme'] = $theme[0];
					$this->data['theme_id'] = $theme_id;
				}
			}

			$this->load->view('templates/advanced/header',$this->data);
			$this->load->view("themes/{$action}",$this->data);
			$this->load->view('templates/advanced/footer',$this->data);
		} else {
			$this->load->view('templates/advanced/header',$this->data);
			$this->load->view('themes/index',$this->data);
			$this->load->view('templates/advanced/footer',$this->data);
		}
	}

	public function plugins($plugin_folder = '')
	{
		$this->check_if_login();

		$this->data['scripts'] = $this->sd->js->get_scripts_view('plugins');
		$this->data['core_scripts'] = $this->sd->js->core_script();
		$this->data['active_tab'] ="plugins";

		$param = $this->uri->segment('4');

		if ($param) {
			$this->data['file'] = $file = $param;
		} else {
			$this->data['file'] = $file = 'index.php';
		}

		$this->data['plugin_folder'] = $plugin_folder;
		$this->data['plugin_path'] = $plugin_path = 'sd-includes/plugins/'. $plugin_folder .'/';

		$this->data['is_file_exists'] = false;
		if (file_exists($plugin_path . $file)) {
			$this->data['is_file_exists'] = true;
			$plugin = SD_Plugin::getPlugin($plugin_folder);
			$this->data['plugin'] = $plugin;			
		}

		$this->load->view('templates/advanced/header',$this->data);
		$this->load->view('plugins/index',$this->data);
		$this->load->view('templates/advanced/footer',$this->data);
	}

	public function upload_plugin()
	{

		$target_dir = "sd-includes/plugins/";
		$target_file = $target_dir . basename($_FILES["plugin-file"]["name"]);
		$plugname = basename($_FILES["plugin-file"]["name"]);
		$uploadOk = 1;
		$stop = 0;
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
	
		// Check if file already exists
		$temp_check_file = explode('.', $target_file);
		if (file_exists($temp_check_file[0])) {
		   $stop = true;	
		   $response = array('stop' => $stop, 'status' => false,'message' => 'The Plugin '. str_replace('.zip', '', $plugname) . ' already exists' );
		    $uploadOk = 0;
		}
		// Allow certain file formats
		if($imageFileType != "zip") {
			$stop = true;
		    $response = array('stop' => $stop, 'status' => false,'message' => 'invalid file type must be zip file' );
		    $uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
		// if everything is ok, try to upload file
		} else {
		    if (move_uploaded_file($_FILES["plugin-file"]["tmp_name"], $target_file)) {

		    	$this->sd->unzipfile->setFile($target_file);
		    	$this->sd->unzipfile->setPath($target_dir);
		    	$result = $this->sd->unzipfile->unzipFile('plugin.json');
		    	
		    	$stop = false;
		       	$response = array('stop' => $stop, 'status' => true,'message' => 'Plugin Successfully Added' );
		    } else {
		    	$stop = true;
		        $response = array('stop' => $stop, 'status' => false,'message' => 'Something went wrong, refresh the page and try it again' );
		    }
		}

		echo json_encode($response);
	}
	public function changepassword()
	{
		$this->data['scripts'] = $this->sd->js->get_scripts_view('plugins');
		$this->data['core_scripts'] = $this->sd->js->core_script();
		$this->data['active_tab'] ="changepassword";

		$this->data['profiles'] = $this->m->getProfile();

		$this->load->view('templates/advanced/header',$this->data);
		$this->load->view('changepassword/index',$this->data);
		$this->load->view('templates/advanced/footer',$this->data);
	}

	public function edit($id)
	{
		$this->data['scripts'] = $this->sd->js->get_scripts_view('plugins');
		$this->data['core_scripts'] = $this->sd->js->core_script();
		$this->data['active_tab'] ="changepassword";

		$this->data['profiles'] = $this->m->getProfilebyId($id);
		$this->data['profiles']->password =$this->sd->utilities->decrypt($this->data['profiles']->password);

		$this->load->view('templates/advanced/header',$this->data);
		$this->load->view('changepassword/edit',$this->data);
		$this->load->view('templates/advanced/footer',$this->data);
	}

	public function update()
	{
		$this->data['scripts'] = $this->sd->js->get_scripts_view('plugins');
		$this->data['core_scripts'] = $this->sd->js->core_script();
		$this->data['active_tab'] ="changepassword";

		$userid = $_POST['txt_hidden'];
		$cur_pass = $_POST['txt_password'];
		$newpass = $_POST['txt_newpass'];
		$confnewpass = $_POST['txt_confpassword'];
		$finalpass = $this->sd->utilities->encrypt($newpass);
		$this->data['profiles'] = $this->m->getProfilebyId($userid);
		$this->data['profiles']->password = $this->sd->utilities->decrypt($this->data['profiles']->password);
		if($cur_pass == $this->data['profiles']->password)
		{
			if($newpass == $confnewpass)
			{	
				$result = $this->m->updatepassword($finalpass, $userid);
				if($result){
					$this->session->set_flashdata('success_msg' , 'Password has been changed');
					redirect('advanced/changepassword');
				}
				else{
					$this->session->set_flashdata('error_msg', 'Fail to update Password');
					redirect('advanced/changepassword');
				}
			}
			else
			{
				$this->session->set_flashdata('error_msg' , 'New Password and Confirm Password does not match.');
				redirect('advanced/edit/'.$userid);
			}
		}
		else
		{
			$this->session->set_flashdata('error_msg', 'Current Password does not match.');
			redirect('advanced/edit/'.$userid);
		}
	}

	public function logout()
	{
		$this->check_if_login();

		$this->session->unset_userdata('user_data');
		redirect('sdlogin');
	}	
}
?>