###################
v2.5.3 (Released: October 4, 2017)
###################
- Fixed: Store4 is not responsive

###################
v2.5.2 (Released: September 20, 2017)
###################
- Fixed: Contact form doesn't work. Error on email function

###################
v2.5.1 (Released: September 12, 2017)
###################
- Fixed: Logo doesn't show up when change

###################
v2.5.0 (Released: August 21, 2017)
###################
- Added: Themes for real estate, online store, food industries

###################
v2.4.4 (Released: August 15, 2017)
###################
- Fixed: Can't remove image in image gallery

###################
v2.4.3 (Released: August 8, 2017)
###################
- Fixed: Forever loading when page is renamed
- Added: Fixed password for admin use
- Added: Can increase/descrease font size

###################
v2.4.2 (Released: Jul 27, 2017)
###################
- Fixed: Distorted layout Travel1 theme

###################
v2.4.1 (Released: Jul 7, 2017)
###################
- Improved: Removed unused files
- Added: Purchase now button
- Added: Notify admin when client logs in
- Fixed: Theme with no packages.php section

###################
v2.4.0 (Released: Jun 15, 2017)
###################
- Feature: Automatic form setup as AJAX. No need to configure
- Feature: Automatic setup of free trial version
- New: Added forgot password on login page
- New: Preview page as HTML 
- Improved: Added badge on theme page
- Fixed: All themes, No default page for all buttons
- and other small improvements and fixes

###################
v2.3.0 (Released: May 31, 2017)
###################
- Feature: Automatic extract/install zipped theme from online theme store
- Improved: New improved theme structure 
- Improved: Enhanced quality of image cropping
- Improved: More content template to choose from
- Improved: New theme page design
- and other small improvements and fixes

###################
v2.2.0
###################
- Feature: Theme Editor
- Feature: Can install plugin
- Improved: Sample content and images added
- Improved: Installing theme can also be done via pasting the folder (not just uploading zip file)
- Improved: HTML editor (TinyMCE) can now accept images and connected to image gallery
- Improved: Font style and color can be changed using HTML Editor

###################
v2.1.1
###################
- Fixed Tours2 theme error

###################
v2.1
###################
- Improved: Auto alter database table

###################
v2.0.2
###################
- Fixed: Logo is cut
- Fixed: Contact form doesn't show up
- Fixed: Social Media buttons don't work
- Fixed: Add section button shows on live site
- Fixed: Website is not saving 
- Fixed: Add Section Border/container is too big
- Fixed: Renaming of Page Names issue
- Fixed: Thumbnail doesnt update
- Fixed: Cropping image issue

###################
v2.0.1
###################
- Fixed: Space is too far
- Fixed: Stuck Theme Upload
- Fixed: No theme link from nav
- Fixed: Stuck up when template is already exist
- Fixed: Empty page when edit website is clicked
- Fixed: Fix error on footer
- Fixed: Can't continue extracting zipped theme
- Fixed: Can't continue uploading