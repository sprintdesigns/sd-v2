<?php
	$file = 'corefile/sd.json';
	
	if (file_exists($file)) {
		$str = file_get_contents($file);
		$sd = json_decode($str, true);
	}

	if(!$sd['install'])
	{
?>
<?php
	include 'sd-includes/header.php';
?>
<?php if($_GET['step'] == 0){ ?>
	  <!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="container">
		  <div class="row">
		    <div class="white-box">
			    <p>Welcome to SprintDesignsCMS (sdCMS).</p>
			    <p>Before getting started, we need the following information which your hosting provider should supply you. You may contact them for further information. </p>
			    <ol>
				  <li>Database name</li>
				  <li>Database username</li>
				  <li>Database password</li>
				  <li>Database host</li>
			    </ol>
			    <p class="step"><a href="setup-config.php?step=1" class="button button-large">Got it!</a></p>
		    </div>
		  </div>
		</div>
	</div>
	<!-- END CONTENT -->
<?php } ?>


<?php if($_GET['step'] == 1){ ?>

	<?php
		$file = 'corefile/sd.json';

		if (file_exists($file)) {
			$str = file_get_contents();
			$sd = json_decode($str, true);
		} else {
			$sd = false;
		}

		if(!$sd || !array_key_exists('step1', $sd) || !$sd['step1']) { ?>
			 <!-- BEGIN CONTENT -->
		 	<?php
				if(isset($_POST['submit-step-1']))
				{
					$dbname=$_POST['dbname'];
					$username=$_POST['username'];
					$password=$_POST['password'];
					$dbhost=$_POST['dbhost'];	

					echo "<span id='span-error' style='display: none;' >";
					$conn = new mysqli($dbhost, $username, $password,$dbname);
					echo"</span>";
					
					

					if ($conn->connect_error) { ?>
				<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<div class="container">
					  <div class="row">
					    <div class="white-box">
						    <!--<p></p><h1>Error establishing a database connection</h1>
							<p>This either means that the username and password information in your <code>config.php</code> file is incorrect or we can’t contact the database server at <code>localhost</code>. This could mean your host’s database server is down.</p>
							<ul>
							<li>Are you sure you have the correct username and password?</li>
							<li>Are you sure that you have typed the correct hostname?</li>
							<li>Are you sure that the database server is running?</li>
							</ul>
							<p>If you’re unsure what these terms mean you should probably contact your host. If you still need help you can always visit the <a href="#">sdCMS Tutorials</a>.</p>-->
							<p></p><h1>Oops! You might have typed wrong information.</h1>
							<p>This either means that the username and password information in your <code>config.php</code> file is incorrect or we can’t contact the database server at <code>localhost</code>. This could mean your host’s database server is down.</p>
							<ul>
							<li>Are you sure you have correct username and password?</li>
							<li>Are you sure you have typed the correct hostname?</li>
							<li>Are you sure the database server is working?</li>
							</ul>
							<p>If your answers are yes and still not working, you need to contact your hosting provider.</p>
							<p></p><p class="step"><a href="setup-config.php?step=1" class="button button-large">Try again</a></p>
					    </div>
					  </div>
					</div>
				</div>
				<!-- END CONTENT -->

			<?php
					}else
					{
						$conn->close();
						$sd_json = file_get_contents('corefile/sd.json');
						$sd_json = json_decode($sd_json);
						$sd_json->dbname = ''.$dbname;
						$sd_json->username =''.$username; 
						$sd_json->password = ''.$password; 
						$sd_json->dbhost = ''.$dbhost; 
						$sd_json->step1 = true; 
						
						$jsonData = json_encode($sd_json);
						file_put_contents('corefile/sd.json', $jsonData);
                        echo "<script> location.href='install.php'; </script>";
						// echo "<script> location.href='setup-config.php?step=2'; </script>";
					}
					

					
				}else {
			?>
			<div class="page-content-wrapper">
				<div class="container">
				  <div class="row">
				    <div class="white-box">
					    <form method="post" action="setup-config.php?step=1">
							<p>Please enter your database details bellow. If you are not sure about this, please contact your hosting provider</p>
							<table class="form-table">
								<tbody><tr>
									<th scope="row"><label for="dbname">Database Name</label></th>
									<td><input name="dbname" id="dbname" size="25" placeholder="databasename" type="text" required></td>
									<td>The name of the database you want to use with sdCMS.</td>
								</tr>
								<tr>
									<th scope="row"><label for="uname">Username</label></th>
									<td><input name="username" id="uname" size="25" placeholder="username" type="text" required></td>
									<td>Your database username.</td>
								</tr>
								<tr>
									<th scope="row"><label for="pwd">Password</label></th>
									<td><input name="password" id="pwd" size="25" placeholder="password" autocomplete="off" type="text" ></td>
									<td>Your database password.</td>
								</tr>
								<tr>
									<th scope="row"><label for="dbhost">Database Host</label></th>
									<td><input name="dbhost" id="dbhost" size="25" placeholder="localhost" type="text" required></td>
									<td>Most hosting providers uses <code>localhost</code>”. If it won’t work, contact them.</td>
								</tr>
							    </tbody>
							</table>
						    <input name="language" value="" type="hidden">
							<p class="step"><input name="submit-step-1" value="Submit" class="button button-large" type="submit"></p>
						</form>
				    </div>
				  </div>
				</div>
			</div>
			<!-- END CONTENT -->
			<?php } ?>
		<?php }else{ ?>
			<div class="page-content-wrapper">
				<div class="container">
				  <div class="row">
				    <div class="white-box">
						<p>Oops, You are already done in this step, You may now proceed to the next step.</p>

						<p class="step"><a href="setup-config.php?step=2" class="button button-large">Next</a></p>
				    </div>
				  </div>
				</div>
			</div>
		<?php } ?>
<?php } ?>


<?php if($_GET['step'] == 2){ ?>

	<?php
		$str = file_get_contents('corefile/sd.json');
		$sd = json_decode($str, true);
	?>
	
		<?php if(array_key_exists('step1', $sd) && !array_key_exists('step2', $sd)) { ?>

			<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<div class="container">
				  <div class="row">
				    <div class="white-box">
						<p>Setup success! You’ve made it through this part of the installation. sdCMS can now communicate with your database. If you are ready, let us...</p>

						<form method="post" action="install.php">
							<p class="step"><button class="button button-large" name="submit-step2">Install your sdCMS site</button></p>
						</form>
						
				    </div>
				  </div>
				</div>
			</div>
			<!-- END CONTENT -->
		
		<?php }else{ ?>
			<div class="page-content-wrapper">
				<div class="container">
				  <div class="row">
				    <div class="white-box">
						<p>Oops, You are already done in this step, You may now proceed to the next step.</p>

						<p class="step"><a href="install.php" class="button button-large">Next</a></p>
				    </div>
				  </div>
				</div>
			</div>
		<?php } ?>
	
<?php } ?>


<?php
	include 'sd-includes/footer.php';
?>
<?php }else { 

		$new_request = explode('/', $_SERVER['REQUEST_URI']);
		echo "<script> location.href='http://".$_SERVER['SERVER_NAME'].'/'.$new_request[1]."/sdlogin'; </script>";
	}

?>