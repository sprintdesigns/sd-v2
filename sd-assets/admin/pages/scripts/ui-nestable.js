var UINestable = function () {

    var updateOutput = function (e) {
        var list = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
            console.log('good');
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };




    return {
        //main function to initiate the module
        init: function () {

            // activate Nestable for list 1
            $('#nestable_list_1').nestable({
                group: 1
              
            }) 
                .on('change', updateOutput);

            // activate Nestable for list 2
            $('#nestable_list_2').nestable({
                group: 1
            })
                .on('change', updateOutput);

            // output initial serialised data
            // updateOutput($('#nestable_list_1').data('output', $('#nestable_list_1_output')));
            updateOutput($('#nestable_list_2').data('output', $('#nav_json')));
            updateOutput($('#nestable_list_1').data('output', $('#nav_json_draft')));

            $('#nestable_list_menu').on('click', function (e) {
                var target = $(e.target),
                    action = target.data('action');
                if (action === 'expand-all') {
                    $('.dd').nestable('expandAll');
                }
                if (action === 'collapse-all') {
                    $('.dd').nestable('collapseAll');
                }
            });

            $('#nestable_list_3').nestable();

        }

    };

}();

$(document).ready(function(){
    $(".btn-save-nav-menu").click(function(){
        var url = $("#form-nav-function").attr('data-action');
       $.ajax({ 
            url:url,
            type:'POST',
            data:{nav_json:$("#nav_json").val(),nav_json_draft:$("#nav_json_draft").val()},
            dataType:'json',
            success:function(data)
            {
                if(data.status)
                {
                    location.reload();
                }
            }
       });
     
    });
});