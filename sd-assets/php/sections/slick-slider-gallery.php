  <section class="gallery-slick ui-state-default section-appender">
  <div class="container">
    <div class="row">
        <h1 class="text-only sd-content-title">From a Traveller to a Story teller</h1>
    </div>
    <div class="row">       
      <div class="home-slider">
        <div class="main-slider">
          <div id="slick-slide-1" class="slide sd-image-container with-thumb" data-thumb="slick-slide-nav-1" style="background-image: url('<?php echo base_url(); ?>sd-assets/images/slide01.jpg');">
            <button class="galleryShow cropit"><i class="fa fa-image"></i></button>
          </div>
          <div id="slick-slide-2" class="slide sd-image-container with-thumb" data-thumb="slick-slide-nav-2" style="background-image: url('<?php echo base_url(); ?>sd-assets/images/slide02.jpg');">
            <button class="galleryShow cropit"><i class="fa fa-image"></i></button>
          </div>  
          <div id="slick-slide-3" class="slide sd-image-container with-thumb" data-thumb="slick-slide-nav-3" style="background-image: url('<?php echo base_url(); ?>sd-assets/images/slide03.jpg');">
            <button class="galleryShow cropit"><i class="fa fa-image"></i></button>
          </div>
          <div id="slick-slide-4" class="slide sd-image-container with-thumb" data-thumb="slick-slide-nav-4" style="background-image: url('<?php echo base_url(); ?>sd-assets/images/slide04.jpg');">
            <button class="galleryShow cropit"><i class="fa fa-image"></i></button>
          </div>
          <div id="slick-slide-5" class="slide sd-image-container with-thumb" data-thumb="slick-slide-nav-5" style="background-image: url('<?php echo base_url(); ?>sd-assets/images/slide05.jpg');">
            <button class="galleryShow cropit"><i class="fa fa-image"></i></button>
          </div>
        </div>
        <div class="nav-slider">
          <div id="slick-slide-nav-1" class="slide" style="background-image: url('<?php echo base_url(); ?>sd-assets/images/slide01.jpg');">
            
          </div>
          <div id="slick-slide-nav-2" class="slide" style="background-image: url('<?php echo base_url(); ?>sd-assets/images/slide02.jpg');">
            
          </div>
          <div id="slick-slide-nav-3" class="slide" style="background-image: url('<?php echo base_url(); ?>sd-assets/images/slide03.jpg');">
            
          </div>
          <div id="slick-slide-nav-4" class="slide" style="background-image: url('<?php echo base_url(); ?>sd-assets/images/slide04.jpg');">
            
          </div>
          <div id="slick-slide-nav-5" class="slide" style="background-image: url('<?php echo base_url(); ?>sd-assets/images/slide05.jpg');">
            
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php $sd->column->bar($sd); ?>
</section>

<script>
initSlick();
</script>

