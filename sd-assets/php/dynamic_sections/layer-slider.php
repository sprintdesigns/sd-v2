<div sd-dynamic-function="sd_layer_slider()" class="sd-dynamic-section" id="layerslider" style="width: 100%; height: 400px; margin: 0 auto;">
  <?php if($sliders){ ?>
    <?php foreach ($sliders as $index => $slide) { ?>     
      <div class="ls-slide ls-slide1" data-ls="">
        <img src="<?php echo $slide->url; ?>" class="ls-bg" alt="Slide background">
        <div class="ls-l ls-title" style="top: 96px; left: 35%; white-space: nowrap;" data-ls="">
          <?php echo $slide->text_title; ?>
        </div>
        <div class="ls-l ls-mini-text" style="top: 338px; left: 35%; white-space: nowrap;" data-ls="">
          <?php echo $slide->sub_title; ?>
          <?php if ($slide->button_title != ''):?>
            <a href="<?= $slide->button_url; ?>" class="btn btn-warning" ><?= $slide->button_title; ?></a>
          <?php endif;?>
        </div>
      </div>
    <?php } ?>
  <?php } ?>
</div>

<script>LayersliderInit.initLayerSlider();</script>