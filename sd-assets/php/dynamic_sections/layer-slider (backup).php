   <div sd-dynamic-function="sd_layer_slider()" class="sd-dynamic-section" id="layerslider" style="width: 100%; height: 400px; margin: 0 auto;">
        <div class="ls-slide ls-slide1" data-ls="offsetxin: right; slidedelay: 4000; transition2d: 0,0,0,0;">
            <img src="<?php echo base_url() . 'sd-assets/images/test.jpg';?>" class="ls-bg" alt="Slide background">
            <div class="ls-l ls-title" style="font-size: 48px; top: 96px; left: 20%; white-space: nowrap;" data-ls="
                  fade: true;
			      easingin: easeOutQuint;
			      durationin: 750;
			      scalein: .5;
			      scaleout: .5;
		        ">
                test
            </div>

            <div class="ls-l ls-mini-text" style="top: 208px; left: 50%; white-space: nowrap;" data-ls="
			      fade: true;
			      easingin: easeOutQuint;
			      durationin: 750;
			      delayin: 300;
		        ">
                aa
            </div>

            <div class="ls-l ls-mini-text" style="font-size: 30px; top: 268px; left: 49%; white-space: nowrap;" data-ls="
			      fade: true;
			      easingin: easeOutQuint;
			      durationin: 750;
			      delayin: 300;
		        ">
                <a href="" class="btn-layerslide" >aa</a>
            </div>

        </div>

        <div class="ls-slide ls-slide1" data-ls="offsetxin: right; slidedelay: 4000; transition2d: 24,25,27,28;">
            <img src="<?php echo base_url() . 'sd-assets/images/slide01.jpg';?>" class="ls-bg" alt="Slide background">
            <div class="ls-l ls-title" style="font-size: 48px; top: 96px; left: 50%; white-space: nowrap;" data-ls="
                  fade: true;
			      easingin: easeOutQuint;
			      durationin: 750;
			      scalein: .5;
			      scaleout: .5;
		        ">
                test
            </div>

            <div class="ls-l ls-mini-text" style="top: 208px; left: 50%; white-space: nowrap;" data-ls="
			      fade: true;
			      easingin: easeOutQuint;
			      durationin: 750;
			      delayin: 300;
		        ">
                aa
            </div>

            <div class="ls-l ls-mini-text" style="font-size: 30px; top: 268px; left: 49%; white-space: nowrap;" data-ls="
			      fade: true;
			      easingin: easeOutQuint;
			      durationin: 750;
			      delayin: 300;
		        ">
                <a href="" class="btn-layerslide" >aa</a>
            </div>

        </div>

        <div class="ls-slide ls-slide1" data-ls="offsetxin: right; slidedelay: 4000; transition2d: 24,25,27,28;">
            <img src="<?php echo base_url() . 'sd-assets/images/slide03.jpg';?>" class="ls-bg" alt="Slide background">
            <div class="ls-l ls-title" style="font-size: 48px; top: 96px; left: 50%; white-space: nowrap;" data-ls="
                  fade: true;
			      easingin: easeOutQuint;
			      durationin: 750;
			      scalein: .5;
			      scaleout: .5;
		        ">
                test
            </div>

            <div class="ls-l ls-mini-text" style="top: 208px; left: 50%; white-space: nowrap;" data-ls="
			      fade: true;
			      easingin: easeOutQuint;
			      durationin: 750;
			      delayin: 300;
		        ">
                aa
            </div>

            <div class="ls-l ls-mini-text" style="font-size: 30px; top: 268px; left: 49%; white-space: nowrap;" data-ls="
			      fade: true;
			      easingin: easeOutQuint;
			      durationin: 750;
			      delayin: 300;
		        ">
                <a href="" class="btn-layerslide" >aa</a>
            </div>
        </div>        
    </div>