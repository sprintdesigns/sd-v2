<div class="soc-icons sd-global-soc-icons">
  <?php if($sd->edit->mode()) { ?>
    <div class="social-icons-container add-icon-container">
      <button class="closesocialmodal sm-close"><i class="icon-close"></i></button>
      <div class="social-icons add-icons-list">
        <ul class="list-unstyled list-social">
        <li><a href="javascript:addSocial('fa-facebook');" class="fa fa-facebook"></a></li>
        <li><a href="javascript:addSocial('fa-google-plus');" class="fa fa-google-plus"></a></li>
        <li><a href="javascript:addSocial('fa-twitter');" class="fa fa-twitter"></a></li>
        <li><a href="javascript:addSocial('fa-youtube');" class="fa fa-youtube"></a></li> 
        <li><a href="javascript:addSocial('fa-linkedin');" class="fa fa-linkedin"></a></li>
        <li><a href="javascript:addSocial('fa-instagram');" class="fa fa-instagram"></a></li> 
        <li><a href="javascript:addSocial('fa-vimeo');" class="fa fa-vimeo"></a></li>
        <li><a href="javascript:addSocial('fa-pinterest');" class="fa fa-pinterest"></a></li> 
        </ul>
      </div>  
    </div>
    <div class="social-icons-container url-icon-container">
      <form class="soc-url">
        <fieldset>
        <div class="form-group">
          <label for="url-text">Input Url:</label>
          <label class="onleft">Ex: <span class="changeable">http://facebook.com/john-doe</span></label>
          <input type="url" id="url-text" class="form-control" placeholder="Facebook URL">
          <input type="hidden" id="url-index" class="form-control" >
        </div>
          <button type="button" class="btn btn-primary save-edit-url">OK</button>
          <button type="button" class="btn btn-danger closesocialmodal">Cancel</button>
        </fieldset>
      </form>
    </div>
  <?php } ?>
  <ul class="social-container list-unstyled list-social">
    <?php if($sd->edit->mode()) { ?>
    <?php $social = $sd->social->get_social($sd); ?>

    <input type="hidden" id="sd-social" name="sd-social" value='<?= $social[0]->social ?>' >

      <?php  if($social){ ?>
        <?php foreach (json_decode($social[0]->social) as $index => $value) { ?>
          <li>
            <div class="soc-pointer"></div>
            <button class="btn btn-danger btn-delete-icon-func hidden" data-index="<?= $index ?>" style="position: absolute; top: 0px; right: 0px; width: 0px; height: 22px; padding: 0px 14px 3px 6px;" data-use="false">×</button>
            <a href="javascript:void(0);" class="icon-edit fa <?= $value[0] ?>"></a>
          </li>
      <?php } ?>

      
        <li <?= sizeof(json_decode($social[0]->social)) >= 8? 'class="hidden"':''; ?> >
            <div class="soc-pointer"></div>
            <a href="javascript:void(0);"  class="add-icon fa fa-plus"></a>
          </li>
    
    <?php }else { ?>
        <li>
          <div class="soc-pointer"></div>
          <a href="javascript:void(0);"  class="add-icon fa fa-plus"></a>
        </li>
    <?php } ?>
    <?php } else { ?>
      <?php $social = $sd->social->get_social($sd);?>
      <?php  if($social){ ?>

        <?php foreach (json_decode($social[0]->social) as $index => $value) { ?>
          <li><a target="_blank" href="<?= $value[1] ?>" class="fa <?= $value[0] ?>"></a></li>
      <?php } ?>
    <?php } ?>
  
    <?php } ?>  
  </ul> 
</div>