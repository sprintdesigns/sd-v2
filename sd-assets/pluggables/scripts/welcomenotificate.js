var UINotific8User = function () {

    return {
        //main function to initiate the module
        init: function () {

            
                    setTimeout(function(){ 
                        var settings = {
                                theme: 'teal',
                                sticky: false,
                                horizontalEdge: 'top',
                                verticalEdge: 'right'
                            },
                            $button = $(this);
                            settings.heading = $.trim('Welcome Avatar');
                        
                        if (!settings.sticky) {
                            settings.life = 15000;
                        }

                        $.notific8('zindex', 11500);
                        $.notific8($.trim('You are logged in as Admin'), settings);
                        
                        // $button.attr('disabled', 'disabled');
                        
                        // setTimeout(function() {
                        //     $button.removeAttr('disabled');
                        // }, 1000);

                    }, 1500);

        }

    };

}();