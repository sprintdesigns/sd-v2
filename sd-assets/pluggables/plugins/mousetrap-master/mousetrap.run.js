Mousetrap.bind('ctrl s', function() {
	$('.edit-menu-left').addClass('show');
	$('.layer-3.all-layer').removeClass('open-panel'); 
	$('.layer-2.all-layer').removeClass('open-panel'); 
    $('.layer-4.all-layer').addClass('open-panel');
});

Mousetrap.bind('ctrl p a', function() {
	$('.edit-menu-left').addClass('show');
	$('.layer-3.all-layer').removeClass('open-panel'); 
	$('.layer-4.all-layer').removeClass('open-panel'); 
    $('.layer-2.all-layer').addClass('open-panel'); 
    $('.add-page-item').show();
    setTimeout(function(){ $('#new-page-title').focus(); }, 100); 
    setTimeout(function(){
      $("#nestable_list_2").parent().animate({ scrollTop: $('#nestable_list_2').parent().prop("scrollHeight")}, 1000);
    }, 300);   
});

Mousetrap.bind('ctrl i', function() {
	$('.edit-menu-left').addClass('show');
	$('.layer-2.all-layer').removeClass('open-panel'); 
	$('.layer-4.all-layer').removeClass('open-panel'); 
    $('.layer-3.all-layer').addClass('open-panel');
});
