var tutstarget; 
$(document).ready(function(){
  $('.sd-tutorials').tooltip();	

  $('.sd-tutorials').click(function(){
    if($('.overlay-tutorials').hasClass('run')) {
      closeTutorials();
      resetTuts();
      resetHole();
    }
    else {
      tutstarget = $(this).attr('data-target');
  	  openTutorials();
    }
  });	

  $('.closetuts').click(function(){
  	closeTutorials();
  });
 
  $('.btn-guide').click(function(){
  	if($('.first-cover').hasClass('show')){
  	  $('.first-cover').removeClass('show');	
  	}
    if($('.first-cover-2').hasClass('show')){
      $('.first-cover-2').removeClass('show');  
    }
  	resetTuts();
  	resetHole();
  	tutorialtarget = $(this).attr('data-target');
  	$('.walkthrough-' + tutorialtarget).addClass('showtuts');
  	$('.hole').addClass('show-' + tutorialtarget);
  });
});

function closeTutorials() {
  $('.overlay-tutorials').fadeOut("fast");
  resetTuts();
  resetHole();
  setTimeout(function(){
  	$('.overlay-tutorials').removeClass('run');
  	$('body').removeClass('noscroll');
  },300);
  $('.first-cover').removeClass('show');  
  $('.first-cover-2').removeClass('show');  
  $('.sd-tutorials > i.fa').addClass('hidden');
  $('.sd-tutorials > img').removeClass('hidden');
}

function openTutorials() {
  $("html, body").animate({"scrollTop": "0px"}, 300);
  $('body').addClass('noscroll');
  $('.overlay-tutorials').fadeIn().addClass('run');
  if(tutstarget=='first-load') {
    $('.first-cover.first').addClass('show');
  }
  else if(tutstarget=='page-management') {
    $('.first-cover-2.page-management-cover').addClass('show');
    $(".hole").addClass("show-12");
  }
  $('.sd-tutorials > i.fa').removeClass('hidden');
  $('.sd-tutorials > img').addClass('hidden');
}

function resetTuts() {
  $('.overlay-tutorials > div').removeClass('showtuts');
}

function resetHole(){
 for(x=1; x <= 14; x++) {	
  $('.hole').removeClass('show-' + x);
 } 	
}