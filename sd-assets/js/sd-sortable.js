var first_load = true;
var UINestable = function () {

    var updateOutput = function (e) {
        var list = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
        	if(output != undefined)
        	{
                var arr = list.nestable('serialize');
                arr.splice(-1,1);
                
	            output.val(window.JSON.stringify(arr)); //, null, 2));
				if(!first_load)
	            	$(".btn-save-nav-menu").show();

	            first_load = false;
        	}
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };

    var updateDraftOutput = function (e) {
        var list = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
        	if(output != undefined)
        	{
           		output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
			}
			
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };




    return {
        //main function to initiate the module
        init: function () {

            // activate Nestable for list 1
            $('#nestable_list_1').nestable({
                group: 1,
                maxDepth:1
              
            })
                .on('change', updateDraftOutput);

            // activate Nestable for list 2
            $('#nestable_list_2').nestable({
                group: 1,
                maxDepth:2
            })
                .on('change', updateOutput);

            // output initial serialised data
            // updateOutput($('#nestable_list_1').data('output', $('#nestable_list_1_output')));
            updateOutput($('#nestable_list_2').data('output', $('#nav_json')));
            updateDraftOutput($('#nestable_list_1').data('output', $('#nav_json_draft')));

            $('#nestable_list_menu').on('click', function (e) {
                var target = $(e.target),
                    action = target.data('action');
                if (action === 'expand-all') {
                    $('.dd').nestable('expandAll');
                }
                if (action === 'collapse-all') {
                    $('.dd').nestable('collapseAll');
                }
            });

            $('#nestable_list_3').nestable();

        }

    };

}();

$(document).ready(function(){
	$(".btn-save-nav-menu").hide();
    $(".btn-save-nav-menu").click(function(){
    	 page_manager_response('loading','please wait . . .');
    	$(".btn-save-nav-menu").hide();
        var url = $("#form-nav-function").attr('data-action');
      
       $.ajax({ 
            url:url,
            type:'POST',
            data:{nav_json:$("#nav_json").val(),nav_json_draft:$("#nav_json_draft").val()},
            dataType:'json',
            success:function(data)
            {
                page_manager_response(data.status,data.message);

                if(data.status)
                {
                	$("#nav_bar_web").html(data.menu_web);
                    $("#nav_bar_mobile").html(data.menu_mobile);
                	menu_init();
                }
            }
       });
     
    });

    $(".btn-add-page").click(function(){
        $('.add-page-item').show();
    });

   

   menu_func_init();


});




function delete_page(page_id)
{
    var response = confirm('Are you sure you want to delete this page?');

    if(response)
    {
        
       
        page_manager_response('loading','please wait . . .');

        $.ajax({
            type:'POST',
            url:base_url+'page/delete',
            data:{page_id:page_id},
            dataType:'json',
            success:function(data)
            {
                page_manager_response(data.status,data.message);

                if(data.status)
                {
                     $( "li[data-id='"+page_id+"']" ).remove()
                    $("#nav_json").val(data.menu_data);
                    $("#nestable_list_2").html(data.menu_active);
                    $("#nav_bar_web").html(data.menu_web);
                    $("#nav_bar_mobile").html(data.menu_mobile);
                    $("#nestable_list_1").html(data.menu_draft);
                    menu_init();
                    menu_func_init();
                }else
                {
                    alert(data.message);
                }
            }
        });
    }
}


function page_manager_response(status,message)
{
	if(status == 'loading')
	{
  //       var div = $('<div class="alert alert-warning"><strong>Loading!</strong> '+message+'.</div>').fadeOut(5000);
		// $("#page-manager-response").html(div);
  //       window.setTimeout(function() {
  //         $("#page-manager-response").find('div').fadeTo(500, 0).slideUp(500, function(){
  //             $(this).remove();
  //         });
  //       }, 5000);

    $("#fullpageloader-response").addClass('show');
    $('body').addClass('noscroll');
    var div = $('<div class="full-page-reloader"><p class="big center" >'+message+'</p></div>');
    $("#fullpageloader-response").append(div);

	}else if(status == true)
	{
        $("#fullpageloader-response").removeClass('show');
        $('body').removeClass('noscroll');
        $("#fullpageloader-response").find('.full-page-reloader').remove();
        var div = $('<div class="alert alert-success"><strong>Success!</strong> '+message+'.</div>').fadeOut(5000);
		$("#page-manager-response").html(div);
         window.setTimeout(function() {
          $("#page-manager-response").find('div').fadeTo(500, 0).slideUp(500, function(){
              $(this).remove();
          });
        }, 5000);
	}else
	{
        $("#fullpageloader-response").removeClass('show');
        $('body').removeClass('noscroll');
        $("#fullpageloader-response").find('.full-page-reloader').remove();
        var div = $('<div class="alert alert-danger"><strong>Error!</strong> '+message+'.</div>').fadeOut(5000);
		$("#page-manager-response").html(div);
         window.setTimeout(function() {
          $("#page-manager-response").find('div').fadeTo(500, 0).slideUp(500, function(){
              $(this).remove();
          });
        }, 5000);
	}
}

function duplicate_page(page_id)
{
    page_manager_response('loading','please wait . . .');
    $.ajax({ 
        url:base_url+'page/duplicate',
        type:'POST',
        data:{page_id:page_id},
        dataType:'json',
        success:function(data)
        {
            page_manager_response(data.status,data.message);

            if(data.status)
            {
                $("#nav_json").val(data.menu_data_active);
                $("#nestable_list_2").html(data.menu_active);
                $("#nestable_list_1").html(data.menu_draft);
                $("#nav_bar_web").html(data.menu_web);
                $("#nav_bar_mobile").html(data.menu_mobile);
                menu_func_init();
                menu_init();
            }
        }
   });
}

function menu_init()
{
	$('#bs-example-navbar-no-collapse ul.nav li.dropdown').hover(function() {
	  $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(200);
	}, function() {
	  $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(200);
	});
}


function menu_func_init()
{
    $('.add-page-item').hide();

    $(".ddeye-handle").click(function(){
        page_manager_response('loading','please wait . . .');
        var id = $(this).find('.nav-id').val();
        var eye = $(this);
        if($(this).hasClass('eye-active'))
        {
            
            var status = 0;
        }else
        {
            var status = 1;

        }

        $.ajax({ 
            url:base_url+'menu/menu_active_status',
            type:'POST',
            data:{id:id,status,status},
            dataType:'json',
            success:function(data)
            {
                page_manager_response(data.status,data.message);

                if(data.status)
                {
                    if(data.class == '')
                    {
                        eye.removeClass('eye-active');
                    }
                    eye.addClass(data.class);

                    $("#nav_bar_web").html(data.menu_web);
                    $("#nav_bar_mobile").html(data.menu_mobile);
                    menu_init();
                    
                }
            }
       });

    });


    //hover menu
    $(".dd-content").hover(function(){
        $(this).next().addClass('menu-show');
    },function(){
        $(this).next().removeClass('menu-show');
    });

    $(".menu-button-holder").hover(function(){
        $(this).addClass('menu-show');
    },function(){
        $(this).removeClass('menu-show');
    });

    //rename page
    $(".rename-page").click(function(){
        var page = $(this).attr('data-page');
        $(".page-"+page+' > a').hide();
        $(".page-"+page+' > input').removeClass('hidden');
        $(this).parent().addClass('show-fixed');
        $(this).parent().find(".sd-page-func").hide();
        $(this).parent().find(".sd-page-func-edit").removeClass('hidden');
    });

    $(".cancel-page-rename").click(function(){
        var page = $(this).attr('data-page');
        $(".page-"+page+' > a').show();
        $(".page-"+page+' > input').addClass('hidden');
        $(this).parent().removeClass('show-fixed');
        $(this).parent().find(".sd-page-func").show();
        $(this).parent().find(".sd-page-func-edit").addClass('hidden');
    });

    $(".save-page-rename").click(function(){
        page_manager_response('loading','please wait . . .');
        var old_name = $(this).attr('data-page');
        var page_id = $(this).attr('data-page-id');
        var new_name = $(".page-"+old_name+' > input').val();
        var trim_new_name = new_name.trim();
        var btn = $(this);

        if(new_name == null || trim_new_name == '') {
            page_manager_response(false,'Title must not be empty');
            return false;
        }
        
        $.ajax({ 
            url:base_url+'page/save_rename',
            type:'POST',
            data:{old_name:old_name,new_name:new_name,page_id:page_id},
            dataType:'json',
            success:function(data)
            {
                page_manager_response(data.status,data.message);

                if(data.status)
                {
                    $( "button[data-page='"+old_name+"']" ).attr('data-page',data.new_name);
                    $(".page-"+old_name+' > a').html(new_name);
                    $(".page-"+old_name+' > a').attr('href',base_url+'edit?page='+data.new_name);
                    $(".page-"+old_name+' > input').html(new_name)
                    $(".page-"+old_name+' > a').show();
                    $(".page-"+old_name+' > input').addClass('hidden');
                    btn.parent().removeClass('show-fixed');
                    btn.parent().find(".sd-page-func").show();
                    btn.parent().find(".sd-page-func-edit").addClass('hidden');
                    var div = $(".page-"+old_name);
                    div.removeClass("page-"+old_name);
                    div.addClass("page-"+data.new_name);

                    var menu_live = $.parseJSON($("#nav_json").val());
                    var menu_draft = $.parseJSON($("#nav_json_draft").val());
     //             for(var menu in menu_live) {
                    //    console.log(menu_live[menu]);
                    // }
                    console.log(menu_live);
                    

                    $("#nav_bar_web").html(data.menu_web);
                    $("#nav_bar_mobile").html(data.menu_mobile);
                    menu_init();
                    
                }
            }
       });
    });

    //add page
    $('.btn-cancel-new-page').click(function(){
        $('.add-page-item').hide();
    });


     $(".btn-save-new-page").click(function(){

        var action_url = $(this).attr("data-action");

        var title = $("#new-page-title").val();
        var trim_title = title.trim();

        if(title != null && trim_title != '')
        {
           
           
            page_manager_response('loading','please wait . . .');
            $.ajax({
                type:'POST',
                url:action_url,
                data:{title:title},
                dataType:'json',
                success:function(data)
                {
                    page_manager_response(data.status,data.message);

                    if(data.status)
                    {
                        $("#nav_json").val(data.menu_data_active);
                        $("#nestable_list_2").html(data.menu_active);
                        $("#nestable_list_1").html(data.menu_draft);
                        $("#nav_bar_web").html(data.menu_web);
                        $("#nav_bar_mobile").html(data.menu_mobile);
                        menu_func_init();
                        menu_init();
                    }
                }
            });
           
        }else
        {
            page_manager_response(false,'the title is empty');
        }

        return false;
    });


    var btn = document.getElementsByClassName('sd-copy-url');
    var clipboard = new Clipboard(btn);

    clipboard.on('success', function(e) {
        page_manager_response(true,'Url is now copied on to clipboard');
    });

    // clipboard.on('error', function(e) {
        
    // });

}

