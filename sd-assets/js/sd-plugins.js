$(document).ready(function(){
	
	$(".default-plugin").click(function(){
		var action = $(this).attr('data-action-url');
		var plugin_item_container = $(this);
		preloader(true);

		$.ajax({
			type:'post',
			url:base_url+action,
			success:function(data)
			{
				preloader(false);
				$(".plugins-item").removeClass('active');
				plugin_item_container.parent().addClass("active");
				$(".plugin-settings-container").html(data);
			}
		});
	});
});