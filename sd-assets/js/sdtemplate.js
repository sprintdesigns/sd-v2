$(document).ready(function(){
	$("#template-file").change(function(){
		message_response('clean','');
		var ext = $('#template-file').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['zip']) == -1) {
		    message_response(false,'Invalid File Type');
		}else
		{
			$("#install-theme-submit").removeAttr('disabled');
		}
	});

	$("#upload-template-form").submit(function(){
        $('.installing-overlay.uploading-overlay').addClass('installing');
		var data = new FormData(document.getElementById('upload-template-form'));
        $('#install-theme-submit').attr('disabled', 'disabled');
		$.ajax({
			xhr: function() {
		        var xhr = new window.XMLHttpRequest();
		        // Upload progress
		        xhr.upload.addEventListener("progress", function(evt){
		            if (evt.lengthComputable) {
		                var percentComplete = evt.loaded / evt.total;
		                //Do something with upload progress
		                console.log(percentComplete);
		                $('.themeuploadprogress').css('width', Math.floor((percentComplete * 100)) + '%');
		                $('.numericx').html(Math.floor((percentComplete * 100)));
		            }
		       }, false);
		       // Download progress
		       // xhr.addEventListener("progress", function(evt){
		       //     if (evt.lengthComputable) {
		       //         var percentComplete = evt.loaded / evt.total;
		       //         // Do something with download progress
		       //         console.log(percentComplete);
		       //     }
		       // }, false);
		       return xhr;
		    },
			type:'POST',
			url:base_url+'sdtheme/upload_theme',
			data:data,
			processData: false,
 			contentType: false,
 			dataType:'json',
 			success:function(data)
 			{
 				if (data.status == false) {
 					$('.installing-overlay.uploading-overlay').removeClass('installing');
 					$('.themeuploadprogress').removeAttr('style');
 				}
 				 message_response(data.status,data.message);
 				 $("#install-theme-submit").removeAttr('disabled');
                 if(data.status) {
 				   $('.notificates > h1').html('Theme Successfully Installed!');
 				   $('.notificates > p').html('Please wait a little more, we are readying the theme for you. In <span class="counterx"></span>.');
 				   $('.progress').remove();
 				   var counter = 4;
					var interval = setInterval(function() {
					    counter--;
					    $('.counterx').html(counter);
					    // Display 'counter' wherever you want to display it.
					    if (counter == 0) {
					        // Display a login box
					        clearInterval(interval);
					    }
					}, 1000);
 				   setTimeout(function(){
 				 	
						location.href=base_url+"settings/themes/pick";
					
 				   }, 3100);
 			     }
 				 
 			}
		});

		return false;
	});

	
});

function message_response(status,message)
{
	if(status == 'clean')
	{
		$("#message-response").html('');
	}else
	{
		if(status)
			$("#message-response").html('<div class="alert alert-success"><strong>Success!</strong> '+message+'.</div>');
		else
			$("#message-response").html('<div class="alert alert-danger"><strong>Opps!</strong> '+message+'.</div>');
	}
	
}