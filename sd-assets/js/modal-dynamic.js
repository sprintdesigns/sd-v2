//activate theme
function activateTheme()
{
	var id = $(".dynamic-active").attr("data-object-id");

	if ($.isNumeric(id)) {
		location.href=base_url+"sdtheme/activate_theme/"+id;
	} else {
		location.href=base_url+"sdtheme/install_and_activate_theme/"+id;
	}
}

$(document).ready(function(){
	// this is for slider animation removed

	$('#theme-slides').carousel({
		pause: true,
		interval: false
	}); 

	$('#theme-slides').on('slid.bs.carousel', function () {
	  console.log($('#theme-slides .active').attr('data-object-id'));
	  var active_theme = $('.activated-theme');
	  $(".dynamic-item").removeClass('dynamic-active');
	  $('#theme-slides .active').addClass('dynamic-active');
	  if(active_theme.hasClass('active')) {
        $('#modaldynamic .modal-footer .btn-success').html('Activated');
        $('#modaldynamic .modal-footer .btn-success').addClass('disabled');   
	  }
	  else {
	    $('#modaldynamic .modal-footer .btn-success').html('Activate');
        $('#modaldynamic .modal-footer .btn-success').removeClass('disabled'); 	
	  }
	});

	$(".modal-dynamic").click(function(){

		var id = $(this).attr('data-to-pass');

		$(".dynamic-item").removeClass('active');
		$(".dynamic-item").removeClass('dynamic-active');

		$(".dynamic-item-"+id).addClass('active');
		$(".dynamic-item-"+id).addClass('dynamic-active');
	});
});

$('#modaldynamic').on('shown.bs.modal', function() {
	$("body.modal-open").removeAttr("style");
	$(this).removeAttr("style");
	$( this ).css( "display", "block" );
});

$('#modaldynamic').on('hide.bs.modal', function() {
	$( this ).removeClass( "fade" );
});