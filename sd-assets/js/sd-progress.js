if (document.cookie=='') {
console.log('nocookie');  

$(window).load(function(){
  $(".se-pre-con").fadeOut("slow");
  setTimeout(function(){
    $(".se-pre-con").remove();
  }, 300);

});
  console.log('withcookie');    
  $(".se-pre-con").remove();
  document.onreadystatechange = function(e)
  {
    if(document.readyState=="interactive")
    {
      var script = document.getElementsByTagName("SCRIPT");
      var link = document.getElementsByTagName("LINK");
      var img = document.getElementsByTagName("IMG");
      var div = document.getElementsByTagName("DIV");
      var all = script.length + link.length + img.length + div.length;
      for (var i=0, max=all; i < max; i++) 
      {
        set_ele(all[i]);
      }
    }
  }

  function check_element(ele)
  {
    var script = document.getElementsByTagName("SCRIPT");
      var link = document.getElementsByTagName("LINK");
      var img = document.getElementsByTagName("IMG");
      var div = document.getElementsByTagName("DIV");
      var all = script.length + link.length + img.length + div.length;
    var totalele=all;
    var per_inc=100/all;

    if($(ele).on())
    {
      var prog_width=per_inc+Number(document.getElementById("progress_width").value);
      document.getElementById("progress_width").value=prog_width;
      $("#bar1").animate({width:prog_width+"%"},10,function(){
        $('#numprog').html(Math.round(prog_width));
        if(document.getElementById("bar1").style.width=="100%" || prog_width >= 100)
        {
          $('body').removeClass('noscroll');
          $(".sd-loadbar").fadeOut("slow");
          setTimeout(function(){
            $(".sd-loadbar").remove();
          }, 300);
        }     
      });
    }

    else  
    {
      set_ele(ele);
    }
  }

  function set_ele(set_element)
  {
    check_element(set_element);
  }
  document.cookie="firstload=yes;expires=Wed, 18 Dec 2023 12:00:00 GMT";
  $('body').removeClass('noscroll');
}
else {
  $('body').removeClass('noscroll');
  $(".sd-loadbar").remove(); 
  $(".se-pre-con").remove();
}
