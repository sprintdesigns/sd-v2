$(document).ready(function(){
	
	$('.icon-edit').click(function(){
	  resetIcon();
	  $(this).parent().addClass('active');	
	  $('.url-icon-container').addClass('show');
	  var index = $(this).parent().find('.btn-delete-icon-func').attr('data-index');
	  var social = JSON.parse($("#sd-social").val());
	  $("#url-text").val(social[index][1]);
	  $("#url-index").val(index);
	});
	$('.add-icon').click(function(){
	  resetIcon();
	  $(this).parent().addClass('active');	
	  $('.add-icon-container').addClass('show');
	  var index = $(this).parent().find('.btn-delete-icon-func').attr('data-index');
	  var social = JSON.parse($("#sd-social").val());
	  $("#url-text").val(social[index][1]);
	  $("#url-index").val(index);

	});
	$('.closesocialmodal').click(function(){
	  resetIcon();
	});
	$('.btn-delete-icon-func').click(function(){
	 $(this).parent().find('.icon-edit').addClass('shrink');
	 var index = $(this).attr('data-index');
	 var x = $(this);
	 setTimeout(function(){
	   x.parent().remove();	
	   delete_social_item(index);
	   resetIcon();
	   $(".sd-publish").show();
	 },300); 		
	});

	$(".save-edit-url").click(function(){
		var url = $("#url-text").val();
		var index = $("#url-index").val();

		var social = JSON.parse($("#sd-social").val());

		social[index][1] = url;

		$("#sd-social").val(JSON.stringify(social));
		resetIcon();
		$(".sd-publish").show();


	});
});	

function addSocial(social_class)
{
	$(".sd-publish").show();
	var social = JSON.parse($("#sd-social").val());
	// console.log(social);
	var li = $('<li></li>');
	var div = $('<div class="soc-pointer"></div>');
	var btn = $('<button class="btn btn-danger btn-delete-icon-func hidden" data-index="'+social.length+'" style="position: absolute; top: 0px; right: 0px; width: 0px; height: 22px; padding: 0px 14px 3px 6px;" data-use="false">×</button>');
	btn.click(function(){
	 $(this).parent().find('.icon-edit').addClass('shrink');
	 var index = $(this).attr('data-index');
	 var x = $(this);
	 setTimeout(function(){
	   x.parent().remove();	
	   delete_social_item(index);
	   resetIcon();
	   $(".sd-publish").show();
	 },300); 		
	});
	var a = $('<a href="javascript:void(0);" class="icon-edit fa '+social_class+'"></a>');
	a.click(function(){
	  resetIcon();
	  $(this).parent().addClass('active');	
	  $('.url-icon-container').addClass('show');
	  var index = $(this).parent().find('.btn-delete-icon-func').attr('data-index');
	  var social = JSON.parse($("#sd-social").val());
	  $("#url-text").val(social[index][1]);
	  $("#url-index").val(index);
	});
	li.append(div);
	li.append(btn);
	li.append(a);

	li.insertBefore($(".social-container").find('li').last());

	var data_arr = [social_class,'#'];
	social.push(data_arr);

	$("#sd-social").val(JSON.stringify(social));

	if(social.length >= 8)
	{
		$(".add-icon").parent().addClass('hidden');
		resetIcon();
	}




}

function resetIcon() {
	$('.soc-icons li').removeClass('active');
	$('.social-icons-container').removeClass('show');	

	
}

function delete_social_item(index)
{
	var social = JSON.parse($("#sd-social").val());
	social.splice(index, 1);
	// console.log(index);
	// console.log(social);
	var new_index = 0;
	$(".btn-delete-icon-func").each(function(){
		$(this).attr("data-index",new_index);
		new_index++;
	});
	if(social.length <= 7)
	{
		$(".add-icon").parent().removeClass('hidden');
	}

	$("#sd-social").val(JSON.stringify(social));


}
