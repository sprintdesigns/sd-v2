var global_marketing_upload_resp="";
$(document).ready(function(){
	$(".marketing-banner-settings").hide();

	// $(".open-marketing-banner").click(function(){
	// 	marketing_banner_response('loading','Loading Banner');
	// 	var banner = $(this).find('.sd-marketing-banner').attr('src');
	// 	var title = $(this).find('.sd-marketing-title').html();
	// 	var description = $(this).find('.sd-marketing-description').val();
	// 	var id = $(this).find('.sd-marketing-id').val();
	// 	console.log(banner);
	// 	$("#marketing-title").val(title);
	// 	$("#marketing-description").val(description);
	// 	$("#marketing-banner-id").val();
	// 	$("#marketing-banner-img").val(banner);
	// 	$('<img/>').attr('src', banner).load(function() {
	//         $(this).remove();
	//         $("#marketing-title").val(title);
	// 		$("#marketing-description").val(description);
	// 		$("#marketing-banner-id").val(id);
	// 		$(".marketing-banner-photo").find('img').attr('src',banner);
	      
	//         marketing_banner_response(true,'Banner Load successfully.');
	//         $(".marketing-banner-settings").show();
	         
	//       });
		
	// });
  banner_icon_action();

	$("#update-marketing-banner").click(function(){

    var data = new FormData(document.getElementById('marketing-banner-form'));
    marketing_banner_response('loading','Saving Marketing Banner');
    $.ajax({
      type:'POST',
      url:base_url+'marketing/update_banner',
      data:data,
      processData: false,
      contentType: false,
      dataType:'json',
      success:function(data)
      {
         marketing_banner_response(data.status,data.message);
        //alert_message_contact_form(data.status,data.message);
        if(data.status)
        {
          
          console.log(data);
         
          $(".marketing-banner-list").html(data.banner_view);
          banner_icon_action();
          //location.reload();
        }
      }
    });
	});

  $("#facebookShareLink").on("click",function(){
      var fbpopup = window.open("https://www.facebook.com/sharer/sharer.php?u="+$("#sd-marketing-url").val()+"?banner_id="+$("#marketing-banner-id").val()+"&picture="+$(".marketing-banner-photo").find('img').attr('src')+'&name='+$("#marketing-title").val()+'&description='+$("#marketing-description").val(), "pop", "width=600, height=400, scrollbars=no");
      return false;
  });
  $("#tweeterShareLink").on('click',function(){
      var url_to_share = $("#sd-marketing-url").val();
      if($("#sd-marketing-url").val() == base_url+'home')
      {
        url_to_share = base_url;
      }

      var tpopup =  window.open('//twitter.com/share?url='+url_to_share+"?banner_id="+$("#marketing-banner-id").val()+'&text='+$("#marketing-description").val()+'  ', '_blank', 'width=800,height=300');
      return false;
  });


	Dropzone.options.marketingUpload = {
        // previewTemplate: document.querySelector('#preview-template').innerHTML,
        paramName: 'file',
        // maxFilesize: 2, // MB
        //maxFiles: 1,
        parallelUploads: 1,
        acceptedFiles: 'image/*',
        addRemoveLinks: true,
        dictDefaultMessage: 'Drag an image here to upload, or click to select one',
        init: function() {  
          this.on('sending',function(file, xhr, formData){
             marketing_banner_response('loading','uploading image');
          });
          this.on('success', function( file, resp ){
              global_marketing_upload_resp = jQuery.parseJSON(resp);
              $(".gallery-img-"+global_marketing_upload_resp.img_id).attr('data-use','true');

          });
          this.on("complete", function(file,resp) {
              if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
                var _this = this;
                // Remove all files
                _this.removeAllFiles();  
                $(".slider-gallery-show").removeClass('show');
                marketing_banner_response(true,global_marketing_upload_resp.message);
                $(".marketing-banner-photo").find('img').attr('src',global_marketing_upload_resp.full_path_web);
                $("#marketing-banner-img").val(global_marketing_upload_resp.full_path_web);
                
                load_banner_gallery=true;
                reload_images=true;


              }
              
             
             
          });

        }
        
    };



});


function banner_icon_action()
{
  $(".open-marketing-banner").click(function(){
    marketing_banner_response('loading','Loading Banner');
    var banner = $(this).find('.sd-marketing-banner').attr('src');
    var title = $(this).find('.sd-marketing-title').html();
    var description = $(this).find('.sd-marketing-description').val();
    var url = $(this).find('.sd-marketing-url').val();
    var id = $(this).find('.sd-marketing-id').val();
    console.log(banner);
    $("#marketing-title").val(title);
    $("#marketing-description").val(description);
    $("#marketing-banner-id").val();
    $("#marketing-banner-img").val(banner);
    $('<img/>').attr('src', banner).load(function() {
          $(this).remove();
          $("#marketing-title").val(title);
      $("#marketing-description").val(description);
      $("#marketing-banner-id").val(id);
      $("#sd-marketing-url option").each(function(){
        if($(this).val() == url)
        {
          console.log($(this).val());
          $(this).attr('selected','selected');
        }else
        {
          $(this).removeAttr('selected');
        }
      });
      $(".marketing-banner-photo").find('img').attr('src',banner);
        
          marketing_banner_response(true,'Banner Load successfully.');
          $(".marketing-banner-settings").show();
           
        });
    
  });
}

function marketing_banner_response(status,message)
{
  if(status == 'loading')
  {
    $("#fullpageloader-response").addClass('show');
    $("#fullpageloader-response").addClass('show');
        var div = $('<div class="full-page-reloader"><p class="big center" >'+message+'</p></div>');
    $("#fullpageloader-response").append(div);
    $('body').addClass('noscroll');
        // window.setTimeout(function() {
        //   $("#fullpageloader-response").find('div').fadeTo(500, 0).slideUp(500, function(){
        //       $(this).remove();
        //   });
        // }, 5000);
  }else if(status == true)
  {

    
     setTimeout(function(){
        $("#fullpageloader-response").removeClass('show');
        $('body').removeClass('noscroll');
        $("#fullpageloader-response").find('.full-page-reloader').remove();
     },300);

  
  }else
  {

     setTimeout(function(){
        $("#fullpageloader-response").removeClass('show');
        $('body').removeClass('noscroll');
        $("#fullpageloader-response").find('.full-page-reloader').remove();
     },300);

  
       
  }
}

