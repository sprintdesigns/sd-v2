//var base_url= "<?php echo json_encode(base_url()); ?>";
$(document).ready(function(){

  initTinyMCEFunctions.tinyMCEFunctionsinit();
          
  $.ajax({
    type:'post',
    url:base_url+'column/load',
    data:{type:'main'},
    success:function(data)
    {
      $(".sd-column-shell").remove();
      $(".sd-banner").append(data);
      //appendeditbuttonsonbanner();
      initsectionFunctions.sectionFunctionsinit();
      //appendeditbuttonsInit(); 
      startCarousel();
      $(".section-appender").each(function(){
          var section = $(this);
          $.ajax({
            type:'post',
            url:base_url+'column/load',
            data:{type:'content'},
            success:function(data)
            {

              section.append(data);
              startCarousel();
              disabled_other_section();   
            }
          }); 


      });
    }
  });

  


  //Menu edit


  // $('.layer-close').click(function(){
  //   $('.layer-2').removeClass('open-panel');
  // });

  // $('.layer-close-2').click(function(){
  //   $('.layer-3').removeClass('open-panel');
  // });

  
});
//disabled other section

function disabled_other_section()
{
  if($("#packages").length > 0)
  {
    $(".booking-class-section").addClass("disabled-function");
    
  }else
  {
    $(".booking-class-section").removeClass("disabled-function");
  }

  if($(".gallery-slick").length > 0)
  {
    $(".slick-class-section").addClass("disabled-function");
  }else
  {
    $(".slick-class-section").removeClass("disabled-function");
  } 
}


// burger icon
(function() {

  "use strict";

  var toggles = document.querySelectorAll(".c-hamburger");

  for (var i = toggles.length - 1; i >= 0; i--) {
    var toggle = toggles[i];
    toggleHandler(toggle);
  };

  function toggleHandler(toggle) {
    toggle.addEventListener( "click", function(e) {
      e.preventDefault();
      (this.classList.contains("is-active") === true) ? this.classList.remove("is-active") : this.classList.add("is-active");
    });
  }

})();

function startCarousel() {
  $('.slidemebar').carousel({
    interval: false
  });
  
}

function appendeditbuttonsInit() {
  $('.section-appender').on("click", "button.slidecx", function(){
      if($(this).parent().hasClass('open')) {
          $this = $(this);
          $this.parent().removeClass('open');   
          setTimeout(function() {   
            $this.parent().parent().removeAttr('style');
          }, 300);
          alert('open1')
        }  
        else {
          $this = $(this);
          $this.parent().toggleClass('open');
          $this.parent().parent().css('position', 'relative');
          // $this.parent().parent().css('margin-top', '100px');
          alert('close1')
        }
    });    
}
function appendAddImageButton()
{
  $(".galleryShow").click(function(){
          check_from=false;
          btn_to_view_img=this;
          $('.edit-menu-left').addClass('show',function(){
             $('.layer-3').addClass('open-panel');
          });
         
      });
}
function appendeditbuttonsonbanner() {
  $('.sd-banner').on("click", "button.slidecx", function(){
      if($(this).parent().hasClass('open')) {
          $this = $(this);
          $this.parent().removeClass('open');   
          setTimeout(function() {   
            $this.parent().parent().removeAttr('style');
          }, 300);
          alert('open2')
        }  
        else {
          $this = $(this);
          $this.parent().toggleClass('open');
          $this.parent().parent().css('position', 'relative');
          $this.parent().parent().css('margin-top', '22px');
          alert('close2')
        }
    });    
}

function editButtonsInit() {
    $('.sd-main-content').on("click", "button.slidecx", function(){
      if($(this).parent().hasClass('open')) {
          $this = $(this);
          $this.parent().removeClass('open'); 
        setTimeout(function() { 
          $this.parent().parent().removeAttr('style');
        }, 300);
        alert('open3')
      }  
        else {
          $this = $(this);
        $this.parent().toggleClass('open');
        $this.parent().parent().css('position', 'relative');
        // $this.parent().parent().css('margin-top', '100px');
        alert('close3')
      }
    });
}

function addDynamicSection(content) {
	var dynamic_content = $('[dynamic-section]').html();	
	  $.ajax({
	    type:'post',
	    url:base_url+'section/apply_dynamic_section_ajax',
	    data:{content:dynamic_content},
	    success:function(data) {
	    	$('[dynamic-section]').html(data);
	    	$('[dynamic-section]').removeAttr('dynamic-section');              	
	    }
	  });	
}

var initsectionFunctions = function () {
    return {
        sectionFunctionsinit: function () { 
            //ajax for adding column
            var add_column_btn;
            $(".sd-banner .sd-add-column").click(function(){
              
              add_column_btn = this;
              var section_id = $(this).attr('data-id');
              $.ajax({
                type:'post',
                url:base_url+'column/add_section',
                data:{section_id:section_id},
                success:function(data) {
                  $(".sd-publish").show();
                  $(".sd-main-content").prepend(data);
                  $(".after-section").removeAttr("style");
                        $(".addable").removeClass("open");
                        //appendeditbuttonsInit();
                        appendAddImageButton();
                        initTinyMCEFunctions.tinyMCEFunctionsinit();
                        disabled_other_section();
                  		setTimeout(function(){ startCarousel(); }, 200);

                  		
                  	if ($("[dynamic-section]").length >= 1) {                 		
                  		addDynamicSection();
                  	}
                }
              });
            });

            //ajax for adding column
            $(".section-appender .sd-add-column").click(function(){
              var section_id = $(this).attr('data-id');
              var divputdown = $(this).closest('.section-appender');
              $.ajax({
                type:'post',
                url:base_url+'column/add_section',
                data:{section_id:section_id},
                success:function(data) {
                  $(".sd-publish").show();
                  $(data).insertAfter(divputdown);
                  $(".after-section").removeAttr("style");
                        $(".addable").removeClass("open");
                        //appendeditbuttonsInit();
                        appendAddImageButton();
                        initTinyMCEFunctions.tinyMCEFunctionsinit();
                        disabled_other_section();
                 		setTimeout(function(){ startCarousel();}, 200);   
                 		 
                  	if ($("[dynamic-section]").length >= 1) {                  		
                  		addDynamicSection();
                  	}                 		             
                }
              });
            });

            //ajax for adding column on appended and prepended columns
            $("#sortable.sd-main-content").on('click', '.sd-add-column', function(){
             var section_id = $(this).attr('data-id');
              var divputdown = $(this).closest('.section-appender');
              $.ajax({
                type:'post',
                url:base_url+'column/add_section',
                data:{section_id:section_id},
                success:function(data) {
                  $(".sd-publish").show();
                  $(data).insertAfter(divputdown);
                  $(".after-section").removeAttr("style");
                        $(".addable").removeClass("open");
                        //appendeditbuttonsInit();
                        appendAddImageButton();
                        initTinyMCEFunctions.tinyMCEFunctionsinit();
                        disabled_other_section();
                 		setTimeout(function(){ startCarousel();}, 200);
                  	
                  	if ($("[dynamic-section]").length >= 1) {                  		
                  		addDynamicSection();
                  	}                 		
                }
              });
            });
        }
    };
}();

var initTinyMCEFunctions = function () {
    return {
         tinyMCEFunctionsinit: function() {
            $('.sd-logo > a').addClass('text-logo');
        $('#callustext').addClass('text-only');
        $('#phoneno').addClass('text-only');
        $('.bcard').addClass('text-only');
        $('.sd-logo > a').removeAttr('href');
        tinymce.init({
        selector: '.text-only',
        plugins: 'textcolor',
        inline: true,
        toolbar: 'undo redo | fontselect | forecolor | bold italic',
        textcolor_map: [
          "000000", "Black",
          "993300", "Burnt orange",
          "333300", "Dark olive",
          "003300", "Dark green",
          "003366", "Dark azure",
          "000080", "Navy Blue",
          "333399", "Indigo",
          "333333", "Very dark gray",
          "800000", "Maroon",
          "FF6600", "Orange",
          "808000", "Olive",
          "008000", "Green",
          "008080", "Teal",
          "0000FF", "Blue",
          "666699", "Grayish blue",
          "808080", "Gray",
          "FF0000", "Red",
          "FF9900", "Amber",
          "99CC00", "Yellow green",
          "339966", "Sea green",
          "33CCCC", "Turquoise",
          "3366FF", "Royal blue",
          "800080", "Purple",
          "999999", "Medium gray",
          "FF00FF", "Magenta",
          "FFCC00", "Gold",
          "FFFF00", "Yellow",
          "00FF00", "Lime",
          "00FFFF", "Aqua",
          "00CCFF", "Sky blue",
          "993366", "Red violet",
          "FFFFFF", "White",
          "FF99CC", "Pink",
          "FFCC99", "Peach",
          "FFFF99", "Light yellow",
          "CCFFCC", "Pale green",
          "CCFFFF", "Pale cyan",
          "99CCFF", "Light sky blue",
          "CC99FF", "Plum"
        ],
        font_formats: 'Andale Mono=andale mono,times;Spicy Rice=Spicy Rice, sans-serif;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats;Great Vibes="Great Vibes", cursive;Montserrat="Monteserrat", sans-serif;Acme="Acme", sans-serif;Bad Script="Bad Script", cursive;Bree Serif="Bree Serif", serif;Courgette="Courgette", cursive;Dancing Script="Dancing Script", cursive;Didact Gothic="Didact Gothic", sans-serif;Gloria Hallelujah="Gloria Hallelujah", cursive;Indie Flower="Indie Flower", cursive;Josefin Sans="Josefin Sans", sans-serif;Lobster="Lobster", cursive;Lobster Two="Lobster Two", cursive;Merriweather="Merriweather", serif;Pacifico="Pacifico", cursive;Roboto Slab="Roboto Slab", serif;Roboto="Roboto", sans-serif;Sacramento="Sacramento", cursive;Satisfy="Satisfy", cursive;Slabo 27px="Slabo 27px", serif;Ubuntu="Ubuntu", sans-serif',
        menubar: false,
         setup:function(ed) {
               ed.on('change', function(e) {
                   $(".sd-publish").show();
                   
               });
           }
      });
        
        tinymce.init({
        selector: '.text-logo',
        inline: true,
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
        menubar: false,
        plugins: [
        'searchreplace visualblocks code fullscreen'
        ],
        setup:function(ed) {
             ed.on('change', function(e) {
                 $(".sd-publish").show();
                 
             });
         }
      });

      tinymce.init({
      selector: '.sd-image-logo',
      inline: true,
      toolbar: 'image',
      menubar: false,
      plugins: [
          'image autoresize'
      ],
      file_browser_callback: function(callback, value, meta) {
          imageFilePicker(callback, value, meta);
      },      
      setup:function(ed) {
           ed.on('change', function(e) {
               $(".sd-publish").show();
               
           });
       }
    });        

      tinymce.init({
        selector: 'div.editable',
        inline: true,
        plugins: [
          'advlist autolink lists link image charmap print preview anchor',
          'searchreplace visualblocks code fullscreen',
          'insertdatetime media contextmenu paste textcolor image'
        ],
        menubar: false,
        toolbar: 'insertfile undo redo | image | fontselect | forecolor | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent code',
        fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt',
        textcolor_map: [
          "000000", "Black",
          "993300", "Burnt orange",
          "333300", "Dark olive",
          "003300", "Dark green",
          "003366", "Dark azure",
          "000080", "Navy Blue",
          "333399", "Indigo",
          "333333", "Very dark gray",
          "800000", "Maroon",
          "FF6600", "Orange",
          "808000", "Olive",
          "008000", "Green",
          "008080", "Teal",
          "0000FF", "Blue",
          "666699", "Grayish blue",
          "808080", "Gray",
          "FF0000", "Red",
          "FF9900", "Amber",
          "99CC00", "Yellow green",
          "339966", "Sea green",
          "33CCCC", "Turquoise",
          "3366FF", "Royal blue",
          "800080", "Purple",
          "999999", "Medium gray",
          "FF00FF", "Magenta",
          "FFCC00", "Gold",
          "FFFF00", "Yellow",
          "00FF00", "Lime",
          "00FFFF", "Aqua",
          "00CCFF", "Sky blue",
          "993366", "Red violet",
          "FFFFFF", "White",
          "FF99CC", "Pink",
          "FFCC99", "Peach",
          "FFFF99", "Light yellow",
          "CCFFCC", "Pale green",
          "CCFFFF", "Pale cyan",
          "99CCFF", "Light sky blue",
          "CC99FF", "Plum"
        ],
        file_browser_callback: function(callback, value, meta) {
            imageFilePicker(callback, value, meta);
        },
        font_formats: 'Andale Mono=andale mono,times;Spicy Rice=Spicy Rice, sans-serif;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats;Great Vibes="Great Vibes", cursive;Montserrat="Monteserrat", sans-serif;Acme="Acme", sans-serif;Bad Script="Bad Script", cursive;Bree Serif="Bree Serif", serif;Courgette="Courgette", cursive;Dancing Script="Dancing Script", cursive;Didact Gothic="Didact Gothic", sans-serif;Gloria Hallelujah="Gloria Hallelujah", cursive;Indie Flower="Indie Flower", cursive;Josefin Sans="Josefin Sans", sans-serif;Lobster="Lobster", cursive;Lobster Two="Lobster Two", cursive;Merriweather="Merriweather", serif;Pacifico="Pacifico", cursive;Roboto Slab="Roboto Slab", serif;Roboto="Roboto", sans-serif;Sacramento="Sacramento", cursive;Satisfy="Satisfy", cursive;Slabo 27px="Slabo 27px", serif;Ubuntu="Ubuntu", sans-serif',
        setup:function(ed) {
             ed.on('change', function(e) {
                 $(".sd-publish").show();
                 
             });
         }
      }); 

      tinymce.init({
        selector: 'p.editable',
        inline: true,
        plugins: [
          'advlist autolink lists link image charmap print preview anchor',
          'searchreplace visualblocks code fullscreen',
          'insertdatetime media table contextmenu paste textcolor image'
        ],
        menubar: false,
        toolbar: 'insertfile undo redo | image | fontselect | forecolor | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent code',
        fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt',
        textcolor_map: [
          "000000", "Black",
          "993300", "Burnt orange",
          "333300", "Dark olive",
          "003300", "Dark green",
          "003366", "Dark azure",
          "000080", "Navy Blue",
          "333399", "Indigo",
          "333333", "Very dark gray",
          "800000", "Maroon",
          "FF6600", "Orange",
          "808000", "Olive",
          "008000", "Green",
          "008080", "Teal",
          "0000FF", "Blue",
          "666699", "Grayish blue",
          "808080", "Gray",
          "FF0000", "Red",
          "FF9900", "Amber",
          "99CC00", "Yellow green",
          "339966", "Sea green",
          "33CCCC", "Turquoise",
          "3366FF", "Royal blue",
          "800080", "Purple",
          "999999", "Medium gray",
          "FF00FF", "Magenta",
          "FFCC00", "Gold",
          "FFFF00", "Yellow",
          "00FF00", "Lime",
          "00FFFF", "Aqua",
          "00CCFF", "Sky blue",
          "993366", "Red violet",
          "FFFFFF", "White",
          "FF99CC", "Pink",
          "FFCC99", "Peach",
          "FFFF99", "Light yellow",
          "CCFFCC", "Pale green",
          "CCFFFF", "Pale cyan",
          "99CCFF", "Light sky blue",
          "CC99FF", "Plum"
        ],
        file_browser_callback: function(callback, value, meta) {
            imageFilePicker(callback, value, meta);
        },
        font_formats: 'Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats;Great Vibes="Great Vibes", cursive;Montserrat="Monteserrat", sans-serif;Acme="Acme", sans-serif;Bad Script="Bad Script", cursive;Bree Serif="Bree Serif", serif;Courgette="Courgette", cursive;Dancing Script="Dancing Script", cursive;Didact Gothic="Didact Gothic", sans-serif;Gloria Hallelujah="Gloria Hallelujah", cursive;Indie Flower="Indie Flower", cursive;Josefin Sans="Josefin Sans", sans-serif;Lobster="Lobster", cursive;Lobster Two="Lobster Two", cursive;Merriweather="Merriweather", serif;Pacifico="Pacifico", cursive;Roboto Slab="Roboto Slab", serif;Roboto="Roboto", sans-serif;Sacramento="Sacramento", cursive;Satisfy="Satisfy", cursive;Slabo 27px="Slabo 27px", serif;Ubuntu="Ubuntu", sans-serif',
        setup:function(ed) {
             ed.on('change', function(e) {
                 $(".sd-publish").show();
                 
             });
         }
      }); 
         }
    }

}();

var imageFilePicker = function (callback, value, meta) {          
    check_from=false;
    btn_to_view_img=callback;
    $('.edit-menu-left').addClass('show',function(){
        $('.layer-3').addClass('open-panel');
    });
         
};

function deleteColumn(btn)
{
  var con = confirm("this action will delete this section. delete this section?");

  if(con){
    $(".sd-publish").show();
    $(btn).parent().parent().remove();
    disabled_other_section();
  }
}