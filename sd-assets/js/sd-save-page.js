//var base_url= "<?php echo json_encode(base_url()); ?>";

function savePage(pageName)
{
	$(".btn-save-website").data('clicked', true);

	cleanContent();
    resetDynamicSections();

	var content = $(".sd-main-content").html();
	var logo = $('.sd-image-logo').find('p').html();
    var social_item = $("#sd-social").val();
    var theme_color = $("#body-color-template").val();
    var site_title = "logo";
    var page_name = pageName;
    var page_top_right = '';
    var page_bottom_right = '';

    $("#fullpageloader-response").addClass('show');
    var fd = new FormData();
    fd.append('content',content);
    fd.append('pageName',page_name);
    fd.append('page_logo',logo);
    fd.append('page_top_right',page_top_right);
    fd.append('page_bottom_right',page_bottom_right);
    fd.append('theme_color',theme_color);
    fd.append('site_title',site_title);
    fd.append('social_item', social_item);

    // EXTRA CONTENT
    fd.append('editable_text', get_editable_text());
    // END EXTRA CONTENT

    $.ajax({
        type:'POST',
        url:base_url+'page/save',
        processData: false,
        contentType: false,
        data:fd,
        dataType:'json',
        async: false,
        success:function(data)
        {
            // $("#fullpageloader-response").removeClass('show');
            if(data.status)
            {
                location.reload();
            }else
            {
                alert(data.message);
            }
        }
    });
}

function get_editable_text() {
    var extra_content = [];
    $("._sd-editable-text").each(function() {
        var content = $(this).html();
        var id = $(this).attr('id');
        var new_id = id.substring(1, id.length); // REMOVE FIRST CHARACTER BECAUSE THE ID ALWAYS HAS PREFIX "_"
        extras = { "variable": new_id, "content": content };
        extra_content.push(extras);
    });
    return JSON.stringify(extra_content);
}

function resetDynamicSections() {
    $(".sd-dynamic-section").each(function() {
        var section_function = $(this).attr('sd-dynamic-function');
        $(this).parent('section').html(section_function);        
    });    
}

function cleanContent() {
	removeSectionBar();
    cleanSlickGallery();
	cleanTinyMCE();
}

function cleanSlickGallery() {
    if($('.main-slider').hasClass('slick-initialized')) {
      $('.main-slider').removeClass('slick-initialized');
      $('.main-slider').removeClass('slick-slider');
      $('.nav-slider').removeClass('slick-initialized');
      $('.nav-slider').removeClass('slick-slider');
      $('.slick-prev').remove();
      $('.slick-next').remove();
      str = [];
      strnav = [];
      // get images on slick 
      for(var x=1; x<=5; x ++) {
        var image = $('#slick-slide-' + x).css('background-image');     
        var imagenav = $('#slick-slide-nav' + x).css('background-image');

        $('#slick-slide-' + x).removeAttr('data-slick-index tabindex role aria-describedby aria-hidden');

        $('#slick-slide-nav-' + x).removeAttr('data-slick-index tabindex role aria-describedby aria-hidden');
  
        str[x] =  $('<div/>').append($('#slick-slide-' + x).removeClass("slick-slide").removeAttr('data-slick-index tabindex role aria-describedby aria-hidden').clone()).html();
        strnav[x] =  $('<div/>').append($('#slick-slide-nav-' + x).removeClass("slick-slide").removeAttr('data-slick-index tabindex role aria-describedby aria-hidden').clone()).html();
      }
      $('.main-slider').empty();
      $('.nav-slider').empty();

      for(var y=1; y<=5; y++) {
        $('.main-slider').append(str[y]);
        $('.nav-slider').append(strnav[y]);
      }

    }    
}

function removeSectionBar() {
    $(".section-appender").find(".sd-column-shell").remove();
}

function cleanTinyMCE() {
    $(".sd-main-content").find('h1').removeAttr("spellcheck").removeAttr("contenteditable").removeClass('mce-content-body');
    $(".sd-main-content").find('h2').removeAttr("spellcheck").removeAttr("contenteditable").removeClass('mce-content-body');
    $(".sd-main-content").find('p').removeAttr("spellcheck").removeAttr("contenteditable").removeClass('mce-content-body');
    $(".sd-main-content").find('span').removeAttr("spellcheck").removeAttr("contenteditable").removeClass('mce-content-body');
    $(".sd-main-content").find('small').removeAttr("spellcheck").removeAttr("contenteditable").removeClass('mce-content-body');
    $(".sd-main-content").find('em').removeAttr("spellcheck").removeAttr("contenteditable").removeClass('mce-content-body');
    $(".sd-main-content").find('div').removeAttr("spellcheck").removeAttr("contenteditable").removeClass('mce-content-body');
   	$('.sd-image-logo').find('img').removeAttr('data-mce-src');
}

$(document).ready(function(){
	$(".sd-publish").hide();
});



