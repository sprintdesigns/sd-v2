ace.require("ace/ext/language_tools");
var editor = ace.edit("editor");
editor.session.setMode("ace/mode/php");
editor.setTheme("ace/theme/monokai");
var code = editor.getValue();

var textarea = $('#content');

editor.getSession().on('change', function () {
   textarea.val(editor.getSession().getValue());
});

textarea.val(editor.getSession().getValue());

// enable autocompletion and snippets
editor.setOptions({
    enableBasicAutocompletion: true,
    enableSnippets: true,
    enableLiveAutocompletion: false
});