
$(document).ready(function(){

	add_contact_form();

	//UINotific8User.init();
	 $('.backpage').click(function(){
	 	$('.plugin-1-page').removeClass('backeddown');
	 	$('.plugin-2-page').removeClass('show');
	 });

});

function add_contact_form()
{
	$(".tr-new-contact-form").hide();

	$(".btn-save-new-form").click(function(){
		

		if($("#new-contact-form-name").val() != '')
		{
			preloader(true);
			$.ajax({
				type:'POST',
				url:base_url+'form/add_contact_form',
				data:{form_name:$("#new-contact-form-name").val()},
  				dataType:'json',
  				success:function(data)
  				{
  					
  					//alert_message_contact_form(data.status,data.message);
  					contact_form_response(data.status,data.message);
  					if(data.status)
  					{
  						preloader(false);
  						$('.plugin-1-page').html(data.form_list);
						add_contact_form();
  					}
  				}
			});
		}else
		{
			//alert_message_contact_form(false,'Invalid contact form name');
		}
	});

	$(".btn-add-contact-form-show").click(function(){
		if($('div').hasClass('boxnote-form')) { 
			$('div.boxnote-form').remove();
		}	
		$(".tr-new-contact-form").show();
		setTimeout(function(){$('#new-contact-form-name').focus();}, 300);
	});

	$(".btn-cancel-new-form").click(function(){
		$(".tr-new-contact-form").hide();
	});
}

function switch_status(form_id)
{
	preloader(true);
	$.ajax({
		type:'POST',
		url:base_url+'form/switch_status',
		data:{form_id:form_id},
		dataType:'json',
		success:function(data){

			contact_form_response(data.status,data.message);
			if(data.status)
			{
				preloader(false);
				if(data.form_status == 1)
				{
					$(".main-btn-"+form_id).show();
					$(".delete-"+form_id).hide();
				}else
				{
					$(".main-btn-"+form_id).hide();
					$(".delete-"+form_id).show();
				}
				//$(".status-form-"+form_id).html(data.form_status);
			}
			
		}
	});
	


}

function switch_main(form_id)
{
	preloader(true);
	
	$.ajax({
		type:'POST',
		url:base_url+'form/switch_main',
		data:{form_id:form_id},
		dataType:'json',
		success:function(data){
			contact_form_response(data.status,data.message);
			if(data.status)
			{
				preloader(false);
				$('.plugin-1-page').html(data.form_list);
				add_contact_form();
			}
			
		}
	});
}

function delete_form(form_id)
{
	preloader(true);
	$.ajax({
		type:'POST',
		url:base_url+'form/delete_form',
		data:{form_id:form_id},
		dataType:'json',
		success:function(data){
			contact_form_response(data.status,data.message);
			if(data.status)
			{
				preloader(false);
				$('.plugin-1-page').html(data.form_list);
				$(".form-info-"+form_id).remove();
			}
			
		}
	});
}

function duplicate_form(form_id)
{

	preloader(true);
	$.ajax({
		type:'POST',
		url:base_url+'form/duplicate_form',
		data:{form_id:form_id},
		dataType:'json',
		success:function(data){
			contact_form_response(data.status,data.message);
			if(data.status)
			{
				preloader(false);
				$('.plugin-1-page').html(data.form_list);
				
				add_contact_form();
			}
			
		}
	});
}

function edit_form(form_id)
{
	preloader(true);
	$.ajax({
		type:'POST',
		url:base_url+"form/edit",
		data:{form_id:form_id},
		success:function(data)
		{
			
			$("#contact-form-response").html('');
			$('.plugin-2-page').html(data);
			preloader(false);
			$('.plugin-1-page').addClass('backeddown');
 			$('.plugin-2-page').addClass('show');

 			$('.backpage').click(function(){
			 	$('.plugin-1-page').removeClass('backeddown');
			 	$('.plugin-2-page').removeClass('show');
			 });

 			$(".add-new-field").click(function(){
 				$('.add-new-field-tr').removeClass('hidden');
 			});

 			$(".btn-settings-mailer-save").click(function(){
 				var email = $('#mailer-settings-email').val();
 				var pass = $('#mailer-settings-password').val();
 				var verify = $('#mailer-settings-verify').val();
 				var id = $("#settings-id").val();
 				var form_id = $("#form-id-hidden").val();


 				if(pass == verify)
 				{
 					preloader(true);
					$.ajax({
						type:'POST',
						url:base_url+'form/save_mailer',
						data:{email:email,pass:pass,id:id,form_id:form_id},
						dataType:'json',
						success:function(data){
							contact_form_response(data.status,data.message);
							if(data.status)
							{
								$('#mailer-settings-verify').val('');
								preloader(false);
								$("#settings-id").val(data.id);
								// $('.plugin-1-page').html(data.form_list);
								
								// add_contact_form();
							}
							
						}
					});
 				}else
 				{
 					alert('password and verify not match');
 				}
 			});

 			$(".btn-settings-subject-save").click(function(){
 				var id = $("#settings-id").val();
 				var subject = $("#webmail-subject").val();
 				var form_id = $("#form-id-hidden").val();

 				preloader(true);
				$.ajax({
					type:'POST',
					url:base_url+'form/save_subject',
					data:{subject:subject,id:id,form_id:form_id},
					dataType:'json',
					success:function(data){
						contact_form_response(data.status,data.message);
						if(data.status)
						{
							
							preloader(false);
							$("#settings-id").val(data.id);
							// $('.plugin-1-page').html(data.form_list);
							
							// add_contact_form();
						}
						
					}
				});

 			});


 			$(".btn-plus-email").click(function(){
 				
 				$('<input type="email" class="form-control email-input-plus" placeholder="Email" name="email[]" >').insertBefore("#new-insert-email");	
 			});

 			$("#email-to-form").submit(function(){
 				var data = new FormData(document.getElementById("email-to-form"));
 				var id = $("#settings-id").val();
 				var form_id = $("#form-id-hidden").val();

 				data.append('id',id);
 				data.append('form_id',form_id);

 				preloader(true);
 				$.ajax({
 					type:'post',
 					url:base_url+'form/save_to_email',
 					data:data,
 					processData: false,
  					contentType: false,
  					dataType:'json',
  					success:function(data){
  						contact_form_response(data.status,data.message);
  						if(data.status)
						{
							$(".email-input-plus").each(function(){
								if($(this).val() == '')
								{
									$(this).remove();
								}
							});
							preloader(false);
							$("#settings-id").val(data.id);
							// $('.plugin-1-page').html(data.form_list);
							
							// add_contact_form();
						}
  					}
 				});

 				return false;
 			});

 			$(".btn-test-mailer").click(function(){
 				var form_id = $("#form-id-hidden").val();
 				preloader(true);
				$.ajax({
					type:'POST',
					url:base_url+'email/test_send',
					data:{form_id:form_id},
					dataType:'json',
					success:function(data){
						
						test_email_response(data.status,data.message);
						preloader(false);
	
					
						
					}
				});
 			});

 			init_edit_function();


			
		}
	});
}


function switch_status_field(field_id)
{
	preloader(true);
	$.ajax({
		type:'POST',
		url:base_url+'form/switch_status_field',
		data:{field_id:field_id},
		dataType:'json',
		success:function(data){
			contact_form_response(data.status,data.message);
			if(data.status)
			{
				preloader(false);
				
				//$(".status-form-"+form_id).html(data.form_status);
			}
			
		}
	});
}

function switch_required_field(field_id)
{
	preloader(true);
	$.ajax({
		type:'POST',
		url:base_url+'form/switch_required_field',
		data:{field_id:field_id},
		dataType:'json',
		success:function(data){
			contact_form_response(data.status,data.message);
			if(data.status)
			{
				preloader(false);
				
				//$(".status-form-"+form_id).html(data.form_status);
			}
			
		}
	});
}

function duplicate_field(field_id)
{

	preloader(true);
	$.ajax({
		type:'POST',
		url:base_url+'form/duplicate_field',
		data:{field_id:field_id},
		dataType:'json',
		success:function(data){
			contact_form_response(data.status,data.message);
			if(data.status)
			{
				preloader(false);
				$(".fields-table-container").html(data.field_table);
				init_edit_function();
				//$('.plugin-1-page').html(data.form_list);
				
				
			}
			
		}
	});
}

function delete_field(field_id)
{
	preloader(true);
	$.ajax({
		type:'POST',
		url:base_url+'form/delete_field',
		data:{field_id:field_id},
		dataType:'json',
		success:function(data){
			contact_form_response(data.status,data.message);
			if(data.status)
			{
				preloader(false);
				$(".fields-table-container").html(data.field_table);
				init_edit_function();
				//$('.plugin-1-page').html(data.form_list);
				
				
			}
			
		}
	});
}


function init_edit_function()
{
	$(".edit-field").click(function(){
		var id = $(this).attr('data-id');
		$('.btn-field-func-'+id).hide();
		$('.btn-edit-field-'+id).show();

		$(".type-"+id).hide();
		$(".label-"+id).hide();

		$(".select-type-"+id).show();
		$(".input-label-"+id).show();

	});

	$(".btn-cancel-changes-field").click(function(){
		var id = $(this).attr('data-id');

		$('.btn-field-func-'+id).show();
		$('.btn-edit-field-'+id).hide();

		$(".type-"+id).show();
		$(".label-"+id).show();

		$(".select-type-"+id).hide();
		$(".input-label-"+id).hide();
	});

	$(".btn-save-changes-field").click(function(){
		var id = $(this).attr('data-id');
		var type = $(".select-type-"+id).val();
		var name = $(".input-label-"+id).val();
		

		preloader(true);
		$.ajax({
			type:'POST',
			url:base_url+'form/update_field',
			data:{"type":type,"name":name,id:id},
			dataType:'json',
			success:function(data){
				contact_form_response(data.status,data.message);
				if(data.status)
				{
					preloader(false);
					$(".fields-table-container").html(data.field_table);
					init_edit_function();
					//$('.plugin-1-page').html(data.form_list);
					
					
				}
				
			}
		});

	});

	$(".btn-cancel-new-field").click(function(){
		
		$('.add-new-field-tr').addClass('hidden');
	});

	$(".btn-save-new-field").click(function(){

		var label = $(".new-input-label").val();
		var type = $(".new-select-type").val();
		var id = $("#form-id-hidden").val();


		preloader(true);
		$.ajax({
			type:'POST',
			url:base_url+'form/create_field',
			data:{"type":type,"label":label,id:id},
			dataType:'json',
			success:function(data){
				contact_form_response(data.status,data.message);
				if(data.status)
				{
					preloader(false);
					$(".fields-table-container").html(data.field_table);
					init_edit_function();
					//$('.plugin-1-page').html(data.form_list);
					
					
				}
				
			}
		});
	});
}


function contact_form_response(status,message)
{
	if(status == 'loading')
	{
        var div = $('<div class="alert alert-warning"><strong>Loading!</strong> '+message+'.</div>').fadeOut(5000);
		$("#contact-form-response").html(div);
        window.setTimeout(function() {
          $("#contact-form-response").find('div').fadeTo(500, 0).slideUp(500, function(){
              $(this).remove();
          });
        }, 5000);
	}else if(status == true)
	{
        var div = $('<div class="alert alert-success"><strong>Success!</strong> '+message+'.</div>').fadeOut(5000);
		$("#contact-form-response").html(div);
         window.setTimeout(function() {
          $("#contact-form-response").find('div').fadeTo(500, 0).slideUp(500, function(){
              $(this).remove();
          });
        }, 5000);
	}else
	{
        var div = $('<div class="alert alert-danger"><strong>Error!</strong> '+message+'.</div>').fadeOut(5000);
		$("#contact-form-response").html(div);
         window.setTimeout(function() {
          $("#contact-form-response").find('div').fadeTo(500, 0).slideUp(500, function(){
              $(this).remove();
          });
        }, 5000);
	}
}

function test_email_response(status,message)
{
	if(status == 'loading')
	{
        var div = $('<div class="alert alert-warning"><strong>Loading!</strong> '+message+'.</div>').fadeOut(5000);
		$("#test-email-response").html(div);
        window.setTimeout(function() {
          $("#test-email-response").find('div').fadeTo(500, 0).slideUp(500, function(){
              $(this).remove();
          });
        }, 5000);
	}else if(status == true)
	{
        var div = $('<div class="alert alert-success"><strong>Success!</strong> '+message+'.</div>').fadeOut(5000);
		$("#test-email-response").html(div);
         window.setTimeout(function() {
          $("#test-email-response").find('div').fadeTo(500, 0).slideUp(500, function(){
              $(this).remove();
          });
        }, 5000);
	}else
	{
        var div = $('<div class="alert alert-danger"><strong>Error!</strong> '+message+'.</div>').fadeOut(5000);
		$("#test-email-response").html(div);
         window.setTimeout(function() {
          $("#test-email-response").find('div').fadeTo(500, 0).slideUp(500, function(){
              $(this).remove();
          });
        }, 5000);
	}
}





