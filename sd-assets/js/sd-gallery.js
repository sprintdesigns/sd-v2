fileuploadcount = 0;
reload_images=false;
ctr =0;
fcount=0;
countsend = false;
progress = 0; 
progressctr = 0; 
ctrx = 0;
countsend2 = 0;
var global_btn_delete;
var global_img_id;
var global_use = false;
var check_if_repeat=false;

function resetCtrs() {
  fileuploadcount= 0;
  ctr = 0;
  fcount = 0;
  countsend = false;
  progress = 0; 
  progressctr = 0; 
  ctrx = 0;
}

function counter() {
  ctr = fileuploadcount;
  for(c=0; c<ctr; c++){
    var temp_img='<li id="ctr-' + c + '" class="imglicontainer removeyes">';
    temp_img+='<a class="noimage" style="pointer-events: none;" href="#">';
    temp_img+='<div class="imagebox dropzone"><span>Uploading</span>';
    temp_img+='<div class="progress dropzoneCustom"> <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:15%"></div></div>';
    temp_img+='<div class="centered">';
    temp_img+='</div>';
    temp_img+='</div>';
    temp_img+='</a>';
    temp_img+='</li>';
    $('.image-gallery').prepend(temp_img);  	
  }
  countsend = true;
  // console.log('countsend on counter ' + countsend);
  countsend2 = true;
  filecountprogress();
}


function filecountprogress() {
  var progressctr = fileuploadcount;
}

function setProgress(){
  if(progress == undefined) { 
  	progress = 0;
  }	
  else if(isNaN(progress)) {
  	progress = 0;
  }
}

function addprogress() {
	progress++;
	// console.log('addprogress' + progress );
	progress2 = progress;
}

$(document).ready(function(){
	
	gallery_section_init();


	Dropzone.options.userUpload = {
	    // previewTemplate: document.querySelector('#preview-template').innerHTML,
	    paramName: 'file',
	    // maxFilesize: 2, // MB
	    //maxFiles: 1,
	    parallelUploads: 1,
	    acceptedFiles: 'image/*',
	    addRemoveLinks: true,
	    dictDefaultMessage: 'Drag an image here to upload, or click to select one',
	    init: function() {	
	      this.on('dragover',function(file, resp){
	      });	
	      this.on('dragleave',function(file, resp){
	      });
	      this.on('sending',function(file, xhr, formData){
	      	// console.log('this files length ' + this.files.length);
	      	fileuploadcount = this.files.length;
	      	// console.log('countsend' + countsend)
	      	if(!countsend) {
	        	counter(); 
	        	console.log('countsend after counter ' + countsend);
	        }
	      	$('#user_upload').find('.dz-preview').css('display', 'none');
	      	$('#user_upload').hide();
	      });

          this.on('uploadprogress',function(file, xhr, formData){ 
          	setProgress();
          	filecountprogress();
          	// console.log('progress bar ' + progress);
	        $('.image-gallery #ctr-' + progress +' .progress-bar').css('width', file.upload.progress );  
          });
 
	      this.on('success', function( file, resp ){
	      	load_banner_gallery = true;
	        var resp = jQuery.parseJSON(resp);
	        
	        var new_img='<li class="imglicontainer">';
	        new_img+='<button class="btn btn-danger btn-delete-image-func hidden gallery-img-'+resp.img_id+'" style="position: absolute; top: 0px; right: 0px; width: 0px; height: 22px; padding: 0px 14px 3px 6px;z-index: 1;" onclick="deleteImage(this,\''+resp.img_id+'\')" data-use="false" >&times;</button>';
	        new_img+='<a href="javascript:viewImage(\''+resp.full_path_web+'\');">';
	        new_img+='<div class="imagebox">';
	        new_img+='<div class="centered">';
	        new_img+='<img class="img-responsive" src="'+resp.full_path_thumb+'">';
	        new_img+='</div>';
	        new_img+='</div>';
	        new_img+='</a>';
	        new_img+='</li>';

            // $('.image-gallery').prepend(new_img);
            $('.image-gallery').find('li#ctr-'+ progress +'.removeyes').after(new_img);
            $('.image-gallery').find('li#ctr-'+ progress +'.removeyes').remove();
            $(".imglicontainer").hover(function(){
		
				$(this).find(".btn-delete-image-func").removeClass("hidden");
				
			},function(){
				$(this).find(".btn-delete-image-func").addClass('hidden');
			});
            filecountprogress();  
            addprogress();      
	      });

	      this.on('queuecomplete',function(file, resp){
	      	
	      });
	      this.on("complete", function(file) {
		      // If all files have been uploaded
		      if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
		        var _this = this;
		        // Remove all files
		        _this.removeAllFiles();  
		      resetCtrs();
		      $('#user_upload').show();

		      }
		  });

	    }
	};
	$(".gallery-preloader").hide();
	$(".has-layer").click(function(){
    var layer= $(this).attr('data-layer');
    if(!$('.layer-'+layer).hasClass('open-panel')) {
     	if(reload_images)
     	{
     		$(".image-gallery").empty();
     		$(".gallery-preloader").show();
     		$.ajax({
				type:'POST',
				url:base_url+'gallery/load_images',
				dataType:'json',
				success:function(data)
				{
					
					if(data.status)
					{   
						$(".gallery-preloader").hide();
						$(".image-gallery").html(data.images);
						reload_images = false;
						gallery_section_init();
						
					}
				}
			});

     	}
      

    }
    
  });

});

function gallery_section_init()
{
	$(".btn-delete-image-func").addClass('hidden');
	$(".imglicontainer").hover(function(){
		
		$(this).find(".btn-delete-image-func").removeClass("hidden");
		
	},function(){
		$(this).find(".btn-delete-image-func").addClass('hidden');
	});
	$("img.lazy-image").lazyload({
	  effect : "fadeIn"
	});

	$("#btn_aimg").on("click",function(){
		$("#upload_container").removeClass("hidden");
		$(this).addClass("hidden");
		$("#btn_cimg").removeClass("hidden");
	});

	$("#btn_cimg").on("click",function(){
		$("#upload_container").addClass("hidden");
		$(this).addClass("hidden");
		$("#btn_aimg").removeClass("hidden");
	});
}
function deleteImage(btn,img_id)
{
	$("#if-use-in-banner").html('');
	global_btn_delete = btn;
	global_img_id = img_id;
	global_use = $(btn).attr('data-use');
	$("#modaldynamic").removeClass('medium');
	$("#modaldynamic").removeClass('small');
	
	if(check_if_repeat)
	{
		if(global_use == "true")
		{
			$("#modaldynamic").addClass('medium');	
			$("#if-use-in-banner").html('You are using this photo to one of your slides, if you continue, your slide photo will be deleted.');
			$("#modaldynamic").modal('show');
		}else
		{
			delete_image_action(global_img_id);
		}
	}else
	{
		if(global_use == "true")
		{
			$("#modaldynamic").addClass('medium');
			$("#if-use-in-banner").html('You are using this photo to one of your slides, if you continue, your slide photo will be deleted.');
			$("#modaldynamic").modal('show');
		}else
		{
			$("#modaldynamic").addClass('small');
			$("#modaldynamic").modal('show');
			delete_image_action(global_img_id);
		}
		
	}
	
	
}

function delete_image_action(img_id)
{
	if(document.getElementById('direct-delete-check').checked) {
	    check_if_repeat=true;
	} else {
	    check_if_repeat=false;
	}
	$("#modaldynamic").modal('hide');
	photo_gallery_response('loading','Please wait . . ');
	$.ajax({
		type:'POST',
		url:base_url+'gallery/delete',
		data:{img_id:global_img_id},
		dataType:'json',
		success:function(data)
		{
			photo_gallery_response(data.status,data.message);
			if(data.status)
			{   
				load_banner_gallery = true;
				// $(".btn-delete-image-func").removeClass('hidden');
				$(global_btn_delete).parent().addClass('removeout');
				setTimeout( function(){
				  $(global_btn_delete).parent().remove();  	
				}, 500);		
			}
		}
	});
}

function photo_gallery_response(status,message)
{
	if(status == 'loading')
	{
        var div = $('<div class="alert alert-warning"><strong>Loading!</strong> '+message+'.</div>').fadeOut(5000);
		$("#photo-gallery-response").html(div);
        window.setTimeout(function() {
          $("#photo-gallery-response").find('div').fadeTo(500, 0).slideUp(500, function(){
              $(this).remove();
          });
        }, 5000);
	}else if(status == true)
	{
        var div = $('<div class="alert alert-success"><strong>Success!</strong> '+message+'.</div>').fadeOut(5000);
		$("#photo-gallery-response").html(div);
         window.setTimeout(function() {
          $("#photo-gallery-response").find('div').fadeTo(500, 0).slideUp(500, function(){
              $(this).remove();
          });
        }, 5000);
	}else
	{
        var div = $('<div class="alert alert-danger"><strong>Error!</strong> '+message+'.</div>').fadeOut(5000);
		$("#photo-gallery-response").html(div);
         window.setTimeout(function() {
          $("#photo-gallery-response").find('div').fadeTo(500, 0).slideUp(500, function(){
              $(this).remove();
          });
        }, 5000);
	}
}

function confirm_message(message, success, prevent) {
    var open_time = new Date();
    var result = confirm(message);
    var close_time = new Date();

    if (close_time - open_time < 10) {
        prevent();
    } else {
        success(result);
    }
}