var check_from=true;
var btn_to_view_img='';
var FormImageCrop = function () {

    var demo1 = function() {
        $('#pic_to_crop').Jcrop({
          aspectRatio: 1.5,   
          onSelect: updateCoords
        });

        function updateCoords(c)
          {
            $('#crop_x').val(c.x);
            $('#crop_y').val(c.y);
            $('#crop_w').val(c.w);
            $('#crop_h').val(c.h);
          };

          $('#demo8_form').submit(function(){
            if (parseInt($('#crop_w').val())) return true;
            alert('Please select a crop region then press submit.');
            return false;
            });
    }
     var handleResponsive = function() {
      if ($(window).width() <= 1024 && $(window).width() >= 678) {
        $('.responsive-1024').each(function(){
          $(this).attr("data-class", $(this).attr("class"));
          $(this).attr("class", 'responsive-1024 col-md-12');
        }); 
      } else {
        $('.responsive-1024').each(function(){
          if ($(this).attr("data-class")) {
            $(this).attr("class", $(this).attr("data-class"));  
            $(this).removeAttr("data-class");
          }
        });
      }
    }
    return {
        //main function to initiate the module
        init: function () {
            
            if (!jQuery().Jcrop) {;
                return;
            }

            Metronic.addResizeHandler(handleResponsive);
            handleResponsive();

            demo1();
        }

    };

}();

function getCropPlugin() {
    if ($(btn_to_view_img).hasClass('cropit')) {
        return 'cropit';
    } else {
        return 'default';
    }
}

function showCropPluginCropIt(img) {
    $('body').addClass("noscroll");
    $('.crop-section-core').addClass('swipe');
    $('#function_photo_gallery-core').show();

    var h = $(btn_to_view_img).closest('.sd-image-container').height();
    var w = $(btn_to_view_img).closest('.sd-image-container').width();

    $('.sd-crop-it-image-editor').cropit({
        quality: .9,
        originalSize: true,
        imageBackground: true,
        width: w,
        height: h,
        imageBackgroundBorderWidth: 15,
        smallImage: 'allow',
        maxZoom: 1.5
    });

    $('.sd-crop-it-image-editor').cropit('imageSrc', img);


    $('.saveCrop-core').click(function() {
        var imageData = $('.sd-crop-it-image-editor').cropit('export');

        $('.crop-it-img-src').val(imageData);

        $.ajax({
            type:'POST',
            url:base_url+'gallery/crop_it',
            data:{image_source: imageData},
            dataType:'json',
            beforeSend: function() {
                closeCropItModal();
                crop_image_response('loading','Saving your image...');
            },
            success:function(data) {
                $(btn_to_view_img).closest('.sd-image-container').css('background-image', 'url(' +data.image_filename+ ')');

                if($(btn_to_view_img).parent().hasClass('with-thumb'))
                {
                  $("#"+$(btn_to_view_img).parent().attr('data-thumb')).css("background-image",'url('+data.image_filename+')');
                }

                crop_image_response(true,'Finish.');
            }
        });


    });
}

function closeCropItModal() {
    $(".sd-publish").show();
    $('.edit-menu-left').removeClass('show',function(){
        $('.layer-3').removeClass('open-panel');
    });

    $('.crop-section-core').removeClass('swipe');
    $('body').removeClass("noscroll");
}

function viewImage(img)
{
    if (getCropPlugin() == 'cropit') {
        showCropPluginCropIt(img);
        return;
    }

    // Below is JCrop plugin (the default crop plugin)
  $(".load-for-crop").show();
  $("#sd-image-cropper").attr('src','');
  $("#sd-image-cropper").siblings().remove();
  
  if(!check_from)
  {
    //for tinymce callback
    if(typeof btn_to_view_img === 'string') {
      //alert(img);
      $('.layer-3').removeClass('open-panel');
      $('.edit-menu-left').removeClass('show');
      $('#' + btn_to_view_img).val(img);
    }
    else if(!$(btn_to_view_img).parent().hasClass('logo'))
    {
      $(".function-class-crop").hide();
      if ($('#sd-image-cropper').data('Jcrop')) {
         $('#sd-image-cropper').data('Jcrop').destroy();
         $('#sd-image-cropper').removeAttr('style');
      }
      $("#sd-image-cropper").attr('src',img);
      $("#src_img").val(img);
      $("#sd-image-cropper").load(function(){
        $(".load-for-crop").hide();
        $('#sd-image-cropper').Jcrop({
          aspectRatio: 1.5,
          onSelect: updateCoords_viewImage,
          // boxWidth:450,
          // boxHeight:400
        });
      });
       
       

        

        $('#demo8_form').submit(function(){
          if (parseInt($('#crop_w').val())) return true;
          alert('Please select a crop region then press submit.');
          return false;
          });

      $("#function_photo_gallery").show();
      $('body').addClass("noscroll");
      $('.crop-section').addClass('swipe');
    }else
    {
        $(".sd-publish").show();
        $('.edit-menu-left').removeClass('show',function(){
           $('.layer-3').removeClass('open-panel');
        });
        crop_image_response('loading','Putting your image...');
         $('<img/>').attr('src', img).load(function() {
            $(this).remove();
            $(btn_to_view_img).parent().css("background-image",'url('+img+')');
            crop_image_response(true,'Finish.');
             
        });
    }
  }

}

function updateCoords_viewImage(c)
  {
    $('#crop_x').val(c.x);
    $('#crop_y').val(c.y);
    $('#crop_w').val(c.w);
    $('#crop_h').val(c.h);
  }
$(document).ready(function(){
      $(".galleryShow").click(function(){
          check_from=false;
          btn_to_view_img=this;
          $('.edit-menu-left').addClass('show',function(){
             $('.layer-3').addClass('open-panel');
          });
         
      });
      
      $(".exitCrop").click(function(){
           $('.crop-section').removeClass('swipe');
           $('body').removeClass("noscroll");
      });
       $(".cancelCrop").click(function(){
           $('.crop-section').removeClass('swipe');
           $('body').removeClass("noscroll");
      });
        $(".cancelCrop-core").click(function(){
            $('.crop-section-core').removeClass('swipe');
            $('body').removeClass("noscroll");
        });
      $(".media-open").click(function(){
          check_from=true;
      });

      $("#form-crop-image").submit(function(){

         if (parseInt($('#crop_w').val()))
         {
          $(".sd-publish").show();
          var url = $(this).attr('data-action');
          url+='?x='+$("#crop_x").val();
          url+='&y='+$("#crop_y").val();
          url+='&w='+$("#crop_w").val();
          url+='&h='+$("#crop_h").val();
          url+='&src_img='+$("#src_img").val();
          
         $('.crop-section').removeClass('swipe');
         $('.edit-menu-left').removeClass('show',function(){
             $('.layer-3').removeClass('open-panel');
          });
         $('body').removeClass("noscroll");
         crop_image_response('loading','Cropping your image...');
         $('<img/>').attr('src', url).load(function() {
            $(this).remove();
            $(btn_to_view_img).parent().css("background-image",'url('+url+')');
            crop_image_response(true,'Finish cropping image.');

            if($(btn_to_view_img).parent().hasClass('with-thumb'))
            {
              $("#"+$(btn_to_view_img).parent().attr('data-thumb')).css("background-image",'url('+url+')');
            }

             
          });
        }else
        {
            alert('Please select a crop region then press submit.');
        }

    
        return false;
      });
});


function crop_image_response(status,message)
{
  if(status == 'loading')
  {
    $("#fullpageloader-response").addClass('show');
    $("#fullpageloader-response").addClass('show');
        var div = $('<div class="full-page-reloader"><p class="big center" >'+message+'</p></div>');
    $("#fullpageloader-response").append(div);
    $('body').addClass('noscroll');
        // window.setTimeout(function() {
        //   $("#fullpageloader-response").find('div').fadeTo(500, 0).slideUp(500, function(){
        //       $(this).remove();
        //   });
        // }, 5000);
  }else if(status == true)
  {

    
     setTimeout(function(){
        $("#fullpageloader-response").removeClass('show');
        $('body').removeClass('noscroll');
        $("#fullpageloader-response").find('.full-page-reloader').remove();
     },300);

  
  }else
  {

     setTimeout(function(){
        $("#fullpageloader-response").removeClass('show');
        $('body').removeClass('noscroll');
        $("#fullpageloader-response").find('.full-page-reloader').remove();
     },300);

  
       
  }
}