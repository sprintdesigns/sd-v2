var selected_form;
$(document).ready(function(){

	//if modal add form is close
	$('#sd-add-form-modal').on('hidden.bs.modal', function () {
	  $(".contact-form-response").html('');
	  $("#sd-form-name").val('');
	})

	//show modal for adding form
	$(".sd-add-form").click(function(){
		selected_form = $(this);
		$('#sd-add-form-modal').modal('show');
	});

	//add the form
	$("#sd-add-form-submit").click(function(){
		var form = new FormData(document.getElementById('sd-add-form-form'));

		if($("#sd-form-name").val() != '')
		{
			$.ajax({
				type:'POST',
				url:$("#sd-add-form-form").attr("data-action"),
				data:form,
				processData: false,
  				contentType: false,
  				dataType:'json',
  				success:function(data)
  				{
  					
  					//alert_message_contact_form(data.status,data.message);
  					if(data.status)
  					{
  						
  						location.reload();
  					}
  				}
			});
		}else
		{
			alert_message_contact_form(false,'Invalid contact form name');
		}
	});


	//use form
	$(".sd-use-form").click(function(){

		var action = $(this).parent().attr('data-action');
		var form_id = $(this).parent().find('select').val();
		var form = $(this).parent().next();

		$.ajax({
			type:'POST',
			url:action,
			data:{id:form_id},
			//dataType:'json',
			success:function(data)
			{
				form.html(data);
			}
		});
		
	});


});

function alert_message_contact_form(status,message)
{
	if(status)
	{
		$(".contact-form-response").html('<div class="alert alert-success"><strong>Success!</strong> '+message+'.</div>');

	}else
	{
		$(".contact-form-response").html('<div class="alert alert-danger"><strong>Error!</strong> '+message+'.</div>');
	}
}

//add field to form
function addField(form)
{
	
	var form_id = $(form).attr('data-form-id');
	var form_action= $(form).attr('data-form-action');
	var name = $("#form-add-field-"+form_id).find("#sd-form-field-name").val();
	var type = $("#form-add-field-"+form_id).find("#sd-form-field-type").val();

	if(name != "")
	{
		$.ajax({
			type:'post',
			url:form_action,
			data:{name:name,type:type,id:form_id},
			success:function(data)
			{
				$("#form-to-save").append(data);
				
			}

		});
	}
}


var sd_form_function = function () {

    return {

    	init: function () {

           //if modal add form is close
			$('#sd-add-form-modal').on('hidden.bs.modal', function () {
			  $(".contact-form-response").html('');
			  $("#sd-form-name").val('');
			})

			//show modal for adding form
			$(".sd-add-form").click(function(){
				selected_form = $(this);
				$('#sd-add-form-modal').modal('show');
			});

			//add the form
			$("#sd-add-form-submit").click(function(){
				var form = new FormData(document.getElementById('sd-add-form-form'));

				if($("#sd-form-name").val() != '')
				{
					$.ajax({
						type:'POST',
						url:$("#sd-add-form-form").attr("data-action"),
						data:form,
						processData: false,
		  				contentType: false,
		  				dataType:'json',
		  				success:function(data)
		  				{
		  					
		  					//alert_message_contact_form(data.status,data.message);
		  					if(data.status)
		  					{
		  						
		  						location.reload();
		  					}
		  				}
					});
				}else
				{
					alert_message_contact_form(false,'Invalid contact form name');
				}
			});


			//use form
			$(".sd-use-form").click(function(){

				var action = $(this).parent().attr('data-action');
				var form_id = $(this).parent().find('select').val();
				var form = $(this).parent().next();

				$.ajax({
					type:'POST',
					url:action,
					data:{id:form_id},
					//dataType:'json',
					success:function(data)
					{
						form.html(data);
					}
				});
				
			});



        }
    };

}();



//admin contact form

$(document).ready(function(){


	if($('#contact-form').length)
		$('#contact-form').DataTable();

	$("#contact-form-add-email").submit(function(){
		var data = new FormData(document.getElementById('contact-form-add-email'));
		var url = $(this).attr("data-action");
		$.ajax({
			type:'post',
			url:url,
			data:data,
			processData: false,
			contentType: false,
			dataType:'json',
			success:function(data)
			{
				console.log(data);
			}
		});
		return false;
	});
});

function removeField(id)
{
	$(id).parent().remove();

}

function save_form(save)
{
	var url = $(save).attr('data-action');
	var data = new FormData();
	var form_id = $("#form-to-save").attr('data-form-id');

	$(".input-form-to-save").each(function(){
		data.append('name[]',$(this).attr('data-name'));
		data.append('type[]',$(this).attr('data-type'));
	});

	data.append('form_id',form_id);
	

	$.ajax({
		type:'post',
		url:url,
		data:data,
		processData: false,
		contentType: false,
		success:function(data)
		{
			location.reload();
		}
	});
}
