$(document).ready(function(){
  $(".sd-pannel-out").hide();
  $(".truncate").click(function(){
    $(".edit-menu-left").addClass("show");
  });
  $(".layer-close").click(function(){
    $(".edit-menu-left").removeClass("show");
    var panel = $(this).attr('data-panel');
    $('.layer-'+panel).removeClass('open-panel');
     $(".has-layer").removeClass('highlight');
      $(".sd-pannel-out").hide();
    
  });


  $('.btn-show-menu').click(function(){
    $('.edit-menu-left').addClass('show');
  });
  $('.close-edit-menu').click(function(){
    $('.edit-menu-left').removeClass('show');
    $('.layer-close').trigger('click');
    $('.layer-close-2').trigger('click');
    $(".has-layer").removeClass('highlight');
    $(".sd-pannel-out").hide();
    
   
  });

  $(".has-layer").click(function(){
    var layer= $(this).attr('data-layer');
    if($('.layer-'+layer).hasClass('open-panel')) {
      $('.layer-'+layer).removeClass('open-panel');
      $('.edit-menu-left').removeClass('show');
      $(this).removeClass('highlight');
      $(".sd-pannel-out").hide();
      
    }
    else {
      $('.all-layer').removeClass('open-panel');
      $('.layer-'+layer).addClass('open-panel');
      $(".has-layer").removeClass('highlight');
      $(this).addClass('highlight');
      $(".sd-pannel-out").show(function(){
        if(layer == "2")
        {
          var tutorial = $("#check-tutorial").val();
          if(tutorial == "true")
          {
            if($("#check-tutorial-page-manager").val() == "false")
            {
              $(".overlay-tutorials").addClass("run");
              $(".overlay-tutorials").show();
              $(".page-management-cover").addClass("show");
              $("#check-tutorial-page-manager").val("true");
              $(".hole").addClass("show-12");
              $(".sd-tutorials").each(function(){
                if($(this).attr("data-target") == "first-load")
                {
                  $(this).find('i').removeClass("hidden");
                  $(this).find('img').addClass("hidden");
                }
              });
            }
            
          }
        }
      });
      
      
      
    }   


  });

  $(".colorpaint").click(function(){
    $(".sd-publish").show();
    $('.colorpaint').each(function(){
      $('body').removeClass($(this).attr('data-color')+'-template');
    });
    if($(this).attr('data-color') != 'default')
    {
      $('body').addClass($(this).attr('data-color')+'-template');
      $('#body-color-template').val($(this).attr('data-color')+'-template');
    }else
    {
      $('body').addClass('default');
      $('#body-color-template').val('default');
    }
    
    
  });

  $(".sd-pannel-out").click(function(){
    $('.edit-menu-left').removeClass('show');
    $('.layer-close').trigger('click');
    $('.layer-close-2').trigger('click');
    $(".has-layer").removeClass('highlight');
    $(".sd-pannel-out").hide();
    
  });

  $('.dd-handle.dd3-handle').hover(function(){
  $(this).parent().addClass('hoverme');
}, function() { $(this).parent().removeClass('hoverme'); });

//check if there is need to save
  $(window).on('beforeunload', function(){
    var need_to_save_nav = false;
    var need_to_save_slide = false;
    var need_to_save_page = false;

    var str_need_to_save = "";    
      if($(".btn-save-nav-menu:visible").length > 0){
        need_to_save_nav = true;
        str_need_to_save += "Page Manager Section,";
      }
     if($(".btn-save-slide-changes:visible").length > 0){
        need_to_save_slide = true;
        str_need_to_save += "Slider Settings,";
     }
     if($(".sd-publish:visible").length > 0){
        need_to_save_page = true;
        str_need_to_save += "Save Website,";
     }

    if($(".btn-save-nav-menu").data('clicked'))
    {
      need_to_save_nav = false;
    }
    if($(".btn-save-slide-changes").data('clicked'))
    {
      need_to_save_slide = false;
    }
    if($(".btn-save-website").data('clicked'))
    {
      
      need_to_save_page = false;
    }

    if(need_to_save_nav || need_to_save_slide || need_to_save_page)
    {
      console.log(need_to_save_nav+' '+need_to_save_slide+' '+need_to_save_page);
      return "You need to save the changes in "+str_need_to_save+" before leaving this page or else you changes will not save";
    }
});

//tutorial setup in first login

var tutorial = $("#check-tutorial").val();

if(tutorial == "true")
{
  $(".overlay-tutorials").addClass("run");
  $(".first").addClass("show");

  $(".sd-tutorials").each(function(){
    if($(this).attr("data-target") == "first-load")
    {
      $(this).find('i').removeClass("hidden");
      $(this).find('img').addClass("hidden");
    }
  });
}

$(".btn-switch-website-title").click(function(){
    var switch_to = $(this).attr("data-switch");

    if(switch_to == "text")
    {
      $(this).attr("data-switch",'image');
      $("#sd-page-logo").addClass("hidden");
      $(".sd-img-logo").removeClass("hidden");
      $(this).html('switch to text');
    }

    if(switch_to == "image")
    {
      $(this).attr("data-switch",'text');
      $("#sd-page-logo").removeClass("hidden");
      $(".sd-img-logo").addClass("hidden");
      $(this).html('switch to logo');
    }
});

});