var change_side_image_go = false;
var change_side_image_id = "";
var global_banner_upload_resp = "";
  $( function() {


   


    $(".slider-gallery-preloader").hide();
    $('.btn-save-slide-changes').hide();

    banner_slider_init();
   
    

    $(".slider-gallery-close").click(function(){
        $(".slider-gallery-show").removeClass('show');
    });
    
    $('.sd-slmenu').on('click', function() {
      if($('.slider-list-menu').hasClass('show')) {
        $('.slider-list-menu').removeClass('show');
      }
      else {
        $('.slider-list-menu').addClass('show'); 
      }
    });

    $('.sd-slmenu').hover(
      function() {
       $('.slider-list-menu').addClass('onhover');    
    }, function() {
       if($('.slider-list-menu').hasClass('show')) {
        $('.slider-list-menu').removeClass('show');
        }
        else {
          $('.slider-list-menu').addClass('show'); 
        }  
    });
    $('.relative.sd-sl').hover(function(){ }, function() { $('.slider-list-menu').removeClass('onhover');  });

    $('.open-sliderbanner-panel').click(function(){
      $('.edit-menu-left').addClass('show');
      $('.layer-3.all-layer').removeClass('open-panel'); 
      $('.layer-2.all-layer').removeClass('open-panel'); 
      $('.layer-4.all-layer').addClass('open-panel');
      $(".sd-pannel-out").show();
    });

    $('.open-sliderbanner-then-upload').click(function(){
      $('.edit-menu-left').addClass('show');
      $('.layer-3.all-layer').removeClass('open-panel'); 
      $('.layer-2.all-layer').removeClass('open-panel'); 
      $('.layer-4.all-layer').addClass('open-panel');
      $(".btn-add-slide").trigger('click');
      $(".sd-pannel-out").show();
      $("#slide_id_new_image").val('none');
    });


    Dropzone.options.bannerUpload = {
        // previewTemplate: document.querySelector('#preview-template').innerHTML,
        paramName: 'file',
        // maxFilesize: 2, // MB
        //maxFiles: 1,
        parallelUploads: 1,
        acceptedFiles: 'image/*',
        addRemoveLinks: true,
        dictDefaultMessage: 'Drag an image here to upload, or click to select one',
        init: function() {  
          this.on('sending',function(file, xhr, formData){
              if($("#slide_id_new_image").val() == 'none'){
                slider_settings_response('loading','Uploading and creating slide...');
              }else
              {
                slider_settings_response('loading','Uploading and changing slide image...');
              }
          });
          this.on('success', function( file, resp ){
              global_banner_upload_resp = jQuery.parseJSON(resp);
              $(".gallery-img-"+global_banner_upload_resp.img_id).attr('data-use','true');

          });
          this.on("complete", function(file,resp) {
              if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
                var _this = this;
                // Remove all files
                _this.removeAllFiles();  
                slider_settings_response(global_banner_upload_resp.status,global_banner_upload_resp.message);
                $(".gallery-img-"+global_banner_upload_resp.img_id).attr('data-use','true');
                $(".slider-gallery-show").removeClass('show');
                $('.slideview').html(global_banner_upload_resp.slide);
                $(".slidelist").html(global_banner_upload_resp.menu_slide);
                banner_slider_init();
                load_banner_gallery=true;
                reload_images=true;


              }
              
             
             
          });

        }
        
    };

  } );
  
  function banner_slider_init()
  {

     // off slide title
    if(sd_slide_title == false)
    {
      $("#sd-slide-title-view").hide();
      $("#sd-slide-title").addClass("disabled-function");
    }

    // off slide button
    if(sd_slide_btn_text == false)
    {
      $("#sd-slide-btn-text-view").hide();
      $("#sd-slide-btn-text").addClass("disabled-function");
      $("#sd-slide-btn-url").addClass("disabled-function");
    }

    // off slide sub title
    if(sd_slide_sub == false)
    {
      $("#sd-slide-sub-view").hide();
      $("#sd-slide-sub").addClass("disabled-function");
    }

    var opac = $( ".ls-sd-overlay" );

    $( ".opac-slider" ).slider({
      max: 100,
      min: 0,
      value: 0,
      range:'min',
      value:100 - (parseFloat($("#sd-slide-opacity").val())*100),
      create: function() {
        // handle.text( $( this ).slider( "value" ) );
      },
      slide: function( event, ui ) {
        opac.css('opacity', ((100 - ui.value) / 100));
        $("#sd-slide-opacity").val((100 - ui.value) / 100);
        $('.btn-save-slide-changes').show();

      }
    });

    $('#sd-slide-title').keyup(function () {
     $('#sd-slide-title-view').text($(this).val());
     $('.btn-save-slide-changes').show();

    });

    $('#sd-slide-sub').keyup(function () {
     $('#sd-slide-sub-view').text($(this).val());
     $('.btn-save-slide-changes').show();
    });

    $('#sd-slide-btn-text').keyup(function () {
      
      if($(this).val().trim() == "")
      {
        $('#sd-slide-btn-text-view').text($(this).val()).hide();
        // $("#sd-slide-btn-url").prop('disabled', true);
      }else
      {
        $('#sd-slide-btn-text-view').text($(this).val()).show();
        // $("#sd-slide-btn-url").prop('disabled', false);
      }
     
     $('.btn-save-slide-changes').show();
    });

    $('#sd-slide-btn-url').change(function(){
      $('.btn-save-slide-changes').show();
    });




    $("#form-crop-slide").submit(function(){

       if (parseInt($('#crop_w').val()))
       {
    
        var url = $(this).attr('data-action');
        url+='?x='+$("#crop_x").val();
        url+='&y='+$("#crop_y").val();
        url+='&w='+$("#crop_w").val();
        url+='&h='+$("#crop_h").val();
        url+='&src_img='+$("#src_img").val();
        //$(btn_to_view_img).parent().css("background-image",'url('+url+')');
        $("#sd-slide-image-view").attr('src',url);
        $("#sd-slide-image").val(url);
        $('.crop-section').removeClass('swipe');
        $('body').removeClass("noscroll");
        slider_settings_response('loading','Cropping your image...');
        $("#sd-slide-image-view").on('load',function(){
            slider_settings_response(true,'Finish cropping image.');
           $(".btn-save-slide-changes").show();
        });
      
       
      }else
      {
          alert('Please select a crop region then press submit.');
      }

  
      return false;
    });

    $(".custom-menu li").click(function(){
    
      // This is the triggered action name
      switch($(this).attr("data-action")) {
          
          // A case for each action. Your actions here
          case "first": $(".btn-add-slide").trigger('click');
                       break;
          case "second": alert("second"); break;
          case "third": alert("third"); break;
      }
    
      // Hide it AFTER the action was triggered
      $(".custom-menu").hide(100);
    });


    $(".btn-add-slide").click(function(){
      change_side_image_go = false;
      var check_if_from_marketing = $(this).hasClass('marketing-upload');
      $("#slide_id_new_image").val('none');
      if(load_banner_gallery)
      {
        $("#banner-gallery-container").html('');
        $(".slider-gallery-show").addClass('show');
        $(".slider-gallery-preloader").show();
        $.ajax({
          type:'POST',
          url:base_url+'banner/banner_images',
          success:function(data)
          {
            $(".slider-gallery-preloader").hide();
            if(check_if_from_marketing)
            {
              $("#marketing-gallery-container").html(data); 
            }else
            {
              $("#banner-gallery-container").html(data);  
            }
            
            load_banner_gallery=false;
          }
        });
      }else
      {
        $(".slider-gallery-show").addClass('show');
      }
    });
  }

  function select_slide(index,slide_id)
  {
    $('.btn-save-slide-changes').hide();
    slider_settings_response('loading','Slider is switching...');
    
    $.ajax({
      type:'POST',
      url:base_url+'banner/get_slide',
      data:{slide_id:slide_id},
      dataType:'json',
      success:function(data)
      {
        slider_settings_response(data.status,data.message);
        if(data.status)
        {   

          $(".sel-slider").removeClass('sd-slider-active');
          $(".slider-index-"+index).addClass('sd-slider-active');

          $('.slideview').html(data.slide);
          banner_slider_init();

        }
      }
    });
  }

function save_slide_changes()
{
    slider_settings_response('loading','please wait...');

    var slide_data = new FormData(document.getElementById('form-save-create-slide'));

     $.ajax({
      type:'POST',
      url:base_url+'banner/save_slide',
      data:slide_data,
      dataType:'json',
      cache: false,
      processData: false,
      contentType: false,
      success:function(data)
      {
       
        slider_save_response
        if(data.status)
        {
          $(".btn-save-slide-changes").hide();
          slider_save_response(data.message);
        }else
        {
           slider_settings_response(data.status,data.message);
        }
      }
    });
}

function crop_slide_img(img)
{
  $(".function-class-crop").hide();
 
  if ($('#sd-image-cropper').data('Jcrop')) {
     $('#sd-image-cropper').data('Jcrop').destroy();
     $('#sd-image-cropper').removeAttr('style');
  }
  $("#sd-image-cropper").attr('src',img); 
   $("#src_img").val(img);
   $('#sd-image-cropper').Jcrop({
      aspectRatio: 1.9,
      onSelect: updateCoords_viewImage
    });    

    $('#demo8_form').submit(function(){
      if (parseInt($('#crop_w').val())) return true;
      alert('Please select a crop region then press submit.');
      return false;
      });

  $("#function_slider_settings").show();
  $('body').addClass("noscroll");
  $('.crop-section').addClass('swipe');
}

function change_side_image(slide_id)
{
  change_side_image_id = slide_id;
  change_side_image_go = true;
  $("#slide_id_new_image").val(slide_id);
  if(load_banner_gallery)
  {
    $("#banner-gallery-container").html('');
    $(".slider-gallery-show").addClass('show');
    $(".slider-gallery-preloader").show();
    $.ajax({
      type:'POST',
      url:base_url+'banner/banner_images',
      success:function(data)
      {
        $(".slider-gallery-preloader").hide();
        $("#banner-gallery-container").html(data);
        load_banner_gallery=false;
      }
    });
  }else
  {
    $(".slider-gallery-show").addClass('show');
  }
}

function create_new_slide(url,id)
{
  $('.btn-save-slide-changes').hide();
  
  if(change_side_image_go)
  {
    slider_settings_response('loading','Changing image...');
    $.ajax({
      type:'POST',
      url:base_url+'banner/change_slide_image',
      data:{url:url,slide_id:change_side_image_id},
      dataType:'json',
      success:function(data)
      {

        slider_settings_response(data.status,data.message);
        if(data.status)
        {   
          $(".gallery-img-"+id).attr('data-use','true');
          $(".slider-gallery-show").removeClass('show');
          $('.slideview').html(data.slide);
          $(".slidelist").html(data.menu_slide);
          banner_slider_init();

        }
      }
    });
  }else
  {

    slider_settings_response('loading','Creating slide...');
     $.ajax({
      type:'POST',
      url:base_url+'banner/create_slide',
      data:{url:url},
      dataType:'json',
      success:function(data)
      {

        slider_settings_response(data.status,data.message);
        if(data.status)
        {   
          $(".gallery-img-"+id).attr('data-use','true');
          $(".slider-gallery-show").removeClass('show');
          $('.slideview').html(data.slide);
          $(".slidelist").html(data.menu_slide);
          banner_slider_init();

        }
      }
    });
  } 
 
}

function delete_slide(index,slide_id,img_id)
{
  var con_delete = confirm('This action will delete the slide, are you sure you want to delete the slide?');

  if(con_delete)
  {
    slider_settings_response('loading','please wait. . ');

    $.ajax({
      type:'POST',
      url:base_url+'banner/delete_slide',
      data:{slide_id:slide_id},
      dataType:'json',
      success:function(data)
      {

        slider_settings_response(data.status,data.message);

        if(data.status)
        {   
          if(img_id != 'false')
          {
            $(".gallery-img-"+img_id).attr('data-use','false');
          }
          $(".parent-slider-index-"+index).addClass('removeout');

          setTimeout(function(){
            $('.slideview').html(data.slide);
            $(".slidelist").html(data.menu_slide);
            banner_slider_init();
          },400);
          

        }
      }
    });

  }
  

}


function slider_settings_response(status,message)
{
  if(status == 'loading')
  {
    $("#fullpageloader-response").addClass('show');
    $("#fullpageloader-response").addClass('show');
        var div = $('<div class="full-page-reloader"><p class="big center" >'+message+'</p></div>');
    $("#fullpageloader-response").append(div);
    $('body').addClass('noscroll');
        // window.setTimeout(function() {
        //   $("#fullpageloader-response").find('div').fadeTo(500, 0).slideUp(500, function(){
        //       $(this).remove();
        //   });
        // }, 5000);
  }else if(status == true)
  {

    
     setTimeout(function(){
        $("#fullpageloader-response").removeClass('show');
        $('body').removeClass('noscroll');
        $("#fullpageloader-response").find('.full-page-reloader').remove();
        var div = $('<div class="alert alert-success"><strong>Success!</strong> '+message+'.</div>').fadeOut(5000);
        $("#slider-settings-response").html(div);
        window.setTimeout(function() {
          $("#slider-settings-response").find('div').fadeTo(1000, 0).slideUp(1000, function(){
              $(this).remove();
          });
        }, 5000);
     },300);

     
  }else
  {

     setTimeout(function(){
        $("#fullpageloader-response").removeClass('show');
        $('body').removeClass('noscroll');
        $("#fullpageloader-response").find('.full-page-reloader').remove();
        var div = $('<div class="alert alert-danger"><strong>Error!</strong> '+message+'.</div>').fadeOut(5000);
        $("#slider-settings-response").html(div);
         window.setTimeout(function() {
          $("#slider-settings-response").find('div').fadeTo(500, 0).slideUp(500, function(){
              $(this).remove();
          });
    }, 5000);
     },300);

   
       
  }
}




function slider_save_response(message)
{
  setTimeout(function(){
      $("#fullpageloader-response").removeClass('show');
      $('body').removeClass('noscroll');
      $("#fullpageloader-response").find('.full-page-reloader').remove();
      var div = $('<div class="alert alert-success"><strong>Success!</strong> '+message+'.</div>').fadeOut(5000);
      $("#slider-settings-response").html(div);
    
   },300);
}

// JAVASCRIPT (jQuery) add the right click on panels

// Trigger action when the contexmenu is about to be shown
$(document).bind("contextmenu", function (event) {
  if (!$(event.target).is(".slidelist")) {
       console.log('bad');
     } else {
    // Avoid the real one
    event.preventDefault();
    console.log('good');
    // Show contextmenu
    $(".custom-menu").finish().toggle(100).
    
    // In the right position (the mouse)
    css({
        top: event.clientY + "px",
        left: (event.clientX - 50) + "px"
    });
    console.log(event.clientY);
    }
});


// If the document is clicked on slidelist
$(document).bind("mousedown", function (e) {
    
    // If the clicked element is not the menu
    if (!$(e.target).parents(".custom-menu").length > 0) {
        
        // Hide it
        $(".custom-menu").hide(100);
    }
});
 

// If the menu element is clicked

