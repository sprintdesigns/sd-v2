-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 11, 2016 at 07:47 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wordpress`
--

-- --------------------------------------------------------

--
-- Table structure for table `sd_incs`
--

CREATE TABLE `sd_incs` (
  `id` int(6) UNSIGNED NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `type` varchar(30) DEFAULT NULL,
  `banner` varchar(30) DEFAULT NULL,
  `description` text,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sd_incs`
--

INSERT INTO `sd_incs` (`id`, `name`, `type`, `banner`, `description`, `status`) VALUES
(2, 'sample template', 'theme', 'base-image.png', 'This is description of sample template', 0),
(3, 'maro template', 'theme', 'base-image.png', 'This is description of sample template', 0),
(4, 'mer template', 'theme', 'base-image.png', 'This is description of sample template', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sd_login`
--

CREATE TABLE `sd_login` (
  `id` int(6) UNSIGNED NOT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sd_login`
--

INSERT INTO `sd_login` (`id`, `username`, `password`, `name`, `email`) VALUES
(1, 'wanjoh', 'Ar2Aaz12jTAaTERCM7QIrP9I9N1wqKXiZyB9Q_zrjFA', 'sample site', 'jouen.rectog160403@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `sd_metadata`
--

CREATE TABLE `sd_metadata` (
  `id` int(6) UNSIGNED NOT NULL,
  `user_id` int(6) DEFAULT NULL,
  `data_id` int(6) DEFAULT NULL,
  `data_value` varchar(50) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sd_nav_menus`
--

CREATE TABLE `sd_nav_menus` (
  `id` int(6) UNSIGNED NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `page_id` int(16) DEFAULT NULL,
  `orders` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sd_options`
--

CREATE TABLE `sd_options` (
  `id` int(6) UNSIGNED NOT NULL,
  `Site_name` varchar(30) NOT NULL,
  `theme_name` varchar(30) NOT NULL,
  `theme_version` varchar(50) DEFAULT NULL,
  `site_title` varchar(50) DEFAULT NULL,
  `base_url` varchar(50) DEFAULT NULL,
  `custom_color` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sd_pages`
--

CREATE TABLE `sd_pages` (
  `id` int(6) UNSIGNED NOT NULL,
  `page_title` varchar(30) DEFAULT NULL,
  `template_name` varchar(30) DEFAULT NULL,
  `page_content` varchar(50) DEFAULT NULL,
  `parent` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sd_posts`
--

CREATE TABLE `sd_posts` (
  `id` int(6) UNSIGNED NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `post_type` varchar(30) DEFAULT NULL,
  `post_content` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sd_incs`
--
ALTER TABLE `sd_incs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sd_login`
--
ALTER TABLE `sd_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sd_metadata`
--
ALTER TABLE `sd_metadata`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sd_nav_menus`
--
ALTER TABLE `sd_nav_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sd_options`
--
ALTER TABLE `sd_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sd_pages`
--
ALTER TABLE `sd_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sd_posts`
--
ALTER TABLE `sd_posts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sd_incs`
--
ALTER TABLE `sd_incs`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sd_login`
--
ALTER TABLE `sd_login`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sd_metadata`
--
ALTER TABLE `sd_metadata`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sd_nav_menus`
--
ALTER TABLE `sd_nav_menus`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sd_options`
--
ALTER TABLE `sd_options`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sd_pages`
--
ALTER TABLE `sd_pages`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sd_posts`
--
ALTER TABLE `sd_posts`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
