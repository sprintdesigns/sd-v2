 <?php
 	define('BASEPATH',true);
	include 'application/libraries/Rx_utilities.php';

	$rx_utilities = new Rx_Utilities();

 	$sd_object = file_get_contents('corefile/sd.json');
	$sd_json = $sd_object = json_decode($sd_object);

	$servername = $sd_json->dbhost;
	$username =  $sd_json->username;
	$password = $sd_json->password;
	$dbname = $sd_json->dbname;
	$dberror = false;

	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	}

	//create table sd_options

	$sql = "CREATE TABLE sd_options (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	Site_name VARCHAR(30) NOT NULL,
	theme_name VARCHAR(30) NOT NULL,
	theme_version VARCHAR(50),
	site_title VARCHAR(50),
	base_url VARCHAR(50),
	custom_color VARCHAR(50)
	)";

	if ($conn->query($sql) === TRUE) {
	  
	} else {
	    echo "Error creating table sd_options: " . $conn->error;
	    $dberror = true;
	}

	// end create table sd_options

	//create table sd_login

	$sql = "CREATE TABLE sd_login (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	username VARCHAR(30),
	password VARCHAR(100),
	name VARCHAR(50),
	email VARCHAR(50)
	)";

	if ($conn->query($sql) === TRUE) {
	  
	} else {
	    echo "Error creating table sd_login: " . $conn->error;
	    $dberror = true;
	}

	// end create table sd_login

	//create table sd_metadata

	$sql = "CREATE TABLE sd_metadata (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	user_id INT(6),
	data_id INT(6),
	data_value VARCHAR(50),
	status BOOLEAN
	)";

	if ($conn->query($sql) === TRUE) {
	  
	} else {
	    echo "Error creating table sd_metadata: " . $conn->error;
	    $dberror = true;
	}

	// end create table sd_metadata

	//create table sd_pages

	$sql = "CREATE TABLE sd_pages (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	page_title VARCHAR(30),
	page_url VARCHAR(30),
	template_name VARCHAR(30),
	meta_description TEXT,
	page_content TEXT,
	theme_id int(6),
	type BOOLEAN,
	status BOOLEAN,
	clone INT(6),
	is_external tinyint(4)
	)";

	if ($conn->query($sql) === TRUE) {
	  
	} else {
	    echo "Error creating table sd_pages: " . $conn->error;
	    $dberror = true;
	}

	// end create table sd_pages


	//create table sd_posts

	$sql = "CREATE TABLE sd_posts (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(30),
	post_type VARCHAR(30),
	post_content VARCHAR(50)
	)";

	if ($conn->query($sql) === TRUE) {
	  
	} else {
	    echo "Error creating table sd_posts: " . $conn->error;
	    $dberror = true;
	}

	// end create table sd_posts

	//create table sd_incs 

	$sql = "CREATE TABLE sd_incs  (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(30),
	type VARCHAR(30),
	banner VARCHAR(30),
	description TEXT,
	author TEXT,
	color VARCHAR(20),
	status BOOLEAN

	)";

	if ($conn->query($sql) === TRUE) {
	  
	} else {
	    echo "Error creating table sd_incs : " . $conn->error;
	    $dberror = true;
	}

	// end create table sd_incs 

	//create table sd_nav_menus 

	$sql = "CREATE TABLE sd_nav_menus  (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(30),
	orders TEXT,
	theme_id INT(6)
	)";

	if ($conn->query($sql) === TRUE) {
	  
	} else {
	    echo "Error creating table sd_nav_menus : " . $conn->error;
	    $dberror = true;
	}

	// end create table sd_nav_menus


	//create table sd_site 

	$sql = "CREATE TABLE sd_site  (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(30)
	)";

	if ($conn->query($sql) === TRUE) {
	  
	} else {
	    echo "Error creating table sd_site : " . $conn->error;
	    $dberror = true;
	}

	// end create table sd_site

	//create table sd_forms 

	$sql = "CREATE TABLE sd_forms  (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(30),
	is_main BOOLEAN,
	created_by VARCHAR(30),
	status BOOLEAN,
	create_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	clone_from INT(6)
	)";

	if ($conn->query($sql) === TRUE) {
	  
	} else {
	    echo "Error creating table sd_forms : " . $conn->error;
	    $dberror = true;
	}

	// end create table sd_forms

	//create table sd_form_data 

	$sql = "CREATE TABLE sd_form_data  (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(30),
	label VARCHAR(30),
	type VARCHAR(30),
	position INT(6),
	form_id INT(6),
	status BOOLEAN,
	required BOOLEAN,
	clone_from INT(6)
	)";

	if ($conn->query($sql) === TRUE) {
	  
	} else {
	    echo "Error creating table sd_form_data : " . $conn->error;
	    $dberror = true;
	}

	// end create table sd_form_data


	//create table sd_form_settings 

	$sql = "CREATE TABLE sd_form_settings (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	to_email TEXT,
	from_email TEXT,
	subject TEXT,
	message TEXT,
	type VARCHAR(45),
	form_id INT(6)
	)";

	if ($conn->query($sql) === TRUE) {
	  
	} else {
	    echo "Error creating table sd_form_settings : " . $conn->error;
	    $dberror = true;
	}

	// end create table sd_form_settings

	//create table sd_gallery

	$sql = "CREATE TABLE sd_gallery (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	image_data TEXT NOT NULL,
	create_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	)";

	if ($conn->query($sql) === TRUE) {
	  
	} else {
	    echo "Error creating table sd_gallery: " . $conn->error;
	    $dberror = true;
	}

	// end create table sd_gallery

	//create table sd_template

	$sql = "CREATE TABLE sd_template (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	template_name VARCHAR(30),
	file VARCHAR(30),
	theme_id int(6)
	)";

	if ($conn->query($sql) === TRUE) {
	  
	} else {
	    echo "Error creating table sd_template: " . $conn->error;
	    $dberror = true;
	}

	// end create table sd_template

	//create table sd_header

	$sql = "CREATE TABLE sd_header (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	page_logo TEXT,
	page_top_right TEXT,
	page_bottom_right TEXT,
	site_title TEXT
	)";

	if ($conn->query($sql) === TRUE) {
	  
	} else {
	    echo "Error creating table sd_header: " . $conn->error;
	    $dberror = true;
	}

	// end create table sd_header

	//create table sd_slider

	$sql = "CREATE TABLE sd_slider (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	url TEXT,
	original_url TEXT,
	text_title TEXT,
	sub_title TEXT,
	opacity VARCHAR(45),
	button_title VARCHAR(45),
	button_url VARCHAR(45),
	theme_id INT(6)
	)";

	if ($conn->query($sql) === TRUE) {
	  
	} else {
	    echo "Error creating table sd_slider: " . $conn->error;
	    $dberror = true;
	}

	// end create table sd_slider


	//create table sd_column

	$sql = "CREATE TABLE sd_column (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	title VARCHAR(45),
	file VARCHAR(45),
	class_img VARCHAR(45),
	theme_id INT(6)
	)";

	if ($conn->query($sql) === TRUE) {
	  
	} else {
	    echo "Error creating table sd_column: " . $conn->error;
	    $dberror = true;
	}

	// end create table sd_column

	//create table sd_social

	$sql = "CREATE TABLE sd_social (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	social TEXT,
	theme_id INT(6)
	)";

	if ($conn->query($sql) === TRUE) {
	  
	} else {
	    echo "Error creating table sd_social: " . $conn->error;
	    $dberror = true;
	}
	
	// end create table sd_social

	//create table sd_marketing

	$sql = "CREATE TABLE IF NOT EXISTS sd_marketing (
	  id int(6) unsigned NOT NULL AUTO_INCREMENT,
	  banner text,
	  title varchar(30) NOT NULL,
	  description text,
	  page_url text,
	  PRIMARY KEY (id)
	)";

	if ($conn->query($sql) === TRUE) {
	  
	} else {
	    echo "Error creating table sd_marketing: " . $conn->error;
	    $dberror = true;
	}

	// end create table sd_marketing	

	//insert marketing banner
    for ($i=0; $i < 10; $i++) { 
        $sql = "INSERT INTO sd_marketing (banner,title,description,page_url)
        VALUES ('','banner ".($i+1)."','','".$_POST['base_url']."/home')";
        if ($conn->query($sql) === TRUE) {
          
        } else {
            echo "Error creating page info: " . $conn->error;
            $dberror = true;
        }
    }
    // end insert marketing banner

	//create table categories
    $sql = "CREATE TABLE IF NOT EXISTS categories (
		id int(11) NOT NULL AUTO_INCREMENT,
		category_name varchar(255) NOT NULL,
		category_image varchar(350),
		PRIMARY KEY (id)
	)";

	if ($conn->query($sql) === TRUE) {
	  
	} else {
	    echo "Error creating table sd_column: " . $conn->error;
	    $dberror = true;
	}	
	//end create table categories

	//create table messages
	$sql = "CREATE TABLE IF NOT EXISTS messages (
		id int(11) NOT NULL AUTO_INCREMENT,
		thankyou_message varchar(350),
		note_message varchar(350),
		purchase_message varchar(350),
		PRIMARY KEY (id)	
	)";

	if ($conn->query($sql) === TRUE) {
	  
	} else {
	    echo "Error creating table sd_column: " . $conn->error;
	    $dberror = true;
	}	
	//end create table messages

	//create table orderinfo
	$sql = "CREATE TABLE IF NOT EXISTS orderinfo (
	  	id int(11) NOT NULL AUTO_INCREMENT,
		email varchar(50) NOT NULL,
	  	cust_name varchar(40) NOT NULL,
	  	cust_address varchar(100),
	  	cust_number int(11),
	  	mode_payment varchar(20),
	  	quantity int(11),
	  	color varchar(100),
	  	size varchar(20),
	  	prodname varchar(50),
	  	price float,
	  	purchase_date datetime NOT NULL,
	  	PRIMARY KEY (id)
	)";

	if ($conn->query($sql) === TRUE) {
	  
	} else {
	    echo "Error creating table sd_column: " . $conn->error;
	    $dberror = true;
	}	
	//end create table orderinfo

	//create table orders
	$sql = "CREATE TABLE IF NOT EXISTS orders (
		id int(11) NOT NULL AUTO_INCREMENT,
		product_id int(11) NOT NULL,
		email varchar(50) NOT NULL,
		cust_name varchar(40) NOT NULL,
		cust_address varchar(100),
		cust_number int(11),
		mode_payment varchar(20),
		quantity int(11),
		color varchar(100),
		size varchar(20),
		prodname varchar(50),
		price float,
		status varchar(30) NOT NULL,
		purchase_date datetime NOT NULL,
		PRIMARY KEY (id)
	)";

	if ($conn->query($sql) === TRUE) {
	  
	} else {
	    echo "Error creating table sd_column: " . $conn->error;
	    $dberror = true;
	}	
	//end create table orders

	//create table photos
	$sql = "CREATE TABLE IF NOT EXISTS photos (
		id int(11) NOT NULL AUTO_INCREMENT,
		image varchar(255) DEFAULT NULL,
		prodid int(11) DEFAULT NULL,
		product_id int(11),
		label varchar(150),
		is_default varchar(20) NOT NULL COMMENT 'Yes or No',
		PRIMARY KEY (id),
		KEY prodid (prodid)
	)";

	if ($conn->query($sql) === TRUE) {
	  
	} else {
	    echo "Error creating table sd_column: " . $conn->error;
	    $dberror = true;
	}	
	//end create table photos	

	//create table products
	$sql = "CREATE TABLE IF NOT EXISTS products (
		id int(11) NOT NULL AUTO_INCREMENT,
		name varchar(255) DEFAULT NULL,
		description longtext,
		price double DEFAULT NULL,
		image varchar(255) DEFAULT NULL,
		categoryid int(11) DEFAULT NULL,
		quantity int(11),
		color varchar(100),
		size varchar(20),
		status varchar(20) NOT NULL,
		PRIMARY KEY (id),
		KEY categoryid (categoryid)
	)";

	if ($conn->query($sql) === TRUE) {
	  
	} else {
	    echo "Error creating table sd_column: " . $conn->error;
	    $dberror = true;
	}	
	//end create table products

	//create admin

	$site_name = $conn->real_escape_string(addcslashes($sd_object->admin_data->site, "'"));

	$sql = "INSERT INTO sd_login (username, password, name, email)  
	VALUES ('".$sd_object->admin_data->username."','".$rx_utilities->encrypt($sd_object->admin_data->password)."','".$site_name."','".$sd_object->admin_data->email."');";

	if ($conn->query($sql) === TRUE) {
	  
	} else {
	    echo "Error creating admin login info: " . $conn->error;
	    $dberror = true;
	}

	// end create admin

	//create header

	$sql = "INSERT INTO sd_header (page_logo, page_top_right, page_bottom_right, site_title)  
	VALUES ('','contact us','1234567890','text')";

	if ($conn->query($sql) === TRUE) {
	  
	} else {
	    echo "Error creating admin login info: " . $conn->error;
	    $dberror = true;
	}

	// end create header

	//create default page (home)

	// $sql = "INSERT INTO sd_pages (page_title, page_url,template_name, meta_description, page_content, type, status, clone)
	// VALUES ('home','home','frontpage.php','','',true,true,0)";

	// if ($conn->query($sql) === TRUE) {
	  
	// } else {
	//     echo "Error creating page info: " . $conn->error;
	//     $dberror = true;
	// }

	// end create default page (home)

	//create default nav (home)

	$sql = 'INSERT INTO sd_nav_menus (name, orders)
	VALUES ("default","[]")';

	if ($conn->query($sql) === TRUE) {
	  
	} else {
	    echo "Error creating page info: " . $conn->error;
	    $dberror = true;
	}

	//end create default nav (home)

	//insert site name

	$sql = "INSERT INTO sd_site (name)
	VALUES ('".$sd_object->admin_data->site."')";

	if ($conn->query($sql) === TRUE) {
	  
	} else {
	    echo "Error creating page info: " . $conn->error;
	    $dberror = true;
	}

	//end insert site name

	$conn->close();

	if($dberror == true) {
		exit();
	}

?> 